package org.cocktail.physalisw.serveur.bdd;

import static org.junit.Assert.*;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.cocktail.physalisw.serveur.bdd.WebDriverFactory.WebDriverType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

public class AuthentificationSteps {

	@cucumber.api.java.Before
	public void setUp() {
		WebDriverFactory.getWebDriverFactory().setWebDriverType(WebDriverType.FIREFOX);
	}

	public WebDriver getWebDriver() {
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();
	}

	@Soit("^(\\w+) habilité(?:e)? à utiliser (\\w+)$")
	public void habilite_a_utiliser(String identification, String application) {
		getWebDriver().navigate().to(NavigationHelper.URL_ACCUEIL);
		getWebDriver().findElement(By.id("LoginId")).sendKeys(identification);
	}

	@Lorsqu("^elle se connecte$")
	public void elle_se_connecte() {
		getWebDriver().findElement(By.name("mot_de_passe")).sendKeys("achanger");
		getWebDriver().findElement(By.name("validerLogin")).click();
	}

	@Alors("^elle se retrouve sur la page d'accueil de Physalis$")
	public void elle_se_retrouve_sur_la_page_d_accueil_de_Physalis() throws Throwable {
		
		// Express the Regexp above with the code you wish you had
		
		assertTrue(getWebDriver().getPageSource().contains("Physalis"));
	}
}