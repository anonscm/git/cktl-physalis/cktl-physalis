package org.cocktail.physalisw.serveur.components;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DirecteurTheseService;
import org.cocktail.fwkcktlrecherche.server.metier.service.FiltrerThesesService;
import org.cocktail.fwkcktlrecherche.server.metier.service.FiltrerThesesService.EtatsThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.FiltrerThesesService.Sexe;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxModalDialog;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Composant pour filtrer des theses
 */
public class FiltresThese extends PhysalisWOComponent {
    
	public final List<String> codesAnnees = FiltrerThesesService.CODES_ANNEES;

	public static final Map<String, String> codeAnneeLibelles = new HashMap<String, String>();
	
	static {
		codeAnneeLibelles.put(FiltrerThesesService.CODE_ANNEE_C, "Civile");
		codeAnneeLibelles.put(FiltrerThesesService.CODE_ANNEE_U, "Universitaire");
	}
	
	private String currentCodeAnnee;
	private String selectedCodeAnnee = FiltrerThesesService.CODE_ANNEE_U;
    private static final long serialVersionUID = 7085207960750822059L;
    private static final Integer NUMBER_OF_OBJECTS_PER_BATCH = 15;
    
    private static final String BINDING_DISPLAY_GROUP = "displayGroup";
    private static final String BINDING_CALLBACK_ON_FILTRER = "callbackOnFiltrer";
    private static final String BINDING_CALLBACK_ON_REINITIALISER = "callbackOnReinitialiser";
    private static final String BINDING_USE_CKTL_SUBMIT_BUTTON = "useCktlSubmitButton";
    private static final String BINDING_THESES_A_TRIER = "thesesATrier";
    private static final String BINDING_THESES_TRIEES = "thesesTriees";
    private static final String BINDING_LABORATOIRE_SELECTION_MULTIPLE_ENABLED = "laboratoireSelectionMultipleEnabled";
    private static final String BINDING_SHOW_ETAT = "showEtat";
    private static final String BINDING_SHOW_NOM_ET_PRENOM = "showNomEtPrenom";
    private static final String BINDING_SHOW_SEXE = "showSexe";
    private static final String BINDING_SHOW_SUJET_THESE = "showSujetThese";
    private static final String BINDING_WANT_RESET = "wantReset";
    
    private String callbackOnFiltrer;
    private String callbackOnReinitialiser;
    private Boolean useCktlSubmitButton;
    
    // Variables des filtres de recherche
	private Integer tokenAnneeCivile;
	private Integer tokenAnneeCivileSoutenance;
  	private String tokenNomDoctorant;
  	private String tokenPrenomDoctorant;
  	private String tokenTitreThese;
  	private Boolean tokenPremiereInscription;
  	
  	private WODisplayGroup dgLaboratoires;
    private NSArray<EOStructure> selectedLaboratoires;
  	
  	private EOStructure selectedEcoleDoctorale;
    private EOStructure selectedLaboratoire;
    private EOStructure currentEcoleDoctorale;
    private EOStructure currentLaboratoire;
    
	private EODomaineScientifique selectedDomaineScientifique;
  	
  	private EOIndividu selectedEncadrant;
	private EOIndividu selectedDirecteur;
	
	private NSArray<EtatsThese> etatsThese; 
    private EtatsThese currentEtatThese;
    private EtatsThese selectedEtatThese;
	
	private NSArray<Sexe> listeSexe; 
    private Sexe currentSexe;
    private Sexe selectedSexe;
    
	/**
     * Constructeur
     * @param context contexte
     */
	public FiltresThese(WOContext context) {
        super(context);
    }
	
	public String getListeFiltresContainerId() {
		return getComponentId() + "_listeFiltresContainerId";
	}
	
	public String getFiltreAnneeUniversitaireId() {
		return getComponentId() + "_filtreAnneeUniversitaireId";
	}
	
	public String getFiltreAnneeCivileSoutenanceId() {
		return getComponentId() + "_filtreAnneeCivileSoutenanceId";
	}
	
	public String getFiltreEtatTheseId() {
		return getComponentId() + "_filtreEtatTheseId";
	}
	
	public String getEtatTheseId() {
		return getComponentId() + "_etatTheseId";
	}
	
	public String getFiltreSexeId() {
		return getComponentId() + "_filtreSexeId";
	}

	public String getAnneeUniversaireId() {
		return getComponentId() + "_anneeUniversaireId";
	}
	
	public String getFiltreNomDoctorantId() {
		return getComponentId() + "_filtreNomDoctorantId";
	}
	
	public String getFiltrePrenomDoctorantId() {
		return getComponentId() + "_filtrePrenomDoctorantId";
	}
	
	public String getFiltreSujetTheseId() {
		return getComponentId() + "_filtreSujetTheseId";
	}
	
	public String getContainerDomainesScientifiquesId() {
		return getComponentId() + "_containerDomainesScientifiques";
	}
	
	public String getFiltreEcoleDoctoraleId() {
		return getComponentId() + "_filtreEcoleDoctoraleId";
	}
	
	public String getFiltreLaboratoireId() {
		return getComponentId() + "_filtreLaboratoireId";
	}
	
	public String getTableViewLaboratoireId() {
		return getComponentId() + "_tableViewLaboratoireId";
	}
	
	public String getWindowDirecteurId() {
		return getComponentId() + "_windowDirecteurId";
	}
	
	public String getWindowEncadrantId() {
		return getComponentId() + "_windowEncadrantId";
	}
	
	/* Bindings */
	
	/**
	 * @return l'eventuel display group passé en binding
	 */
    @SuppressWarnings("unchecked")
    public ERXDisplayGroup<EODoctorantThese> getDisplayGroup() {
		if (!hasBinding(BINDING_DISPLAY_GROUP)) {
			return null;
		}
		return (ERXDisplayGroup<EODoctorantThese>) valueForBinding(BINDING_DISPLAY_GROUP);
	}
    
    public void setThesesATrier(NSArray<EODoctorantThese> thesesATrier) {
    	setValueForBinding(thesesATrier, BINDING_THESES_A_TRIER);
    }
    
    @SuppressWarnings("unchecked")
    public NSArray<EODoctorantThese> getThesesATrier() {
    	return (NSArray<EODoctorantThese>) valueForBinding(BINDING_THESES_A_TRIER);
    }
    
    public void setThesesTriees(NSArray<EODoctorantThese> thesesTriees) {
    	setValueForBinding(thesesTriees, BINDING_THESES_TRIEES);
    }
    
    @SuppressWarnings("unchecked")
    public NSArray<EODoctorantThese> getThesesTriees() {
    	return (NSArray<EODoctorantThese>) valueForBinding(BINDING_THESES_TRIEES);
    }
    
    public boolean getUseCktlSubmitButton() {
    	if (useCktlSubmitButton == null) {
    		useCktlSubmitButton = booleanValueForBinding(BINDING_USE_CKTL_SUBMIT_BUTTON, false);
    	}
    	return useCktlSubmitButton;
    }
    
    public String getCallbackOnFiltrer() {
		if (callbackOnFiltrer == null) {
			callbackOnFiltrer = (String) valueForBinding(BINDING_CALLBACK_ON_FILTRER);
		}
		return callbackOnFiltrer;
	}
    
    public String getCallbackOnReinitialiser() {
		if (callbackOnReinitialiser == null) {
			callbackOnReinitialiser = (String) valueForBinding(BINDING_CALLBACK_ON_REINITIALISER);
		}
		return callbackOnReinitialiser;
	}
    
    public boolean isShowEtat() {
    	return booleanValueForBinding(BINDING_SHOW_ETAT, true);
    }
    
    public boolean isShowSexe() {
    	return booleanValueForBinding(BINDING_SHOW_SEXE, false);
    }
    
    public boolean isLaboratoireSelectionMultipleEnabled() {
    	return booleanValueForBinding(BINDING_LABORATOIRE_SELECTION_MULTIPLE_ENABLED, false);
    }
    
    public boolean isShowNomEtPrenom() {
    	return booleanValueForBinding(BINDING_SHOW_NOM_ET_PRENOM, true);
    }
    
    public boolean isShowSujetThese() {
    	return booleanValueForBinding(BINDING_SHOW_SUJET_THESE, true);
    }
    
    public boolean isWantReset() {
    	return booleanValueForBinding(BINDING_WANT_RESET, false);
    }
	
	/* Methodes */
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        if (isWantReset()) {
        	reinitialiserFiltresRecherche();
        }
        super.appendToResponse(response, context);
    }
	
	/**
	 * @return l'année suivante à celle renseignée dans le champ de l'année civile
	 */
	public Integer getAnneeCivileSuivante() {
		if (tokenAnneeCivile == null) {
			return null;
		}
		return tokenAnneeCivile + 1;
	}
	
	public String getCurrentCodeAnnee() {
		return currentCodeAnnee;
	}

	public void setCurrentCodeAnnee(String currentCodeAnnee) {
		this.currentCodeAnnee = currentCodeAnnee;
	}

	public String getSelectedCodeAnnee() {
		return selectedCodeAnnee;
	}

	public void setSelectedCodeAnnee(String selectedCodeAnnee) {
		this.selectedCodeAnnee = selectedCodeAnnee;
	}

	public String getCurrentCodeAnneeLibelle() {
		return codeAnneeLibelles.get(getCurrentCodeAnnee());
	}
	
	public Boolean isShowComplementAnneeUniversitaire() {
		return getTokenAnneeCivile() != null && StringUtils.equals(getSelectedCodeAnnee(), FiltrerThesesService.CODE_ANNEE_U);
	}
	
	public Boolean isShowPremiereInscription() {
		return getTokenAnneeCivile() != null && StringUtils.equals(getSelectedCodeAnnee(), FiltrerThesesService.CODE_ANNEE_U);
	}
	
	public Boolean isShowAnneeCivileSoutenance() {
		return getSelectedEtatThese() != null && getSelectedEtatThese().equals(EtatsThese.SOUTENUE);
	}
	
	/**
	 * @return Liste des ecoles doctorales affichées dans le filtre de recherche
	 */
	public NSArray<EOStructure> getEcolesDoctorales() {
		if (getDisplayGroup() == null) {
			DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
			return dts.listeEcolesDoctorales();
		}
		@SuppressWarnings("unchecked")
        NSArray<EOStructure> eds = (NSArray<EOStructure>) getDisplayGroup().allObjects().valueForKey(EODoctorantThese.ECOLE_DOCTORALE.key()); //FIXME: qd pas de dg
		eds = ERXS.sorted(eds, EOStructure.LL_STRUCTURE.ascInsensitives());
		return ERXArrayUtilities.arrayWithoutDuplicates(eds);
	}
	
	/**
	 * @return Liste des laboratoires affichés dans le filtre de recherche
	 */
	public NSArray<EOStructure> getLaboratoires() {
		
		if (getDisplayGroup() == null) {
			DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
//			if (getSelectedEcoleDoctorale() == null) {
				return dts.listeLaboratoires();
//			} else {
//				return dts.getLaboratoiresEcoleDoctorale(getSelectedEcoleDoctorale(), factoryAssociation().membreAssociation(edc()));
//			}
		}
		
		@SuppressWarnings("unchecked")
        NSArray<EOStructure> labos = 
				ERXArrayUtilities.flatten(
						(NSArray<NSArray<EOStructure>>) getDisplayGroup().allObjects().valueForKey(EODoctorantThese.LABORATOIRES.key()), //FIXME: qd pas de dg
						true
					);
//		Dans le cas ou on veut afficher les laboratoires d'une ecole doctorale selectionnée
		NSArray<EOStructure> laboratoiresFiltres = new NSMutableArray<EOStructure>();
		if (getSelectedEcoleDoctorale() != null) {
			DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
			NSArray<EOStructure> labosED = dts.getLaboratoiresEcoleDoctorale(getSelectedEcoleDoctorale(), factoryAssociation().membreAssociation(edc()));
			if (!labosED.isEmpty()) {
				for (EOStructure labo : labos) {
					if (labosED.contains(labo) && !laboratoiresFiltres.contains(labo)) {
						laboratoiresFiltres.add(labo);
					}
				}
			}
			return laboratoiresFiltres;
		}
		
		return ERXArrayUtilities.arrayWithoutDuplicates(ERXS.sorted(labos, EOStructure.LL_STRUCTURE.ascInsensitives()));
	}
	
	/**
	 * @return displayGroup
	 */
	public WODisplayGroup getDgLaboratoires() {
		if (dgLaboratoires == null) {
			dgLaboratoires = new WODisplayGroup();
			dgLaboratoires.setDelegate(new LaboratoiresDisplayGroupDelegate());
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOStructure.LC_STRUCTURE_KEY, EOSortOrdering.CompareAscending));
			dgLaboratoires.setSortOrderings(sortOrderings);
			dgLaboratoires.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
			dgLaboratoires.setSelectsFirstObjectAfterFetch(false);
			dgLaboratoires.setObjectArray(getLaboratoires());
		}

		return dgLaboratoires;
	}
	
	/**
	 * classe déléguée à la gestion de la sélection des laboratoires
	 */
	public class LaboratoiresDisplayGroupDelegate {

		/**
		 * @param group group
		 */
		@SuppressWarnings("unchecked")
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			NSArray<EOStructure> array = group.selectedObjects();
	    	if (array.size() > 0) {
	    		setSelectedLaboratoires((NSArray<EOStructure>) group.selectedObjects());
	    	} else {
	    		setSelectedLaboratoires(null);
	    	}
		}
	}
	
	/**
	 * @return true
	 */
	public Boolean wantResetPersonneSearch() {
		return true;
	}
	
	/**
	 * @param wantResetPersonneSearch wantResetPersonneSearch
	 */
	public void setWantResetPersonneSearch(Boolean wantResetPersonneSearch) {
	}
	
	public WOActionResults reinitialiserFiltresRecherche() {
		setTokenAnneeCivile(null);
		setTokenAnneeCivileSoutenance(null);
		setTokenPremiereInscription(null);
		setSelectedEtatThese(null);
		setSelectedSexe(null);
		setTokenNomDoctorant(null);
		setTokenPrenomDoctorant(null);
		setTokenTitreThese(null);
    	setSelectedEcoleDoctorale(null);
    	setSelectedLaboratoire(null);
    	setSelectedDomaineScientifique(null);
    	setSelectedDirecteur(null);
    	setSelectedEncadrant(null);
    	setSelectedLaboratoires(null);
    	if (dgLaboratoires != null) {
    		dgLaboratoires.setSelectedObjects(null);
    	}
        return performParentAction(getCallbackOnReinitialiser());
    }
	
	public WOActionResults onFiltrer() {
		FiltrerThesesService fts = FiltrerThesesService.creerNouvelleInstance(edc());
		
		NSArray<EODoctorantThese> thesesFiltrees = fts.filtrerListeTheses(getThesesATrier(), getTokenTitreThese(), getTokenNomDoctorant(), 
				getTokenPrenomDoctorant(), getSelectedEcoleDoctorale(), getSelectedLaboratoire(), getSelectedLaboratoires(), getSelectedEtatThese(), 
				getTokenAnneeCivileSoutenance(), getSelectedDirecteur(), getSelectedEncadrant(), getTokenAnneeCivile(), getSelectedCodeAnnee(), 
				getTokenPremiereInscription(), getSelectedDomaineScientifique(), getSelectedSexe());
		
		setThesesTriees(thesesFiltrees);
		
		return performParentAction(getCallbackOnFiltrer());
	}
	
	/**
	 * @return les etats possible d'une these
	 */
	public NSArray<EtatsThese> etatsThese() {
		if (etatsThese == null) {
			etatsThese = new NSMutableArray<EtatsThese>();
			etatsThese.add(EtatsThese.EN_COURS);
			etatsThese.add(EtatsThese.SOUTENUE);
			etatsThese.add(EtatsThese.ABANDONNEE);
		}
		return etatsThese;
	}
	
	/**
	 * @return les etats possible d'une these
	 */
	public NSArray<Sexe> listeSexe() {
		if (listeSexe == null) {
			listeSexe = new NSMutableArray<Sexe>();
			listeSexe.add(Sexe.FEMME);
			listeSexe.add(Sexe.HOMME);
		}
		return listeSexe;
	}
	
	/* Directeur */
	
	/**
	 * Ouvre fenetre directeur
	 * @return null
	 */
	public WOActionResults selectDirTheseHab() {
		AjaxModalDialog.open(context(), getWindowDirecteurId(), "");
		return doNothing();
	}
	
	/**
	 * @return action lancée de l'annulation d'un directeur
	 */
	public WOActionResults annulerDirecteur() {
		setSelectedDirecteur(null);
		return doNothing();
	}
	
	/**
	 * Action lancée lors de l'ajout d'un directeur
	 * @return doNothing()
	 */
	public WOActionResults validerDirecteur() {
		
		if (getSelectedDirecteur() == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez sélectionner une personne !");
		} 
		
		return doNothing();
	}

	/* Encadrant */
	
	/**
	 * Ouvre fenetre encadrant
	 * @return null
	 */
	public WOActionResults selectEncadrantThese() {
		AjaxModalDialog.open(context(), getWindowEncadrantId(), "");
		return doNothing();
	}
	
	/**
	 * Action lancée lors de l'ajout d'un encadrant
	 * @return doNothing()
	 */
	public WOActionResults validerEncadrant() {
		
		if (getSelectedEncadrant() == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez sélectionner une personne !");
		} 
		
		return doNothing();
	}
	
	/**
	 * @return action lancée de l'annulation d'un encadrant
	 */
	public WOActionResults annulerEncadrant() {
		setSelectedEncadrant(null);
		return doNothing();
	}
	
	/* Getters / Setters */
	
    public Integer getTokenAnneeCivile() {
		return tokenAnneeCivile;
	}

	public void setTokenAnneeCivile(Integer tokenAnneeCivile) {
		this.tokenAnneeCivile = tokenAnneeCivile;
	}
	
    public Integer getTokenAnneeCivileSoutenance() {
		return tokenAnneeCivileSoutenance;
	}

	public void setTokenAnneeCivileSoutenance(Integer tokenAnneeCivileSoutenance) {
		this.tokenAnneeCivileSoutenance = tokenAnneeCivileSoutenance;
	}

	public String getTokenNomDoctorant() {
		return tokenNomDoctorant;
	}

	public void setTokenNomDoctorant(String tokenNomDoctorant) {
		this.tokenNomDoctorant = tokenNomDoctorant;
	}

	public String getTokenPrenomDoctorant() {
		return tokenPrenomDoctorant;
	}

	public void setTokenPrenomDoctorant(String tokenPrenomDoctorant) {
		this.tokenPrenomDoctorant = tokenPrenomDoctorant;
	}

	public String getTokenTitreThese() {
		return tokenTitreThese;
	}

	public void setTokenTitreThese(String tokenTitreThese) {
		this.tokenTitreThese = tokenTitreThese;
	}

	public EOIndividu getSelectedEncadrant() {
		return selectedEncadrant;
	}

	public void setSelectedEncadrant(EOIndividu selectedEncadrant) {
		this.selectedEncadrant = selectedEncadrant;
	}

	public EOIndividu getSelectedDirecteur() {
		return selectedDirecteur;
	}

	public void setSelectedDirecteur(EOIndividu selectedDirecteur) {
		this.selectedDirecteur = selectedDirecteur;
	}

	public EODomaineScientifique getSelectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EOStructure getSelectedEcoleDoctorale() {
		return selectedEcoleDoctorale;
	}

	public void setSelectedEcoleDoctorale(EOStructure selectedEcoleDoctorale) {
		this.selectedEcoleDoctorale = selectedEcoleDoctorale;
	}

	public EOStructure getSelectedLaboratoire() {
		return selectedLaboratoire;
	}

	public void setSelectedLaboratoire(EOStructure selectedLaboratoire) {
		this.selectedLaboratoire = selectedLaboratoire;
	}

	public EOStructure getCurrentEcoleDoctorale() {
		return currentEcoleDoctorale;
	}

	public void setCurrentEcoleDoctorale(EOStructure currentEcoleDoctorale) {
		this.currentEcoleDoctorale = currentEcoleDoctorale;
	}

	public EOStructure getCurrentLaboratoire() {
		return currentLaboratoire;
	}

	public void setCurrentLaboratoire(EOStructure currentLaboratoire) {
		this.currentLaboratoire = currentLaboratoire;
	}
	
	public EtatsThese getCurrentEtatThese() {
	    return currentEtatThese;
    }
	
	public NSArray<EOStructure> getSelectedLaboratoires() {
	    return selectedLaboratoires;
    }

	public void setSelectedLaboratoires(NSArray<EOStructure> selectedLaboratoires) {
	    this.selectedLaboratoires = selectedLaboratoires;
    }

	public void setCurrentEtatThese(EtatsThese currentEtatThese) {
	    this.currentEtatThese = currentEtatThese;
    }

	public EtatsThese getSelectedEtatThese() {
	    return selectedEtatThese;
    }

	public void setSelectedEtatThese(EtatsThese selectedEtatThese) {
	    this.selectedEtatThese = selectedEtatThese;
    }

	public Sexe getCurrentSexe() {
		return currentSexe;
	}

	public void setCurrentSexe(Sexe currentSexe) {
		this.currentSexe = currentSexe;
	}

	public Sexe getSelectedSexe() {
		return selectedSexe;
	}

	public void setSelectedSexe(Sexe selectedSexe) {
		this.selectedSexe = selectedSexe;
	}

	public Boolean getTokenPremiereInscription() {
		return tokenPremiereInscription;
	}

	public void setTokenPremiereInscription(Boolean tokenPremiereInscription) {
		this.tokenPremiereInscription = tokenPremiereInscription;
	}
	
}