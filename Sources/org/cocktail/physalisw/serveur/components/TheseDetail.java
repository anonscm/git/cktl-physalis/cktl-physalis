package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;

public class TheseDetail extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1753108094859113026L;
	public static final String B_laThese = "laThese";
	private static final String B_ecoleDoctorale = "ecoleDoctorale";

	public TheseDetail(WOContext context) {
		super(context);
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	public String ecoleDoctorale() {
		return (String) valueForBinding(B_ecoleDoctorale);
	}

}