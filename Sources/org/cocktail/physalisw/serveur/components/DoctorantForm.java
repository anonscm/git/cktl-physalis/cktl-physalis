package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class DoctorantForm extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 546653267876657748L;

	private static final String B_doctorant = "doctorant";
	private static final String B_isModification = "isModification";
	private static final String B_isEditing = "isEditing";
	private NSArray<EOCivilite> civilites = null;
	private EOCivilite uneCivilite;
	private NSArray<EOSituationFamiliale> situationFamiliales = null;
	private EOSituationFamiliale uneSituationFamiliale;
	private NSArray<EOSituationMilitaire> situationMilitaires = null;
	private EOSituationMilitaire uneSituationMilitaire;
	private Boolean isEditingAdresse = false;
	private Boolean isEditing;
	private String dptNaissance;
	private String paysNaissance;
	private String paysNationalite;
	private String paysDiplome;
	private String etablissementDiplomeOrigine;

	public EODoctorantOrigine itemDoctorantOrigine, selectedDoctorantOrigine;
    public EODoctorantStatut itemDoctorantStatut, selectedDoctorantStatut;

    public DoctorantForm(WOContext context) {
        super(context);
    }
    
    public String aucEtablissementId() {
    	return getComponentId() + "_aucEtablissementId";
    }
    
	/**
	 * @return the civilites
	 */
    
	public NSArray<EOCivilite> civilites() {
		if (civilites == null) {
			EOSortOrdering sexeOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.SEXE_KEY, EOSortOrdering.CompareAscending);
			EOSortOrdering libelleLongOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.L_CIVILITE_KEY, EOSortOrdering.CompareAscending);
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] {sexeOrdering, libelleLongOrdering});
			civilites = EOCivilite.fetchAll(edc(), sortOrderings);
		}
		return civilites;
	}

	public void setUneCivilite(EOCivilite uneCivilite) {
		this.uneCivilite = uneCivilite;
	}

	public EOCivilite uneCivilite() {
		return uneCivilite;
	}

	public NSArray<EOSituationFamiliale> situationFamiliales() {
		if (situationFamiliales == null) {
			EOSortOrdering libOrder = EOSortOrdering.sortOrderingWithKey(EOSituationFamiliale.L_SITUATION_FAMILLE_KEY, EOSortOrdering.CompareAscending);
			situationFamiliales = EOSituationFamiliale.fetchAll(edc(), new NSArray<EOSortOrdering>(libOrder));
		}
		return situationFamiliales;
	}

	public void setUneSituationFamiliale(EOSituationFamiliale uneSituationFamiliale) {
		this.uneSituationFamiliale = uneSituationFamiliale;
	}

	public EOSituationFamiliale uneSituationFamiliale() {
		return uneSituationFamiliale;
	}
	
	public NSArray<EOSituationMilitaire> situationMilitaires() {
		if (situationMilitaires == null) {
			EOSortOrdering libOrder = EOSortOrdering.sortOrderingWithKey(EOSituationMilitaire.L_SIT_MILITAIRE_KEY, EOSortOrdering.CompareAscending);
			situationMilitaires = EOSituationMilitaire.fetchAll(edc(), new NSArray<EOSortOrdering>(libOrder));
		}
		return situationMilitaires;
	}

	public Boolean isNoInseeProvisoire() {
		Boolean isNoInseeProvisoire = false;
		if (editingDoctorant() != null && editingDoctorant().toIndividuFwkpers() != null && editingDoctorant().toIndividuFwkpers().priseCptInsee() != null
				&& editingDoctorant().toIndividuFwkpers().priseCptInsee().equalsIgnoreCase("P")) {
			isNoInseeProvisoire = true;
		}
		return isNoInseeProvisoire;
	}

	public void setUneSituationMilitaire(EOSituationMilitaire uneSituationMilitaire) {
		this.uneSituationMilitaire = uneSituationMilitaire;
	}

	public EOSituationMilitaire uneSituationMilitaire() {
		return uneSituationMilitaire;
	}

	public boolean isIdentiteDisabled() {
		//return (isModification() != null && isModification().booleanValue()) && (utilisateur() == null || utilisateur().isIdentiteDisabled());
		return (isModification() != null && !isModification());
	}

	public boolean isIneDisabled() {
		//return (isModification() != null && isModification().booleanValue()) && (utilisateur() == null || utilisateur().isIneDisabled());
		return (isModification() != null && !isModification());
	}

	public boolean isChampDisabled() {
		return (isModification() != null && !isModification());
	}
	
	public NSArray<EODoctorantOrigine> listeDoctorantOrigines() {
		return EODoctorantOrigine.fetchAllDoctorantOrigines(edc(), new NSArray<EOSortOrdering>(EODoctorantOrigine.SORT_ORIGINE_LIB)	);
	}
	
	public NSArray<EODoctorantStatut> listeDoctorantStatuts() {
		return EODoctorantStatut.fetchAllDoctorantStatuts(edc(), new NSArray<EOSortOrdering>(EODoctorantStatut.SORT_STATUT_LIB)	);
	}

	public EODoctorant editingDoctorant() {
		return (EODoctorant) valueForBinding(B_doctorant);
	}

	public Boolean isModification() {
		return (Boolean) valueForBinding(B_isModification);
	}

	public Boolean isEditing() {
		return (Boolean) valueForBinding(B_isEditing);
	}
	
	public void setIsEditing(Boolean isEditing) {
		setValueForBinding(isEditing, B_isEditing);
	}

	public void setIsEditingAdresse(Boolean isEditingAdresse) {
		setIsEditing(isEditingAdresse);
		this.isEditingAdresse = isEditingAdresse;
	}

	public Boolean isEditingAdresse() {
		return isEditingAdresse;
	}

	public WOActionResults valideDptNaissance() {
		return null;
	}


	/**
	 * @return the dptNaissance
	 */
	public String dptNaissance() {
		if (dptNaissance == null) {
			if (editingDoctorant().toIndividuFwkpers().toDepartement() != null) {
				setDptNaissance(editingDoctorant().toIndividuFwkpers().toDepartement().getLibelleAndCode());
			}
		}
		return dptNaissance;
	}

	/**
	 * @param dptNaissance the dptNaissance to set
	 */
	public void setDptNaissance(String dptNaissance) {
		this.dptNaissance = dptNaissance;
	}


	/**
	 * @return the paysNaissance
	 */
	public String paysNaissance() {
		if (paysNaissance == null) {
			if (editingDoctorant().toIndividuFwkpers().toPaysNaissance() != null) {
				setPaysNaissance(editingDoctorant().toIndividuFwkpers().toPaysNaissance().llPays());
			}
		}
		return paysNaissance;
	}

	/**
	 * @param paysNaissance the paysNaissance to set
	 */
	public void setPaysNaissance(String paysNaissance) {
		this.paysNaissance = paysNaissance;
	}


	/**
	 * @return the paysNationalite
	 */
	public String paysNationalite() {
		if (paysNationalite == null) {
			if (editingDoctorant().toIndividuFwkpers().toPaysNationalite() != null) {
				setPaysNationalite(editingDoctorant().toIndividuFwkpers().toPaysNationalite().lNationalite());
			}
		}
		return paysNationalite;
	}

	/**
	 * @param paysNationalite the paysNationalite to set
	 */
	public void setPaysNationalite(String paysNationalite) {
		this.paysNationalite = paysNationalite;
	}
	
	/**
	 * @return the paysDiplome
	 */
	public EOPays paysDiplome() {
		return editingDoctorant().toPaysDiplome();
	}

	/**
	 * @param paysDiplome the paysDiplome to set
	 */
	public void setPaysDiplome(EOPays paysDiplome) {
		editingDoctorant().setToPaysDiplomeRelationship(paysDiplome);
	}

	/**
	 * @return etablissementDiplomeOrigine
	 */
	public EORne etablissementDiplomeOrigine() {
		return editingDoctorant().toEtablissementDiplomeOrigine();
    }

	/**
	 * 
	 * @param etablissementDiplomeOrigine le nouvel etablissement
	 */
	public void setEtablissementDiplomeOrigine(EORne etablissementDiplomeOrigine) {
	    editingDoctorant().setToEtablissementDiplomeOrigineRelationship(etablissementDiplomeOrigine);
    }
	
	public Boolean showEtablissement() {
		if (editingDoctorant().toPaysDiplome() == null) {
			return false;
		}
		return "FRANCE".equals(editingDoctorant().toPaysDiplome().llPays());
	}
}