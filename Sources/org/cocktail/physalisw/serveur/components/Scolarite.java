package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class Scolarite extends PhysalisWOComponent {

	private static final long serialVersionUID = -611868120385164968L;
	
	private final String BINDING_LEDOCTORANT = "leDoctorant";
	private EOIndividuDiplome currentDiplome;
	private EOHistorique currentHistorique;
	private EOInscDipl currentInscriptionDiplome;
	
    public Scolarite(WOContext context) {
        super(context);
    }
    
    public EODoctorant leDoctorant() {
		return (EODoctorant) valueForBinding(BINDING_LEDOCTORANT);
	}
    
    public NSArray<EOIndividuDiplome> getDiplomes() {
    	NSArray<EOIndividuDiplome> listeDiplomes = EOIndividuDiplome.fetchAllForIndividu(edc(), leDoctorant().toIndividuFwkpers());
    	return listeDiplomes;
    }

	public EOIndividuDiplome getCurrentDiplome() {
		return currentDiplome;
	}

	public void setCurrentDiplome(EOIndividuDiplome currentDiplome) {
		this.currentDiplome = currentDiplome;
	}
	
	public NSArray<EOHistorique> getHistorique() {
		EOQualifier qualifier = EOHistorique.ETUD_NUMERO.eq(leDoctorant().etudNumero());
		@SuppressWarnings("unchecked")
        NSArray<EOHistorique> historiqueATrier = EOHistorique.fetchAll(edc(), qualifier);
		int sizeHistoriqueATrier = historiqueATrier.size();
		
		NSMutableArray<EOHistorique> historiqueTrie = new NSMutableArray<EOHistorique>();
		
		if(sizeHistoriqueATrier > 0) {
			boolean trouve;
			int sizeHistoriqueTrie = 0;
			historiqueTrie.add(historiqueATrier.get(0));
			
			for(int i=1; i<sizeHistoriqueATrier; i++) {
				sizeHistoriqueTrie ++;
				trouve = false;
				
				for(int j=0; j<sizeHistoriqueTrie && !trouve; j++ ) {
					if(historiqueATrier.get(i).histAnneeScol() > historiqueTrie.get(j).histAnneeScol()) {
						historiqueTrie.add(j, historiqueATrier.get(i));
						trouve = true;
					} else if(j == sizeHistoriqueTrie-1) {
						historiqueTrie.add(j+1, historiqueATrier.get(i));
						trouve = true;
					}
				}
			}
		}
		
		return historiqueTrie.immutableClone();
	}
	
	public NSArray<EOScolFormationSpecialisation> specialisations() {
		return null;
	}

	public EOHistorique getCurrentHistorique() {
		return currentHistorique;
	}

	public void setCurrentHistorique(EOHistorique currentHistorique) {
		this.currentHistorique = currentHistorique;
	}
	
	@SuppressWarnings("unchecked")
    public NSArray<EOInscDipl> getInscDipl() {
		return currentHistorique.toInscDipls();
	}
	
	public EOInscDipl getCurrentInscriptionDiplome() {
		return currentInscriptionDiplome;
	}

	public void setCurrentInscriptionDiplome(EOInscDipl currentInscriptionDiplome) {
		this.currentInscriptionDiplome = currentInscriptionDiplome;
	}
	
	public String anneeBacPlusUn() {
		Long annee = leDoctorant().toEtudiantFwkpers().etudAnbac();
		annee++;
		return annee.toString();
	}
	
	public String anneePlusUn() {
		Integer annee = getCurrentInscriptionDiplome().toHistorique().histAnneeScol();
		annee++;
		return annee.toString();
	}
	
	public String infosEtablissementBac() {
		if(leDoctorant().toEtudiantFwkpers().toRneCodeBac() != null) {
			return leDoctorant().toEtudiantFwkpers().toRneCodeBac().llRne() + " (" + leDoctorant().toEtudiantFwkpers().toRneCodeBac().lcRne()
				+ ") - " + leDoctorant().toEtudiantFwkpers().toRneCodeBac().ville();
		}
		return "-";
	}
    
}