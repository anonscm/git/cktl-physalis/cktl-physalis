package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class DirecteurTheseHabiliteForm extends PhysalisWOComponent {

	private static final long serialVersionUID = 2263581098832231369L;
	public static final String B_dirTheseHab = "dirTheseHab";
	public static final String B_isLaboratoireLocal = "isLaboratoireLocal";
	public static final String B_isEtablissementFrancais = "isEtablissementFrancais";
	private Boolean isLaboratoireLocal = null;
	private Boolean isLaboratoireExterne = null;
	private Boolean isEtablissementFrancais = null;
	private Boolean isEtablissementEtranger = null;
	private EOStructure selectedEtablissement = null;

	public EOStructure itemEcoleDoctorale;
	public EOStructure selectedEcoleDoctorale;
	public EOStructure itemStructureLabo;

	private IPersonne selectedPersonne;
	private EOIndividu individuDirecteur;
	public NSTimestamp dateHDR;

	public DirecteurTheseHabiliteForm(WOContext context) {
		super(context);
	}

	public NSArray<EOStructure> listeEcolesDoctorales() {

		EOQualifier qualDoct = ERXQ.and(ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "ED"),
				ERXQ.equals(EOStructure.TEM_VALIDE_KEY, EOStructure.TEM_VALIDE_OUI));
		NSArray<EOStructure> lesEds = EOStructure.fetchAll(edc(), qualDoct, null);
		if (lesEds.count() == 0) {
			session.addSimpleErrorMessage("Erreur", "Aucune ecole doctorales trouvées, il faut au moins une structure ayant un type_groupe = ED dans votre référentiel");
			return null;
		} else
			return lesEds;

	}

	public NSArray<EOStructure> listeStructuresLabo() {
		/*EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, dirTheseHab().toStructureEdFwkpers().cStructure()), ERXQ.equals(
				EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().membreAssociation(edc())), ERXQ.in(EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.'
				+ EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, new NSArray<String>(EOTypeGroupe.TGRP_CODE_LA, EOTypeGroupe.TGRP_CODE_SR)));*/
		// si on est en train d'ajouter un dth, apres avoir sélectionner la personne, il n'existe pas d'ecole doctorale donc on initialise avec la premiere valeur affichée dans la liste des ecoles doctorales
		if (dirTheseHab().toStructureEdFwkpers() == null) {
			dirTheseHab().setToStructureEdFwkpersRelationship(listeEcolesDoctorales().objectAtIndex(0));
		}
		EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, dirTheseHab().toStructureEdFwkpers().cStructure()), ERXQ.equals(
				EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().membreAssociation(edc())), ERXQ.equals(EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.'
				+ EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA));

		NSArray<EORepartAssociation> lesRepartAssoLabo = ERXArrayUtilities.arrayWithoutDuplicates(EORepartAssociation.fetchAll(edc(), qualLabos));
		if (lesRepartAssoLabo == null)
			return null;
		else
			return (NSArray) lesRepartAssoLabo.valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	}

	public EODirecteurTheseHabilite dirTheseHab() {
		EODirecteurTheseHabilite leDTH = (EODirecteurTheseHabilite) valueForBinding(B_dirTheseHab);
		return leDTH;
	}

	public void setDirTheseHab(EODirecteurTheseHabilite leDirTheseHab) {
		setValueForBinding(leDirTheseHab, B_dirTheseHab);
	}

	public void setSelectedPersonne(IPersonne selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
	}

	public IPersonne selectedPersonne() {
		return selectedPersonne;
	}

	public WOActionResults changeEtablissement() {
		return null;
	}

	public WOActionResults changeLaboratoire() {
		return null;
	}

	public void setSelectedEtablissement(EOStructure selectedEtablissement) {
		this.selectedEtablissement = selectedEtablissement;
	}

	public EOStructure selectedEtablissement() {
		return selectedEtablissement;
	}

	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
		setValueForBinding(isEtablissementFrancais, B_isEtablissementFrancais);
	}

	public Boolean isEtablissementFrancais() {
		return (Boolean) valueForBinding(B_isEtablissementFrancais);
	}

	public void setIsEtablissementEtranger(Boolean isEtablissementEtranger) {
		this.isEtablissementEtranger = isEtablissementEtranger;
	}

	public Boolean isEtablissementEtranger() {
		return !isEtablissementFrancais();
	}

	public void setIsLaboratoireLocal(Boolean isLaboratoireLocal) {
		setValueForBinding(isLaboratoireLocal, B_isLaboratoireLocal);
		this.isLaboratoireLocal = isLaboratoireLocal;
	}

	public Boolean isLaboratoireLocal() {
		return (Boolean) valueForBinding(B_isLaboratoireLocal);
	}

	public void setIsLaboratoireExterne(Boolean isLaboratoireExterne) {
		this.isLaboratoireExterne = isLaboratoireExterne;
	}

	public Boolean isLaboratoireExterne() {
		if (isLaboratoireExterne == null)
			return !isLaboratoireLocal();
		return isLaboratoireExterne;
	}
	
	public String aucLaboratoireId() {
		return getComponentId() + "_aucLaboratoireDTHId";
	}

	public String aucEtablissementId() {
		return getComponentId() + "_aucEtablissementDTHId";
	}

	public String etablissementSearchWindowId() {
		return getComponentId() + "etablissementSearchWindowId";
	}

	public WOActionResults annulerSelectionnerEtablissement() {
		setSelectedEtablissement(null);
		CktlAjaxWindow.close(context(), etablissementSearchWindowId());
		return null;
	}

	public WOActionResults selectionnerEtablissement() {
		dirTheseHab().setToStructureEtabFwkPersRelationship(selectedEtablissement());
		setSelectedEtablissement(null);
		CktlAjaxWindow.close(context(), etablissementSearchWindowId());
		return null;
	}
	
}