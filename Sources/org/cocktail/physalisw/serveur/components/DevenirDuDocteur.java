package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartSituationDocteur;
import org.cocktail.fwkcktlrecherche.server.metier.EOSituationDocteur;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;

/**
 * Devenir du docteur
 */
public class DevenirDuDocteur extends PhysalisWOComponent {

	private static final long serialVersionUID = -5809175905747683395L;
	private static final String B_laThese = "laThese";
	private static final String BINDING_IS_MODIFICATION_BOTTOM = "isModificationBottom";

	private WODisplayGroup dgSituation;
	private NSArray<EOSituationDocteur> listSituationsDocteur;
	private EOSituationDocteur itemSituationDocteur;
	private Boolean isEditionSituationDocteur = false;
	private EORepartSituationDocteur selectedRepartSituationDocteur;
	private EORepartSituationDocteur currentRepartSituationDocteur;
	private EORepartSituationDocteur editingRepartSituationDocteur;

	/**
	 * Constructeur
	 * @param context contexte
	 */
	public DevenirDuDocteur(WOContext context) {
		super(context);
	}
	
	/**
	 * @return l'identifiant de la table
	 */
	public String situationDocteurTableViewId() {
		return getComponentId() + "_situationDocteurTableViewId";
	}
	
	/**
	 * @return identifiant du container
	 */
	public String containerSituationDocteurId() {
		return getComponentId() + "_containerSituationDocteurId";
	}

	/**
	 * @return la thèse en cours
	 */
	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	/**
	 * @param laThese affecte la these
	 */
	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, B_laThese);
	}

	/**
	 * @return la liste des situations existantes
	 */
	public NSArray<EOSituationDocteur> getListSituationsDocteur() {
		if (listSituationsDocteur == null) {
			listSituationsDocteur = EOSituationDocteur.fetchAll(edc());
		}
		return listSituationsDocteur;
	}

	/**
	 * DisplayGroup situation
	 * @return dg
	 */
	public WODisplayGroup dgSituation() {
		if (dgSituation == null) {
			dgSituation = new WODisplayGroup();
			dgSituation.setDelegate(new DgDelegateSituation());
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EORepartSituationDocteur.DATE_DEBUT_CONTRAT_KEY, EOSortOrdering.CompareAscending));
			dgSituation.setSortOrderings(sortOrderings);
			dgSituation.setObjectArray(listeSituationsDocteur());
		}

		return dgSituation;
	}
	
	private NSArray<EORepartSituationDocteur> listeSituationsDocteur() {
		EOQualifier qualifier = EORepartSituationDocteur.TO_DOCTORANT_THESE.eq(laThese());
		return EORepartSituationDocteur.fetchAll(edc(), qualifier);
	}
	
	/**
	 * Classe DisplayGroup pour la situation du docteur
	 */
	public class DgDelegateSituation {
		/**
		 * Changement de selection
		 * @param group group
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedRepartSituationDocteur((EORepartSituationDocteur) dgSituation.selectedObject());
			if (selectedRepartSituationDocteur != null) {
				setSelectedRepartSituationDocteur(selectedRepartSituationDocteur);
			} else {
				selectedRepartSituationDocteur = null;
			}
		}
	}
	
	/**
	 * Action lancee au click sur le bouton ajouter 
	 * @return null
	 */
	public WOActionResults ajouterSituationDocteur() {
		
		for (EORepartSituationDocteur rsd : listeSituationsDocteur()) {
			if (rsd.dateFinContrat() == null) {
				session().addSimpleErrorMessage("Erreur", "Vous devez indiquer une date de fin pour la situation précédente avant d'en rajouter une nouvelle.");
				return doNothing();
			}
		}
		
		setIsEditionSituationDocteur(true);
		editingRepartSituationDocteur = new EORepartSituationDocteur();
		edc().insertObject(editingRepartSituationDocteur);
		editingRepartSituationDocteur.setDCreation(new NSTimestamp());
		editingRepartSituationDocteur.setPersIdCreation(getUtilisateurPersId());
		return doNothing();
	}
	
	/**
	 * Action lancee au click sur le bouton modifier
	 * @return null
	 */
	public WOActionResults modifierSituationDocteur() {
		setIsEditionSituationDocteur(true);
		editingRepartSituationDocteur = selectedRepartSituationDocteur;
		return doNothing();
	}
	
	/**
	 * Action lancee au click sur le bouton supprimer
	 * @return null
	 */
	public WOActionResults supprimerSituationDocteur() {
		try {
			edc().deleteObject(getSelectedRepartSituationDocteur());
			edc().saveChanges();
 			setSelectedRepartSituationDocteur(null);
 			dgSituation = null;
 			session().addSimpleSuccessMessage("Suppression", "La situation a bien été supprimée");
 	 		AjaxUpdateContainer.updateContainerWithID(containerSituationDocteurId(), context());
 		} catch (Exception e) {
 			session().addSimpleErrorMessage("Erreur", e.toString());
 		}
 		return doNothing();
	}

	/**
	 * Action lancée lors de la validation de l'ajout d'une situation
	 * @return null
	 */
	public WOActionResults validerSituationDocteur() {
		
		if (editingRepartSituationDocteur.dateDebutContrat() != null && editingRepartSituationDocteur.dateFinContrat() != null 
				&& editingRepartSituationDocteur.dateDebutContrat().after(editingRepartSituationDocteur.dateFinContrat())) {
			session().addSimpleErrorMessage("Erreur", "La date de fin doit être après la date de début.");
			return doNothing();
		}
		
		for (EORepartSituationDocteur rsd : listeSituationsDocteur()) {
			if (DateCtrl.isBetweenEq(editingRepartSituationDocteur.dateDebutContrat(), rsd.dateDebutContrat(), rsd.dateFinContrat())) {
				session().addSimpleErrorMessage("Erreur", "Vous devez indiquer une date de debut n'appartenant à aucun autre contrat.");
				return doNothing();
			}
			if (DateCtrl.isBetweenEq(editingRepartSituationDocteur.dateFinContrat(), rsd.dateDebutContrat(), rsd.dateFinContrat())) {
				session().addSimpleErrorMessage("Erreur", "Vous devez indiquer une date de fin n'appartenant à aucun autre contrat.");
				return doNothing();
			}
			if (editingRepartSituationDocteur.dateFinContrat() == null && rsd.dateDebutContrat().after(editingRepartSituationDocteur.dateDebutContrat())) {
				session().addSimpleErrorMessage("Erreur", "La date de fin doit être renseignée lorsqu'il y a un contrat débutant après la date de début actuelle.");
				return doNothing();
			}
		}
		
		editingRepartSituationDocteur.setToDoctorantTheseRelationship(laThese());
		editingRepartSituationDocteur.setDModification(new NSTimestamp());
		editingRepartSituationDocteur.setPersIdModification(getUtilisateurPersId());
		
		try {
			edc().saveChanges();
			setIsEditionSituationDocteur(false);
			session().addSimpleSuccessMessage("Confirmation", "La situation est enregistrée");
			setEditingRepartSituationDocteur(null);
			dgSituation = null;
			AjaxUpdateContainer.updateContainerWithID(containerSituationDocteurId(), context());
		} catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Erreur", e.getMessage());
		}
		
		return doNothing();
	}
	
	/**
	 * Action lancée lors de l'annulation de l'ajout d'une situation
	 * @return null
	 */
	public WOActionResults annulerSituationDocteur() {
		edc().revert();
		setIsEditionSituationDocteur(false);
		setEditingRepartSituationDocteur(null);
		return doNothing();
	}
	
	public EOSituationDocteur getItemSituationDocteur() {
		return itemSituationDocteur;
	}

	public void setItemSituationDocteur(EOSituationDocteur itemSituationDocteur) {
		this.itemSituationDocteur = itemSituationDocteur;
	}
	
	public EORepartSituationDocteur getSelectedRepartSituationDocteur() {
	    return selectedRepartSituationDocteur;
    }

	public void setSelectedRepartSituationDocteur(EORepartSituationDocteur selectedRepartSituationDocteur) {
	    this.selectedRepartSituationDocteur = selectedRepartSituationDocteur;
    }

	public EORepartSituationDocteur getCurrentRepartSituationDocteur() {
	    return currentRepartSituationDocteur;
    }

	public void setCurrentRepartSituationDocteur(EORepartSituationDocteur currentRepartSituationDocteur) {
	    this.currentRepartSituationDocteur = currentRepartSituationDocteur;
    }

	public Boolean getIsEditionSituationDocteur() {
	    return isEditionSituationDocteur;
    }

	public void setIsEditionSituationDocteur(Boolean isEditionSituationDocteur) {
	    this.isEditionSituationDocteur = isEditionSituationDocteur;
	    disableOnglets(isEditionSituationDocteur, true);
    }

	public EORepartSituationDocteur getEditingRepartSituationDocteur() {
	    return editingRepartSituationDocteur;
    }

	public void setEditingRepartSituationDocteur(EORepartSituationDocteur editingRepartSituationDocteur) {
	    this.editingRepartSituationDocteur = editingRepartSituationDocteur;
    }
	

}