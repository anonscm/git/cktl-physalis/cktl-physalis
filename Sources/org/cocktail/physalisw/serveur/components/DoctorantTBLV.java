package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.physalisw.serveur.components.common.PhysalisTableView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class DoctorantTBLV extends PhysalisTableView {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7975104430393407670L;
	private static final String CIVILITE = EODoctorant.TO_INDIVIDU_FWKPERS_KEY+"."+EOIndividu.TO_CIVILITE_KEY+"."+EOCivilite.C_CIVILITE_KEY;
	private static final String NOM = EODoctorant.TO_INDIVIDU_FWKPERS_KEY+"."+EOIndividu.NOM_USUEL_KEY;
	private static final String NOM_FAMILLE = EODoctorant.TO_INDIVIDU_FWKPERS_KEY+"."+EOIndividu.NOM_PATRONYMIQUE_KEY;
	private static final String PRENOM = EODoctorant.TO_INDIVIDU_FWKPERS_KEY+"."+EOIndividu.PRENOM_KEY;
	private static final String D_NAISSANCE = EODoctorant.TO_INDIVIDU_FWKPERS_KEY+"."+EOIndividu.D_NAISSANCE_KEY;
	
	static {

		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Civilité");
		col0.setOrderKeyPath(CIVILITE);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + CIVILITE, "--");
		col0.setAssociations(ass0);
		col0.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col0, CIVILITE);
		
		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Nom");
		col1.setOrderKeyPath(NOM);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + NOM, "--");
		col1.setAssociations(ass1);
		col1.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col1, NOM);
		
		CktlAjaxTableViewColumn col10 = new CktlAjaxTableViewColumn();
		col10.setLibelle("Nom de famille");
		col10.setOrderKeyPath(NOM_FAMILLE);
		CktlAjaxTableViewColumnAssociation ass10 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + NOM_FAMILLE, "--");
		col10.setAssociations(ass10);
		col10.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col10, NOM_FAMILLE);

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Prénom");
		col2.setOrderKeyPath(PRENOM);
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + PRENOM, "--");
		col2.setAssociations(ass2);
		col2.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col2, PRENOM);

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Naissance");
		col3.setOrderKeyPath(D_NAISSANCE);
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + D_NAISSANCE, "--");
		ass3.setDateformat("%d/%m/%Y");
		col3.setAssociations(ass3);
		col3.setRowCssStyle("text-align:left;padding-left:3px;");
		_colonnesMap.takeValueForKey(col3, D_NAISSANCE);
	}

	public DoctorantTBLV(WOContext context) {
        super(context);
    }
	
	public WOActionResults onDblClickAction() {
		return null;
	}
	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] {CIVILITE, NOM, NOM_FAMILLE, PRENOM, D_NAISSANCE});
	}

	public WOActionResults commitSave() {
		return null;
	}

}