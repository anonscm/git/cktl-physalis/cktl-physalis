package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DroitsService;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Page affichant une liste de thèse selon certains droits
 */
public class ListeTheses extends PhysalisWOComponent {
    
    private static final long serialVersionUID = -1263379258795995631L;
    private static final Integer NUMBER_OF_OBJECTS_PER_BATCH = 15;
    
    private ERXDisplayGroup<EODoctorantThese> displayGroupTheses;
    private EODoctorantThese currentThese;
    private EODoctorantThese selectedThese;
    private NSArray<EOQualifier> qualifiers = null;
    private Boolean isListeThesesSoutenues = false;
    private Boolean isListeThesesAbandonnees = false;
	private DroitsService ds;
	
	private NSMutableArray<EODoctorantThese> thesesVisibles;
	private NSMutableArray<EODoctorantThese> theses;

    private boolean wantResetPage = false;
    
    /**
     * @param context context
     */
	public ListeTheses(WOContext context) {
        super(context);
    }
	
	public WOActionResults filtrer() {
		getDisplayGroupTheses().setObjectArray(getTheses());
		getDisplayGroupTheses().updateDisplayedObjects();
		setSelectedThese(getDisplayGroupTheses().selectedObject());
		AjaxUpdateContainer.updateContainerWithID(getListeThesesContainerId(), context());
		
		return doNothing();
	}
	
	public WOActionResults reinitialiser() {
		thesesVisibles = null;
		getDisplayGroupTheses().setObjectArray(getListeTheses());
		getDisplayGroupTheses().setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
		getDisplayGroupTheses().updateDisplayedObjects();
		setSelectedThese(getDisplayGroupTheses().selectedObject());
		AjaxUpdateContainer.updateContainerWithID(getListeThesesContainerId(), context());
		
		return doNothing();
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (isWantResetPage()) {
			reinitialiser();
		}
	    super.appendToResponse(response, context);
	}
	
	public String getThesesTableViewId() {
		return getComponentId() + "_thesesTableViewId";
	}
	
	public String getListeThesesContainerId() {
		return getComponentId() + "_listeThesesContainerId";
	}
	
	/**
	 * @return le libellé du fil d'ariane
	 */
	public String sousTitre() {
		if (getIsListeThesesAbandonnees()) {
			session().setLibellePageIntermediaire("Liste des thèses abandonnées");
			return " - Thèses > Liste des thèses abandonnées";
		}
		if (getIsListeThesesSoutenues()) {
			session().setLibellePageIntermediaire("Liste des thèses soutenues");
			return " - Thèses > Liste des thèses soutenues";
		}
		session().setLibellePageIntermediaire("Liste des thèses en cours");
		return " - Thèses > Liste des thèses en cours";
	}
	
	/**
	 * @return le service DroitsService
	 */
	public DroitsService ds() {
		if (ds == null) {
			ds = DroitsService.creerNouvelleInstance(edc(), factoryAssociation(), session().applicationUser().getPersId());
		}
		return ds;
	}

	public void setDs(DroitsService ds) {
		this.ds = ds;
	}
	
	/**
	 * @return : displayGroup des liens avec les enfants
	 */
	public ERXDisplayGroup<EODoctorantThese> getDisplayGroupTheses() {
		if (displayGroupTheses == null) {
			displayGroupTheses = new ERXDisplayGroup<EODoctorantThese>();
			displayGroupTheses.setDelegate(new ThesesDisplayGroupDelegate());
			displayGroupTheses.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
			displayGroupTheses.setObjectArray(getListeTheses());
		}
		return displayGroupTheses;
	}

	public NSMutableArray<EODoctorantThese> getListeTheses() {
		if (thesesVisibles == null) {
		    thesesVisibles = new NSMutableArray<EODoctorantThese>();
		    // le type de these que l'on doit afficher : soutenues, en cours, abandonnées
		    if (getQualifiers() == null) {
		    	thesesVisibles.addAll(EODoctorantThese.fetchAll(edc(), EODoctorantThese.TO_CONTRAT.dot(Contrat.CON_OBJET).ascs()));
		    } else {
		    	for (EOQualifier qualifier : getQualifiers()) {	
		    		thesesVisibles.addAll(EODoctorantThese.fetchAll(edc(), qualifier, EODoctorantThese.TO_CONTRAT.dot(Contrat.CON_OBJET).ascs()));
		    	}
		    }
		    
		    // restriction selon les droits 
		    if (!session().getRechercheApplicationAutorisationCache().hasDroitVoirToutesTheses()) {
		    	NSArray<EODoctorantThese> thesesED = new NSMutableArray<EODoctorantThese>();
		    	NSArray<EODoctorantThese> thesesLabo = new NSMutableArray<EODoctorantThese>();
		    	thesesED.addAll(ERXQ.filtered(thesesVisibles, EODoctorantThese.ECOLE_DOCTORALE.in(ds().listeEcolesDoctoralesVisiblesSansDroit())));
		    	
		    	EOQualifier qualifierLabo = null;
		    	for (EOStructure labo : ds().listeLaboratoiresVisiblesSansDroit()) {
		    		qualifierLabo = ERXQ.or(qualifierLabo, EODoctorantThese.LABORATOIRES.containsObject(labo));
		    	}
		    	thesesLabo = ERXQ.filtered(thesesVisibles, qualifierLabo);
		    	
		    	thesesVisibles.clear();
		    	thesesVisibles.addAll(thesesED);
		    	thesesVisibles.addAll(thesesLabo);
		    }
		    thesesVisibles = ERXArrayUtilities.arrayWithoutDuplicates(thesesVisibles).mutableClone();
		    theses = new NSMutableArray<EODoctorantThese>();
		    theses.addAll(thesesVisibles);
		}
	    return thesesVisibles;
    }
	
	/**
	 * @return la page de la these selectionnee
	 */
	public WOActionResults afficherThese() {
		if (selectedThese != null) {
			setWantResetPage(true);
			GestionDoctorant gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
			gd.setLeDoctorant(selectedThese.toDoctorant());
			gd.setEdcDoctorant(edc());
			return gd;
		} else {
			return null;
		}
	}
	
	public EODoctorantThese getCurrentThese() {
		return currentThese;
	}

	public void setCurrentThese(EODoctorantThese currentThese) {
		this.currentThese = currentThese;
	}
	
	public EODoctorantThese getSelectedThese() {
		return selectedThese;
	}

	public void setSelectedThese(EODoctorantThese selectedThese) {
		this.selectedThese = selectedThese;
	}
	
	public boolean isBoutonAfficherTheseDisabled() {
		return (getSelectedThese() == null);
	}
	
	/**
	 * Update
	 * @return null
	 */
	public WOActionResults update() {
 		AjaxUpdateContainer.updateContainerWithID(getBoutonsContainerId(), context());
 		return doNothing();
 	}
	
	public String getBoutonsContainerId() { 
    	return getComponentId() + "_boutonsContainerId";
    }
	
	public String getCurrentEcoleDoctorale() {
		return getCurrentThese().toContrat().structuresPartenairesForAssociation(
					factoryAssociation().ecoleDoctoraleAssociation(edc())
				).lastObject().libelle();
	}
	
	/**
	 * @return la liste des libellés des laboratoires
	 */
	public String getCurrentLaboratoire() {
		
		NSArray<EOStructure> laboratoires = getCurrentThese().toContrat().structuresPartenairesForAssociation(
				factoryAssociation().laboratoireTheseAssociation(edc()));
		
		String listeLaboratoires = "";
		
		for (EOStructure laboratoire : laboratoires) {
			if (!listeLaboratoires.equals("")) {
				listeLaboratoires += ", ";
			}
			listeLaboratoires += laboratoire.lcStructure();
		}
		
		return listeLaboratoires;
	}
	
	public NSArray<EOQualifier> getQualifiers() {
	    return qualifiers;
    }
	
	public void setQualifiers(NSArray<EOQualifier> qualifiers) {
	    this.qualifiers = qualifiers;
    }

	/**
	 * Display group theses
	 */
	public class ThesesDisplayGroupDelegate {
		/**
		 * @param group groupe
		 */
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		    @SuppressWarnings("unchecked")
            ERXDisplayGroup<EODoctorantThese> _groupe = (ERXDisplayGroup<EODoctorantThese>) group;
		    if (_groupe.selectedObject() != null) {
		    	setSelectedThese(_groupe.selectedObject());
		    } else {
		    	setSelectedThese(null);
		    }
		}
    }

	public Boolean getIsListeThesesSoutenues() {
	    return isListeThesesSoutenues;
    }

	public void setIsListeThesesSoutenues(Boolean isListeThesesSoutenues) {
	    this.isListeThesesSoutenues = isListeThesesSoutenues;
    }

	public Boolean getIsListeThesesAbandonnees() {
	    return isListeThesesAbandonnees;
    }

	public void setIsListeThesesAbandonnees(Boolean isListeThesesAbandonnees) {
	    this.isListeThesesAbandonnees = isListeThesesAbandonnees;
    }
	
	public boolean isWantResetPage() {
	    return wantResetPage;
    }

	public void setWantResetPage(boolean wantResetPage) {
	    this.wantResetPage = wantResetPage;
    }

	public NSMutableArray<EODoctorantThese> getTheses() {
	    return theses;
    }

	public void setTheses(NSMutableArray<EODoctorantThese> theses) {
	    this.theses = theses;
    }
	
}