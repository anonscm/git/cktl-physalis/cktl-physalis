package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;

public class DirecteurTheseHabiliteAff extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 975729896705158798L;
	public static final String B_dirTheseHab = "dirTheseHab";

	public DirecteurTheseHabiliteAff(WOContext context) {
        super(context);
    }
	
	public EODirecteurTheseHabilite dirTheseHab() {
		return (EODirecteurTheseHabilite)valueForBinding(B_dirTheseHab);
	}

}