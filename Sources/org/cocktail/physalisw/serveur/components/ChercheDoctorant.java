package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ChercheDoctorant extends PhysalisWOComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1602991994649551362L;
	private String nom;
	private String prenom;
	private WODisplayGroup dgDoctorant;
	private EODoctorant selectedDoctorant;
	private final DgDelegate dgDelegate = new DgDelegate();
	private EOEditingContext edcDoctorant = null;
	private boolean rechercheLancee = false;

	public ChercheDoctorant(WOContext context) {
		super(context);
	}

	public WOActionResults validerDoctorant() {
		if (selectedDoctorant != null) {
			GestionDoctorant gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
			gd.setLeDoctorant(selectedDoctorant());
			gd.setEdcDoctorant(edcDoctorant());
			return gd;
		} else {
			return null;
		}
	}

	public WOActionResults searchDoctorant() {
		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
		String tmpField = null;

		if (nom != null) {
			tmpField = "*" + nom + "*";
			NSMutableArray<EOQualifier> arrayNom = new NSMutableArray<EOQualifier>();
			arrayNom.addObject(new EOKeyValueQualifier(EODoctorant.TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.NOM_USUEL_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
					tmpField));
			arrayNom.addObject(new EOKeyValueQualifier(EODoctorant.TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.NOM_PATRONYMIQUE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike,
					tmpField));
			
			array.add(new EOOrQualifier(arrayNom));
		}

		if (prenom != null) {
			tmpField = "*" + prenom + "*";
			array.addObject(new EOKeyValueQualifier(EODoctorant.TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.PRENOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, tmpField));
		}

		NSArray<EODoctorant> doctorants = EODoctorant.fetchDoctorants(edcDoctorant(), new EOAndQualifier(array), null);
		setIsSelectionEnable(true);
		dgDoctorant().setObjectArray(doctorants);
		setRechercheLancee(true);
		return doNothing();
	}

	public WODisplayGroup dgDoctorant() {
		if (dgDoctorant == null) {
			dgDoctorant = new WODisplayGroup();
			dgDoctorant.setDelegate(dgDelegate);
		}
		return dgDoctorant;
	}
	
	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (group == dgDoctorant) {
				setSelectedDoctorant((EODoctorant) dgDoctorant.selectedObject());
			}
		}
	}

	/**
	 * @return the aucDoctorantTVId
	 */
	public String aucDoctorantTVId() {
		return getComponentId() + "_aucDoctorantTV";
	}

	public EODoctorant selectedDoctorant() {
		return selectedDoctorant;
	}

	public void setSelectedDoctorant(EODoctorant selectedDoctorant) {
		this.selectedDoctorant = selectedDoctorant;
	}

	public void setEdcDoctorant(EOEditingContext edcDoctorant) {
		this.edcDoctorant = edcDoctorant;
	}

	public EOEditingContext edcDoctorant() {
		if (edcDoctorant == null) {
			edcDoctorant = edc();
		}
		return edcDoctorant;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public boolean isRechercheLancee() {
	    return rechercheLancee;
    }

	public void setRechercheLancee(boolean rechercheLancee) {
	    this.rechercheLancee = rechercheLancee;
    }

}