package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.physalisw.serveur.components.common.PhysalisTableView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class RepartAssociationTBLV extends PhysalisTableView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2192218913214110233L;
	private static final String LIBELLE_STRUC = EORepartAssociation.TO_PERSONNE_ELT_KEY + "." + IPersonne.PERS_LIBELLE_KEY;
	// private static final String LIBELLE_INDIVIDU =
	// EORepartAssociation.TO_PERSONNE_ELT_KEY + ".getNomPrenomAffichage";
	private static final String LIBELLE_INDIVIDU = EORepartAssociation.TO_PERSONNE_ELT_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;

	private static final String CODE_STRUC = EORepartAssociation.TO_PERSONNE_ELT_KEY + "." + EOStructure.LC_STRUCTURE_KEY;
	private static final String DEBUT_ASS = EORepartAssociation.RAS_D_OUVERTURE_KEY;
	private static final String FIN_ASS = EORepartAssociation.RAS_D_FERMETURE_KEY;
	private static final String ROLE = EORepartAssociation.TO_ASSOCIATION_KEY + "." + EOAssociation.ASS_LIBELLE_KEY;
	private static final String B_libelleColonneNom = "libelleColonneNom";
	// valable uniquement si la repart association pointe sur une structure
	// ayant une adresse principale avec un pays non null
	private static final String PAYS = EORepartAssociation.TO_PERSONNE_ELT_KEY + ".adressePrincipale." + EOAdresse.TO_PAYS_KEY + "." + EOPays.LL_PAYS_KEY;

	static {

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Rôle");
		col2.setOrderKeyPath(ROLE);
		col2.setRowCssClass("alignToLeft useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + EORepartAssociation.TO_ASSOCIATION_KEY + "."
				+ EOAssociation.ASS_LIBELLE_KEY, " ");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, "ROLE");

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Nom");
		col3.setOrderKeyPath(LIBELLE_INDIVIDU);
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + LIBELLE_INDIVIDU, " ");
		// OBJ_KEY+".toPersonneElt.persLibelle", "emptyValue");
		col3.setAssociations(ass3);
		// _colonnesMap.takeValueForKey(col4, LIBELLE_STRUC);
		_colonnesMap.takeValueForKey(col3, "NOM");

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Laboratoire");
		col4.setOrderKeyPath(LIBELLE_STRUC);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + EORepartAssociation.TO_STRUCTURE_KEY + "." + IPersonne.PERS_LIBELLE_KEY, " ");
		// OBJ_KEY+".toPersonneElt.persLibelle", "emptyValue");
		col4.setAssociations(ass4);
		// _colonnesMap.takeValueForKey(col4, LIBELLE_STRUC);
		_colonnesMap.takeValueForKey(col4, "LIBELLE");

		CktlAjaxTableViewColumn col5 = new CktlAjaxTableViewColumn();
		col5.setLibelle("Code");
		col5.setOrderKeyPath(CODE_STRUC);
		col5.setRowCssClass("alignToLeft useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass5 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + EORepartAssociation.TO_STRUCTURE_KEY + "." + EOStructure.LC_STRUCTURE_KEY, " ");
		col5.setAssociations(ass5);
		// _colonnesMap.takeValueForKey(col5, CODE_STRUC);
		_colonnesMap.takeValueForKey(col5, "CODE");

		CktlAjaxTableViewColumn col6 = new CktlAjaxTableViewColumn();
		col6.setLibelle("Début");
		col6.setOrderKeyPath(DEBUT_ASS);
		col6.setRowCssClass("alignToCenter useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass6 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + DEBUT_ASS, " ");
		ass6.setDateformat("%d/%m/%Y");
		col6.setAssociations(ass6);
		// _colonnesMap.takeValueForKey(col6, DEBUT_ASS);
		_colonnesMap.takeValueForKey(col6, "DEBUT");

		CktlAjaxTableViewColumn col7 = new CktlAjaxTableViewColumn();
		col7.setLibelle("Fin");
		col7.setOrderKeyPath(FIN_ASS);
		col7.setRowCssClass("alignToCenter useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass7 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + FIN_ASS, " ");
		ass7.setDateformat("%d/%m/%Y");
		col7.setAssociations(ass7);
		// _colonnesMap.takeValueForKey(col7, FIN_ASS);
		_colonnesMap.takeValueForKey(col7, "FIN");

		CktlAjaxTableViewColumn col8 = new CktlAjaxTableViewColumn();
		col8.setLibelle("Pays");
		col8.setOrderKeyPath(PAYS);
		col8.setRowCssClass("alignToLeft useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass8 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + PAYS, " ");
		col8.setAssociations(ass8);
		// _colonnesMap.takeValueForKey(col8, PAYS);
		_colonnesMap.takeValueForKey(col8, "PAYS");

		CktlAjaxTableViewColumn col9 = new CktlAjaxTableViewColumn();
		col9.setLibelle("Rapporteurs");
		col9.setOrderKeyPath(LIBELLE_INDIVIDU);
		CktlAjaxTableViewColumnAssociation ass9 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + LIBELLE_INDIVIDU, " ");
		col9.setAssociations(ass9);
		_colonnesMap.takeValueForKey(col9, "RAPPORTEUR");

	}

	public RepartAssociationTBLV(WOContext context) {
		super(context);
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { "LIBELLE", "CODE", "DEBUT", "FIN"});
	}

	public WOActionResults commitSave() {
		return null;
	}

}