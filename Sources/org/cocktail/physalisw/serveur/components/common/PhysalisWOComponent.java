package org.cocktail.physalisw.serveur.components.common;

import java.util.Calendar;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.CktlResourceProvider;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlEnvironmentBackground;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.PhysalisWApplicationUser;
import org.cocktail.physalisw.serveur.Session;

import com.google.inject.Inject;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.ajax.AjaxUtils;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

public abstract class PhysalisWOComponent extends CktlAjaxWOComponent implements CktlResourceProvider {
	/** The onload js. */
	private String onloadJS;

	/** The application. */
	// public Application application = (Application)application();

	/** The Constant BINDING_utilisateurPersId. */
	public static final String BINDING_utilisateurPersId = "utilisateurPersId";

	/** The Constant BINDING_editingContext. */
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_isOngletSelected = "isOngletSelected";

	/** utile pour les tables views appelées **/
	private Boolean isSelectionEnable = true;

	/** The session. */
	public Session session = (Session) session();
	/** The container erreur msg id. */
	private String containerErreurMsgId;
	/** The application user. */
	private PhysalisWApplicationUser applicationUser;

	/** The erreur saisie message. */
	private String erreurSaisieMessage;

	/** The container info msg id. */
	private String containerInfoMsgId;

	/** The info message. */
	private String infoMessage;

	/** The utilisateur pers id. */
	private Integer utilisateurPersId;

	private String groupeEmployeurs = null;
	
	private EOEditingContext edc = null;

	private Boolean isGrhInUse;

	// private EOEditingContext edc;

	// public EOEditingContext edc = session.defaultEditingContext();
	
	/**
	 * Instantiates a new Physalis wo component.
	 * 
	 * @param context
	 *            the context
	 */
	public PhysalisWOComponent(WOContext context) {
		super(context);
	}
	
	public Application application() {
		return (Application) super.application();
	}

	public Session session() {
		return (Session) super.session();
	}

	/**
	 * Onload js.
	 * 
	 * @return the onloadJS
	 */
	public String onloadJS() {
		return onloadJS;
	}

	/**
	 * Sets the onload js.
	 * 
	 * @param onloadJS
	 *            the onloadJS to set
	 */
	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	/**
	 * Gets the container erreur msg id.
	 * 
	 * @return the container erreur msg id
	 */
	public String getContainerErreurMsgId() {
		if (containerErreurMsgId == null) {
			containerErreurMsgId = getComponentId() + "_containerErreurMsg";
		}
		return containerErreurMsgId;
	}

	/**
	 * Gets the erreur saisie message.
	 * 
	 * @return the erreur saisie message
	 */
	public String getErreurSaisieMessage() {
		String tmp = erreurSaisieMessage;
		return tmp;
	}

	/**
	 * Sets the erreur saisie message.
	 * 
	 * @param erreurSaisieMessage
	 *            the new erreur saisie message
	 */
	public void setErreurSaisieMessage(String erreurSaisieMessage) {
		this.erreurSaisieMessage = erreurSaisieMessage;
	}

	/**
	 * Gets the container info msg id.
	 * 
	 * @return the container info msg id
	 */
	public String getContainerInfoMsgId() {
		if (containerInfoMsgId == null) {
			containerInfoMsgId = getComponentId() + "_containerInfoMsg";
		}
		return containerInfoMsgId;
	}

	/**
	 * Sets the info message.
	 * 
	 * @param infoMessage
	 *            the new info message
	 */
	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	/**
	 * Gets the info message.
	 * 
	 * @return the info message
	 */
	public String getInfoMessage() {
		return infoMessage;
	}

	/**
	 * Clear erreur message.
	 */
	public void clearErreurMessage() {
		erreurSaisieMessage = "";
	}

	/**
	 * Clear info message.
	 */
	public void clearInfoMessage() {
		infoMessage = "";
	}

	/**
	 * Gets the utilisateur pers id.
	 * 
	 * @return the utilisateur pers id
	 */
	public Integer getUtilisateurPersId() {
		return session().applicationUser().getPersId();
	}

	/**
	 * Sets the utilisateur pers id.
	 * 
	 * @param utilisateurPersId
	 *            the new utilisateur pers id
	 */
	public void setUtilisateurPersId(Integer utilisateurPersId) {
		this.utilisateurPersId = utilisateurPersId;
	}

	/*
	 * public void setEdc(EOEditingContext e){ this.edc = e; }
	 */

	/**
	 * Nom du domaine.
	 * 
	 * @return the string
	 * 
	 *         public abstract String nomDuDomaine();
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent#edc()
	 */
	@Override
	public EOEditingContext edc() {
		
		if (edc == null) {
		    if (hasBinding(BINDING_editingContext) && valueForBinding(BINDING_editingContext) != null) {
		    	edc = (EOEditingContext) valueForBinding(BINDING_editingContext);
		    } else {
		    	edc = ERXEC.newEditingContext();
		    }
		}
		return edc;
	}

	/*
	 * Getters pour les objets utilitaires
	 */
	/**
	 * Gets the application user.
	 * 
	 * @return the application user
	 */
	public PhysalisWApplicationUser getApplicationUser() {
		if (applicationUser == null) {
			applicationUser = new PhysalisWApplicationUser(edc(), session().applicationUser().getPersId());
		}
		return applicationUser;
	}

	public Boolean isOngletSelected() {
		if (hasBinding(BINDING_isOngletSelected)) {
			return (Boolean) valueForBinding(BINDING_isOngletSelected);
		} else {
			return Boolean.FALSE;
		}
	}

	public Boolean isSelectionEnable() {
		return isSelectionEnable;
	}

	public void setIsSelectionEnable(Boolean isSelectionEnable) {
		this.isSelectionEnable = isSelectionEnable;
	}

	public EOQualifier qualifierEtablissementEtranger() {
		String leGroupeEtablissements = EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_ANNUAIRE_ETABLISSEMENTS_ETRANGERS);
		if (leGroupeEtablissements != null) {
			return EOStructure.C_STRUCTURE.eq(leGroupeEtablissements);
		} else {
			session.addSimpleErrorMessage("Erreur paramétrage",
					"le paramètre C_STRUCTURE_ETABLISSEMENTS_ETRANGERS n'est pas défini, veuillez prendre contact avec les informaticiens ");
			return null;
		}
	}

	/**
	 * @return Le c_structure de l'employeur défini dans grhum-parametres }.
	 */
	public String groupeEmployeurs() {
		if (groupeEmployeurs == null) {
			groupeEmployeurs = EOGrhumParametres.parametrePourCle(this.edc(), EOGrhumParametres.PARAM_ANNUAIRE_EMPLOYEUR);
		}
		return groupeEmployeurs;
	}

	public FactoryAssociation factoryAssociation() {
		return FactoryAssociation.shared();
	}

	public void appendStyleSheets(WOResponse response, WOContext context) {
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/physalisw.css");
	}

	public void injectResources(WOResponse response, WOContext context) {
		// appendStyleSheets(response, context);
	}
	
	/**
	 * construit la condition sur la colonne.
	 * @param nomColonne : nom de la colonne sur la quelle on fait la recherche.
	 * @param filtre : element recherché.
	 * @return EOQualifier
	 */
	protected EOQualifier getContainsQualifier(String nomColonne, String filtre) {
		EOQualifier qualifier = null;
		if (filtre != null) {
			qualifier = ERXQ.contains(nomColonne, filtre);
		}
		return qualifier;
	}
	
	protected Integer getAnneeCourante() {
		Calendar calendar = Calendar.getInstance();
		Integer annee = calendar.get(Calendar.YEAR);
		return annee;
	}
	
	/**
	 * @param key :identifiant de la chaine à localiser
	 * @return chaine localisée
	 */
	public String localisation(String key) {
		return session().localizer().localizedStringForKey(key);
	}
	
	protected NSArray<EORepartAssociation> repartAssociationsIndividuRoleDoctorantLaboratoire(EOIndividu individu, EOStructure laboratoire) {
		
		if (individu == null || laboratoire == null) {
			return null;
		}
		
		EOQualifier qual = ERXQ.and(
				EORepartAssociation.TO_ASSOCIATION.eq(factoryAssociation().doctorantAssociation(edc())),
				EORepartAssociation.TO_STRUCTURE.eq(laboratoire),
				EORepartAssociation.TO_INDIVIDUS_ASSOCIES.eq(individu)
				);
				
		return EORepartAssociation.fetchAll(edc(), qual);
	}
	
	protected ERXRedirect doErxRedirect(WOContext context) {
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
	    redirect.setComponent(context.page());
		return redirect;
	}
	
	protected void disableOnglets(boolean disable, boolean shouldUpdate) {
		session().setTabDisabled(disable);
		if (shouldUpdate) {
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		}
    }
	
	public Boolean getIsGrhInUse() {
		if (isGrhInUse == null) {
			isGrhInUse = GRHUtilities.isModuleGRHInUse(edc());
		}
		return isGrhInUse; 
	}

}
