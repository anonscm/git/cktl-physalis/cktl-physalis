package org.cocktail.physalisw.serveur.components.common;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.CktlAjaxTableView;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public abstract class PhysalisTableView extends CktlAjaxTableView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 543213548798454L;
	public static final String BINDING_updateContainerID = "updateContainerID";
	/**
	 * Bindings pour les colonnes a afficher,
	 */
	public static final String BINDING_colonnesKeys = "colonnesKeys";
	public static final NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();
	private NSMutableArray<EOGenericRecord> deletedObjects;
	protected NSArray<CktlAjaxTableViewColumn> colonnes;
	private NSMutableDictionary<Boolean, NSArray<CktlAjaxTableViewColumn>> colsForCanEdit;
	protected EOGenericRecord objectOccur, selectedObject;
	protected static final String OBJ_KEY = "objectOccur";

	public abstract NSArray<String> DEFAULT_COLONNES_KEYS();

	public PhysalisTableView(WOContext context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		NSMutableArray<CktlAjaxTableViewColumn> res = new NSMutableArray<CktlAjaxTableViewColumn>();
		NSArray<String> colkeys = getColonnesKeys();

		for (int i = 0; i < colkeys.count(); i++) {
			CktlAjaxTableViewColumn col = (CktlAjaxTableViewColumn) _colonnesMap.valueForKey((String) colkeys.objectAtIndex(i));
			res.addObject((CktlAjaxTableViewColumn) _colonnesMap.valueForKey((String) colkeys.objectAtIndex(i)));
		}

		return res.immutableClone();
	}

	public NSArray<String> getColonnesKeys() {
		NSArray<String> keys = DEFAULT_COLONNES_KEYS();
		if (hasBinding(BINDING_colonnesKeys)) {
			String keysStr = (String) valueForBinding(BINDING_colonnesKeys);
			keys = NSArray.componentsSeparatedByString(keysStr, ",");
		}
		return keys;
	}

	public boolean showNavigBar() {
		WODisplayGroup dg = (WODisplayGroup) valueForBinding("dg");
		if (dg != null) {
			if (dg.allObjects().count() > dg.displayedObjects().count())
				return true;
		}
		return false;
	}

	public EOGenericRecord objectOccur() {
		if (hasBinding("objectOccur"))
			objectOccur = (EOGenericRecord) valueForBinding("objectOccur");
		return objectOccur;
	}

	public void setObjectOccur(EOGenericRecord objectOccur) {
		this.objectOccur = objectOccur;
		if ((canSetValueForBinding("objectOccur")))
			setValueForBinding(objectOccur, "objectOccur");
	}

	public EOGenericRecord selectedObject() {
		return selectedObject;
	}

	public void setSelectedObject(EOGenericRecord selectedObject) {
		this.selectedObject = selectedObject;
	}

}
