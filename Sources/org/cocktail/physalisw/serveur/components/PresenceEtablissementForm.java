package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class PresenceEtablissementForm extends PhysalisWOComponent {
	
    private static final long serialVersionUID = -6442822565985865608L;
	private static final String B_editingEtablissementPresenceEtablissement = "editingEtablissementPresenceEtablissement";
	private static final String B_editingDateDebutPresenceEtablissement = "editingDateDebutPresenceEtablissement";
	private static final String B_editingDateFinPresenceEtablissement = "editingDateFinPresenceEtablissement";
	private static final String B_isModifPresenceEtablissement = "isModifPresenceEtablissement";
	private static final String B_listEtablissements = "listEtablissements";
	private EOStructure currentEtablissement;
	
	/**
	 * Constructeur
	 * @param context contexte 
	 */
    public PresenceEtablissementForm(WOContext context) {
        super(context);
    }
    
    public EOStructure editingEtablissementPresenceEtablissement() {
		return (EOStructure) valueForBinding(B_editingEtablissementPresenceEtablissement);
	}

	public void setEditingEtablissementPresenceEtablissement(EOStructure editingEtablissementPresenceEtablissement) {
		setValueForBinding(editingEtablissementPresenceEtablissement, B_editingEtablissementPresenceEtablissement);
	}
    
    public NSTimestamp editingDateDebutPresenceEtablissement() {
		return (NSTimestamp) valueForBinding(B_editingDateDebutPresenceEtablissement);
	}

	public void setEditingDateDebutPresenceEtablissement(NSTimestamp editingDateDebutPresenceEtablissement) {
		setValueForBinding(editingDateDebutPresenceEtablissement, B_editingDateDebutPresenceEtablissement);
	}
    
    public NSTimestamp editingDateFinPresenceEtablissement() {
		return (NSTimestamp) valueForBinding(B_editingDateFinPresenceEtablissement);
	}

	public void setEditingDateFinPresenceEtablissement(NSTimestamp editingDateFinPresenceEtablissement) {
		setValueForBinding(editingDateFinPresenceEtablissement, B_editingDateFinPresenceEtablissement);
	}
	
	public Boolean isModifPresenceEtablissement() {
		return (Boolean) valueForBinding(B_isModifPresenceEtablissement);
	}
	
    public NSArray<EOStructure> listEtablissements() {
		return (NSArray<EOStructure>) valueForBinding(B_listEtablissements);
	}

	public EOStructure currentEtablissement() {
	    return currentEtablissement;
    }

	public void setCurrentEtablissement(EOStructure currentEtablissement) {
	    this.currentEtablissement = currentEtablissement;
    }
    
}