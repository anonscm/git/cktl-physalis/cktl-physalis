package org.cocktail.physalisw.serveur.components;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.cocktail.cocowork.server.metier.convention.AvenantDocument;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleAdresseDoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleAdresseEtablissement;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleAnneeInscription;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleAnneeInscriptionDerogation;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleCotutelle;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleDateDeSoutenance;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleDecretDoctorat;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleDirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleDoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleEmailDoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleInformationsCotutelle;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleJuryDeSoutenance;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleLieuDeSoutenance;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleMention;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleRapporteursThese;
import org.cocktail.fwkcktlrecherche.server.metier.controles.doctorantthese.ControleSise;
import org.cocktail.fwkcktlrecherche.server.metier.service.EditionDocumentsService;
import org.cocktail.fwkcktlrecherche.server.metier.service.JuryService;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXResourceManager;
import er.extensions.eof.ERXEOControlUtilities;

public class Documents extends PhysalisWOComponent {

	private static final long serialVersionUID = -2705562548310833090L;

	private Logger logger = Logger.getLogger(Documents.class);
	
	private ERXDisplayGroup<AvenantDocument> dgDocuments;
	private AvenantDocument selectedDocument = null;
	private EODocument editingDocument = null;
	private NSTimestamp dateEditionDiplome;

	private static final String B_laThese = "laThese";
	private static final String B_leDoctorant = "leDoctorant";
	private static final int POURCENTAGE_MAX = 100;

	private EODocument nouveauDocument = null;
	private FactoryAvenant factoryAvenant;
	private Boolean isModif = false;
	private EODoctorantThese laThese = null;

	private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;

	private List<ResultatControle> resultatsControlesFicheDerogationInscription;
	private List<ResultatControle> resultatsControlesFicheInscription;	
	private List<ResultatControle> resultatsControlesConvocationJury;
	private List<ResultatControle> resultatsControlesConvocationSoutenance;
	private List<ResultatControle> resultatsControlesAvisDeSoutenance;
	private List<ResultatControle> resultatsControlesRappelInscription;
	private List<ResultatControle> resultatsControlesRapportThese;
	private List<ResultatControle> resultatsControlesAutorisationSoutenanceCotutelle;
	private List<ResultatControle> resultatsControlesCertificatDocteur;
	private List<ResultatControle> resultatsControlesAvisReproductionThese;
	private List<ResultatControle> resultatsControlesDiplome;

	private ResultatControle currentResultatControle;
	
	private static final String REPORTS_FOLDER = "Reports";

	private static final String REPORT_FICHE_DEROGATION_INSCRIPTION = REPORTS_FOLDER + File.separator + "FicheInscriptionDerogation" + File.separator + "FicheInscriptionDerogation.jasper";
	private static final String REPORT_CONVOCATION_SOUTENANCE = REPORTS_FOLDER + File.separator + "ConvocationSoutenance" + File.separator + "ConvocationSoutenance.jasper";
	private static final String REPORT_RAPPEL_INSCRIPTION = REPORTS_FOLDER + File.separator + "RappelInscription" + File.separator + "RappelInscription.jasper";
	private static final String REPORT_FICHE_INSCRIPTION = REPORTS_FOLDER + File.separator + "FicheInscription" + File.separator + "FicheInscription.jasper";
	private static final String REPORT_CONVOCATION_JURY = REPORTS_FOLDER + File.separator + "ConvocationJury" + File.separator + "ConvocationJury.jasper";
	private static final String REPORT_AVIS_SOUTENANCE = REPORTS_FOLDER + File.separator + "AvisSoutenance" + File.separator + "AvisSoutenance.jasper";
	private static final String REPORT_RAPPORT_THESE = REPORTS_FOLDER + File.separator + "RapportThese" + File.separator + "RapportThese.jasper";
	private static final String REPORT_AUTORISATION_SOUTENANCE_COTUTELLE = REPORTS_FOLDER + File.separator + "AutorisationSoutenanceCotutelle" + File.separator + "AutorisationSoutenanceCotutelle.jasper";
	private static final String REPORT_CERTIFICAT_DOCTEUR = REPORTS_FOLDER + File.separator + "CertificatDocteur" + File.separator + "CertificatDocteur.jasper";
	private static final String REPORT_AVIS_REPRODUCTION_THESE = REPORTS_FOLDER + File.separator + "AvisReproductionThese" + File.separator + "AvisReproductionThese.jasper";
	private static final String REPORT_DIPLOME = REPORTS_FOLDER + File.separator + "Diplome" + File.separator + "Diplome.jasper";

	public Documents(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		resultatsControlesConvocationJury = checkPeutEditerConvocationJury();
		resultatsControlesConvocationSoutenance = checkPeutEditerConvocationSoutenance();
		resultatsControlesAvisDeSoutenance = checkPeutEditerAvisDeSoutenance();
		resultatsControlesRappelInscription = checkPeutEditerRappelInscription();
		resultatsControlesRapportThese = checkPeutEditerRapportThese();
		resultatsControlesFicheDerogationInscription = checkPeutEditerFicheDerogationInscription();
		resultatsControlesFicheInscription = checkPeutEditerFicheInscription();
		setResultatsControlesAutorisationSoutenanceCotutelle(checkPeutEditerAutorisationSoutenanceCotutelle());
		setResultatsControlesCertificatDocteur(checkPeutEditerCertificatDocteur());
		setResultatsControlesAvisReproductionThese(checkPeutEditerAvisReproductionThese());
		setResultatsControlesDiplome(checkPeutEditerDiplome());
		super.appendToResponse(response, context);
	}

	public String getAucDocumentsId() {
		return getComponentId() + "_aucDocumentsId";
	}

	public String getAucDetailDocId() {
		return getComponentId() + "_aucDetailDocId";
	}
	
	public String getDiplomeContainerId() {
		return getComponentId() + "_diplomeContainerId";
	}

	public String getAjouterDocumentWindowId() {
		return getComponentId() + "_ajouterDocumentWindowId";
	}

	public ResultatControle getCurrentResultatControle() {
		return currentResultatControle;
	}

	public void setCurrentResultatControle(ResultatControle currentResultatControle) {
		this.currentResultatControle = currentResultatControle;
	}
	
	public List<ResultatControle> getResultatsControlesFicheDerogationInscription() {
		return resultatsControlesFicheDerogationInscription;
	}

	public void setResultatsControlesFicheDerogationInscription(List<ResultatControle> resultatsControlesFicheDerogationInscription) {
		this.resultatsControlesFicheDerogationInscription = resultatsControlesFicheDerogationInscription;
	}

	public List<ResultatControle> getResultatsControlesFicheInscription() {
		return resultatsControlesFicheInscription;
	}

	public void setResultatsControlesFicheInscription(List<ResultatControle> resultatsControlesFicheInscription) {
		this.resultatsControlesFicheInscription = resultatsControlesFicheInscription;
	}
	
	public List<ResultatControle> getResultatsControlesAvisDeSoutenance() {
		return resultatsControlesAvisDeSoutenance;
	}

	public void setResultatsControlesAvisDeSoutenance(List<ResultatControle> resultatsControlesAvisDeSoutenance) {
		this.resultatsControlesAvisDeSoutenance = resultatsControlesAvisDeSoutenance;
	}

	public List<ResultatControle> getResultatsControlesRappelInscription() {
		return resultatsControlesRappelInscription;
	}

	public void setResultatsControlesRappelInscription(List<ResultatControle> resultatsControlesRappelInscription) {
		this.resultatsControlesRappelInscription = resultatsControlesRappelInscription;
	}

	public List<ResultatControle> getResultatsControlesRapportThese() {
		return resultatsControlesRapportThese;
	}

	public void setResultatsControlesRapportThese(List<ResultatControle> resultatsControlesRapportThese) {
		this.resultatsControlesRapportThese = resultatsControlesRapportThese;
	}

	public List<ResultatControle> getResultatsControlesConvocationJury() {
	    return resultatsControlesConvocationJury;
    }

	public void setResultatsControlesConvocationJury(List<ResultatControle> resultatsControlesConvocationJury) {
	    this.resultatsControlesConvocationJury = resultatsControlesConvocationJury;
    }

	public List<ResultatControle> getResultatsControlesConvocationSoutenance() {
	    return resultatsControlesConvocationSoutenance;
    }

	public void setResultatsControlesConvocationSoutenance(List<ResultatControle> resultatsControlesConvocationSoutenance) {
	    this.resultatsControlesConvocationSoutenance = resultatsControlesConvocationSoutenance;
    }

	public List<ResultatControle> getResultatsControlesAutorisationSoutenanceCotutelle() {
	    return resultatsControlesAutorisationSoutenanceCotutelle;
    }

	public void setResultatsControlesAutorisationSoutenanceCotutelle(List<ResultatControle> resultatsControlesAutorisationSoutenanceCotutelle) {
	    this.resultatsControlesAutorisationSoutenanceCotutelle = resultatsControlesAutorisationSoutenanceCotutelle;
    }
	
	public List<ResultatControle> getResultatsControlesCertificatDocteur() {
		return resultatsControlesCertificatDocteur;
	}

	public void setResultatsControlesCertificatDocteur(List<ResultatControle> resultatsControlesCertificatDocteur) {
		this.resultatsControlesCertificatDocteur = resultatsControlesCertificatDocteur;
	}

	public List<ResultatControle> getResultatsControlesAvisReproductionThese() {
	    return resultatsControlesAvisReproductionThese;
    }

	public void setResultatsControlesAvisReproductionThese(List<ResultatControle> resultatsControlesAvisReproductionThese) {
	    this.resultatsControlesAvisReproductionThese = resultatsControlesAvisReproductionThese;
    }

	public List<ResultatControle> getResultatsControlesDiplome() {
		return resultatsControlesDiplome;
	}

	public void setResultatsControlesDiplome(List<ResultatControle> resultatsControlesDiplome) {
		this.resultatsControlesDiplome = resultatsControlesDiplome;
	}

	public EODoctorantThese laThese() {
		if (laThese != (EODoctorantThese) valueForBinding(B_laThese)) {
			laThese = (EODoctorantThese) valueForBinding(B_laThese);
			dgDocuments = null;
		}
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, B_laThese);
	}

	public EODoctorant leDoctorant() {
		return (EODoctorant) valueForBinding(B_leDoctorant);
	}

	private Object leDoctorantId() {
		return ERXEOControlUtilities.primaryKeyObjectForObject(leDoctorant());
	}

	private Object laTheseId() {
		return ERXEOControlUtilities.primaryKeyObjectForObject(laThese());
	}

	private String nomEtPrenomDoctorant() {
		return leDoctorant().toIndividuFwkpers().nomAffichage() + "_" + leDoctorant().toIndividuFwkpers().prenom();
	}

	public void setFactoryAvenant(FactoryAvenant factoryAvenant) {
		this.factoryAvenant = factoryAvenant;
	}

	public FactoryAvenant factoryAvenant() {
		if (factoryAvenant == null) {
			factoryAvenant = new FactoryAvenant(edc(), false);
		}
		return factoryAvenant;
	}

	public void setSelectedDocument(AvenantDocument selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

	public AvenantDocument selectedDocument() {
		return selectedDocument;
	}

	public EODocument nouveauDocument() {
		return nouveauDocument;
	}

	public void setNouveauDocument(EODocument nouveauDocument) {
		this.nouveauDocument = nouveauDocument;
	}

	public void setEditingDocument(EODocument editingDocument) {
		this.editingDocument = editingDocument;
	}

	public EODocument editingDocument() {
		return editingDocument;
	}

	public void setIsModif(Boolean isModif) {
		this.isModif = isModif;
	}

	public Boolean isModif() {
		return isModif;
	}

	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
		return reporterProgress;
	}

	public String getReportFilename() {
		return reportFilename;
	}

	public void setReportFilename(String reportFilename) {
		this.reportFilename = reportFilename;
	}
	
	public WOActionResults onPrintFicheInscription() {

		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Fiche_inscription_doctorat_" + nomEtPrenomDoctorant() + ".xls";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());
		parameters.put("DEBUT_ANNEE_UNIVERSITAIRE", EditionDocumentsService.debutAnneeUniversitaire(new NSTimestamp()));
		parameters.put("FIN_ANNEE_UNIVERSITAIRE", EditionDocumentsService.finAnneeUniversitaire(new NSTimestamp()));
		parameters.put("ANNEE_INSCRIPTION", EditionDocumentsService.anneeDInscriptionThese(laThese));

		reporter = getReporter(reporterProgress, REPORT_FICHE_INSCRIPTION, parameters, CktlAbstractReporter.EXPORT_FORMAT_XLS, "Impression de la fiche d'inscription du doctorant");
		return doNothing();
	}
	
	public Boolean nePeutEditerInscription() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesFicheInscription);
	}

	public WOActionResults onPrintFicheDerogationInscription() {

		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Fiche_derogation_inscription_doctorat_" + nomEtPrenomDoctorant() + ".xls";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());
		parameters.put("DEBUT_ANNEE_UNIVERSITAIRE", EditionDocumentsService.debutAnneeUniversitaire(new NSTimestamp()));
		parameters.put("FIN_ANNEE_UNIVERSITAIRE", EditionDocumentsService.finAnneeUniversitaire(new NSTimestamp()));
		parameters.put("ANNEE_INSCRIPTION", EditionDocumentsService.anneeDInscriptionThese(laThese));

		reporter = getReporter(reporterProgress, REPORT_FICHE_DEROGATION_INSCRIPTION, parameters, CktlAbstractReporter.EXPORT_FORMAT_XLS, "Impression de la fiche derogation d'inscription du doctorant");
		return doNothing();
	}
	
	public Boolean nePeutEditerDerogationInscription() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesFicheDerogationInscription);
	}
	
	public WOActionResults onPrintConvocationJury() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Convocation_jury_soutenance_" + leDoctorant().toIndividuFwkpers().nomAffichage() + "_" + leDoctorant().toIndividuFwkpers().prenom() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());

		reporter = getReporter(reporterProgress, REPORT_CONVOCATION_JURY, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression des convocations du jury");

		return doNothing();
	}
	
	public Boolean nePeutEditerConvocationJury() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesConvocationJury);
	}
	
	public WOActionResults onPrintAvisSoutenance() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Avis_soutenance_" + nomEtPrenomDoctorant() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());

		reporter = getReporter(reporterProgress, REPORT_AVIS_SOUTENANCE, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression de l'avis de soutenance");

		return doNothing();
	}

	public Boolean nePeutEditerAvisSoutenance() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesAvisDeSoutenance);
	}

	public WOActionResults onPrintConvocationSoutenance() {

		reportFilename = "Convocation_soutenance_" + nomEtPrenomDoctorant() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());

		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reporter = getReporter(reporterProgress, REPORT_CONVOCATION_SOUTENANCE, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression de la convocation à la soutenance");

		return doNothing();
	}
	
	public Boolean nePeutEditerConvocationSoutenance() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesConvocationSoutenance);
	}

	public WOActionResults onPrintRappelInscription() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Rappel_inscription_" + nomEtPrenomDoctorant() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());

		reporter = getReporter(reporterProgress, REPORT_RAPPEL_INSCRIPTION, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression du rappel d'inscription");

		return doNothing();
	}
	
	public Boolean nePeutEditerRappelInscription() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesRappelInscription);
	}

	public WOActionResults onPrintRapportThese() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Rapport_these_" + leDoctorant().toIndividuFwkpers().nomAffichage() + "_" + leDoctorant().toIndividuFwkpers().prenom() + ".pdf";

		Date dateRetour = null;

		if (laThese().dateSoutenance() != null) {
			dateRetour = DateUtils.addMonths(laThese().dateSoutenance(), -1);
		} else if (laThese().datePrevSoutenance() != null) {
			dateRetour = DateUtils.addMonths(laThese().datePrevSoutenance(), -1);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());
		parameters.put("DATE_LIMITE_RETOUR_RAPPORT", dateRetour);

		reporter = getReporter(reporterProgress, REPORT_RAPPORT_THESE, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression du rapport de these");

		return doNothing();
	}
	
	public Boolean nePeutEditerRapportThese() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesRapportThese);
	}

	public WOActionResults onPrintAutorisationSoutenanceCotutelle() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Autorisation_Soutenance_";
		if (laThese().isCotutelle()) {
			reportFilename += "cotutelle_" ;
		}
		reportFilename += leDoctorant().toIndividuFwkpers().nomAffichage() + "_" + leDoctorant().toIndividuFwkpers().prenom() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());
		parameters.put("COTUTELLE", laThese().isCotutelle());
		reporter = getReporter(reporterProgress, REPORT_AUTORISATION_SOUTENANCE_COTUTELLE, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression du rapport de these");

		return doNothing();
	}
	
	public Boolean nePeutEditerAutorisationSoutenanceCotutelle() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesAutorisationSoutenanceCotutelle);
	}

	public WOActionResults onPrintCertificatDocteur() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Certificat_docteur_" + leDoctorant().toIndividuFwkpers().nomAffichage() + "_" + leDoctorant().toIndividuFwkpers().prenom() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());

		reporter = getReporter(reporterProgress, REPORT_CERTIFICAT_DOCTEUR, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression du rapport de these");

		return doNothing();
	}
	
	public Boolean nePeutEditerCertificatDocteur() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesCertificatDocteur);
	}
	
	public WOActionResults onPrintAvisReproductionThese() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Avis_reproduction_these_" + leDoctorant().toIndividuFwkpers().nomAffichage() + "_" + leDoctorant().toIndividuFwkpers().prenom() + ".pdf";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_DOCTORANT", leDoctorantId());

		reporter = getReporter(reporterProgress, REPORT_AVIS_REPRODUCTION_THESE, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression du rapport de these");

		return doNothing();
	}
	
	public Boolean nePeutEditerAvisReproductionThese() {
		return !tousLesResulatsDeControlesSontOk(resultatsControlesAvisReproductionThese);
	}

	public WOActionResults onPrintDiplome() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);

		reportFilename = "Diplome_doctorat_" + nomEtPrenomDoctorant() + ".pdf";
		
		String retourChariot = "<br/>";
		String decrets = "";
		String nom_diplome = "";
		String sous_titre = "";
		
		if (laThese().isCotutelle()) {
			
			nom_diplome = "DOCTORAT";
			sous_titre = "préparé dans le cadre d'une cotutelle internationale de thèse";
		
			decrets = "Vu le code de l’éducation, notamment son article L. 612-7 ;" + retourChariot;
			decrets += "Vu le code de la recherche, notamment son article L.412-1 ;" + retourChariot;
			decrets += "Vu l’arrêté du 3 septembre 1998 relatif à la charte de thèses ;" + retourChariot;
			decrets += "Vu le décret n°2002-481 du 8 avril 2002 relatif aux grades et titres universitaires et aux diplômes nationaux ;" + retourChariot;
			decrets += "Vu l’arrêté du 6 janvier 2005 relatif à la cotutelle internationale de thèse ;";
			decrets += "Vu l’arrêté du 7 août 2006 relatif à la formation doctorale ;";
		
		} else {
			
			nom_diplome = "DOCTORAT";
			
			decrets = "Vu le code de l’éducation, notamment son article L. 612-7 ;" + retourChariot;
			decrets += "Vu le code de la recherche, notamment son article L.412-1 ;" + retourChariot;
			decrets += "Vu l’arrêté du 3 septembre 1998 relatif à la charte de thèses ;" + retourChariot;
			decrets += "Vu le décret n°2002-481 du 8 avril 2002 relatif aux grades et titres universitaires et aux diplômes nationaux ;" + retourChariot;
			//decrets += "Vu l’arrêté du 27 juin 1985 modifié fixant la liste des établissements autorisés à délivrer, seuls, le doctorat ;" + retourChariot;
			decrets += "Vu l’arrêté du 7 août 2006 relatif à la formation doctorale ;";
		}
		JuryService juryService = new JuryService(edc());
		NSArray<EOMembreJuryThese> lesPresidents = juryService.listePresidents(laThese());
		String president = "";
		for (EOMembreJuryThese membreJuryThese : lesPresidents) {
			if (!president.isEmpty()) {
				president += ", ";
			}
			
			EOIndividu membre = ((EOIndividu) membreJuryThese.repartAssociationJury().toPersonne());
			president += StringCtrl.capitalizeWords(membre.prenom().toLowerCase()) + " " + membre.nomPatronymique().toUpperCase() + ", " 
					+ membreJuryThese.libelleCorpsEtTitre().toLowerCase();
		}
		
		NSArray<EOMembreJuryThese> lesMembres = juryService.listeMembresJuryAvecRoleMembre(laThese());
		String membresJury = "";
		for (EOMembreJuryThese membreJuryThese : lesMembres) {
			if (!membresJury.isEmpty()) {
				membresJury += ", ";
			}
			
			
			EOIndividu membre = ((EOIndividu) membreJuryThese.repartAssociationJury().toPersonne());
			membresJury += StringCtrl.capitalizeWords(membre.prenom().toLowerCase()) + " " + membre.nomPatronymique().toUpperCase() + ", " 
					+ membreJuryThese.libelleCorpsEtTitre().toLowerCase();
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID_THESE", laTheseId());
		parameters.put("NOM_DIPLOME", nom_diplome);
		parameters.put("SOUS_TITRE_DIPLOME", sous_titre);
		parameters.put("DECRETS", decrets);
		parameters.put("CODE_TYPE_DIPLOME_EDITION", "DOCTORAT");
		parameters.put("PRESIDENT", president);
		parameters.put("MEMBRES_JURY", membresJury);
		parameters.put("DATE_EDITION", getDateEditionDiplome());
		
		System.err.println(getDateEditionDiplome());

		reporter = getReporter(reporterProgress, REPORT_DIPLOME, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression du diplôme");

		return doNothing();
	}
	
	public Boolean nePeutEditerDiplome() {
		return !tousLesResulatsDeControlesSontOk(getResultatsControlesDiplome());
	}

	protected CktlAbstractReporter getReporter(IJrxmlReportListener listener, String jasperFileName, Map<String, Object> parameters, int format, String logMessage) {

		JrxmlReporter jr = null;

		try {
			Connection connection = application().getJDBCConnection();
			jr = new JrxmlReporter();
			jr.printWithThread(logMessage, connection, null, pathForJasper(jasperFileName), parameters, format, true, true, listener);
		} catch (Throwable e) {
			logger.error(e);
			session().addSimpleErrorMessage("Une erreur s'est produite durant l'edition du document", e);
		}

		return jr;

	}
	
	

	protected String pathForJasper(String reportPath) {
		ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
		URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
		return url.getFile();
	}
	
	public static class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements IJrxmlReportListener {

		public ReporterAjaxProgress(int maximum) {
			super(maximum);
		}

	}
	
	public List<ResultatControle> checkPeutEditerFicheInscription() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleAdresseEtablissement.class);
		controles.add(ControleAdresseDoctorant.class);
		controles.add(ControleAnneeInscription.class);
		controles.add(ControleEmailDoctorant.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerFicheDerogationInscription() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleAdresseEtablissement.class);
		controles.add(ControleAdresseDoctorant.class);
		controles.add(ControleAnneeInscriptionDerogation.class);
		controles.add(ControleEmailDoctorant.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerConvocationJury() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleAdresseEtablissement.class);
		controles.add(ControleDateDeSoutenance.class);
		controles.add(ControleLieuDeSoutenance.class);
		controles.add(ControleJuryDeSoutenance.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerConvocationSoutenance() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleAdresseEtablissement.class);
		controles.add(ControleDateDeSoutenance.class);
		controles.add(ControleLieuDeSoutenance.class);
		controles.add(ControleJuryDeSoutenance.class);
		controles.add(ControleAdresseDoctorant.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerAvisDeSoutenance() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleDateDeSoutenance.class);
		controles.add(ControleLieuDeSoutenance.class);
		controles.add(ControleJuryDeSoutenance.class);
		if (laThese().isCotutelle()) {
			controles.add(ControleInformationsCotutelle.class);
		}
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerRappelInscription() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleAdresseEtablissement.class);
		controles.add(ControleAdresseDoctorant.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerRapportThese() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleAdresseEtablissement.class);
		controles.add(ControleRapporteursThese.class);
		controles.add(ControleDirecteurThese.class);
		controles.add(ControleDateDeSoutenance.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerAutorisationSoutenanceCotutelle() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		if (laThese().isCotutelle()) {
			controles.add(ControleCotutelle.class);
			controles.add(ControleInformationsCotutelle.class);
		}
		controles.add(ControleRapporteursThese.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerCertificatDocteur() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleJuryDeSoutenance.class);
		controles.add(ControleDateDeSoutenance.class);
		if (laThese().isCotutelle()) {
			controles.add(ControleInformationsCotutelle.class);
		}
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerAvisReproductionThese() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleJuryDeSoutenance.class);
		controles.add(ControleDateDeSoutenance.class);
		return passerLesControles(controles);
	}
	
	public List<ResultatControle> checkPeutEditerDiplome() {
		List<Class<? extends ControleDoctorantThese>> controles = new ArrayList<Class<? extends ControleDoctorantThese>>();
		controles.add(ControleDecretDoctorat.class);
		controles.add(ControleJuryDeSoutenance.class);
		controles.add(ControleDateDeSoutenance.class);
		controles.add(ControleSise.class);
		controles.add(ControleMention.class);
		if (laThese().isCotutelle()) {
			controles.add(ControleInformationsCotutelle.class);
		}
		return passerLesControles(controles);
	}
	
	
	/**
	 * Passe les controles et retourne les resultats
	 * @param typeControles Un tableau de controles
	 * @return le tableau des resultats
	 */
	public List<ResultatControle> passerLesControles(List<Class<? extends ControleDoctorantThese>> typeControles) {
		
		List<ResultatControle> resultats = new ArrayList<ResultatControle>();
		
		for (Class<? extends ControleDoctorantThese> typeControle : typeControles) {
			ControleDoctorantThese controle;
			try {
				controle = typeControle.newInstance();
				resultats.add(controle.check(laThese()));
			} catch (Exception e) {
				logger.error("Une erreur est survenue lors du controle avant impression du document");
			}
		}
		
		return resultats;
	}
	
	/**
	 * Vérifie les résultats des controles
	 * @param resultats Les resultats à vérifier 
	 * @return true si tous les resultats sont ok
	 */
	public Boolean tousLesResulatsDeControlesSontOk(List<ResultatControle> resultats) {
		for (ResultatControle resultat : resultats) {
			if (!resultat.valide()) {
				return false;
			}
		}
		return true;
	}
	
	/** GED **/
	
	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (group.selectedObject() != null) {
				setSelectedDocument((AvenantDocument) group.selectedObject());
			} else {
				setSelectedDocument(null);
			}
		}
	}

	public ERXDisplayGroup<AvenantDocument> dgDocuments() {
		if (dgDocuments == null) {
			dgDocuments = new ERXDisplayGroup<AvenantDocument>();
			dgDocuments.setDelegate(new DgDelegate());
			dgDocuments.setObjectArray(laThese().toContrat().avenantZero().avenantDocuments());
		}

		return dgDocuments;
	}

	public void setDgDocuments(ERXDisplayGroup<AvenantDocument> dgDocuments) {
		this.dgDocuments = dgDocuments;
	}

	public WOActionResults supprimerUnDocument() {
		PersonneApplicationUser persAppUser = new PersonneApplicationUser(edc(), session().applicationUser().getPersId());

		if (selectedDocument != null) {
			try {
				FactoryAvenant fa = new FactoryAvenant(edc(), application().isModeDebug);
				fa.supprimerDocument(edc(), selectedDocument, persAppUser, null);
				// setDgDocuments(null);
				edc().saveChanges();

				dgDocuments.setObjectArray(laThese().toContrat().avenantZero().avenantDocuments());

			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}

		return null;
	}

	public WOActionResults validerDocument() {
		PersonneApplicationUser persAppUser = new PersonneApplicationUser(edc(), session().applicationUser().getPersId());
		AvenantDocument _avenantDocument = factoryAvenant().ajouterDocument(edc(), persAppUser, laThese().toContrat().avenantZero(), nouveauDocument());
		try {
			edc().saveChanges();
			dgDocuments.setObjectArray(laThese().toContrat().avenantZero().avenantDocuments());
			/*
			 * documentDataSource().insertObject(_avenantDocument);
			 * dgDocuments().fetch();
			 */
			CktlAjaxWindow.close(context());
			setIsSelectionEnable(true);

			AjaxUpdateContainer.updateContainerWithID(getAucDocumentsId(), context());
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		return null;
	}

	public WOActionResults annulerDocument() {
		edc().revert();
		CktlAjaxWindow.close(context());
		return null;
	}
	
	public boolean getWantRefreshGED() {
		return true;
	}
	
	public void setWantRefreshGED(boolean wantReset) {
	}
	
	public boolean canAfficherDoctorat() {
		return true;
	}

	public NSTimestamp getDateEditionDiplome() {
		if (dateEditionDiplome == null) {
			dateEditionDiplome = new NSTimestamp();
		}
		return dateEditionDiplome;
	}

	public void setDateEditionDiplome(NSTimestamp dateEditionDiplome) {
		this.dateEditionDiplome = dateEditionDiplome;
	}

}
