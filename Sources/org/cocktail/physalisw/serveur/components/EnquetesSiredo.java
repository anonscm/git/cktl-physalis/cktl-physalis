package org.cocktail.physalisw.serveur.components;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.service.DirecteurTheseService;
import org.cocktail.fwkcktlrecherche.server.metier.service.DroitsService;
import org.cocktail.fwkcktlrecherche.server.metier.service.EnquetesSiredoService;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.physalisw.serveur.components.Documents.ReporterAjaxProgress;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Page permettant de telecharger les enquetes siredo
 */
public class EnquetesSiredo extends PhysalisWOComponent {
    
    private static final long serialVersionUID = -7228166763562256322L;
    private static final int POURCENTAGE_MAX = 100;
    private static final String REPORTS_FOLDER = "Reports";

	private static final String REPORT_ENQUETE_SIREDO = REPORTS_FOLDER + File.separator + "Siredo" + File.separator + "Siredo.jasper";
	private static final String REPORT_ENQUETE_SIREDO_ECOLE_DOCTORALE = REPORTS_FOLDER + File.separator + "SiredoED" + File.separator + "SiredoED.jasper";
	private static final String REPORT_ENQUETE_SIREDO_ETABLISSEMENT = REPORTS_FOLDER + File.separator + "SiredoEtablissement" + File.separator + "SiredoEtablissement.jasper";
    
    private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;
	private DirecteurTheseService dts;
	private DroitsService ds;
	
	private Integer anneeCivileEnqueteSiredo;
	private Integer anneeCivileEnqueteSiredoED;
	private Integer anneeCivileEnqueteSiredoEtablissement;
	
	private EOStructure currentEcoleDoctorale;
	private EOStructure selectedEcoleDoctoraleSiredo;
	
	private NSArray<EOStructure> listeEcolesDoctoralesVisibles;

	/**
	 * Contructeur
	 * @param context contexte
	 */
	public EnquetesSiredo(WOContext context) {
        super(context);
    }
	
	public CktlAbstractReporter getReporter() {
		return reporter;
	}
	
	public CktlAbstractReporterAjaxProgress getReporterProgress() {
		return reporterProgress;
	}
	
	public String getReportFilename() {
		return reportFilename;
	}
	
	public void setReportFilename(String reportFilename) {
		this.reportFilename = reportFilename;
	}

	/**
	 * @return le service DirecteurTheseService
	 */
	public DirecteurTheseService dts() {
		if (dts == null) {
			dts = DirecteurTheseService.creerNouvelleInstance(edc());
		}
		return dts;
	}

	public void setDts(DirecteurTheseService dts) {
		this.dts = dts;
	}
	
	/**
	 * @return le service DroitsService
	 */
	public DroitsService ds() {
		if (ds == null) {
			ds = DroitsService.creerNouvelleInstance(edc(), factoryAssociation(), session().applicationUser().getPersId());
		}
		return ds;
	}

	public void setDs(DroitsService ds) {
		this.ds = ds;
	}
	
	/**
	 * @return la liste des ecoles doctorales visibles selon les droits
	 */
	public NSArray<EOStructure> listeEcolesDoctoralesVisibles() {
		if (listeEcolesDoctoralesVisibles == null) {
			if (session().getRechercheApplicationAutorisationCache().hasDroitVoirToutesEnquetesSiredo()) {
				listeEcolesDoctoralesVisibles = dts().listeEcolesDoctorales();
			} else {
				listeEcolesDoctoralesVisibles = ds().listeEcolesDoctoralesVisiblesSansDroit();
			}
		}
		
		return listeEcolesDoctoralesVisibles;
	}
	
	/**
	 * @return l'ecole doctorale selectionnée
	 */
	public EOStructure getSelectedEcoleDoctoraleSiredo() {
		if (selectedEcoleDoctoraleSiredo == null) {
			selectedEcoleDoctoraleSiredo = ERXArrayUtilities.firstObject(listeEcolesDoctoralesVisibles());
		}
		return selectedEcoleDoctoraleSiredo;
	}

	public void setSelectedEcoleDoctoraleSiredo(EOStructure selectedEcoleDoctoraleSiredo) {
		this.selectedEcoleDoctoraleSiredo = selectedEcoleDoctoraleSiredo;
	}
	
	public EOStructure getCurrentEcoleDoctorale() {
	    return currentEcoleDoctorale;
    }

	public void setCurrentEcoleDoctorale(EOStructure currentEcoleDoctorale) {
	    this.currentEcoleDoctorale = currentEcoleDoctorale;
    }
	
	/**
	 * @return l'année civile pour l'enquete siredo avec comme valeur par defaut l'année précédente
	 */
	public Integer anneeCivileEnqueteSiredo() {
		if (anneeCivileEnqueteSiredo == null) {
			anneeCivileEnqueteSiredo = getAnneeCourante() - 1;
		}
	    return anneeCivileEnqueteSiredo;
    }

	public void setAnneeCivileEnqueteSiredo(Integer anneeCivileEnqueteSiredo) {
	    this.anneeCivileEnqueteSiredo = anneeCivileEnqueteSiredo;
    }

	/**
	 * @return l'année civile pour l'enquete siredo d'une ED avec comme valeur par defaut l'année précédente
	 */
	public Integer anneeCivileEnqueteSiredoED() {
		if (anneeCivileEnqueteSiredoED == null) {
			anneeCivileEnqueteSiredoED = getAnneeCourante() - 1;
		}
	    return anneeCivileEnqueteSiredoED;
    }

	public void setAnneeCivileEnqueteSiredoED(Integer anneeCivileEnqueteSiredoED) {
	    this.anneeCivileEnqueteSiredoED = anneeCivileEnqueteSiredoED;
    }

	/**
	 * @return l'année civile pour l'enquete siredo 1 de l'etablissement avec comme valeur par defaut l'année précédente
	 */
	public Integer anneeCivileEnqueteSiredoEtablissement() {
		if (anneeCivileEnqueteSiredoEtablissement == null) {
			anneeCivileEnqueteSiredoEtablissement = getAnneeCourante() - 1;
		}
		return anneeCivileEnqueteSiredoEtablissement;
	}

	public void setAnneeCivileEnqueteSiredoEtablissement(Integer anneeCivileEnqueteSiredoEtablissement) {
		this.anneeCivileEnqueteSiredoEtablissement = anneeCivileEnqueteSiredoEtablissement;
	}
	
	/**
	 * Imprime l'enquete siredo d'une ecole doctorale
	 * @return doNothing
	 */
	public WOActionResults onPrintEnqueteSiredoED() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);
		reportFilename = "Enquete_siredo_" + getSelectedEcoleDoctoraleSiredo().lcStructure() + "_" + DateCtrl.currentDateHeureVerbeux() + ".pdf";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("anneeCivile", anneeCivileEnqueteSiredoED());
		parameters.put("debutAnneeCivile", EnquetesSiredoService.debutAnneeCivile(anneeCivileEnqueteSiredoED()));
		parameters.put("finAnneeCivile", EnquetesSiredoService.finAnneeCivile(anneeCivileEnqueteSiredoED()));
		parameters.put("cStructureEcoleDoctorale", getSelectedEcoleDoctoraleSiredo().cStructure());
		parameters.put("debutAnneeUniversitaire", EnquetesSiredoService.debutAnneeUniversitaire(anneeCivileEnqueteSiredoED()));
		parameters.put("finAnneeUniversitaire", EnquetesSiredoService.finAnneeUniversitaire(anneeCivileEnqueteSiredoED()));
		parameters.put("debutAnneeUniversitairePrecedente", EnquetesSiredoService.debutAnneeUniversitairePrecedente(anneeCivileEnqueteSiredoED()));
		parameters.put("finAnneeUniversitairePrecedente", EnquetesSiredoService.finAnneeUniversitairePrecedente(anneeCivileEnqueteSiredoED()));
		
		reporter = getReporter(reporterProgress, REPORT_ENQUETE_SIREDO_ECOLE_DOCTORALE, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression de l'enquête siredo de l'école doctorale");
		return doNothing();
	}

	/**
	 * Imprime l'enquete siredo de toutes les ED
	 * @return doNothing
	 */
	public WOActionResults onPrintEnqueteSiredo() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);
		reportFilename = "Enquete_siredo_" + DateCtrl.currentDateHeureVerbeux() + ".pdf";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("anneeCivile", anneeCivileEnqueteSiredo());
		parameters.put("debutAnneeCivile", EnquetesSiredoService.debutAnneeCivile(anneeCivileEnqueteSiredo()));
		parameters.put("finAnneeCivile", EnquetesSiredoService.finAnneeCivile(anneeCivileEnqueteSiredo()));
		parameters.put("debutAnneeUniversitaire", EnquetesSiredoService.debutAnneeUniversitaire(anneeCivileEnqueteSiredo()));
		parameters.put("finAnneeUniversitaire", EnquetesSiredoService.finAnneeUniversitaire(anneeCivileEnqueteSiredo()));
		parameters.put("debutAnneeUniversitairePrecedente", EnquetesSiredoService.debutAnneeUniversitairePrecedente(anneeCivileEnqueteSiredo()));
		parameters.put("finAnneeUniversitairePrecedente", EnquetesSiredoService.finAnneeUniversitairePrecedente(anneeCivileEnqueteSiredo()));
		
		reporter = getReporter(reporterProgress, REPORT_ENQUETE_SIREDO, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression de l'enquête siredo de toutes les ecoles doctorales");
		return doNothing();
	}
	
	/**
	 * Imprime l'enquete siredo d'un etablissement
	 * @return doNothing
	 */
	public WOActionResults onPrintEnqueteSiredoEtablissement() {
		reporterProgress = new ReporterAjaxProgress(POURCENTAGE_MAX);
		reportFilename = "Enquete_siredo_etablissement_" + DateCtrl.currentDateHeureVerbeux() + ".pdf";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("anneeCivile", anneeCivileEnqueteSiredoEtablissement());
		parameters.put("debutAnneeCivile", EnquetesSiredoService.debutAnneeCivile(anneeCivileEnqueteSiredoEtablissement()));
		parameters.put("finAnneeCivile", EnquetesSiredoService.finAnneeCivile(anneeCivileEnqueteSiredoEtablissement()));
		parameters.put("debutAnneeUniversitaire", EnquetesSiredoService.debutAnneeUniversitaire(anneeCivileEnqueteSiredoEtablissement()));
		parameters.put("finAnneeUniversitaire", EnquetesSiredoService.finAnneeUniversitaire(anneeCivileEnqueteSiredoEtablissement()));
		parameters.put("debutAnneeUniversitairePrecedente", EnquetesSiredoService.debutAnneeUniversitairePrecedente(anneeCivileEnqueteSiredoEtablissement()));
		parameters.put("finAnneeUniversitairePrecedente", EnquetesSiredoService.finAnneeUniversitairePrecedente(anneeCivileEnqueteSiredoEtablissement()));
		
		reporter = getReporter(reporterProgress, REPORT_ENQUETE_SIREDO_ETABLISSEMENT, parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, "Impression de l'enquête siredo de l'établissement");
		return doNothing();
	}
	
	protected CktlAbstractReporter getReporter(IJrxmlReportListener listener, String jasperFileName, Map<String, Object> parameters, int format, String logMessage) {
		JrxmlReporter jr = null;
		try {
			Connection connection = application().getJDBCConnection();
			jr = new JrxmlReporter();
			jr.printWithThread(logMessage, connection, null, pathForJasper(jasperFileName), parameters, format, true, true, listener);
		} catch (Throwable e) {
			session().addSimpleErrorMessage("Une erreur s'est produite durant l'edition du document", e);
		}
		return jr;
	}

	protected String pathForJasper(String reportPath) {
		ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
		URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
		return url.getFile();
	}

}