package org.cocktail.physalisw.serveur.components;

import org.cocktail.application.client.eof.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Formulaire de gestion de cotutelle
 */
public class CotutelleForm extends PhysalisWOComponent {

	private static final long serialVersionUID = -6105387402188308705L;
	private static final String B_editingEtablissementCotutelle = "editingEtablissementCotutelle";
	private static final String B_editingDateSignatureCotutelle = "editingDateSignatureCotutelle";
	private static final String B_editingDateDebutCotutelle = "editingDateDebutCotutelle";
	private static final String B_editingDateFinCotutelle = "editingDateFinCotutelle";
	private static final String B_editingCommentaireCotutelle = "editingCommentaireCotutelle";
	private static final String B_isCotutelleModif = "isCotutelleModif";
	private static final String B_selectedEtablissementCotutelle = "selectedEtablissementCotutelle";
	private static final String B_editingPays = "editingPays";

	public CotutelleForm(WOContext context) {
		super(context);
	}

	public String aucEtablissementId() {
		return getComponentId() + "_aucEtablissement";
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (editingEtablissementCotutelle() != null && editingEtablissementCotutelle().getAdressePrincipale() != null) {
			setEditingPays(editingEtablissementCotutelle().getAdressePrincipale().toPays());
		}
	    super.appendToResponse(response, context);
	}

	public EOStructure editingEtablissementCotutelle() {
		return (EOStructure) valueForBinding(B_editingEtablissementCotutelle);
	}

	public void setEditingEtablissementCotutelle(EOStructure editingEtablissementCotutelle) {
		if (editingEtablissementCotutelle != null && editingEtablissementCotutelle.getAdressePrincipale() != null && editingEtablissementCotutelle.getAdressePrincipale().toPays() != null) {
			setEditingPays(editingEtablissementCotutelle.getAdressePrincipale().toPays());
		}
		setValueForBinding(editingEtablissementCotutelle, B_editingEtablissementCotutelle);
	}

	public NSTimestamp editingDateSignatureCotutelle() {
		return (NSTimestamp) valueForBinding(B_editingDateSignatureCotutelle);
	}

	public void setEditingDateSignatureCotutelle(NSTimestamp editingDateSignatureCotutelle) {
		setValueForBinding(editingDateSignatureCotutelle, B_editingDateSignatureCotutelle);
	}

	public NSTimestamp editingDateDebutCotutelle() {
		return (NSTimestamp) valueForBinding(B_editingDateDebutCotutelle);
	}

	public void setEditingDateDebutCotutelle(NSTimestamp editingDateDebutCotutelle) {
		setValueForBinding(editingDateDebutCotutelle, B_editingDateDebutCotutelle);
	}

	public NSTimestamp editingDateFinCotutelle() {
		return (NSTimestamp) valueForBinding(B_editingDateFinCotutelle);
	}

	public void setEditingDateFinCotutelle(NSTimestamp editingDateFinCotutelle) {
		setValueForBinding(editingDateFinCotutelle, B_editingDateFinCotutelle);
	}

	public String editingCommentaireCotutelle() {
		return (String) valueForBinding(B_editingCommentaireCotutelle);
	}

	public void setEditingCommentaireCotutelle(String editingCommentaireCotutelle) {
		setValueForBinding(editingCommentaireCotutelle, B_editingCommentaireCotutelle);
	}

	public Boolean isCotutelleModif() {
		return (Boolean) valueForBinding(B_isCotutelleModif);
	}

	public EOStructure selectedEtablissementCotutelle() {
		return (EOStructure) valueForBinding(B_selectedEtablissementCotutelle);
	}

	public void setEditingPays(EOPays editingPays) {
		if (editingPays == null && editingEtablissementCotutelle() != null && editingEtablissementCotutelle().getAdressePrincipale() != null) {
			setValueForBinding(editingEtablissementCotutelle().getAdressePrincipale().toPays(), B_editingPays);
		} else {
			setValueForBinding(editingPays, B_editingPays);
		}
	}

	public EOPays editingPays() {
		return (EOPays) valueForBinding(B_editingPays);
	}
	
	public NSArray<EOStructure> lesEtablissements() {
		NSArray<EOStructure> groupes = EOStructure.fetchAll(edc(), qualifierEtablissementEtranger());
		EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).in(groupes);
		
		if (editingPays() != null) {
			qualifier = ERXQ.and(qualifier,
					EOStructure.TO_REPART_PERSONNE_ADRESSES.dot(EORepartPersonneAdresse.TO_ADRESSE.dot(EOAdresse.C_PAYS_KEY)).eq(editingPays().cPays())
					);
		}
		return EOStructure.fetchAll(edc(), qualifier);
	}
}