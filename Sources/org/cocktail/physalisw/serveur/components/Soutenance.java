package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOTheseMention;
import org.cocktail.fwkcktlrecherche.server.metier.service.DoctorantTheseService;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class Soutenance extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 5282015794967911329L;
	private static final String BINDING_LA_THESE = "laThese";

	private EODoctorantThese editingThese = null;
	private Boolean isEditionMembreJury = false;
	private Boolean isEditionRapporteur = false;
	private Boolean isEditionResultats = false;

	private EOTheseMention itemMention;
	private EOTheseMention selectedMention = null;
	private NSArray<EOTheseMention> lesMentions = null;
	private Boolean isRechargerJury = false;
	private Boolean isRechargerRapporteur = false;
	
	private NSTimestamp ancienneDateSoutenance;

	public Soutenance(WOContext context) {
		super(context);
	}

	public NSArray<EOTheseMention> listeMention() {
		if (lesMentions == null) {
			lesMentions = EOTheseMention.fetchAllTheseMentions(edc());
		}
		return lesMentions;
	}

	public WOActionResults modifResultats() {
		setIsEditionResultats(true);
		setAncienneDateSoutenance(laThese().dateSoutenance());
		setEditingThese(laThese());
		setSelectedMention(editingThese.toTheseMention());
		return null;
	}

	public WOActionResults validerResultats() {
		try {
			editingThese().setToTheseMentionRelationship(selectedMention);
			
			if (editingThese().dateSoutenance() != null && !editingThese().dateSoutenance().equals(getAncienneDateSoutenance())) {
		    	DoctorantTheseService dts = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
		    	if (dts.modifierDatesLorsDUneSoutenance(editingThese(), editingThese().dateSoutenance())) {
		    		session().addSimpleInfoMessage("Remarque", "Les dates de fin ont été changées.");
		    	} else {
		    		session().addSimpleInfoMessage("Remarque", "Les dates de fin n'ont pas été changées.");
		    	}
		    }
			
			edc().saveChanges();

		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			e.printStackTrace();
		}
		setIsEditionResultats(false);
		return null;
	}

	public WOActionResults annulerResultats() {
		setIsEditionResultats(false);
		return null;
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(BINDING_LA_THESE);
	}

	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, BINDING_LA_THESE);
	}

	public void setIsEditionMembreJury(Boolean isEditionMembreJury) {
		this.isEditionMembreJury = isEditionMembreJury;
	}

	public Boolean isEditionMembreJury() {
		return isEditionMembreJury;
	}

	public void setIsEditionRapporteur(Boolean isEditionRapporteur) {
		this.isEditionRapporteur = isEditionRapporteur;
	}

	public Boolean isEditionRapporteur() {
		return isEditionRapporteur;
	}

	public String aucSoutenanceId() {
		return getComponentId() + "aucSoutenanceId";
	}

	public String aucResultatsId() {
		return getComponentId() + "aucResultatsId";
	}

	public String aucRapporteurFormId() {
		return getComponentId() + "aucRapporteurFormId";
	}

	public void setItemMention(EOTheseMention itemMention) {
		this.itemMention = itemMention;
	}

	public EOTheseMention itemMention() {
		return itemMention;
	}

	public void setSelectedMention(EOTheseMention selectedMention) {
		this.selectedMention = selectedMention;
	}

	public EOTheseMention selectedMention() {
		return selectedMention;
	}

	public void setIsEditionResultats(Boolean isEditionResultats) {
		this.isEditionResultats = isEditionResultats;
		disableOnglets(isEditionResultats, false);
	}

	public Boolean isEditionResultats() {
		return isEditionResultats;
	}

	public void setEditingThese(EODoctorantThese editingThese) {
		this.editingThese = editingThese;
	}

	public EODoctorantThese editingThese() {
		return editingThese;
	}

	public void setIsRechargerJury(Boolean isRechargerJury) {
		this.isRechargerJury = isRechargerJury;
		if (isRechargerJury && !isRechargerRapporteur) {
			setIsRechargerRapporteur(true);
		}
	}

	public Boolean isRechargerJury() {
		return isRechargerJury;
	}

	public void setIsRechargerRapporteur(Boolean isRechargerRapporteur) {
		this.isRechargerRapporteur = isRechargerRapporteur;
		if (isRechargerRapporteur && !isRechargerJury) {
			setIsRechargerJury(true);
		}
	}

	public Boolean isRechargerRapporteur() {
		return isRechargerRapporteur;
	}

	public NSTimestamp getAncienneDateSoutenance() {
		return ancienneDateSoutenance;
	}

	public void setAncienneDateSoutenance(NSTimestamp ancienneDateSoutenance) {
		this.ancienneDateSoutenance = ancienneDateSoutenance;
	}
	
}