package org.cocktail.physalisw.serveur.components;

import java.math.BigDecimal;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement;
import org.cocktail.fwkcktlrecherche.server.metier.service.DoctorantTheseService;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class Financement extends PhysalisWOComponent {

	private static final long serialVersionUID = -3170575716427220493L;
	private static final String B_laThese = "laThese";
	private static final String B_avenantThese = "avenantThese";
	private static final String BINDING_IS_MODIFICATION_BOTTOM = "isModificationBottom";
	private Boolean isEditionFinancement = false;
	private EODoctorantFinancement selectedFinancement = null;
	private EODoctorantFinanceur currentFinanceur = null;
	private Integer indexFinanceur = 0;
	private Avenant avenantThese = null;
	private EOTypeFinancement editingTypeFinancement = null;
	private EOTheseTypeContrat editingTypeContrat = null;
	private EOTypeContratDoctoral editingTypeContratDoctoral = null;
	private EOStructure editingEmployeur = null;
	private NSTimestamp editingDateDebut = null;
	private NSTimestamp editingDateFin = null;
	private String editingCommentaires = null;
	private Boolean isSansFinancement = true;
	private Boolean isAvecFinancement = false;
	private Boolean isEditionFinanceur = false;
	private Boolean isAjoutFinanceur = false;
	private EODoctorantFinanceur editingFinanceur = null;
	private EOStructure editingStructureFinanceur = null;
	private NSTimestamp editingDateDebutFinanceur = null;
	private NSTimestamp editingDateFinFinanceur = null;
	private BigDecimal editingQuotite = null;
	private NSMutableArray<Boolean> arrayIsModifFinanceur = new NSMutableArray<Boolean>();
	private Boolean doitChargerTableauIsAffichageFinanceur = true;
	private Boolean wantReset = true;
	private DoctorantTheseService doctorantTheseService;

	public Financement(WOContext context) {
		super(context);
	}

	public WOActionResults modifierFinancement() {
		setIsEditionFinancement(true);
		wantReset = true;
		editingTypeFinancement = selectedFinancement.toTypeFinancement();
		editingTypeContrat = selectedFinancement.toTheseTypeContrat();
		editingTypeContratDoctoral = selectedFinancement.toTypeContratDoctoral();
		editingCommentaires = selectedFinancement.commentaireSansFinancement();

		if (selectedFinancement.isFinancement()) {
			if (selectedFinancement.toContratPartenaire() != null) {
				editingEmployeur = (EOStructure) selectedFinancement.toContratPartenaire().partenaire();
			}
			if (selectedFinancement.toRepartAssociation() != null) {
				editingDateDebut = selectedFinancement.toRepartAssociation().rasDOuverture();
				editingDateFin = selectedFinancement.toRepartAssociation().rasDFermeture();
			} else {
				editingDateDebut = selectedFinancement.dateDebutFinancement();
				editingDateFin = selectedFinancement.dateFinFinancement();
			}
			isAvecFinancement = true;
			isSansFinancement = false;
		} else {
			editingEmployeur = null;
			editingDateDebut = avenantThese().avtDateDeb();
			editingDateFin = avenantThese().avtDateFin();
			isAvecFinancement = false;
			isSansFinancement = true;
		}

		return doErxRedirect(context());
	}

	public WOActionResults validerFinancement() {
		ContratPartenaire cp = null;
		FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
		ContratPartenaire ancienCp = selectedFinancement().toContratPartenaire();
		Integer nbErreurs = 0;

		if (editingTypeFinancement == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez choisir un type de financement.");
			nbErreurs++;
		} else {
			if (editingTypeFinancement.typeFinancement().equals("O")) {
				if (editingTypeContrat == null) {
					session.addSimpleErrorMessage("Erreur", "Vous devez choisir un type de contrat.");
					nbErreurs++;
				}
				if (editingDateDebut == null || editingDateFin == null) {
					session.addSimpleErrorMessage("Erreur", "Vous devez saisir les dates de début et de fin de financement.");
					nbErreurs++;
				} else {
					if (editingDateDebut.before(avenantThese.avtDateDeb()) || editingDateFin.after(avenantThese.avtDateFin())) {
						session.addSimpleErrorMessage("Erreur", "Les dates de début et de fin de financement doivent être comprises dans l'intervale de l'avenant (du "
								+ avenantThese.avtDateDeb().toString() + " au " + avenantThese.avtDateFin().toString() + ")");
						nbErreurs++;
					}

				}
			} else {
				if (!listeFinanceurs().isEmpty()) {
					session.addSimpleErrorMessage("Erreur", "Des financeurs éxistent, vous devez les supprimer avant de pouvoir indiquer que la thèse n'est pas financée");
					nbErreurs++;
				}

			}
		}

		if (nbErreurs > 0) {
			return null;
		}

		try {
			if (editingTypeFinancement.typeFinancement().equals("N")) {
				editingEmployeur = null;
				editingDateDebut = null;
				editingDateFin = null;
				editingTypeContrat = null;
				editingTypeContratDoctoral = null;
			} else {
				editingCommentaires = null;
			}
			// ----
			// si il y a deja un partenaire et qu'il est différent du nouveau on
			// le supprime
			if (ancienCp != null) {
				if (!ancienCp.partenaire().equals(editingEmployeur)) {
					NSArray<EOAssociation> tousLesRoles = ancienCp.rolesPartenaireDansContrat(ancienCp.partenaire(), laThese().toContrat());
					// fc.supprimerLeRole(ancienCp,
					// factoryAssociation().employeurThese(edc()));
					// on supprime le repart_association lié au financement
					edc().deleteObject(selectedFinancement.toRepartAssociation());
					// il existe forcement le role d'employeur si + d'un role on
					// ne supprimer pas le partenaire
					if (tousLesRoles == null || tousLesRoles.count() == 1) {
						fc.supprimerContratPartenaire(ancienCp, getUtilisateurPersId());
					}

				} else if (editingEmployeur == null) { // on supprime l'employeur
					NSArray<EOAssociation> tousLesRoles = ancienCp.rolesPartenaireDansContrat(ancienCp.partenaire(), laThese().toContrat());
					// on supprime le repart_association lié au financement
					edc().deleteObject(selectedFinancement.toRepartAssociation());
					// il existe forcement le role d'employeur si + d'un role on
					// ne supprimer pas le partenaire
					if (tousLesRoles == null || tousLesRoles.count() == 1) {
						fc.supprimerContratPartenaire(ancienCp, getUtilisateurPersId());
					}
				}
			}

			if (editingEmployeur != null) {
				if (ancienCp == null) {
					cp = creerEmployeur(editingEmployeur);
				} else {
					if (!ancienCp.partenaire().equals(editingEmployeur)) {
						cp = creerEmployeur(editingEmployeur);
					}
				}
				
				selectedFinancement.setDateDebutFinancement(null);
				selectedFinancement.setDateFinFinancement(null);
				
				selectedFinancement.toRepartAssociation().setRasDFermeture(editingDateFin());
				selectedFinancement.toRepartAssociation().setRasDOuverture(editingDateDebut());

			} else {
				selectedFinancement.setToContratPartenaireRelationship(null);
				selectedFinancement.setToRepartAssociationRelationship(null);

				selectedFinancement.setDateDebutFinancement(editingDateDebut());
				selectedFinancement.setDateFinFinancement(editingDateFin());
			}
			
			if (editingDateDebut() != null && editingDateFin() != null) {
				if (editingDateDebut().after(avenantThese().avtDateDeb()) || editingDateFin().before(avenantThese().avtDateFin())) {
					session().addSimpleInfoMessage("Attention", "Les dates de financement ne couvrent pas toute la thèse.");
				}
			}
			
			selectedFinancement.setToTypeFinancementRelationship(editingTypeFinancement);
			selectedFinancement.setToTheseTypeContratRelationship(editingTypeContrat);
			selectedFinancement.setToTypeContratDoctoralRelationship(editingTypeContratDoctoral);
			selectedFinancement.setCommentaireSansFinancement(editingCommentaires);
			edc().saveChanges();
			session().addSimpleSuccessMessage("Modification", "Le financement est enregistré.");
			setIsEditionFinancement(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			e.printStackTrace();
		}
		return null;
		// --
	}

	private ContratPartenaire creerEmployeur(EOStructure employeur) throws ExceptionFinder, ExceptionUtilisateur, Exception {
		FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
		FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
		ContratPartenaire cp;
		NSArray<EORepartAssociation> lesRepartAssociations = null;

		cp_exist.setContrat(laThese().toContrat());
		cp_exist.setPartenaire(employeur);
		cp = (ContratPartenaire) cp_exist.find().lastObject();

		// si le contrat partenaire n'existe pas, on le crée
		// sinon on ajoute le role au partenaire
		if (cp == null) {
			cp = fc.creerContratPartenaire(laThese().toContrat(), editingEmployeur(), new NSArray<EOAssociation>(factoryAssociation().employeurThese(edc())), Boolean.FALSE, null);
		} else {
			fc.ajouterLeRolePourLesDates(cp, factoryAssociation().employeurThese(edc()), editingDateDebut, editingDateFin);
		}
		selectedFinancement.setToContratPartenaireRelationship(cp);
		// oncherche le repart_association ajouté pour le lié au financement
		EOQualifier qualRole = ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().employeurThese(edc()));
		lesRepartAssociations = cp.partenaire().getRepartAssociationsInGroupe(laThese().toContrat().groupePartenaire(), qualRole);
		selectedFinancement().setToRepartAssociationRelationship(lesRepartAssociations.lastObject());
		return cp;
	}

	public WOActionResults annulerFinancement() {
		try {
			edc().revert();
			setIsEditionFinancement(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public NSArray<EODoctorantFinanceur> listeFinanceurs() {
		EOQualifier qual = ERXQ.equals(EODoctorantFinanceur.TO_DOCTORANT_FINANCEMENT_KEY, selectedFinancement());
		// EOSortOrdering sort = new
		// EOSortOrdering(EODoctorantFinanceur.TO_CONTRAT_PARTENAIRE_KEY + "." +
		// ContratPartenaire.PARTENAIRE_KEY + "." +
		// IPersonne.NOM_PRENOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);
		NSArray<EODoctorantFinanceur> lesFinanceurs = EODoctorantFinanceur.fetchDoctorantFinanceurs(edc(), qual, null);

		if (doitChargerTableauIsAffichageFinanceur) {
			Integer indice = 0;
			arrayIsModifFinanceur.removeAllObjects();
			for (EODoctorantFinanceur eoDoctorantFinanceur : lesFinanceurs) {
				arrayIsModifFinanceur.add(indice, Boolean.TRUE);
				indice++;
			}
			doitChargerTableauIsAffichageFinanceur = false;
		}
		return lesFinanceurs;
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, B_laThese);
	}

	public Avenant avenantThese() {
		Avenant lAvenant = null;
		if (hasBinding(B_avenantThese)) {
			lAvenant = (Avenant) valueForBinding(B_avenantThese);
		}
		if (lAvenant != avenantThese) {
			avenantThese = lAvenant;
			doitChargerTableauIsAffichageFinanceur = true;
			EOQualifier qual = ERXQ.is(EODoctorantFinancement.TO_AVENANT_KEY, avenantThese);
			NSArray<EODoctorantFinancement> financements = EODoctorantFinancement.fetchDoctorantFinancements(edc(), qual, null);
			if (!financements.isEmpty()) {
				selectedFinancement = financements.get(0);
			} else {
				selectedFinancement = null;
			}
		}
		return lAvenant;
	}

	public WOActionResults ajouterFinanceur() {
		isAjoutFinanceur = true;
		setIsEditionFinanceur(true);
		editingFinanceur = null;
		editingStructureFinanceur = null;
		if (selectedFinancement.toRepartAssociation() != null) {
			editingDateDebutFinanceur = selectedFinancement.toRepartAssociation().rasDOuverture();
			editingDateFinFinanceur = selectedFinancement.toRepartAssociation().rasDFermeture();
		} else {
			editingDateDebutFinanceur = selectedFinancement.dateDebutFinancement();
			editingDateFinFinanceur = selectedFinancement.dateFinFinancement();
		}
		editingQuotite = new BigDecimal(100.00);
		return null;
	}

	public WOActionResults modifierFinanceur() {
		arrayIsModifFinanceur.set(indexFinanceur, Boolean.FALSE);
		setIsEditionFinanceur(true);
		return null;
	}

	public Boolean verifierFinanceur() {
		Integer nbErreur = 0;

		if (editingDateDebutFinanceur == null || editingDateFinFinanceur == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez saisir les dates de début et de fin");
			nbErreur++;
		} else {
			if (selectedFinancement.toRepartAssociation() != null) {
				if (editingDateDebutFinanceur.before(selectedFinancement.toRepartAssociation().rasDOuverture())
						|| editingDateFinFinanceur.after(selectedFinancement.toRepartAssociation().rasDFermeture())) {
					session.addSimpleErrorMessage("Erreur", "Les dates de début et de fin doivent être comprise dans l'intervale du financement " + "(du "
							+ selectedFinancement.toRepartAssociation().rasDOuverture().toString() + " au " + selectedFinancement.toRepartAssociation().rasDFermeture().toString()
							+ ")");
					nbErreur++;
				}
			} else {
				if (editingDateDebutFinanceur.before(selectedFinancement.dateDebutFinancement())
						|| editingDateFinFinanceur.after(selectedFinancement.dateFinFinancement())) {
					session.addSimpleErrorMessage("Erreur", "Les dates de début et de fin doivent être comprise dans l'intervale du financement " + "(du "
							+ selectedFinancement.dateDebutFinancement().toString() + " au " + selectedFinancement.dateFinFinancement().toString()
							+ ")");
					nbErreur++;
				}
			}

		}

		if (editingQuotite == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez saisir une quotité");
			nbErreur++;
		} else {

			if (editingQuotite.compareTo(new BigDecimal(100.00)) > 0 || editingQuotite.compareTo(new BigDecimal(0.01)) < 0) {
				session().addSimpleErrorMessage("Erreur", "La quotité doit être comprise entre 0 et 100");
				nbErreur++;
			}

			// on cherche la somme des quotité pour la période et on ajoute la
			// quotite saisie, le total ne doit pas dépasser 100
			// NSArray<EORepartAssociation> lesRepartAssociations =
			// laThese().toContrat().repartAssociationPartenairesForAssociationsAuxDates(new
			// NSArray<EOAssociation>(factoryAssociation().financeurThese(edc())),
			// editingDateDebutFinanceur, editingDateFinFinanceur);
			NSArray<EORepartAssociation> lesRepartAssociations = (NSArray<EORepartAssociation>) selectedFinancement.toFinanceur().valueForKey(
					EODoctorantFinanceur.TO_REPART_ASSOCIATION_KEY);
			BigDecimal totalQuotite = new BigDecimal(0);
			for (EORepartAssociation leRA : lesRepartAssociations) {
				if (currentFinanceur == null || leRA != currentFinanceur.toRepartAssociation()) {
					if (((leRA.rasDFermeture().compareTo(editingDateDebutFinanceur) >= 0) || (leRA.rasDOuverture().compareTo(editingDateDebutFinanceur) <= 0))
							|| ((leRA.rasDFermeture().compareTo(editingDateFinFinanceur) >= 0) || (leRA.rasDOuverture().compareTo(editingDateFinFinanceur) <= 0))) {
						totalQuotite = totalQuotite.add(leRA.rasQuotite());
					}
				}
			}
			totalQuotite = totalQuotite.add(editingQuotite);
			if (totalQuotite.compareTo(new BigDecimal(100)) > 0) {
				session().addSimpleErrorMessage("Erreur", "La somme des quotités dépasse 100% à cette période (" + totalQuotite.toString() + ")");
				nbErreur++;
			}
		}

		if (editingStructureFinanceur == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez choisir un financeur");
			nbErreur++;
		}

		if (nbErreur > 0) {
			return false;
		} else {
			return true;
		}
	}

	public WOActionResults validerModifFinanceur() {
		try {
			editingDateDebutFinanceur = currentFinanceur.toRepartAssociation().rasDOuverture();
			editingDateFinFinanceur = currentFinanceur.toRepartAssociation().rasDFermeture();
			editingQuotite = currentFinanceur.toRepartAssociation().rasQuotite();
			editingStructureFinanceur = (EOStructure) currentFinanceur.toContratPartenaire().partenaire();

			if (!verifierFinanceur()) {
				return null;
			}

			edc().saveChanges();
			arrayIsModifFinanceur.set(indexFinanceur, Boolean.TRUE);
			session().addSimpleSuccessMessage("Confirmation", "Les modifications ont été enregistrées");
			setIsEditionFinanceur(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults annulerModifFinanceur() {
		try {
			edc().revert();
			arrayIsModifFinanceur.set(indexFinanceur, Boolean.TRUE);
			setIsEditionFinanceur(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults supprimerFinanceur() {
		try {
			getDoctorantTheseService().supprimerFinanceur(laThese(), currentFinanceur());
			edc().saveChanges();
			session().addSimpleSuccessMessage("Suppression", "Le financeur a été supprimé");
			doitChargerTableauIsAffichageFinanceur = true;
			AjaxUpdateContainer.updateContainerWithID(aucFinanceurId(), context());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}
		return doNothing();
	}

	public WOActionResults validerFinanceur() {
		ContratPartenaire cp = null;
		FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
		FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
		if (!verifierFinanceur()) {
			return null;
		}

		try {
			NSArray<EORepartAssociation> lesRepartAssociations = null;

			cp_exist.setContrat(laThese().toContrat());
			cp_exist.setPartenaire(editingStructureFinanceur);
			cp = (ContratPartenaire) cp_exist.find().lastObject();

			// si le contrat partenaire n'existe pas, on le crée
			// sinon on ajoute le role au partenaire
			if (cp == null) {
				cp = fc.creerContratPartenaire(laThese().toContrat(), editingStructureFinanceur(), new NSArray<EOAssociation>(factoryAssociation().financeurThese(edc())),
						Boolean.FALSE, null);
			} else {
				fc.ajouterLesRolesPourLesDatesEtQuotite(cp, new NSArray<EOAssociation>(factoryAssociation().financeurThese(edc())), editingDateDebutFinanceur,
						editingDateFinFinanceur, editingQuotite);
			}
			// oncherche le repart_association ajouté pour le lié au financement
			EOQualifier qualRole = ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().financeurThese(edc()));
			lesRepartAssociations = cp.partenaire().getRepartAssociationsInGroupe(laThese().toContrat().groupePartenaire(), qualRole);
			lesRepartAssociations.lastObject().setRasQuotite(editingQuotite);
			editingFinanceur = EODoctorantFinanceur.createDoctorantFinanceur(edc(), cp, selectedFinancement, lesRepartAssociations.lastObject());
			edc().saveChanges();
			session().addSimpleSuccessMessage("Ajout", "le financeur est enregistré");
			setIsEditionFinanceur(false);
			isAjoutFinanceur = false;
			doitChargerTableauIsAffichageFinanceur = true;
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults annulerFinanceur() {
		try {
			edc().revert();
			setIsEditionFinanceur(false);
			isAjoutFinanceur = false;
		} catch (Exception e) {
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public void setAvenantThese(Avenant avenantThese) {
		setValueForBinding(avenantThese, B_avenantThese);
	}

	public String onValider() {
		if (isAjoutFinanceur) {
			return "validerFinanceur";
		} else {
			return "validerModifFinanceur";
		}
	}

	public String onAnnuler() {
		if (isAjoutFinanceur) {
			return "annulerFinanceur";
		} else {
			return "annulerModifFinanceur";
		}
	}

	/**
	 * @return the aucFinancementId
	 */
	public String aucFinancementId() {
		return getComponentId() + "aucFinancementId";
	}

	public String aucFinanceurId() {
		return getComponentId() + "aucFinanceurId";
	}

	public String aucFinanceurDetailId() {
		return "aucFinanceurDetailId" + indexFinanceur.toString();
	}

	public String formFinanceurDetailId() {
		return "formFinanceurDetailId" + indexFinanceur.toString();
	}

	public void setIsEditionFinancement(Boolean isEditionFinancement) {
		this.isEditionFinancement = isEditionFinancement;
		disableOnglets(isEditionFinancement, false);
	}

	public Boolean isEditionFinancement() {
		return isEditionFinancement;
	}

	public void setSelectedFinancement(EODoctorantFinancement selectedFinancement) {
		this.selectedFinancement = selectedFinancement;
	}

	public EODoctorantFinancement selectedFinancement() {
		return selectedFinancement;
	}

	public void setCurrentFinanceur(EODoctorantFinanceur currentFinanceur) {
		this.currentFinanceur = currentFinanceur;
	}

	public EODoctorantFinanceur currentFinanceur() {
		return currentFinanceur;
	}

	public void setEditingTypeFinancement(EOTypeFinancement editingTypeFinancement) {
		this.editingTypeFinancement = editingTypeFinancement;
	}

	public EOTypeFinancement editingTypeFinancement() {
		return editingTypeFinancement;
	}

	public void setEditingTypeContrat(EOTheseTypeContrat editingTypeContrat) {
		this.editingTypeContrat = editingTypeContrat;
	}

	public EOTheseTypeContrat editingTypeContrat() {
		return editingTypeContrat;
	}

	public void setEditingTypeContratDoctoral(EOTypeContratDoctoral editingTypeContratDoctoral) {
		this.editingTypeContratDoctoral = editingTypeContratDoctoral;
	}

	public EOTypeContratDoctoral editingTypeContratDoctoral() {
		return editingTypeContratDoctoral;
	}

	public void setEditingEmployeur(EOStructure editingEmployeur) {
		this.editingEmployeur = editingEmployeur;
	}

	public EOStructure editingEmployeur() {
		return editingEmployeur;
	}

	public void setEditingDateDebut(NSTimestamp editingDateDebut) {
		this.editingDateDebut = editingDateDebut;
	}

	public NSTimestamp editingDateDebut() {
		return editingDateDebut;
	}

	public void setEditingDateFin(NSTimestamp editingDateFin) {
		this.editingDateFin = editingDateFin;
	}

	public NSTimestamp editingDateFin() {
		return editingDateFin;
	}

	public void setEditingCommentaires(String editingCommentaires) {
		this.editingCommentaires = editingCommentaires;
	}

	public String editingCommentaires() {
		return editingCommentaires;
	}

	public void setIsSansFinancement(Boolean isSansFinancement) {
		this.isSansFinancement = isSansFinancement;
	}

	public Boolean isSansFinancement() {
		return isSansFinancement;
	}

	public void setIsAvecFinancement(Boolean isAvecFinancement) {
		this.isAvecFinancement = isAvecFinancement;
	}

	public Boolean isAvecFinancement() {
		return isAvecFinancement;
	}

	public void setIsEditionFinanceur(Boolean isEditionFinanceur) {
		this.isEditionFinanceur = isEditionFinanceur;
		disableOnglets(isEditionFinanceur, false);
	}

	public Boolean isEditionFinanceur() {
		return isEditionFinanceur;
	}

	public void setEditingFinanceur(EODoctorantFinanceur editingFinanceur) {
		this.editingFinanceur = editingFinanceur;
	}

	public EODoctorantFinanceur editingFinanceur() {
		return editingFinanceur;
	}

	public void setEditingDateDebutFinanceur(NSTimestamp editingDateDebutFinanceur) {
		this.editingDateDebutFinanceur = editingDateDebutFinanceur;
	}

	public NSTimestamp editingDateDebutFinanceur() {
		return editingDateDebutFinanceur;
	}

	public void setEditingDateFinFinanceur(NSTimestamp editingDateFinFinanceur) {
		this.editingDateFinFinanceur = editingDateFinFinanceur;
	}

	public NSTimestamp editingDateFinFinanceur() {
		return editingDateFinFinanceur;
	}

	public void setEditingQuotite(BigDecimal editingQuotite) {
		this.editingQuotite = editingQuotite;
	}

	public BigDecimal editingQuotite() {
		return editingQuotite;
	}

	public void setEditingStructureFinanceur(EOStructure editingStructureFinanceur) {
		this.editingStructureFinanceur = editingStructureFinanceur;
	}

	public EOStructure editingStructureFinanceur() {
		return editingStructureFinanceur;
	}

	public Boolean isAffichageDetailFinanceur() {
		return (Boolean) arrayIsModifFinanceur.get(indexFinanceur);
	}

	public void setIndexFinanceur(Integer indexFinanceur) {
		this.indexFinanceur = indexFinanceur;
	}

	public Integer indexFinanceur() {
		return indexFinanceur;
	}

	public void setIsAjoutFinanceur(Boolean isAjoutFinanceur) {
		this.isAjoutFinanceur = isAjoutFinanceur;
	}

	public Boolean isAjoutFinanceur() {
		return isAjoutFinanceur;
	}

	public void setWantReset(Boolean wantReset) {
		this.wantReset = wantReset;
	}

	public Boolean wantReset() {
		return wantReset;
	}
	
	public NSTimestamp dateDebutFinancement() {
		if (selectedFinancement.toRepartAssociation() != null) {
			return selectedFinancement.toRepartAssociation().rasDOuverture();
		} else {
			return selectedFinancement.dateDebutFinancement();
		}
	}
	
	public NSTimestamp dateFinFinancement() {
		if (selectedFinancement.toRepartAssociation() != null) {
			return selectedFinancement.toRepartAssociation().rasDFermeture();
		} else {
			return selectedFinancement.dateFinFinancement();
		}
	}
	
	public Boolean isTypeContratDoctoral() {
		if (selectedFinancement == null || selectedFinancement.toTheseTypeContrat() == null || selectedFinancement.toTheseTypeContrat().libTypeContrat() == null) {
			return false;
		}
		return EOTheseTypeContrat.LIBELLE_TYPE_CONTRAT_DOCTORAL.equals(selectedFinancement.toTheseTypeContrat().libTypeContrat());
	}


	public DoctorantTheseService getDoctorantTheseService() {
		if (doctorantTheseService == null) {
			doctorantTheseService = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
		}
		return doctorantTheseService;
	}

	public void setDoctorantTheseService(DoctorantTheseService doctorantTheseService) {
		this.doctorantTheseService = doctorantTheseService;
	}
	
}