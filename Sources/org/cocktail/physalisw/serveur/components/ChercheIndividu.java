package org.cocktail.physalisw.serveur.components;

import java.util.Calendar;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;
import org.cocktail.scolaritefwk.serveur.finder.FinderScolFormationAnnee;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.appserver.ERXRedirect;

public class ChercheIndividu extends PhysalisWOComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4302683698472887785L;
	private EOIndividu selectedPersonne = null;
	private EOEditingContext edcDoctorant = null;
	private Boolean isSelectionDisable = true;
	public ChercheIndividu(WOContext context) {
		super(context);
	}

	public WOActionResults validerPersonne() {
		GestionDoctorant gd = null;
		if (selectedPersonne == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez sélectionner une personne !");
		} else {
			// EOQualifier qual = new
			// EOKeyValueQualifier(EODoctorant.TO_INDIVIDU_FWKPERS_KEY,
			// EOQualifier.QualifierOperatorEqual, selectedPersonne());
			// NSArray<EODoctorant> objects =
			// EODoctorant.fetchDoctorants(edcDoctorant(), qual, null);
			EODoctorant leDoctorant = EODoctorant.fetchDoctorant(edcDoctorant(), EODoctorant.TO_INDIVIDU_FWKPERS_KEY, selectedPersonne());
			// if (objects.count() > 0) {
			if (leDoctorant != null) {

				session.addSimpleInfoMessage("Attention", "Cette personne est déjà doctorante, Vous ne pouvez pas la recréer");
				// gd.setLeDoctorant(objects.lastObject());
				if (leDoctorant.lienDoctorantEtudiant()) {
					try {
						edcDoctorant().saveChanges();
						session.addSimpleInfoMessage("Information", "Ce doctorant est maintenant inscrit administrativement dans la scolarité");
					} catch (Exception e) {
						session.addSimpleErrorMessage("Erreur", e.getMessage());
						e.printStackTrace();
					}

				}
				gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
				gd.setLeDoctorant(leDoctorant);
				gd.setEdcDoctorant(edcDoctorant());
			} else {
				Calendar calendar = Calendar.getInstance();
				Integer anneeInscThese = calendar.get(Calendar.YEAR); //FinderScolFormationAnnee.getScolFormationAnneeCourante(edcDoctorant()).fannKey();
				EODoctorant doctorant = EODoctorant.createDoctorant(edcDoctorant(), anneeInscThese, selectedPersonne);
				if (doctorant.lienDoctorantEtudiant()) {
					session.addSimpleInfoMessage("Information", "Ce doctorant possède un dossier d'étudiant dans la scolarité");
				}
				try {
					edcDoctorant().saveChanges();
					session.addSimpleSuccessMessage("Confirmation", "Cette personne est maintenant doctorante. Compléter les détails de sa thèse");
					gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
					gd.setLeDoctorant(doctorant);
					gd.setEdcDoctorant(edcDoctorant());
				} catch (Exception e) {
					session.addSimpleErrorMessage("Erreur", e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return gd;
	}

	public EOEditingContext edcDoctorant() {
		if (edcDoctorant == null) {
			edcDoctorant = edc();
		}
		return edcDoctorant;
	}

	public WOActionResults onCreerPersonne() {
		GestionDoctorant gd = null;
		if (selectedPersonne == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez sélectionner une personne !");
		} else {
			try {
				String nomUpperCase = selectedPersonne.nomUsuel().toUpperCase();
				String prenomUpperCase = selectedPersonne.prenomAffichage().toUpperCase();
				
				selectedPersonne.setNomUsuel(nomUpperCase);
				selectedPersonne.setNomPatronymique(nomUpperCase);
				selectedPersonne.setPrenomAffichage(prenomUpperCase);
				
				selectedPersonne.setToCiviliteRelationship(EOCivilite.fetchAll(edcDoctorant()).lastObject());
				Calendar calendar = Calendar.getInstance();
				Integer anneeInscThese = calendar.get(Calendar.YEAR); //FinderScolFormationAnnee.getScolFormationAnneeCourante(edcDoctorant()).fannKey();
				EODoctorant doctorant = EODoctorant.createDoctorant(edcDoctorant(), anneeInscThese, selectedPersonne);
				edcDoctorant().saveChanges();
				session.addSimpleInfoMessage("Information", "Vous devez compléter l'état civil et les coordonnées de cette personne afin de l'enregistrer comme doctorant");
				gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
				gd.setLeDoctorant(doctorant);
				gd.setEdcDoctorant(edcDoctorant());
				//gd.seted
				ERXRedirect redirection = new ERXRedirect(context());
				redirection.setComponent(gd);
				return redirection;
			} catch (Exception e) {
				session.addSimpleErrorMessage("Erreur", e.getMessage());
				e.printStackTrace();
			}

		}
		return null;
	}

	public void onModifierPersonne() {
	}

	public String aucPersonneSelectId() {
		return getComponentId() + "aucPersonneSelect";
	}

	public void setSelectedPersonne(EOIndividu selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
	}

	public EOIndividu selectedPersonne() {
		return selectedPersonne;
	}
	public Boolean isSelectionDisable() {
		return isSelectionDisable;
	}
	public void setIsSelectionDisable(Boolean isDisable) {
		isSelectionDisable = isDisable;
	}
}