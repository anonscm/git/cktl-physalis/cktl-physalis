package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class CotutelleEncadrantForm extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2661473330111670513L;

	private NSArray<EOCivilite> civilites = null;
	private EOCivilite uneCivilite;
	private static final String B_coEncadrant = "editingCoEncadrant";
	private static final String B_isEditing = "isEditing";
	private static final String B_isBoutonValiderDisable = "isBoutonValiderEnable";
	private Boolean isEditingAdresse = false;
	private Boolean isEditing;
	private Boolean isRechercheIndividu = false;

	public CotutelleEncadrantForm(WOContext context) {
		super(context);
	}

	public String aucCotutelleEncadrantId() {
		return getComponentId() + "_aucCotutelleEncadrant";
	}

	/**
	 * @return the civilites
	 */

	public NSArray<EOCivilite> civilites() {
		if (civilites == null) {
			EOSortOrdering sexeOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.SEXE_KEY, EOSortOrdering.CompareAscending);
			EOSortOrdering libelleLongOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.L_CIVILITE_KEY, EOSortOrdering.CompareAscending);
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] { sexeOrdering, libelleLongOrdering });
			civilites = EOCivilite.fetchAll(edc(), sortOrderings);
		}
		return civilites;
	}

	public WOActionResults onCreerPersonne() {
		setIsRechercheIndividu(false);
		editingCoEncadrant().setToCiviliteRelationship(civilites().get(0));
		return null;
	}

	public WOActionResults onModifierPersonne() {
		setIsRechercheIndividu(false);
		return null;
	}

	public WOActionResults onSelectionnerPersonne() {
		AjaxUpdateContainer.updateContainerWithID("AUCBoutonSelectionCoencadrant", context());
		setIsBoutonValiderDisable(true);
		return null;
	}

	public WOActionResults selectionnerCoEncadrant() {
		setIsRechercheIndividu(false);
		return null;
	}

	public String libelleBoutonSelectionner() {
		return new String("Sélectionner " + editingCoEncadrant().nomUsuel() + " " + editingCoEncadrant().prenom() + " comme encadrant");
	}

	public void setUneCivilite(EOCivilite uneCivilite) {
		this.uneCivilite = uneCivilite;
	}

	public EOCivilite uneCivilite() {
		return uneCivilite;
	}

	public EOIndividu editingCoEncadrant() {
		return (EOIndividu) valueForBinding(B_coEncadrant);
	}

	public void setEditingCoEncadrant(EOIndividu editingCoEncadrant) {
		setValueForBinding(editingCoEncadrant, B_coEncadrant);
	}

	public Boolean isEditing() {
		return (Boolean) valueForBinding(B_isEditing);
	}

	public void setIsEditing(Boolean isEditing) {
		setValueForBinding(isEditing, B_isEditing);
	}

	public Boolean isBoutonValiderDisable() {
		return (Boolean) valueForBinding(B_isBoutonValiderDisable);
	}

	public void setIsBoutonValiderDisable(Boolean isBoutonValiderDisable) {
		setValueForBinding(isBoutonValiderDisable, B_isBoutonValiderDisable);
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
	}

	public void setIsEditingAdresse(Boolean isEditingAdresse) {
		if (isEditingAdresse)
			setIsBoutonValiderDisable(true);
		else
			setIsBoutonValiderDisable(false);
		this.isEditingAdresse = isEditingAdresse;
	}

	public Boolean isEditingAdresse() {
		return isEditingAdresse;
	}

	public void setIsRechercheIndividu(Boolean isRechercheIndividu) {
		if (isRechercheIndividu & !isBoutonValiderDisable())
			setIsBoutonValiderDisable(true);
		if (!isRechercheIndividu & isBoutonValiderDisable())
			setIsBoutonValiderDisable(false);

		this.isRechercheIndividu = isRechercheIndividu;
	}

	public Boolean isRechercheIndividu() {
		// if (!isRechercheIndividu && editingCoEncadrant() == null) {
		if (editingCoEncadrant() == null) {
			setIsRechercheIndividu(true);
		}
		return isRechercheIndividu;
	}

}