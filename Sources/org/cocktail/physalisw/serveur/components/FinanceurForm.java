package org.cocktail.physalisw.serveur.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

public class FinanceurForm extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2979900019625909729L;
	private static final String B_financeur = "financeur";
	private static final String B_dateDebutFinanceur = "dateDebutFinanceur";
	private static final String B_dateFinFinanceur = "dateFinFinanceur";
	private static final String B_quotiteFinanceur = "quotiteFinanceur";
	private static final String B_isAjout = "isAjout";
	private static final String B_isAffichage = "isAffichage";
	private static final String B_isBoutonDisable = "isBoutonDisable";
	private static final String B_actionValider = "actionValider";
	private static final String B_actionAnnuler = "actionAnnuler";
	private static final String B_actionModifier = "actionModifier";
	private static final String B_actionSupprimer = "actionSupprimer";
	private static final String PARAM_GROUPE_PARTENAIRES_RECHERCHE = "GROUPE_PARTENAIRES_RECHERCHE";
	private EOStructure selectedFinanceur = null;

	public FinanceurForm(WOContext context) {
		super(context);
	}

	public EOQualifier qualifierForGroupePartenairesRecherche() {
		String codeStructureGroupePartenairesRecherche = EOGrhumParametres.parametrePourCle(edc(), PARAM_GROUPE_PARTENAIRES_RECHERCHE);
		return EOStructure.C_STRUCTURE.eq(codeStructureGroupePartenairesRecherche);
	}

	public WOActionResults selectionnerFinanceur() {
		setFinanceur(selectedFinanceur);
		setSelectedFinanceur(null);
		CktlAjaxWindow.close(context(), financeurSearchWindowId());
		return null;
	}

	public WOActionResults annulerSelectionnerFinanceur() {
		// setSelectedFinanceur(null);
		CktlAjaxWindow.close(context(), financeurSearchWindowId());
		return null;
	}

	public Boolean isAjout() {
		return (Boolean) valueForBinding(B_isAjout);
	}

	public Boolean isAffichage() {
		return (Boolean) valueForBinding(B_isAffichage);
	}

	public Boolean isBoutonDisable() {
		return (Boolean) valueForBinding(B_isBoutonDisable);
	}

	public WOActionResults validerFinanceur() {
		return performParentAction((String) valueForBinding(B_actionValider));
	}

	public WOActionResults annulerFinanceur() {
		return performParentAction((String) valueForBinding(B_actionAnnuler));
	}

	public WOActionResults modifierFinanceur() {
		return performParentAction((String) valueForBinding(B_actionModifier));
	}

	public WOActionResults supprimerFinanceur() {
		return performParentAction((String) valueForBinding(B_actionSupprimer));
	}

	public String aucFinanceurId() {
		return getComponentId() + "aucFinanceurId";
	}

	public String financeurSearchWindowId() {
		return getComponentId() + "financeurSearchWindowId";
	}

	public void setFinanceur(EOStructure financeur) {
		setValueForBinding(financeur, B_financeur);
	}

	public EOStructure financeur() {
		return (EOStructure) valueForBinding(B_financeur);
	}

	public void setDateDebutFinanceur(NSTimestamp dateDebutFinanceur) {
		setValueForBinding(dateDebutFinanceur, B_dateDebutFinanceur);
	}

	public NSTimestamp dateDebutFinanceur() {
		return (NSTimestamp) valueForBinding(B_dateDebutFinanceur);
	}

	public void setDateFinFinanceur(NSTimestamp dateFinFinanceur) {
		setValueForBinding(dateFinFinanceur, B_dateFinFinanceur);
	}

	public NSTimestamp dateFinFinanceur() {
		return (NSTimestamp) valueForBinding(B_dateFinFinanceur);
	}

	public void setQuotiteFinanceur(BigDecimal quotiteFinanceur) {
		setValueForBinding(quotiteFinanceur, B_quotiteFinanceur);
	}

	public BigDecimal quotiteFinanceur() {
		return (BigDecimal) valueForBinding(B_quotiteFinanceur);
	}

	public EOStructure selectedFinanceur() {
		return selectedFinanceur;
	}

	public void setSelectedFinanceur(EOStructure selectedFinanceur) {
		this.selectedFinanceur = selectedFinanceur;
	}

}