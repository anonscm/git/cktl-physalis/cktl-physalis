package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOTheseTypeContrat;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeContratDoctoral;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class FinancementForm extends PhysalisWOComponent {

	private static final long serialVersionUID = -3680521579914481609L;
	private static final String B_typeFinancement = "typeFinancement";
	private static final String B_typeContrat = "typeContrat";
	private static final String B_typeContratDoctoral = "typeContratDoctoral";
	private static final String B_employeur = "employeur";
	private static final String B_dateDebutFinancement = "dateDebutFinancement";
	private static final String B_dateFinFinancement = "dateFinFinancement";
//	private static final String B_isSansFinancement = "isSansFinancement";
	private static final String B_isAvecFinancement = "isAvecFinancement";
	private static final String B_commentairesSansFinancement = "commentairesSansFinancement";
	private EOTypeFinancement itemTypeFinancement = null;
	private EOTheseTypeContrat itemTypeContrat = null;
	private EOStructure selectedEmployeur = null;
	private EOTypeContratDoctoral itemTypeContratDoctoral;
	

	public FinancementForm(WOContext context) {
		super(context);
	}

	public NSArray<EOTypeFinancement> listeTypesFinancement() {
		NSArray<EOTypeFinancement> lesTf = null;
		EOQualifier qualTypeFinancement = null;
		if (isSansFinancement()) {
			qualTypeFinancement = ERXQ.equals(EOTypeFinancement.TYPE_FINANCEMENT_KEY, "N");
		} else {
			qualTypeFinancement = ERXQ.equals(EOTypeFinancement.TYPE_FINANCEMENT_KEY, "O");
		}

		lesTf = EOTypeFinancement.fetchTypeFinancements(edc(), qualTypeFinancement, null);

		if (lesTf != null && !lesTf.isEmpty()) {
			return lesTf;
		} else {
			return null;
		}

	}

	public NSArray<EOTheseTypeContrat> listeTypesContrat() {
		NSArray<EOTheseTypeContrat> lesTc = null;

		lesTc = EOTheseTypeContrat.fetchAllTheseTypeContrats(edc());

		if (lesTc != null && !lesTc.isEmpty()) {
			return lesTc;
		} else {
			return null;
		}

	}

	public EOQualifier qualifierEmployeur() {
		if (groupeEmployeurs() != null) {
			// return ERXQ.equals(EOStructure.TO_REPART_STRUCTURES_KEY + "." +
			// EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." +
			// EOStructure.C_STRUCTURE_KEY, leGroupeEtablissements);
			return ERXQ.equals(EOStructure.TO_STRUCTURE_PERE_KEY + "." + EOStructure.C_STRUCTURE_KEY, groupeEmployeurs());
		} else {
			session.addSimpleErrorMessage("Erreur paramétrage",
					"le paramètre ANNUAIRE_EMPLOYEUR n'est pas défini dans GRHUM_PARAMETRES, veuillez prendre contact avec les informaticiens ");
			return null;
		}
	}

	public WOActionResults selectionnerEmployeur() {
		setEmployeur(selectedEmployeur);
		setSelectedEmployeur(null);
		CktlAjaxWindow.close(context(), employeurSearchWindowId());
		return null;
	}

	public WOActionResults annulerSelectionnerEmployeur() {
		setSelectedEmployeur(null);
		CktlAjaxWindow.close(context(), employeurSearchWindowId());
		return null;
	}

	public Boolean isFinancement() {
		if (itemTypeFinancement == null) {
			return false;
		}
		if (itemTypeFinancement.typeFinancement().equals("O")) {
			return true;
		} else {
			return false;
		}
	}

	public String employeurSearchWindowId() {
		return getComponentId() + "employeurSearchWindowId";
	}

	public String aucEmployeurId() {
		return getComponentId() + "aucEmployeurId";
	}

	public void setTypeFinancement(EOTypeFinancement typeFinancement) {
		setValueForBinding(typeFinancement, B_typeFinancement);
	}

	public EOTypeFinancement typeFinancement() {
		return (EOTypeFinancement) valueForBinding(B_typeFinancement);
	}

	public void setItemTypeFinancement(EOTypeFinancement itemTypeFinancement) {
		this.itemTypeFinancement = itemTypeFinancement;
	}

	public EOTypeFinancement itemTypeFinancement() {
		return itemTypeFinancement;
	}

	public void setTypeContrat(EOTheseTypeContrat typeContrat) {
		setValueForBinding(typeContrat, B_typeContrat);
		if (typeContrat != null && !typeContrat.libTypeContrat().equals(EOTheseTypeContrat.LIBELLE_TYPE_CONTRAT_DOCTORAL)) {
			setTypeContratDoctoral(null);
		}
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
	}

	public EOTheseTypeContrat typeContrat() {
		return (EOTheseTypeContrat) valueForBinding(B_typeContrat);
	}

	public void setTypeContratDoctoral(EOTypeContratDoctoral typeContratDoctoral) {
		setValueForBinding(typeContratDoctoral, B_typeContratDoctoral);
	}

	public EOTypeContratDoctoral typeContratDoctoral() {
		return (EOTypeContratDoctoral) valueForBinding(B_typeContratDoctoral);
	}

	public void setItemTypeContrat(EOTheseTypeContrat itemTypeContrat) {
		this.itemTypeContrat = itemTypeContrat;
	}

	public EOTheseTypeContrat itemTypeContrat() {
		return itemTypeContrat;
	}

	public void setEmployeur(EOStructure employeur) {
		setValueForBinding(employeur, B_employeur);
	}

	public EOStructure employeur() {
		return (EOStructure) valueForBinding(B_employeur);
	}

	public void setDateDebutFinancement(NSTimestamp dateDebutFinancement) {
		setValueForBinding(dateDebutFinancement, B_dateDebutFinancement);
	}

	public NSTimestamp dateDebutFinancement() {
		return (NSTimestamp) valueForBinding(B_dateDebutFinancement);
	}

	public void setDateFinFinancement(NSTimestamp dateFinFinancement) {
		setValueForBinding(dateFinFinancement, B_dateFinFinancement);
	}

	public NSTimestamp dateFinFinancement() {
		return (NSTimestamp) valueForBinding(B_dateFinFinancement);
	}

	public void setSelectedEmployeur(EOStructure selectedEmployeur) {
		this.selectedEmployeur = selectedEmployeur;
	}

	public EOStructure selectedEmployeur() {
		return selectedEmployeur;
	}

	public void setCommentairesSansFinancement(String commentairesSansFinancement) {
		setValueForBinding(commentairesSansFinancement, B_commentairesSansFinancement);
	}

	public String commentairesSansFinancement() {
		return (String) valueForBinding(B_commentairesSansFinancement);
	}

	public void setIsSansFinancement(Boolean isSansFinancement) {
		setValueForBinding(!isSansFinancement, B_isAvecFinancement);
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
	}

	public Boolean isSansFinancement() {
		return booleanValueForBinding(B_isAvecFinancement, false) == false;
	}

	public void setIsAvecFinancement(Boolean isAvecFinancement) {
		setValueForBinding(isAvecFinancement, B_isAvecFinancement);
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
	}

	public Boolean isAvecFinancement() {
		return (Boolean) valueForBinding(B_isAvecFinancement);
	}

	public EOTypeContratDoctoral getItemTypeContratDoctoral() {
	    return itemTypeContratDoctoral;
    }

	public void setItemTypeContratDoctoral(EOTypeContratDoctoral itemTypeContratDoctoral) {
	    this.itemTypeContratDoctoral = itemTypeContratDoctoral;
    }
	
	public NSArray<EOTypeContratDoctoral> listeTypesContratDoctoraux() {
		NSArray<EOTypeContratDoctoral> lesTcd = null;

		lesTcd = EOTypeContratDoctoral.fetchAll(edc());

		if (lesTcd != null && !lesTcd.isEmpty()) {
			return lesTcd;
		} else {
			return null;
		}

	}
	
	public Boolean isContratDoctoral() {
		if (typeContrat() != null && typeContrat().libTypeContrat() != null) {
			return typeContrat().libTypeContrat().equals(EOTheseTypeContrat.LIBELLE_TYPE_CONTRAT_DOCTORAL);
		}
		return false;
	}

}