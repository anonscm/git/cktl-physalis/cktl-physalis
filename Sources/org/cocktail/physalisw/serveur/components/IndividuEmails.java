package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public class IndividuEmails extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7170583034813540753L;
	private static final String B_personne = "personne";
	private String currentEmail = null;

	public IndividuEmails(WOContext context) {
		super(context);
	}

	public NSArray<String> listeEmails() {
		EOQualifier qualRpa = ERXQ.and(ERXQ.notEquals(EORepartPersonneAdresse.E_MAIL_KEY, null), ERXQ.notEquals(EORepartPersonneAdresse.E_MAIL_KEY, ""), 
				EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE, EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_PRO);
		NSMutableArray<String> listeEmail = new NSMutableArray<String>();
		NSArray<EORepartPersonneAdresse> lesRpas = personne().toRepartPersonneAdresses(qualRpa);
		listeEmail.addObjectsFromArray(personne().getEmails(null));
		for (EORepartPersonneAdresse rpa : lesRpas) {
			listeEmail.add(rpa.eMail());
		}
		return listeEmail;

	}

	public EOIndividu personne() {
		return (EOIndividu) valueForBinding(B_personne);
	}

	public void setCurrentEmail(String currentEmail) {
		this.currentEmail = currentEmail;
	}

	public String currentEmail() {
		return currentEmail;
	}

	public WOActionResults envoiMail() {
		return null;
	}

	public String mailto() {
		return "location.href='mailto:" + currentEmail + "'";
	}
}