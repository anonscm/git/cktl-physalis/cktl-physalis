package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine;
import org.cocktail.fwkcktlrecherche.server.metier.finder.FinderProjetScientifique;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class TheseForm extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9132931979413398147L;
	private static final String B_laThese = "laThese";
	private static final String B_ecoleDoctorale = "ecoleDoctorale";
	private static final String B_theseTitre = "theseTitre";
	private static final String B_theseDomaine = "theseDomaine";
	private static final String B_theseDateDebutThese = "theseDateDebutThese";
	private static final String B_theseDateFinThese = "theseDateFinThese";
	private static final String B_theseDateFinAnticipee = "theseDateFinAnticipee";
	private static final String B_theseMotifFinAnticipee = "theseMotifFinAnticipee";
	private static final String B_theseDatePrevueSoutenance = "theseDatePrevueSoutenance";
	private static final String B_theseReference = "theseReference";
	private static final String B_theseProjetScientifique = "theseProjetScientifique";
	private static final String B_theseResume = "theseResume";
	private static final String B_theseIsCotutelle = "theseIsCotutelle";
	private static final String B_theseDomaineScientifique = "theseDomaineScientifique";
	private static final String B_theseCodeSiseDiplome = "theseCodeSiseDiplome";
	public EOTheseDomaine itemTheseDomaine;
	public EOStructure itemEcoleDoctorale;
	public EOProjetScientifique itemProjetScientifique;
	private EOStructure selectedEcoleDoctorale;
	private EOProjetScientifique selectedProjetScientifique;
//	private EODomaineScientifique domaineScientifiqueAAjouter;
//	private AvenantDomaineScientifique currentAvenantDomaineScientifique;

	public TheseForm(WOContext context) {
		super(context);
	}
	
	public String containerTheseFormId() {
		return getComponentId() + "containerTheseForm";
	}

	public String containerDomainesScientifiquesId() {
		return getComponentId() + "_containerDomainesScientifiques";
	}
	
	public NSArray<EOStructure> listeEcolesDoctorales() {

		EOQualifier qualDoct = ERXQ.and(ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "ED"),
				ERXQ.equals(EOStructure.TEM_VALIDE_KEY, EOStructure.TEM_VALIDE_OUI));
		NSArray<EOStructure> lesEds = EOStructure.fetchAll(edc(), qualDoct, null);
		if (lesEds.count() == 0) {
			session.addSimpleErrorMessage("Erreur", "Aucune ecole doctorale trouvé, il faut au moins une structure ayant un type_groupe = ED dans votre base");
			return null;
		} else
			return lesEds;

	}

	public NSArray<EOTheseDomaine> listeTheseDomaines() {
		return EOTheseDomaine.fetchAllTheseDomaines(edc(), new NSArray<EOSortOrdering>(EOTheseDomaine.SORT_LIBELLE_DOMAINE));
	}

	public NSArray<EOProjetScientifique> listeProjetsScientifiques() {
		NSArray<EOProjetScientifique> lesProjets = ERXArrayUtilities.arrayWithoutDuplicates(EOProjetScientifique.fetchAll(edc(), EOProjetScientifique.PROJET_SUPPRIME.isFalse()));
		
		if (lesProjets != null && !lesProjets.isEmpty()) {
			return lesProjets;
		} else {
			return null;
		}
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	public void setSelectedEcoleDoctorale(EOStructure selectedEcoleDoctorale) {
		setValueForBinding(selectedEcoleDoctorale, B_ecoleDoctorale);
		this.selectedEcoleDoctorale = selectedEcoleDoctorale;
	}

	public EOStructure selectedEcoleDoctorale() {
		selectedEcoleDoctorale = (EOStructure) valueForBinding(B_ecoleDoctorale);
		return selectedEcoleDoctorale;
	}

	public void setSelectedProjetScientifique(EOProjetScientifique selectedProjetScientifique) {
		this.selectedProjetScientifique = selectedProjetScientifique;
	}

	public EOProjetScientifique selectedProjetScientifique() {
		return selectedProjetScientifique;
	}

	public void setTheseTitre(String theseTitre) {
		setValueForBinding(theseTitre, B_theseTitre);
	}

	public String theseTitre() {
		return (String) valueForBinding(B_theseTitre);
	}

	public void setTheseDomaine(EOTheseDomaine theseDomaine) {
		setValueForBinding(theseDomaine, B_theseDomaine);
	}

	public EOTheseDomaine theseDomaine() {
		return (EOTheseDomaine) valueForBinding(B_theseDomaine);
	}

	public void setTheseReference(String theseReference) {
		setValueForBinding(theseReference, B_theseReference);
	}

	public String theseReference() {
		return (String) valueForBinding(B_theseReference);
	}

	public void setTheseResume(String theseResume) {
		setValueForBinding(theseResume, B_theseResume);
	}

	public String theseResume() {
		return (String) valueForBinding(B_theseResume);
	}

	public void setTheseDateDebutThese(NSTimestamp theseDateDebutThese) {
		setValueForBinding(theseDateDebutThese, B_theseDateDebutThese);
	}

	public NSTimestamp theseDateDebutThese() {
		return (NSTimestamp) valueForBinding(B_theseDateDebutThese);
	}

	public void setTheseDateFinThese(NSTimestamp theseDateFinThese) {
		setValueForBinding(theseDateFinThese, B_theseDateFinThese);
	}

	public NSTimestamp theseDateFinThese() {
		return (NSTimestamp) valueForBinding(B_theseDateFinThese);
	}

	public void setTheseDatePrevueSoutenance(NSTimestamp theseDatePrevueSoutenance) {
		setValueForBinding(theseDatePrevueSoutenance, B_theseDatePrevueSoutenance);
	}

	public NSTimestamp theseDatePrevueSoutenance() {
		return (NSTimestamp) valueForBinding(B_theseDatePrevueSoutenance);
	}

	public void setTheseProjetScientifique(EOProjetScientifique theseProjetScientifique) {
		setValueForBinding(theseProjetScientifique, B_theseProjetScientifique);
	}

	public EOProjetScientifique theseProjetScientifique() {
		return (EOProjetScientifique) valueForBinding(B_theseProjetScientifique);
	}

	public void setTheseDateFinAnticipee(NSTimestamp theseDateFinAnticipee) {
		if(theseDateFinAnticipee == null) {
			setTheseMotifFinAnticipee(null);
		}
		setValueForBinding(theseDateFinAnticipee, B_theseDateFinAnticipee);
	}

	public NSTimestamp theseDateFinAnticipee() {
		return (NSTimestamp) valueForBinding(B_theseDateFinAnticipee);
	}
	
	public void setTheseMotifFinAnticipee(String motifFinAnticipee) {
		setValueForBinding(motifFinAnticipee, B_theseMotifFinAnticipee);
	}

	public String theseMotifFinAnticipee() {
		return (String) valueForBinding(B_theseMotifFinAnticipee);
	}
	
	public void setTheseCodeSiseDiplome(String theseCodeSiseDiplome) {
		setValueForBinding(theseCodeSiseDiplome, B_theseCodeSiseDiplome);
	}

	public String theseCodeSiseDiplome() {
		return (String) valueForBinding(B_theseCodeSiseDiplome);
	}
	
	public Boolean isCotutelle() {
		return (Boolean) valueForBinding(B_theseIsCotutelle);
	}
	
	public void setIsCotutelle(Boolean isCotutelle) {
		setValueForBinding(isCotutelle, B_theseIsCotutelle);
	}
	
	public EODomaineScientifique theseDomaineScientifique() {
		return (EODomaineScientifique) valueForBinding(B_theseDomaineScientifique);
	}
	
	public void setTheseDomaineScientifique(EODomaineScientifique theseDomaineScientifique) {
		setValueForBinding(theseDomaineScientifique, B_theseDomaineScientifique);
	}
	
//	public EODomaineScientifique getDomaineScientifiqueAAjouter() {
//	    return domaineScientifiqueAAjouter;
//    }
//
//	public void setDomaineScientifiqueAAjouter(EODomaineScientifique domaineScientifiqueAAjouter) {
//	    this.domaineScientifiqueAAjouter = domaineScientifiqueAAjouter;
//    }
//
//	public WOActionResults ajouterDomaineScientifiqueDansThese() {
//		if (getDomaineScientifiqueAAjouter() == null) {
//			session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner un domaine scientifique");
//			return doNothing();
//		}
//		if (laThese().toContrat().avenantZero().avenantDomaineScientifiques(AvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE.eq(getDomaineScientifiqueAAjouter())).count() > 0) {
//			session().addSimpleErrorMessage("Erreur", "Le domaine scientifique sélectionné est déjà rattaché à la thèse");
//			return doNothing();
//		}
//		AvenantDomaineScientifique.create(edc(), laThese().toContrat().avenantZero(), getDomaineScientifiqueAAjouter());
//		setDomaineScientifiqueAAjouter(null);
//	    return doNothing();
//	}
//
//	/**
//	 * @return the currentAvenantDomaineScientifique
//	 */
//	public AvenantDomaineScientifique getCurrentAvenantDomaineScientifique() {
//	    return currentAvenantDomaineScientifique;
//	}
//
//	/**
//	 * @param currentAvenantDomaineScientifique the currentAvenantDomaineScientifique to set
//	 */
//	public void setCurrentAvenantDomaineScientifique(AvenantDomaineScientifique currentAvenantDomaineScientifique) {
//	    this.currentAvenantDomaineScientifique = currentAvenantDomaineScientifique;
//	}
//
//	public WOActionResults supprimerDomaineScientifique() {
//		laThese().toContrat().avenantZero().deleteAvenantDomaineScientifiquesRelationship(getCurrentAvenantDomaineScientifique());
//		getCurrentAvenantDomaineScientifique().delete();
//	    return doNothing();
//	}

}