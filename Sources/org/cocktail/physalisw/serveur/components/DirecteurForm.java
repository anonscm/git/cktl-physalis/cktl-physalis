package org.cocktail.physalisw.serveur.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DirecteurTheseService;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxModalDialog;
import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class DirecteurForm extends PhysalisWOComponent {

	private static final long serialVersionUID = -8588967029329774787L;
	private static final String B_encadrant = "selectedEncadrant";
	private static final String B_directeurThese = "directeurThese";
	private static final String B_roleDirecteurThese = "roleDirecteurThese";
	private static final String B_isModif = "isModif";
	private static final String B_isEtablissementFrancais = "isEtablissementFrancais";
	private static final String B_isLaboratoireLocal = "isLaboratoireLocal";
	private static final String B_isCotutelle = "isCotutelle";
	private static final String B_dateDebutEncadrement = "dateDebutEncadrement";
	private static final String B_dateFinEncadrement = "dateFinEncadrement";
	private static final String B_quotiteEncadrement = "quotiteEncadrement";
	private static final String B_commentaireEncadrement = "commentaireEncadrement";
	private static final String B_editingPays = "editingPays";
	private static final String B_laThese = "laThese";
	private static final String B_dateAutorisation = "dateAutorisation";
	private static final String B_showDateAutorisation = "showDateAutorisation";
	
	private String libellePays = null;
	private String typeEtablissement = null;
	private String typeLaboratoire = null;
	private EOStructure selectedEtablissement = null;
	public EOStructure itemStructureLabo;
	public EOIndividu selectedIndividuDirTheseHab;
	public EOIndividu itemIndividuDirThese;
	public EORne itemRne;
	private EOAssociation selectedAssocTypeDirecteur;
	public EOAssociation itemAssocTypeDirecteur;
	private String individuDirTheseHabInput;
	private String rneInput;
	public EORne selectedRne;
	private Boolean showDateAutorisation = false;
	private Boolean wantResetEtablissementEtrangerSearch = false;

	public DirecteurForm(WOContext context) {
		super(context);
	}

	public NSArray<EOAssociation> listeAssocTypeDirecteur() {
		NSArray<EOAssociation> roles = null;
		EOAssociation associationDirection = factoryAssociation().directionTheseAssociation(edc());
		if (associationDirection != null) {
			roles = associationDirection.getFils(edc());
		}
		return roles;
	}

	public NSArray<EOStructure> listeStructuresLabo() {		
		EOQualifier qualLabos = ERXQ.and(
			ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, laThese().ecoleDoctorale().cStructure()), 
			ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().membreAssociation(edc())), 
			ERXQ.equals(EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.' + EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, 
			EOTypeGroupe.TGRP_CODE_LA)
		);

		NSArray<EORepartAssociation> lesRepartAssoLabo = ERXArrayUtilities.arrayWithoutDuplicates(EORepartAssociation.fetchAll(edc(), qualLabos));
		if (lesRepartAssoLabo == null) {
			return null;
		} else {
			return (NSArray) lesRepartAssoLabo.valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
		}
	}

	public WOActionResults changeEtablissement() {
		AjaxUpdateContainer.updateContainerWithID(aucCotutelleId(), context());
		return null;
	}

	public WOActionResults changeLaboratoire() {
		return null;
	}

	public WOActionResults changeCotutelle() {
		return null;
	}

	public EODirecteurThese directeurThese() {
		return (EODirecteurThese) valueForBinding(B_directeurThese);
	}

	public EOAssociation roleDirecteurThese() {
		return (EOAssociation) valueForBinding(B_roleDirecteurThese);
	}

	public Boolean isModif() {
		return (Boolean) valueForBinding(B_isModif);
	}

	public WOActionResults selectDirTheseHab() {
		AjaxModalDialog.open(context(), winSelectDirTheseId(), "Choisir un directeur habilité");
		return null;
	}

	public String individuDirTheseHabInput() {
		return individuDirTheseHabInput;
	}

	public void setIndividuDirTheseHabInput(String str) {
		this.individuDirTheseHabInput = str;
	}

	public String rneInput() {
		return rneInput;
	}

	public void setRneInput(String rneInput) {
		this.rneInput = rneInput;
	}

	public EOAssociation selectedAssocTypeDirecteur() {
		selectedAssocTypeDirecteur = (EOAssociation) valueForBinding(B_roleDirecteurThese);
		return selectedAssocTypeDirecteur;
	}

	public void setSelectedAssocTypeDirecteur(EOAssociation selectedAssocTypeDirecteur) {
		if (selectedAssocTypeDirecteur() != null && selectedAssocTypeDirecteur != selectedAssocTypeDirecteur()) { // si on change de type de role
			//si on passe d'un directeur de these a un encadrant
			if (selectedAssocTypeDirecteur.equals(factoryAssociation().coEncadrantTheseAssociation(edc()))) {
				directeurThese().setDateAutorisation(null);
				setShowDateAutorisation(false);
				setQuotiteEncadrement(null);
			} else { // on passe d'un co encadrant a un directeur de these
				//On verifie s'il existe dans les directeurs de these
				EODirecteurTheseHabilite dth = getIndividuDirecteurTheseHabilite(selectedEncadrant(), laThese());

				if (dth == null && selectedEncadrant() != null) {
					setShowDateAutorisation(true);
				}
				setQuotiteEncadrement(new BigDecimal(0));
			}
		}
		
		this.selectedAssocTypeDirecteur = selectedAssocTypeDirecteur;
		setValueForBinding(selectedAssocTypeDirecteur, B_roleDirecteurThese);
	}

	public EOIndividu selectedEncadrant() {
		return (EOIndividu) valueForBinding(B_encadrant);
	}

	/**
	 * Set de l'encadrant selectionné et cherche s'il est rattaché à un établissement
	 * @param selectedEncadrant l'encadrant selectionné
	 */
	public void setSelectedEncadrant(EOIndividu selectedEncadrant) {
		setValueForBinding(selectedEncadrant, B_encadrant);
		
		// on récupere le labo et l'etablissement du directeur de these habilité
		// pour initialiser les données du directeur
		EODirecteurTheseHabilite dth = getIndividuDirecteurTheseHabilite(selectedEncadrant, laThese());
		
		if (dth != null) {
			if (dth.laboExterneDth() != null) {
				directeurThese().setLibelleLabo(dth.laboExterneDth());
				setIsLaboratoireExterne(true);
				setIsLaboratoireLocal(false);
			} else {
				directeurThese().setToStructureLaboFwkpersRelationship(dth.toStructureLaboFwkpers().localInstanceIn(directeurThese().editingContext()));
				setIsLaboratoireExterne(false);
				setIsLaboratoireLocal(true);
			}
			if (dth.toStructureEtabFwkPers() != null) {
				directeurThese().setToStructureEtabFwkPersRelationship(dth.toStructureEtabFwkPers().localInstanceIn(directeurThese().editingContext()));
				setIsEtablissementEtranger(true);
				setIsEtablissementFrancais(false);
			} else {
				directeurThese().setToRneFwkpersRelationship(dth.toRneFwkPers().localInstanceIn(directeurThese().editingContext()));
				setIsEtablissementEtranger(false);
				setIsEtablissementFrancais(true);
			}
		} else {
			NSArray<EOStructure> etablissements = selectedEncadrant.getEtablissementsAffectation(null);
			for (EOStructure etablissement : etablissements) {
				if (etablissement.toRne() != null) {
					directeurThese().setToRneFwkpersRelationship(etablissement.toRne().localInstanceIn(directeurThese().editingContext()));
					setIsEtablissementEtranger(false);
					setIsEtablissementFrancais(true);
				}
			}
		}
	}
	
	/**
	 * Action lancée lors de l'ajout d'un encadrant : affecte etablissement et laboratoire si trouvés
	 * @return doNothing()
	 */
	public WOActionResults validerEncadrant() {
		
		if (selectedEncadrant() == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez sélectionner une personne !");
			return doNothing();
		} 
		
		DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
		directeurThese().setToRneFwkpersRelationship(dts.getEtablissementIndividu(selectedEncadrant()));
		if (dts.getLaboratoireIndividu(selectedEncadrant()) != null) {
			EOStructure laboratoire = dts.getLaboratoireIndividu(selectedEncadrant()).localInstanceIn(directeurThese().editingContext());
			directeurThese().setToStructureLaboFwkpersRelationship(laboratoire);
		}
		
		return doNothing();
	}
	
	public WOActionResults annulerEncadrant() {
		edc().revert();
		setSelectedEncadrant(null);
		return doNothing();
	}
	
	public WOActionResults onCreerPersonne() {

		if (selectedEncadrant() != null) {
			EOIndividu individu = selectedEncadrant();
			if (individu.toCivilite() == null) {
				NSArray<EOCivilite> civilites = EOCivilite.fetchAll(edc());
				individu.setToCiviliteRelationship((EOCivilite) civilites.objectAtIndex(0));
			}
		}

		if (selectedEncadrant().persIdCreation() == null) {
			selectedEncadrant().setPersIdCreation(session().applicationUser().getPersId());
		}
		if (selectedEncadrant().persIdModification() == null) {
			selectedEncadrant().setPersIdModification(session().applicationUser().getPersId());
		}
		
		CktlAjaxWindow.close(context());
		
		return doNothing();
	}

	public Boolean onReturnSelectDth() {
		return true;
	}

	public void setTypeEtablissement(String typeEtablissement) {
		this.typeEtablissement = typeEtablissement;
	}

	public String typeEtablissement() {
		return typeEtablissement;
	}

	public void setTypeLaboratoire(String typeLaboratoire) {
		this.typeLaboratoire = typeLaboratoire;
	}

	public String typeLaboratoire() {
		return typeLaboratoire;
	}

	public String winSelectDirTheseId() {
		return getComponentId() + "_winSelectDirThese";
	}

	public String aucFormTheseId() {
		return getComponentId() + "_aucFormTheseId";
	}

	public String aucCotutelleId() {
		return getComponentId() + "_aucCotutelleId";
	}

	public String aucLeDirecteurId() {
		return getComponentId() + "_aucLeDirecteurId";
	}

	public String aucEtablissementId() {
		return getComponentId() + "_aucEtablissementId";
	}

	public String aucLaboratoireId() {
		return getComponentId() + "_aucLaboratoireId";
	}
	
	public String getContainerEtablissementId() {
		return getComponentId() + "_containerEtablissementId";
	}

	public NSArray<String> onUpdateEtablissement() {
		NSArray<String> containers = new NSMutableArray<String>();
		containers.add(aucEtablissementId());
		containers.add(aucCotutelleId());
		return containers;
	}

	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
		setValueForBinding(isEtablissementFrancais, B_isEtablissementFrancais);
	}

	public Boolean isEtablissementFrancais() {
		return (Boolean) valueForBinding(B_isEtablissementFrancais);
	}

	public void setIsEtablissementEtranger(Boolean isEtablissementEtranger) {
	}

	public Boolean isEtablissementEtranger() {
		return !isEtablissementFrancais();
	}

	public void setIsLaboratoireLocal(Boolean isLaboratoireLocal) {
		setValueForBinding(isLaboratoireLocal, B_isLaboratoireLocal);
	}

	public Boolean isLaboratoireLocal() {
		return (Boolean) valueForBinding(B_isLaboratoireLocal);
	}

	public Boolean isCotutelle() {
		return (Boolean) valueForBinding(B_isCotutelle);
	}

	public void setIsCotutelle(Boolean isCotutelle) {
		if (isCotutelle) {
			if (directeurThese().toStructureEtabFwkPers() == null) {
				session.addSimpleErrorMessage("Erreur", "Vous devez saisir un établissement étranger !");
				isCotutelle = false;
			} else {
				setEditingPays(directeurThese().toStructureEtabFwkPers().getAdressePrincipale().toPays());
				setLibellePays(directeurThese().toStructureEtabFwkPers().getAdressePrincipale().toPays().llPays());
			}
		}
		setValueForBinding(isCotutelle, B_isCotutelle);
	}

	public void setIsLaboratoireExterne(Boolean isLaboratoireExterne) {
	}

	public Boolean isLaboratoireExterne() {
		return !isLaboratoireLocal();
	}

	public String etablissementSearchWindowId() {
		return getComponentId() + "etablissementSearchWindowId";
	}

	public WOActionResults annulerSelectionnerEtablissement() {
		setSelectedEtablissement(null);
		wantResetEtablissementEtrangerSearch = true;
		CktlAjaxWindow.close(context(), etablissementSearchWindowId());
		return null;
	}

	public WOActionResults selectionnerEtablissement() {
		directeurThese().setToStructureEtabFwkPersRelationship(selectedEtablissement());
		setEditingPays(selectedEtablissement.getAdressePrincipale().toPays());
		setLibellePays(selectedEtablissement.getAdressePrincipale().toPays().llPays());
		setSelectedEtablissement(null);
		wantResetEtablissementEtrangerSearch = true;
		AjaxUpdateContainer.updateContainerWithID(aucCotutelleId(), context());
		CktlAjaxWindow.close(context(), etablissementSearchWindowId());
		return null;
	}
	
	public Boolean wantResetEtablissementEtrangerSearch() {
		return wantResetEtablissementEtrangerSearch;
	}
	
	public void setWantResetEtablissementEtrangerSearch(Boolean wantResetEtablissementEtrangerSearch) {
		this.wantResetEtablissementEtrangerSearch = wantResetEtablissementEtrangerSearch;
	}

	public void setSelectedEtablissement(EOStructure selectedEtablissement) {
		this.selectedEtablissement = selectedEtablissement;
	}

	public EOStructure selectedEtablissement() {
		return selectedEtablissement;
	}

	public String commentaireEncadrement() {
		return (String) valueForBinding(B_commentaireEncadrement);
	}

	public void setCommentaireEncadrement(String commentaireEncadrement) {
		setValueForBinding(commentaireEncadrement, B_commentaireEncadrement);
	}

	public NSTimestamp dateDebutEncadrement() {
		return (NSTimestamp) valueForBinding(B_dateDebutEncadrement);
	}

	public NSTimestamp dateFinEncadrement() {
		return (NSTimestamp) valueForBinding(B_dateFinEncadrement);
	}
	
	public void setQuotiteEncadrement(BigDecimal quotiteEncadrement) {
		setValueForBinding(quotiteEncadrement, B_quotiteEncadrement);
	}

	public BigDecimal quotiteEncadrement() {
		return (BigDecimal) valueForBinding(B_quotiteEncadrement);
	}

	public void setDateDebutEncadrement(NSTimestamp dateDebutEncadrement) {
		setValueForBinding(dateDebutEncadrement, B_dateDebutEncadrement);
	}

	public void setDateFinEncadrement(NSTimestamp dateFinEncadrement) {
		setValueForBinding(dateFinEncadrement, B_dateFinEncadrement);
	}

	public void setEditingPays(EOPays editingPays) {
		setValueForBinding(editingPays, B_editingPays);
	}

	public EOPays editingPays() {
		return (EOPays) valueForBinding(B_editingPays);
	}

	public void setLibellePays(String libellePays) {
		this.libellePays = libellePays;
	}

	public String libellePays() {
		if (libellePays == null && editingPays() != null) {
			setLibellePays(editingPays().llPays());
		}
		return libellePays;
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}
	
	public Boolean isEncadrant() {
		return (selectedAssocTypeDirecteur() != null && factoryAssociation().coEncadrantTheseAssociation(edc()).assCode().equals(selectedAssocTypeDirecteur().assCode()));
	}
	
	public String nomEncadrant() {
		if (selectedEncadrant() != null) {
			return selectedEncadrant().getNomAndPrenom();
		}
		return "";
	}
	
	public String titleWindowEncadrant() {
		if (isEncadrant()) {
			return "Rechercher un encadrant de thèse";
		}
		return "Rechercher un directeur de thèse";
	}

	public NSTimestamp dateAutorisation() {
	    return (NSTimestamp) valueForBinding(B_dateAutorisation);
    }

	public void setDateAutorisation(NSTimestamp dateAutorisation) {
		directeurThese().setDateAutorisation(dateAutorisation);
	    setValueForBinding(dateAutorisation, B_dateAutorisation);
    }

	public Boolean showDateAutorisation() {
	    return (Boolean) valueForBinding(B_showDateAutorisation);
    }

	public void setShowDateAutorisation(Boolean showDateAutorisation) {
		this.showDateAutorisation = showDateAutorisation;
	    setValueForBinding(showDateAutorisation, B_showDateAutorisation);
    }

	public Boolean canShowDateAutorisation() {
		return (showDateAutorisation() || directeurThese().dateAutorisation() != null);
	}

	public Boolean showQuotite() {
		return ((selectedAssocTypeDirecteur() == null) || (factoryAssociation().directeurTheseAssociation(edc()).equals(selectedAssocTypeDirecteur())));
	}
	
	/**
	 * Retourne le directeur de these habilité correspondant a un individu
	 * @return
	 */
	private EODirecteurTheseHabilite getIndividuDirecteurTheseHabilite(EOIndividu individu, EODoctorantThese these) {
	    DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
	    return dts.getDirecteurTheseHabiliteFromIndividu(individu, these);
    }
	
	public Boolean isAjoutEtablissementDisabled() {
		return (selectedEtablissement == null);
	}
	
	public Boolean wantResetEncadrantSearch() {
		return true;
	}
	
	public void setWantResetEncadrantSearch(Boolean wantResetEncadrantSearch) {
		
	}

}