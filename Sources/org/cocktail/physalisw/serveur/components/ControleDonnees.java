package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DroitsService;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXUtilities;

public class ControleDonnees extends PhysalisWOComponent {
	
	private static final Integer NUMBER_OF_OBJECTS_PER_BATCH = 15;
	
	private ERXDisplayGroup<EODoctorantThese> displayGroupTheses;
    private EODoctorantThese currentThese;
    private EODoctorantThese selectedThese;
	private DroitsService ds;
	
    public ControleDonnees(WOContext context) {
        super(context);
    }
    
    public String getThesesTableViewId() {
		return getComponentId() + "_thesesTableViewId";
	}
    
    public String getBoutonsContainerId() { 
    	return getComponentId() + "_boutonsContainerId";
    }
    
    public NSArray<EODoctorantThese> listeThesesIncompletes() {

    	EOQualifier qualifierDiplomeNull = EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_DOCTORANT_ORIGINE).isNull();
    	
    	EOQualifier qualifierPaysOrigineNull = EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_PAYS_DIPLOME).isNull();
    	
    	EOQualifier qualifierEtablissementFrancaisNull = ERXQ.and(EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_PAYS_DIPLOME).dot(EOPays.C_PAYS).eq(EOPays.CODE_PAYS_FRANCE),
    															EODoctorantThese.TO_DOCTORANT.dot(EODoctorant.TO_ETABLISSEMENT_DIPLOME_ORIGINE).isNull());
    	
//    	EOQualifier qualifier = ERXQ.or(qualifierDiplomeNull, qualifierPaysOrigineNull, qualifierEtablissementFrancaisNull); //FIXME: fetch ne marche pas
    	
    	NSArray<EODoctorantThese> theses = new NSMutableArray<EODoctorantThese>();
    	theses.addAll(EODoctorantThese.fetchAll(edc(), qualifierDiplomeNull));
    	theses.addAll(EODoctorantThese.fetchAll(edc(), qualifierPaysOrigineNull));
    	theses.addAll(EODoctorantThese.fetchAll(edc(), qualifierEtablissementFrancaisNull));
    	theses = ERXArrayUtilities.arrayWithoutDuplicates(theses);
    	
    	if (!session().getRechercheApplicationAutorisationCache().hasDroitVoirToutesTheses()) {
			NSArray<EODoctorantThese> thesesED = new NSMutableArray<EODoctorantThese>();
			NSArray<EODoctorantThese> thesesLabo = new NSMutableArray<EODoctorantThese>();
			thesesED.addAll(ERXQ.filtered(theses, EODoctorantThese.ECOLE_DOCTORALE.in(ds().listeEcolesDoctoralesVisiblesSansDroit())));
			
			EOQualifier qualifierLabo = null;
			for (EOStructure labo : ds().listeLaboratoiresVisiblesSansDroit()) {
				qualifierLabo = ERXQ.or(qualifierLabo, EODoctorantThese.LABORATOIRES.containsObject(labo));
			}
			thesesLabo = ERXQ.filtered(theses, qualifierLabo);
			
			theses.clear();
			theses.addAll(thesesED);
			theses.addAll(thesesLabo);
		}
    	
    	return theses;
    }
    
    /**
	 * @return : displayGroup des liens avec les enfants
	 */
	public ERXDisplayGroup<EODoctorantThese> getDisplayGroupTheses() {
		if (displayGroupTheses == null) {
			displayGroupTheses = new ERXDisplayGroup<EODoctorantThese>();
			displayGroupTheses.setDelegate(new ThesesDisplayGroupDelegate());
			displayGroupTheses.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
			displayGroupTheses.setObjectArray(listeThesesIncompletes());
		}
		return displayGroupTheses;
	}
	
	/**
	 * @return la liste des libellés des laboratoires
	 */
	public String getCurrentLaboratoire() {
		
		NSArray<EOStructure> laboratoires = getCurrentThese().toContrat().structuresPartenairesForAssociation(
				factoryAssociation().laboratoireTheseAssociation(edc()));
		
		String listeLaboratoires = "";
		
		for (EOStructure laboratoire : laboratoires) {
			if (!listeLaboratoires.equals("")) {
				listeLaboratoires += ", ";
			}
			listeLaboratoires += laboratoire.lcStructure();
		}
		
		return listeLaboratoires;
	}
	
	/**
	 * @return le service DroitsService
	 */
	private DroitsService ds() {
		if (ds == null) {
			ds = DroitsService.creerNouvelleInstance(edc(), factoryAssociation(), session().applicationUser().getPersId());
		}
		return ds;
	}

	public void setDs(DroitsService ds) {
		this.ds = ds;
	}
	
	/**
	 * Display group theses
	 */
	public class ThesesDisplayGroupDelegate {
		/**
		 * @param group groupe
		 */
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		    @SuppressWarnings("unchecked")
            ERXDisplayGroup<EODoctorantThese> _groupe = (ERXDisplayGroup<EODoctorantThese>) group;
		    if (_groupe.selectedObject() != null) {
		    	setSelectedThese(_groupe.selectedObject());
		    } else {
		    	setSelectedThese(null);
		    }
		}
    }
	
	/**
	 * @return la page de la these selectionnee
	 */
	public WOActionResults afficherThese() {
		if (selectedThese != null) {
			session().setLibellePageIntermediaire("Enquêtes siredo");
			GestionDoctorant gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
			gd.setLeDoctorant(selectedThese.toDoctorant());
			gd.setEdcDoctorant(edc());
			return gd;
		} else {
			return null;
		}
	}
	
	public boolean isBoutonAfficherTheseDisabled() {
		return (getSelectedThese() == null);
	}
	
	public EODoctorantThese getCurrentThese() {
		return currentThese;
	}

	public void setCurrentThese(EODoctorantThese currentThese) {
		this.currentThese = currentThese;
	}
	
	public EODoctorantThese getSelectedThese() {
		return selectedThese;
	}

	public void setSelectedThese(EODoctorantThese selectedThese) {
		this.selectedThese = selectedThese;
	}
}