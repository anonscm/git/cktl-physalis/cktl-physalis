package org.cocktail.physalisw.serveur.components;

import java.util.Date;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;

/**
 * Tableau avec les informations sur la thèse
 */
public class DoctorantTheseTBLV extends PhysalisWOComponent {
	
    private static final long serialVersionUID = -4458706707411670456L;
	private EODoctorantThese doctorantThese;

	/**
	 * Contructeur
	 * @param context contexte
	 */
	public DoctorantTheseTBLV(WOContext context) {
		super(context);
	}
	
	/**
	 * @return l'identifiant de la table
	 */
	public String doctorantTheseTableViewId() {
		return getComponentId() + "_doctorantTheseTableViewId";
	}

	/**
	 * @return la thèse
	 */
	public EODoctorantThese doctorantThese() {
	    return doctorantThese;
    }

	public void setDoctorantThese(EODoctorantThese doctorantThese) {
	    this.doctorantThese = doctorantThese;
    }
	
	/**
	 * @return l'état actuel de la thèse
	 */
	public String etatThese() {
		if (doctorantThese().dateFinAnticipee() != null) {
			return "Abandonnée";
		} else if (doctorantThese().dateSoutenance() != null && doctorantThese().dateSoutenance().before(new Date())) {
			return "Soutenue";
		} else {
			return "En&nbsp;cours";
		}
	}
}