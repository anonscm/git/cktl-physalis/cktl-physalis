package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Pop up pour choisir le directeur de these
 */
public class DirecteurTheseHabiliteSelect extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 8903513940957016205L;
	private static final String BINDING_DIRECTEUR_THESE = "selectedDirecteurThese";
	private static final String BINDING_ECOLE_DOCTORALE = "ecoleDoctorale";
	private static final String BINDING_DATE_AUTORISATION = "dateAutorisation";
	private static final String BINDING_IS_ETABLISSEMENT_FRANCAIS = "isEtablissementFrancais";
	private final DgDelegate dgDelegate = new DgDelegate();
	private String nom;
	private String prenom;
	private WODisplayGroup dgDirecteurThese;
	private Boolean isDirecteurTheseHabilite = true;
	private Boolean isDirecteurTheseAutorise = false;
	private Boolean isDirecteurTheseCotutelle = false;
	
	private NSTimestamp dateAutorisation = null;

	public DirecteurTheseHabiliteSelect(WOContext context) {
		super(context);
	}
	
	public WOActionResults searchDirTheseHab() {
		NSMutableArray<EOQualifier> array = new NSMutableArray<EOQualifier>();
		EOQualifier qual = null;
		if ((nom != null && nom.length() > 2) || (nom != null && StringCtrl.containsIgnoreCase(nom, "*"))) {
			qual = new EOKeyValueQualifier(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.NOM_USUEL_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ nom + "*");
			array.add(qual);
		} else {
			session().addSimpleErrorMessage("Attention", "Vous devez saisir au moins 3 caractères du nom pour effectuer une recherche");
			return null;
		}

		if (prenom != null && prenom.length() > 2) {
			qual = new EOKeyValueQualifier(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY + "." + EOIndividu.PRENOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*"
					+ prenom + "*");
			array.add(qual);
		}

		// Le tri se fait par école doctorale
		qual = ERXQ.equals(EODirecteurTheseHabilite.TO_STRUCTURE_ED_FWKPERS_KEY, ecoleDoctorale());
		array.add(qual);

		if (array.count() > 0) {
			qual = new EOAndQualifier(array);
			dgDirecteurThese().setObjectArray(EODirecteurTheseHabilite.fetchDirecteurTheseHabilites(edc(), qual, EODirecteurTheseHabilite.ARRAY_SORT_NOM));
		} else {
			session().addSimpleInfoMessage("Attention", "Aucun directeur de thèse habilité ne correspond aux critères");
		}
		return null;
	}

	public WODisplayGroup dgDirecteurThese() {
		if (dgDirecteurThese == null) {
			dgDirecteurThese = new WODisplayGroup();
			dgDirecteurThese.setDelegate(dgDelegate);
			dgDirecteurThese.setSelectsFirstObjectAfterFetch(true);
		}
		return dgDirecteurThese;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {

		}
	}

	public WOActionResults selectionDirecteur() {

		if ((EODirecteurTheseHabilite) dgDirecteurThese.selectedObject() != null) {
			EODirecteurTheseHabilite directeurTheseHabilite = (EODirecteurTheseHabilite) dgDirecteurThese.selectedObject();
			setSelectedDirecteurThese(directeurTheseHabilite.toIndividuFwkpers());
			CktlAjaxWindow.close(context());
			reinitialiserRecherche();
		} else {
			session().addSimpleErrorMessage("Attention", "Cliquer sur un directeur de thèse habilité pour le sélectionner ou cliquer sur annuler");
		}
		return null;
	}
	
	public WOActionResults validerDirecteurAutorise() {
		
		int nbErreurs = 0;
		
		if (selectedDirecteurThese() == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez sélectionner une personne !");
			nbErreurs++;
		} 
		
		if (isDirecteurTheseAutorise() && dateAutorisation() == null) {
			session.addSimpleErrorMessage("Attention", "Vous devez indiquer la date d'autorisation !");
			nbErreurs++;
		} 
		
		if (nbErreurs == 0) {
			CktlAjaxWindow.close(context());
			reinitialiserRecherche();
		}
		return doNothing();
	}
	
	public WOActionResults annulerDirecteurAutorise() {
		edc().revert();
		CktlAjaxWindow.close(context());
		reinitialiserRecherche();

		return doNothing();
	}
	
	private void reinitialiserRecherche() {
	    setSelectedDirecteurThese(null);
	    setDateAutorisation(null);
	    setNom(null);
		setPrenom(null);
		dgDirecteurThese = null;
    }
	
	public WOActionResults onCreerPersonne() {

		if (selectedDirecteurThese() != null) {
			EOIndividu individu = selectedDirecteurThese();
			if (individu.toCivilite() == null) {
				NSArray<EOCivilite> civilites = EOCivilite.fetchAll(edc());
				individu.setToCiviliteRelationship((EOCivilite) civilites.objectAtIndex(0));
			}
		}

		if (selectedDirecteurThese().persIdCreation() == null) {
			selectedDirecteurThese().setPersIdCreation(session().applicationUser().getPersId());
		}
		if (selectedDirecteurThese().persIdModification() == null) {
			selectedDirecteurThese().setPersIdModification(session().applicationUser().getPersId());
		}
		
		CktlAjaxWindow.close(context());
		
		return doNothing();
	}
	
	public String aucAjoutDirecteurId() {
		return getComponentId() + "_aucAjoutDirecteurId";
	}

	public String aucDirTheseHabTVId() {
		return getComponentId() + "_aucDirTheseHabTVId";
	}
	
	public EOStructure ecoleDoctorale() {
		return (EOStructure) valueForBinding(BINDING_ECOLE_DOCTORALE);
	}

	public EOIndividu selectedDirecteurThese() {
		return (EOIndividu) valueForBinding(BINDING_DIRECTEUR_THESE);
	}

	public void setSelectedDirecteurThese(EOIndividu selectedDirecteurThese) {
		if (selectedDirecteurThese != null) {
			setValueForBinding(selectedDirecteurThese, BINDING_DIRECTEUR_THESE);
		}
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public NSTimestamp dateAutorisation() {
		return dateAutorisation;
	}
	
	public void setDateAutorisation(NSTimestamp dateAutorisation) {
		this.dateAutorisation = dateAutorisation;
		setValueForBinding(dateAutorisation, BINDING_DATE_AUTORISATION);
	}
	
	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
		setValueForBinding(isEtablissementFrancais, BINDING_IS_ETABLISSEMENT_FRANCAIS);
	}

	public Boolean isEtablissementFrancais() {
		return (Boolean) valueForBinding(BINDING_IS_ETABLISSEMENT_FRANCAIS);
	}

	public Boolean isDirecteurTheseHabilite() {
	    return isDirecteurTheseHabilite;
    }

	public void setIsDirecteurTheseHabilite(Boolean isDirecteurTheseHabilite) {
	    this.isDirecteurTheseHabilite = isDirecteurTheseHabilite;
	    setIsEtablissementFrancais(true);
	    reinitialiserRecherche();
    }
	
	public Boolean isDirecteurTheseAutorise() {
	    return isDirecteurTheseAutorise;
    }

	public void setIsDirecteurTheseAutorise(Boolean isDirecteurTheseAutorise) {
		this.isDirecteurTheseAutorise = isDirecteurTheseAutorise;
	    setIsEtablissementFrancais(true);
	    reinitialiserRecherche();
    }
	
	public Boolean getIsDirecteurTheseCotutelle() {
	    return isDirecteurTheseCotutelle;
    }

	public void setIsDirecteurTheseCotutelle(Boolean isDirecteurTheseCotutelle) {
	    this.isDirecteurTheseCotutelle = isDirecteurTheseCotutelle;
	    setIsEtablissementFrancais(!isDirecteurTheseCotutelle);
	    reinitialiserRecherche();
    }
	
	public Boolean wantResetDirecteurTheseSearch() {
		return true;
	}
	
	public void setWantResetDirecteurTheseSearch(Boolean wantResetDirecteurTheseSearch) {
		
	}
	
}
