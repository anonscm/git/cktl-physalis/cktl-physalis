package org.cocktail.physalisw.serveur.components;

import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DoctorantTheseService;
import org.cocktail.fwkcktlrecherche.server.metier.service.LaboratoireService;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Onglet Laboratoires
 */
public class TheseAffectation extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 7108319154517375589L;
	private static final String BINDING_LA_THESE = "laThese";
	private EORepartAssociation selectedRepartAssociation;
	private EORepartAssociation editingRepartAssociation;
	private Boolean isModifAffectation = false;
	private Boolean isAffectationEditable = false;
	private WODisplayGroup dgLabosAffectation;
	private EODoctorantThese laThese = null;
	private EOStructure editingLaboratoire = null;
	private NSTimestamp editingDateDebutAffectation;
	private NSTimestamp editingDateFinAffectation;
	private String editingCommentaireAffectation;
	private DoctorantTheseService doctorantTheseService;

	public TheseAffectation(WOContext context) {
		super(context);
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (group == dgLabosAffectation) {
				selectedRepartAssociation = (EORepartAssociation) dgLabosAffectation.selectedObject();
			}
		}
	}

	public WODisplayGroup dgLabosAffectation() {
		if (dgLabosAffectation == null) {
			dgLabosAffectation = new WODisplayGroup();
			dgLabosAffectation.setDelegate(new DgDelegate());
			dgLabosAffectation.setSortOrderings(new NSArray<EOSortOrdering>(EORepartAssociation.SORT_NOM_AFFICHAGE_ASC));
			dgLabosAffectation.setObjectArray(getDoctorantTheseService().repartAssociationLaboratoires(laThese));
		}
		return dgLabosAffectation;
	}

	public WOActionResults addLabo() {
		editingRepartAssociation = null;
		setIsAffectationEditable(true);
		setIsModifAffectation(false);
		editingLaboratoire = null;
		editingDateDebutAffectation = laThese().toContrat().dateDebut();
		editingDateFinAffectation = laThese().toContrat().dateFin();
		editingCommentaireAffectation = null;
		return doErxRedirect(context());
	}

	public WOActionResults modifyLabo() {
		if (selectedRepartAssociation != null) {
			setIsModifAffectation(true);
			setIsAffectationEditable(true);
			editingRepartAssociation = selectedRepartAssociation;
			editingLaboratoire = (EOStructure) selectedRepartAssociation().toStructure();
			editingDateDebutAffectation = selectedRepartAssociation().rasDOuverture();
			editingDateFinAffectation = selectedRepartAssociation().rasDFermeture();
			editingCommentaireAffectation = selectedRepartAssociation().rasCommentaire();
		}
		return doErxRedirect(context());
	}

	public WOActionResults validerAffectation() {
		Integer nbErreurs = 0;
		
		if (editingLaboratoire() == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez saisir un laboratoire.");
			nbErreurs++;
		}

		if (editingDateDebutAffectation == null || editingDateFinAffectation == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez saisir les dates de début et de fin d'encadrement.");
			nbErreurs++;
		} else {
			if (editingDateFinAffectation.before(editingDateDebutAffectation)) {
				session.addSimpleErrorMessage("Erreur", "La date de début doit être avant la date de fin.");
				nbErreurs++;
			} else {
				if (!laThese.isDateCoherente(editingDateDebutAffectation, editingDateFinAffectation)) {
					session.addSimpleErrorMessage("Erreur", "Les dates de début et de fin d'affectation doivent être comprises dans l'intervale de la thèse (du "
							+ new SimpleDateFormat("dd/MM/yyyy").format(laThese.toContrat().dateDebut()) + " au " 
							+ new SimpleDateFormat("dd/MM/yyyy").format(laThese.toContrat().dateFin()) + ").");
					nbErreurs++;
				}
			}
		}

		if (nbErreurs > 0) {
			return null;
		}
		if (!isModifAffectation()) {
			LaboratoireService laboratoireService = new LaboratoireService();
			laboratoireService.ajouterLaboratoire(laThese(), editingLaboratoire(), editingDateDebutAffectation(), 
					editingDateFinAffectation(), editingCommentaireAffectation(), getUtilisateurPersId(), Application.isModeDebug);
		} else {
			//modification du repart association entre le doctorant et le laboratoire
			editingRepartAssociation.setRasDOuverture(editingDateDebutAffectation());
			editingRepartAssociation.setRasDFermeture(editingDateFinAffectation());
			editingRepartAssociation.setRasCommentaire(editingCommentaireAffectation());
			
			//modification du repart association entre le laboratoire et le contrat
			NSArray<EORepartAssociation> repartAssociationsLabosContrat = laThese().toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().laboratoireTheseAssociation(edc()));
			for (EORepartAssociation raLaboContrat : repartAssociationsLabosContrat) {
				if (editingRepartAssociation.toStructure().equals(raLaboContrat.toPersonne())) {
					raLaboContrat.setRasDOuverture(editingDateDebutAffectation());
					raLaboContrat.setRasDFermeture(editingDateFinAffectation());
					raLaboContrat.setRasCommentaire(editingCommentaireAffectation());
				}
			}
		}

		try {
			edc().saveChanges();
			session().addSimpleSuccessMessage("Confirmation", "L'affectation est enregistrée");
			setIsModifAffectation(false);
			setIsAffectationEditable(false);
			dgLabosAffectation = null;
//			dgLabosAffectation().setObjectArray(laThese().toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().laboratoireTheseAssociation(edc())));
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	private Boolean hasIndividuRoleDoctorantLaboratoire(EOIndividu individu, EOStructure laboratoire, NSTimestamp editingDateDebutAffectation) {

		if (individu == null || laboratoire == null || editingDateDebutAffectation == null) {
			return false;
		}
		
		NSArray<EORepartAssociation> ras = individu.toRepartAssociations();
		
		for (EORepartAssociation ra : ras) {
			if (laboratoire.equals(ra.toStructure()) && factoryAssociation().doctorantAssociation(edc()).equals(ra.toAssociation())
					&& editingDateDebutAffectation.equals(ra.rasDOuverture())) {
				return true;
			}
		}
		
		return false;
	}

	public WOActionResults annulerAffectation() {
		edc().revert();
		setIsAffectationEditable(false);
		setIsModifAffectation(false);
		return null;
	}

	/**
	 * Supprime le laboratoire selectionné
	 * @return null
	 */
	public WOActionResults supprLabo() {
		
		try {
			getDoctorantTheseService().supprimerLaboratoire(laThese(), selectedRepartAssociation(), Application.isModeDebug);
			edc().saveChanges();
			session().addSimpleSuccessMessage("Suppression", "L'affectation au laboratoire a bien été supprimée.");
			selectedRepartAssociation = null;
			dgLabosAffectation = null;
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}

		return doNothing();
	}

	public EORepartAssociation selectedRepartAssociation() {
		return selectedRepartAssociation;
	}

	public void setSelectedRepartAssociation(EORepartAssociation selectedRepartAssociation) {
		this.selectedRepartAssociation = selectedRepartAssociation;
	}

	public EORepartAssociation editingRepartAssociation() {
		return editingRepartAssociation;
	}

	public void setEditingRepartAssociation(EORepartAssociation editingRepartAssociation) {
		this.editingRepartAssociation = editingRepartAssociation;
	}

	public Boolean isModifAffectation() {
		return isModifAffectation;
	}

	public void setIsModifAffectation(Boolean isModifAffectation) {
		this.isModifAffectation = isModifAffectation;
	}

	public void setIsAffectationEditable(Boolean isAffectationEditable) {
		this.isAffectationEditable = isAffectationEditable;
		disableOnglets(isAffectationEditable, false);
	}

	public Boolean isAffectationEditable() {
		return isAffectationEditable;
	}

	public boolean isAffectationSelectable() {
		return !isAffectationEditable;
	}

	public String aucLabosAffectationId() {
		return getComponentId() + "_aucLabosAffectation";
	}

	public String aucBoutonsLaboId() {
		return getComponentId() + "_aucBoutonsLaboId";
	}

	public NSArray<String> onUpdateLabo() {
		NSArray<String> containers = new NSMutableArray<String>();
		containers.add(aucLabosAffectationFormId());
		return containers;
	}

	public String aucLabosAffectationFormId() {
		return getComponentId() + "_aucLabosAffectationForm";
	}

	public EODoctorantThese laThese() {
		if (laThese != (EODoctorantThese) valueForBinding(BINDING_LA_THESE)) {
			laThese = (EODoctorantThese) valueForBinding(BINDING_LA_THESE);
			dgLabosAffectation = null;
		}
		return (EODoctorantThese) valueForBinding(BINDING_LA_THESE);
	}

	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, BINDING_LA_THESE);
	}

	public void setEditingLaboratoire(EOStructure editingLaboratoire) {
		this.editingLaboratoire = editingLaboratoire;
	}

	public EOStructure editingLaboratoire() {
		return editingLaboratoire;
	}

	public void setEditingDateDebutAffectation(NSTimestamp editingDateDebutAffectation) {
		this.editingDateDebutAffectation = editingDateDebutAffectation;
	}

	public NSTimestamp editingDateDebutAffectation() {
		return editingDateDebutAffectation;
	}

	public void setEditingDateFinAffectation(NSTimestamp editingDateFinAffectation) {
		this.editingDateFinAffectation = editingDateFinAffectation;
	}

	public NSTimestamp editingDateFinAffectation() {
		return editingDateFinAffectation;
	}

	public void setEditingCommentaireAffectation(String editingCommentaireAffectation) {
		this.editingCommentaireAffectation = editingCommentaireAffectation;
	}

	public String editingCommentaireAffectation() {
		return editingCommentaireAffectation;
	}

	public DoctorantTheseService getDoctorantTheseService() {
		if (doctorantTheseService == null) {
			doctorantTheseService = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
		}
		return doctorantTheseService;
	}

	public void setDoctorantTheseService(DoctorantTheseService doctorantTheseService) {
		this.doctorantTheseService = doctorantTheseService;
	}
	
}