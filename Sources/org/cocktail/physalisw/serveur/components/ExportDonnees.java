package org.cocktail.physalisw.serveur.components;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinanceur;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;
import er.extensions.eof.ERXBatchFetchUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

/**
 * Classe affichant la page export de données
 */
public class ExportDonnees extends PhysalisWOComponent {

	private static final long serialVersionUID = -1690118571878853233L;
	
	private static final String REPORTS_FOLDER = "Reports";
	private static final String REPORT_EXPORT_DOCTORANTS = REPORTS_FOLDER + File.separator + "ExportDoctorants" + File.separator + "ExportDoctorants.xls";
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	private static final int WIDTH_BOUTTON_FILTRE = 1300;

	private NSArray<EODoctorantThese> theses;
	private NSArray<EODoctorantThese> thesesTriees;
	private NSArray<EOAssociation> lesAssociationsCotutelle;
	private List<String> listeNbMaxAvenants;

	/**
	 * Constructeur
	 * @param context contexte
	 */
	public ExportDonnees(WOContext context) {
        super(context);
    }
	
	public NSArray<EODoctorantThese> getThesesTriees() {
	    return thesesTriees;
    }

	public void setThesesTriees(NSArray<EODoctorantThese> thesesTriees) {
	    this.thesesTriees = thesesTriees;
    }
	
	/**
	 * Retourne toutes les theses
	 * @return Toutes les theses 
	 */
	public NSArray<EODoctorantThese> getTheses() {
		if (theses == null) {
			ERXFetchSpecification<EODoctorantThese> fetchSpecification = new ERXFetchSpecification<EODoctorantThese>(EODoctorantThese.ENTITY_NAME);
			NSArray<String> keyPaths = new NSMutableArray<String>();
			keyPaths.add(EODoctorantThese.TO_DOCTORANT.key());
			keyPaths.add(EODoctorantThese.TO_CONTRAT.key());
			keyPaths.add(
				EODoctorantThese.TO_CONTRAT
					.dot(Contrat.GROUPE_PARTENAIRE)
					.dot(EOStructure.TO_REPART_ASSOCIATIONS_ELTS)
					.key());
			keyPaths.add(
				EODoctorantThese.TO_CONTRAT
					.dot(Contrat.AVENANTS)
					.key());
			keyPaths.add(
					EODoctorantThese.TO_CONTRAT
						.dot(Contrat.CONTRAT_PARTENAIRES)
						.key());
			keyPaths.add(EODoctorantThese.TO_DIRECTEURS_THESE.key());
			keyPaths.add(
				EODoctorantThese.TO_DIRECTEURS_THESE
					.dot(EODirecteurThese.TO_CONTRAT_PARTENAIRE_CW)
					.key());
			
			theses = EODoctorantThese.fetchAll(edc(), new NSArray<EOSortOrdering>(EODoctorantThese.SORT_NOM_DOCTORANT));
			fetchSpecification.setPrefetchingRelationshipKeyPaths(keyPaths);
			ERXBatchFetchUtilities.batchFetch(theses, keyPaths);
		}
		return theses;
	}

	/**
	 * Generation du fichier excel
	 * @return le fichier excel
	 */
    public WOActionResults exportExcel() {
    	
    	Map<String, Object> beans = generationDonneesExport();
        
        XLSTransformer transformer = new XLSTransformer();
        try {
        	ERXResourceManager erm = (ERXResourceManager) ERXApplication.application().resourceManager();
        	URL url = erm.pathURLForResourceNamed(REPORT_EXPORT_DOCTORANTS, "app", null);
            InputStream is = new BufferedInputStream(url.openStream());
            Workbook workbook = transformer.transformXLS(is, beans);
            
            // width auto pour les colonnes
            Sheet firstSheet = workbook.getSheetAt(0);
            Row firstRow = firstSheet.getRow(0);
            for (int i = 0; i < firstRow.getLastCellNum(); i++) {
            	firstSheet.autoSizeColumn(i);
            	try {
            		firstSheet.setColumnWidth(i, firstSheet.getColumnWidth(i) + WIDTH_BOUTTON_FILTRE);
	            } catch (final Exception e) {
	            }
            }
            
            // téléchargement du fichier généré
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            workbook.write(os);
            byte[] bytes = os.toByteArray();
    		CktlDataResponse resp = new CktlDataResponse();
			resp.setHeader("maxage=1", "Cache-Control");
			resp.setHeader("public", "Pragma");
			resp.setContent(bytes, "application/xls");
			resp.setFileName(fileName());
			return resp.generateResponse();
        } catch (Exception e) {
	        e.printStackTrace();
	        session().addSimpleErrorMessage("Erreur", "Erreur lors de la génération du fichier : " + e);
        }
    	
    	return doNothing();
    }
	
    /**
     * @return Nom du fichier excel généré
     */
    public String fileName() {
    	return "Physalis_doctorants_" + DateCtrl.currentDateHeureVerbeux() + ".xls";
    }
    
	@SuppressWarnings("unchecked")
    private Map<String, Object> generationDonneesExport() {
		List<ExportDoctorant> exportDoctorants = new ArrayList<ExportDoctorant>();

		for (EODoctorantThese these : getThesesTriees()) {
			ExportDoctorant exportDoctorant = new ExportDoctorant();
			if (these.toDoctorant().toIndividuFwkpers().toCivilite().sexeOnp().equals(EOCivilite.SEXE_FEMININ)) {
				exportDoctorant.setSexe("F");
			} else if (these.toDoctorant().toIndividuFwkpers().toCivilite().sexeOnp().equals(EOCivilite.SEXE_MASCULIN)) {
				exportDoctorant.setSexe("H");
			}
			exportDoctorant.setNomEtPrenom(these.toDoctorant().toIndividuFwkpers().getNomAndPrenom());
			if (these.toDoctorant().toIndividuFwkpers().toPaysNationalite() != null) {
				exportDoctorant.setNationalite(these.toDoctorant().toIndividuFwkpers().toPaysNationalite().lNationalite());
			}
			exportDoctorant.setResume(these.resumeThese());
				
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
			exportDoctorant.setDateDebutThese(simpleDateFormat.format(these.dateDeb()));
			exportDoctorant.setDateFinThese(simpleDateFormat.format(these.dateFin()));
			if (these.dateSoutenance() != null) {
				exportDoctorant.setDateReelleSoutenance(simpleDateFormat.format(these.dateSoutenance()));
			}
			if (these.toDoctorant().toDoctorantOrigine() != null) {
				exportDoctorant.setDiplomeOrigine(these.toDoctorant().toDoctorantOrigine().libOrigine());
			}	
			if (these.toDoctorant().toPaysDiplome() != null) {
				exportDoctorant.setPaysDiplomeOrigine(these.toDoctorant().toPaysDiplome().llPays());
			}
			if (these.toDoctorant().toEtablissementDiplomeOrigine() != null) {
				exportDoctorant.setEtablissementFrancaisDiplomeOrigine(these.toDoctorant().toEtablissementDiplomeOrigine().llRne());
			}		
			if (these.ecoleDoctorale() != null) {
				exportDoctorant.setEcoleDoctorale(these.ecoleDoctorale().lcStructure());
			}
			exportDoctorant.setLaboratoires(getListeNomsLaboratoires(these));
			exportDoctorant.setNomEtPrenomDirecteurs(getListeNomsDirecteurs(these));
			exportDoctorant.setNomEtPrenomEncadrants(getListeNomsEncadrants(these));
			generationDonneesCotutelle(these, exportDoctorant);
			if (these.toContrat().avenantZero().domaineScientifique() != null) {
				exportDoctorant.setDomaineScientifique(these.toContrat().avenantZero().domaineScientifique().dsCodePlusLibelle());
			}
			
			NSArray<Avenant> avenants = (NSArray<Avenant>) these.toContrat().avenantsDontInitialNonSupprimesEtValides();
			for (Avenant avenant : avenants) {
				exportDoctorant.addFinancement(generationExportFinancement(avenant));
			}
			
			exportDoctorants.add(exportDoctorant);
		}
    	
        Map<String, Object> beans = new HashMap<String, Object>();
        beans.put("doctorants", exportDoctorants);
        beans.put("listeNbMaxAvenants", listeNbMaxAvenants());
	    return beans;
    }

	private void generationDonneesCotutelle(EODoctorantThese these, ExportDoctorant exportDoctorant) {
		if (!these.isCotutelle()) {
			exportDoctorant.setIsCotutelle("Non");
			return;
		}
		
    	exportDoctorant.setIsCotutelle("Oui");
    	NSArray<EORepartAssociation> raEtablissementsCotutelle = listeRepartAssociationEtablissementsCotutelle(these);
    	if (raEtablissementsCotutelle.size() > 0) {
    		EORepartAssociation raEtablissementCotutelle = raEtablissementsCotutelle.get(0);
    		if (raEtablissementCotutelle != null) {
    			exportDoctorant.setEtablissementCotutelle(raEtablissementCotutelle.toPersonne().getNomCompletAffichage());
    			if (raEtablissementCotutelle.toPersonne().getAdressePrincipale() != null 
    					&& raEtablissementCotutelle.toPersonne().getAdressePrincipale().toPays() != null) {
    				exportDoctorant.setPaysCotutelle(raEtablissementCotutelle.toPersonne().getAdressePrincipale().toPays().llPays());
    			}
    		}
    	}
    }

	private ExportFinancement generationExportFinancement(Avenant avenant) {
	    EODoctorantFinancement financement = getCurrentFinancement(avenant);
	    ExportFinancement exportFinancement = new ExportFinancement();
	    if (financement != null && financement.isFinancementSaisi()) {
	    	if (financement.isFinancement()) {
	    		exportFinancement.setIsFinance("Oui");
	    		exportFinancement.setTypeContrat(financement.toTheseTypeContrat().libTypeContrat());
	    		if (financement.toContratPartenaire() != null) {
	    			exportFinancement.setEmployeur(financement.toContratPartenaire().partenaire().getNomCompletAffichage());
	    		}
	    		exportFinancement.setFinanceurs(getListeNomFinanceurs(avenant));
	    	} else {
	    		exportFinancement.setIsFinance("Non");
	    	}
	    } else {
	    	exportFinancement.setIsFinance("Non renseigné");
	    }
	    return exportFinancement;
    }
    
    /**
     * @param currentAvenant avenant
     * @return le financement de l'avenant
     */
    private EODoctorantFinancement getCurrentFinancement(Avenant currentAvenant) {
		EOQualifier qual = ERXQ.is(EODoctorantFinancement.TO_AVENANT_KEY, currentAvenant);
		NSArray<EODoctorantFinancement> financements = EODoctorantFinancement.fetchDoctorantFinancements(edc(), qual, null);
		if (!financements.isEmpty()) {
			return financements.get(0);
		} 
		return null;
	}
	
	/**
	 * @param currentAvenant l'avenant
	 * @return Liste des EODoctorantFinanceur de l'avenant
	 */
    private NSArray<EODoctorantFinanceur> listeFinanceurs(Avenant currentAvenant) {
		EOQualifier qual = ERXQ.equals(EODoctorantFinanceur.TO_DOCTORANT_FINANCEMENT_KEY, getCurrentFinancement(currentAvenant));
		return EODoctorantFinanceur.fetchDoctorantFinanceurs(edc(), qual, null);
	}
	
	/**
	 * @param currentAvenant l'avenant
	 * @return un string avec les nom des financeurs separés par une virgule
	 */
    private String getListeNomFinanceurs(Avenant currentAvenant) {
		
		NSArray<EODoctorantFinanceur> financeurs = listeFinanceurs(currentAvenant);
		
		String listeNoms = "";
		
		for (EODoctorantFinanceur financeur : financeurs) {
			if (!listeNoms.isEmpty()) {
				listeNoms += ", ";
			}
			listeNoms += financeur.toContratPartenaire().partenaire().getNomCompletAffichage();
		}
		
		return listeNoms;
	}
    
	/**
	 * @param these la these
	 * @return les EORepartAssociation des etablissements de cotutelle de la these
	 */
    private NSArray<EORepartAssociation> listeRepartAssociationEtablissementsCotutelle(EODoctorantThese these) {
		if (lesAssociationsCotutelle == null) {
			EOQualifier qual = ERXQ.equals(EOAssociation.ASS_CODE_KEY, FactoryAssociation.shared().etablissementCotutelleAssociation(edc()).assCode());
			lesAssociationsCotutelle = EOAssociation.fetchAll(edc(), qual);
		}
		return these.toContrat().repartAssociationPartenairesForAssociations(lesAssociationsCotutelle);
	}
    
    /**
     * @param these la these
     * @return un string avec les nom des laboratoires separés par une virgule
     */
    private String getListeNomsLaboratoires(EODoctorantThese these) {
		
		NSArray<EOStructure> laboratoires = these.toContrat().structuresPartenairesForAssociation(
				FactoryAssociation.shared().laboratoireTheseAssociation(edc()));
		
		String listeLaboratoires = "";
		
		for (EOStructure laboratoire : laboratoires) {
			if (!listeLaboratoires.equals("")) {
				listeLaboratoires += ", ";
			}
			listeLaboratoires += laboratoire.lcStructure();
		}
		
		return listeLaboratoires;
	}
	
	/**
	 * @param these la these
	 * @return un string avec les nom des directeurs separés par une virgule
	 */
    private String getListeNomsDirecteurs(EODoctorantThese these) {
		
		NSArray<EODirecteurThese> encadrement = these.toDirecteursThese();
		NSArray<EODirecteurThese> directeurs = new NSMutableArray<EODirecteurThese>();
		
		for (EODirecteurThese encadrant : encadrement) {
			if (FactoryAssociation.shared().directeurTheseAssociation(edc()).equals(encadrant.roleDirecteur())) {
				directeurs.add(encadrant);
			}
		}
		
		String listeNoms = "";
		
		for (EODirecteurThese directeur : directeurs) {
			if (!listeNoms.isEmpty()) {
				listeNoms += ", ";
			}
			listeNoms += directeur.toIndividuDirecteur().getNomAndPrenom();
		}
		
		return listeNoms;
	}
	
	/**
	 * @param these la these
	 * @return un string avec les nom des encadrants separés par une virgule
	 */
    private String getListeNomsEncadrants(EODoctorantThese these) {
		
		NSArray<EODirecteurThese> encadrement = these.toDirecteursThese();
		NSArray<EODirecteurThese> encadrants = new NSMutableArray<EODirecteurThese>();
		
		for (EODirecteurThese encadrant : encadrement) {
			if (FactoryAssociation.shared().coEncadrantTheseAssociation(edc()).equals(encadrant.roleDirecteur())) {
				encadrants.add(encadrant);
			}
		}
		
		String listeNoms = "";
		
		for (EODirecteurThese encadrant : encadrants) {
			if (!listeNoms.isEmpty()) {
				listeNoms += ", ";
			}
			listeNoms += encadrant.toIndividuDirecteur().getNomAndPrenom();
		}
		
		return listeNoms;
	}
	
	/**
	 * @return le nombre maximal d'avenant pour les theses
	 */
    private int nbMaxAvenants() {
		int nbMaxAvenants = 0;
		int nb = 0;
		for (EODoctorantThese these : getThesesTriees()) {
			nb = these.toContrat().avenantsDontInitialNonSupprimesEtValides().count();
			if (nb > nbMaxAvenants) {
				nbMaxAvenants = nb;
			}
		}
		return nbMaxAvenants;
	}
	
	/**
	 * @return une liste avec les types de financements pour l'affichage ds le fichier excel
	 */
    private List<String> listeNbMaxAvenants() {
		if (listeNbMaxAvenants == null) {
			listeNbMaxAvenants = new ArrayList<String>();
			int _nbMaxAvenants = nbMaxAvenants();
			for (int j = 0; j < _nbMaxAvenants; j++) {
				if (j == 0) {
					listeNbMaxAvenants.add("Financement initial");
				} else {
					listeNbMaxAvenants.add("Financement complémentaire");
				}
			}
		}
		return listeNbMaxAvenants;
	}

	/**
	 * Classe pour l'export des financements
	 */
	public class ExportFinancement {
		
		private String isFinance;
		private String typeContrat;
		private String employeur;
		private String financeurs;
		
		/**
		 * Constructeur
		 */
		public ExportFinancement() {
	        super();
        }
		public String getIsFinance() {
			return isFinance;
		}
		public void setIsFinance(String isFinance) {
			this.isFinance = isFinance;
		}
		public String getTypeContrat() {
			return typeContrat;
		}
		public void setTypeContrat(String typeContrat) {
			this.typeContrat = typeContrat;
		}
		public String getEmployeur() {
			return employeur;
		}
		public void setEmployeur(String employeur) {
			this.employeur = employeur;
		}
		public String getFinanceurs() {
			return financeurs;
		}
		public void setFinanceurs(String financeurs) {
			this.financeurs = financeurs;
		}
		
	}
	
	/**
	 * Classe pour l'export des doctorants
	 */
	public class ExportDoctorant {

		private String sexe;
		private String nomEtPrenom;
		private String nationalite;
		private String resume;
		private String ecoleDoctorale;
		private String dateDebutThese;
		private String dateFinThese;
		private String dateReelleSoutenance;
		private String paysDiplomeOrigine;
		private String diplomeOrigine;
		private String etablissementFrancaisDiplomeOrigine;
		private String nomEtPrenomDirecteurs;
		private String nomEtPrenomEncadrants;
		private String laboratoires;
		private String isCotutelle;
		private String etablissementCotutelle;
		private String paysCotutelle;
		private String domaineScientifique;
		private List<ExportFinancement> financements;
		
		/**
		 * Constructeur
		 */
		public ExportDoctorant() {
			super();
			financements = new ArrayList<ExportFinancement>();
		}

		public String getResume() {
	        return resume;
        }

		public void setResume(String resume) {
	        this.resume = resume;
        }

		public String getNomEtPrenom() {
	        return nomEtPrenom.toUpperCase();
        }

		public void setNomEtPrenom(String nomEtPrenom) {
	        this.nomEtPrenom = nomEtPrenom;
        }

		public String getEcoleDoctorale() {
	        return ecoleDoctorale;
        }

		public void setEcoleDoctorale(String ecoleDoctorale) {
	        this.ecoleDoctorale = ecoleDoctorale;
        }

		public String getDateDebutThese() {
	        return dateDebutThese;
        }

		public void setDateDebutThese(String dateDebutThese) {
	        this.dateDebutThese = dateDebutThese;
        }

		public String getDateFinThese() {
	        return dateFinThese;
        }

		public void setDateFinThese(String dateFinThese) {
	        this.dateFinThese = dateFinThese;
        }

		public String getDateReelleSoutenance() {
	        return dateReelleSoutenance;
        }

		public void setDateReelleSoutenance(String dateReelleSoutenance) {
	        this.dateReelleSoutenance = dateReelleSoutenance;
        }

		public String getLaboratoires() {
	        return laboratoires;
        }

		public void setLaboratoires(String laboratoires) {
	        this.laboratoires = laboratoires;
        }

		public String getNomEtPrenomDirecteurs() {
			return nomEtPrenomDirecteurs;
		}

		public void setNomEtPrenomDirecteurs(String nomEtPrenomDirecteurs) {
			this.nomEtPrenomDirecteurs = nomEtPrenomDirecteurs;
		}

		public String getNomEtPrenomEncadrants() {
			return nomEtPrenomEncadrants;
		}

		public void setNomEtPrenomEncadrants(String nomEtPrenomEncadrants) {
			this.nomEtPrenomEncadrants = nomEtPrenomEncadrants;
		}

		public String getIsCotutelle() {
			return isCotutelle;
		}

		public void setIsCotutelle(String isCotutelle) {
			this.isCotutelle = isCotutelle;
		}

		public String getEtablissementCotutelle() {
			return etablissementCotutelle;
		}

		public void setEtablissementCotutelle(String etablissementCotutelle) {
			this.etablissementCotutelle = etablissementCotutelle;
		}

		public String getPaysCotutelle() {
			return paysCotutelle;
		}

		public void setPaysCotutelle(String paysCotutelle) {
			this.paysCotutelle = paysCotutelle;
		}

		public String getDomaineScientifique() {
	        return domaineScientifique;
        }

		public void setDomaineScientifique(String domaineScientifique) {
	        this.domaineScientifique = domaineScientifique;
        }

		public List<ExportFinancement> getFinancements() {
			return financements;
		}

		public void setFinancements(List<ExportFinancement> financements) {
			this.financements = financements;
		}
		
		/**
		 * Ajoute un financement
		 * @param financement financement
		 */
		public void addFinancement(ExportFinancement financement) {
			 financements.add(financement);
	    }

		public String getSexe() {
			return sexe;
		}

		public void setSexe(String sexe) {
			this.sexe = sexe;
		}

		public String getPaysDiplomeOrigine() {
			return paysDiplomeOrigine;
		}

		public void setPaysDiplomeOrigine(String paysDiplomeOrigine) {
			this.paysDiplomeOrigine = paysDiplomeOrigine;
		}

		public String getEtablissementFrancaisDiplomeOrigine() {
			return etablissementFrancaisDiplomeOrigine;
		}

		public void setEtablissementFrancaisDiplomeOrigine(
				String etablissementFrancaisDiplomeOrigine) {
			this.etablissementFrancaisDiplomeOrigine = etablissementFrancaisDiplomeOrigine;
		}

		public String getDiplomeOrigine() {
			return diplomeOrigine;
		}

		public void setDiplomeOrigine(String diplomeOrigine) {
			this.diplomeOrigine = diplomeOrigine;
		}

		public String getNationalite() {
			return nationalite;
		}

		public void setNationalite(String nationalite) {
			this.nationalite = nationalite;
		}
		
	}
}