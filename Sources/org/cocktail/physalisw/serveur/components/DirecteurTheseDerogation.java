package org.cocktail.physalisw.serveur.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EODatabaseDataSource;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

public class DirecteurTheseDerogation extends PhysalisWOComponent {
    
	private ERXDisplayGroup<EODirecteurThese> directeursDisplayGroup;
	private ERXDatabaseDataSource directeursDataSource;
	private ERXFetchSpecification<EODirecteurThese> directeursFetchSpec;
	
	private EODirecteurThese currentDirecteurThese;
	
	public DirecteurTheseDerogation(WOContext context) {
        super(context);
    }
    
    public String getDirecteursTableViewId() {
    	return getComponentId() + "_directeursTableView";
    }

	public ERXDisplayGroup<EODirecteurThese> getDirecteursDisplayGroup() {
		if (directeursDisplayGroup == null) {
			directeursDisplayGroup = new ERXDisplayGroup<EODirecteurThese>();
			directeursDisplayGroup.setDataSource(getDirecteursDataSource());
			directeursDisplayGroup.fetch();
		}
		return directeursDisplayGroup;
	}

	public ERXDatabaseDataSource getDirecteursDataSource() {
		if (directeursDataSource == null) {
			directeursDataSource = new ERXDatabaseDataSource(edc(), EODirecteurThese.ENTITY_NAME);
			directeursDataSource.setFetchSpecification(getDirecteursFetchSpec());
		}
		return directeursDataSource;
	}

	public ERXFetchSpecification<EODirecteurThese> getDirecteursFetchSpec() {
		if (directeursFetchSpec == null) {
			EOQualifier qualifier = EODirecteurThese.DATE_AUTORISATION.isNotNull();
			ERXSortOrderings sortOrderings = EODirecteurThese.DATE_AUTORISATION.ascs();
			directeursFetchSpec = new ERXFetchSpecification<EODirecteurThese>(EODirecteurThese.ENTITY_NAME, qualifier, sortOrderings);
		}
		return directeursFetchSpec;
	}

	public EODirecteurThese getCurrentDirecteurThese() {
		return currentDirecteurThese;
	}

	public void setCurrentDirecteurThese(EODirecteurThese currentDirecteurThese) {
		this.currentDirecteurThese = currentDirecteurThese;
	}

    
    
}