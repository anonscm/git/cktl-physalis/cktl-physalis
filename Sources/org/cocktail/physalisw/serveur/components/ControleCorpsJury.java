package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DroitsService;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * Composant affichant une table avec les membres de jury ayant un corps different de celui indiqué lors de l'enregistrement dans la partie soutenance
 */
public class ControleCorpsJury extends PhysalisWOComponent {
	
    private static final long serialVersionUID = -6940778333415723689L;

	private static final Integer NUMBER_OF_OBJECTS_PER_BATCH = 15;
	
	private ERXDisplayGroup<EOMembreJuryThese> displayGroupMembreAvecCorpsPasAJour;
    private NSMutableArray<EOMembreJuryThese> membresJury;
    private EOMembreJuryThese currentMembre;
    private EOMembreJuryThese selectedMembre;
	private DroitsService ds;
    
	/**
	 * Constructeur
	 * @param context contexte
	 */
    public ControleCorpsJury(WOContext context) {
        super(context);
    }
    
    public String getMembresJuryTableViewId() {
    	return getComponentId() + "_membresJuryTableViewId";
    }
    
    public String getContainerControleJuryId() {
    	return getComponentId() + "_containerControleJuryId";
    }
    
    public String getBoutonsContainerId() { 
    	return getComponentId() + "_boutonsContainerId";
    }
    
    /**
     * @return displaygroup
     */
    public ERXDisplayGroup<EOMembreJuryThese> dgMembre() {
		if (displayGroupMembreAvecCorpsPasAJour == null) {
			displayGroupMembreAvecCorpsPasAJour = new ERXDisplayGroup<EOMembreJuryThese>();
			displayGroupMembreAvecCorpsPasAJour.setDelegate(new DgDelegate());
			displayGroupMembreAvecCorpsPasAJour.setObjectArray(getMembresJuryAvecCorpsPasAJour());
			displayGroupMembreAvecCorpsPasAJour.setSelectsFirstObjectAfterFetch(true);
			displayGroupMembreAvecCorpsPasAJour.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
		}
		
		return displayGroupMembreAvecCorpsPasAJour;
    }
    
    /**
     * Delegate
     */
	public class DgDelegate {
		/**
		 * Action lancée lors d'un changement de selection dans la table
		 * @param group group
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			@SuppressWarnings("unchecked")
            ERXDisplayGroup<EOMembreJuryThese> _displayGroup = (ERXDisplayGroup<EOMembreJuryThese>) group;
			if (_displayGroup.selectedObject() != null) {
				setSelectedMembre(_displayGroup.selectedObject());
			}
		}
	}
    
	/**
	 * Change le corps du membre du jury avec le corps calculé par getCorpsReelMembre()
	 * @return null
	 */
    public WOActionResults changerCorpsJury() {
    	if (getSelectedMembre() == null) {
    		session().addSimpleErrorMessage("Erreur", "Veuillez sélectionner un membre de jury");
    		return doNothing();
    	}
    	getSelectedMembre().setToCorpsRelationship(getCorpsReelMembre(getSelectedMembre()));
    	edc().saveChanges();
    	session.addSimpleSuccessMessage("Succès", "Le corps de jury a bien été modifié");
    	membresJury.remove(getSelectedMembre());
    	displayGroupMembreAvecCorpsPasAJour.setObjectArray(membresJury);
    	displayGroupMembreAvecCorpsPasAJour.updateDisplayedObjects();
    	return doNothing();
    }
    
    /**
     * @return le corps du membre du jury courant
     */
    public EOCorps getCorpsReelCurrentMembre() {
    	return getCorpsReelMembre(getCurrentMembre());
    }
    
    /**
     * 
     * @param membreJury le membre de jury dont on cherche le corps
     * @return le corps du membre du jury
     */
    public EOCorps getCorpsReelMembre(EOMembreJuryThese membreJury) {
    	IndividusGradesService individusGradesService = IndividusGradesService.creerNouvelleInstance(edc());
    	EOIndividu individu = (EOIndividu) membreJury.toContratPartenaire().partenaire();
    	if (individu != null) {
	    	EOGrade gradeReel = individusGradesService.gradePourIndividu(individu, new NSTimestamp());
			if (gradeReel != null) {
				return gradeReel.toCorps();
			}
    	}
		return null;
    }

	/**
	 * @return les membres de jury avec un corps different de celui enregistré dans la thèse
	 */
    public NSMutableArray<EOMembreJuryThese> getMembresJuryAvecCorpsPasAJour() {
    	if (membresJury == null) {
	    	NSArray<EOMembreJuryThese> mjt = EOMembreJuryThese.fetchAll(edc());
	    	membresJury = new NSMutableArray<EOMembreJuryThese>();
	    	IndividusGradesService individusGradesService = IndividusGradesService.creerNouvelleInstance(edc());
	    	
	    	for (EOMembreJuryThese membre : mjt) {
	    		EOIndividu individu = (EOIndividu) membre.toContratPartenaire().partenaire();
				if (individu != null) {
					EOGrade gradeReel = individusGradesService.gradePourIndividu(individu, new NSTimestamp());
					if (gradeReel != null && !gradeReel.toCorps().equals(membre.toCorps())) {
						membresJury.add(membre);
					}
				}
	    	}
    	}
    	return membresJury;
    }
    
    /**
	 * @return la page de la these selectionnee
	 */
	public WOActionResults afficherThese() {
		if (getSelectedMembre() != null) {
			session().setLibellePageIntermediaire("Gestion des données");
			GestionDoctorant gd = (GestionDoctorant) pageWithName(GestionDoctorant.class.getName());
			gd.setLeDoctorant(getSelectedMembre().toDoctorantThese().toDoctorant());
			gd.setEdcDoctorant(edc());
			return gd;
		} else {
			return null;
		}
	}
    
    /**
	 * @return le service DroitsService
	 */
	public DroitsService ds() {
		if (ds == null) {
			ds = DroitsService.creerNouvelleInstance(edc(), factoryAssociation(), session().applicationUser().getPersId());
		}
		return ds;
	}

	public void setDs(DroitsService ds) {
		this.ds = ds;
	}
	
    public boolean isBoutonsDisabled() {
		return (getSelectedMembre() == null);
	}
    
    public EOMembreJuryThese getCurrentMembre() {
	    return currentMembre;
    }

	public void setCurrentMembre(EOMembreJuryThese currentMembre) {
	    this.currentMembre = currentMembre;
    }

	public EOMembreJuryThese getSelectedMembre() {
	    return selectedMembre;
    }

	public void setSelectedMembre(EOMembreJuryThese selectedMembre) {
	    this.selectedMembre = selectedMembre;
    }
}