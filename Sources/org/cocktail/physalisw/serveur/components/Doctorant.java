package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantOrigine;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantStatut;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class Doctorant extends PhysalisWOComponent {

    private static final long serialVersionUID = 1576500483996783509L;
	private final String B_leDoctorant = "leDoctorant";
	private final String BINDING_IS_MODIFICATION = "isModification";
	public EODoctorantOrigine itemDoctorantOrigine, selectedDoctorantOrigine;
	public EODoctorantStatut itemDoctorantStatut, selectedDoctorantStatut;
	private boolean isModification = false;
	private EOEditingContext edcDoctorant = null;
	private EODoctorant editingDoctorant;
	private EODoctorant leDoctorant = null;
	// permet de savoir si un composant est en mode editing lors de la
	// sauvegarde de l'ecran
	private Boolean isEditing = false;
	
	public Doctorant(WOContext context) {
		super(context);
	}
	
	public String supprimerDoctorantWindowId() {
		return getComponentId() + "_supprimerDoctorantWindowId";
	}

	public NSArray<EODoctorantOrigine> listeDoctorantOrigines() {
		return EODoctorantOrigine.fetchAllDoctorantOrigines(edcDoctorant(), new NSArray<EOSortOrdering>(EODoctorantOrigine.SORT_ORIGINE_LIB));
	}

	public NSArray<EODoctorantStatut> listeDoctorantStatuts() {
		return EODoctorantStatut.fetchAllDoctorantStatuts(edcDoctorant(), new NSArray<EOSortOrdering>(EODoctorantStatut.SORT_STATUT_LIB));
	}

	public WOActionResults modifierFicheDoctorant() {
		setIsModification(true);
		setIsEditing(false);
		editingDoctorant = leDoctorant();
		if (editingDoctorant.toIndividuFwkpers().toPaysNaissance() == null) {
			editingDoctorant.toIndividuFwkpers().setToPaysNaissanceRelationship(EOPays.getPaysDefaut(edcDoctorant()));
		}
		if (editingDoctorant.toIndividuFwkpers().toPaysNationalite() == null) {
			editingDoctorant.toIndividuFwkpers().setToPaysNationaliteRelationship(EOPays.getPaysDefaut(edcDoctorant()));
		}
		editingDoctorant.toIndividuFwkpers().setPersIdModification(session().applicationUser().getPersId());
		return null;
	}
	
	public WOActionResults supprimerDoctorant() {
		if (leDoctorant() != null) {
			if (leDoctorant().toDoctorantTheses().count() > 0) {
				if (leDoctorant().toDoctorantTheses().count() == 1) {
					session().addSimpleErrorMessage("Erreur", "Veuillez supprimer la thèse du doctorant pour pouvoir le supprimer.");
				} else {
					session().addSimpleErrorMessage("Erreur", "Veuillez supprimer les thèses du doctorant pour pouvoir le supprimer.");
				}
				return doNothing();
			}
			try {
				edc().deleteObject(leDoctorant());
				edc().saveChanges();
				session().addSimpleSuccessMessage("Succès", "Le doctorant a été supprimé.");
				return (Accueil) pageWithName(Accueil.class.getName());
			} catch (Exception e) {
				session().addSimpleErrorMessage("Erreur", e.getMessage());
				e.printStackTrace();
			}
		}
		
		return doNothing();
	}

	public WOActionResults enregistrer() {
		Integer nbErreur = 0;
		if (isEditing()) {
			session.addSimpleErrorMessage("Attention", "Vous n'avez pas validé toutes les parties du formulaires.");
			nbErreur++;
		}
		
		if (editingDoctorant().toIndividuFwkpers().dNaissance() == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez saisir la date de naissance de cette personne.");
			nbErreur++;
		}
		
		if (editingDoctorant().anneeM2() != null && editingDoctorant().anneeInscThese() != null) {
			if (editingDoctorant().anneeM2() > editingDoctorant().anneeInscThese()) {
				session.addSimpleErrorMessage("Erreur", "L'année d'obtention du M2 doit être antérieure à l'année d'inscription en thèse.");
				nbErreur++;
			}
		}
		
		if (editingDoctorant().dateCommission() != null && editingDoctorant().avisCommission() == null) {
			session.addSimpleErrorMessage("Erreur", "L'avis de la comission doit être renseigné.");
			nbErreur++;
		}
		
		if (editingDoctorant().avisCommission() != null && editingDoctorant().dateCommission() == null) {
			session.addSimpleErrorMessage("Erreur", "La date de la comission doit être renseignée.");
			nbErreur++;
		}

		if (editingDoctorant().toPaysDiplome() != null && editingDoctorant().toDoctorantOrigine() == null) {
			session.addSimpleErrorMessage("Erreur", "Le diplôme permettant l'inscription en thèse doit être renseigné dans le cas où un pays d'obtention du diplôme est renseigné.");
			nbErreur++;
		}
		
		if (nbErreur > 0) {
			return null;
		}
		
		if (editingDoctorant().toPaysDiplome() == null || !editingDoctorant().toPaysDiplome().llPays().equals("FRANCE")) {
			editingDoctorant().setToEtablissementDiplomeOrigineRelationship(null);
		}
		
		try {
			edcDoctorant.saveChanges();
			setIsModification(false);
			session().addSimpleSuccessMessage("Confirmation", "Les données ont bien été enregistrées.");
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults annuler() {
		try {
			edcDoctorant().revert();
			setIsModification(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public WOActionResults ouvrirFenetreSuppressionDoctorant() {
		CktlAjaxWindow.open(context(), supprimerDoctorantWindowId(), "Confirmer la suppression");
		return doNothing();
	}
	
	public WOActionResults annulerSuppressionDoctorant() {
		CktlAjaxWindow.close(context(), supprimerDoctorantWindowId());
		return doNothing();
	}

	public String aucFicheDoctorantId() {
		return getComponentId() + "_aucFicheDoctorant";
	}

	public void setIsModification(boolean isModification) {
		this.isModification = isModification;
		disableOnglets(isModification, false);
	}

	public boolean isModification() {
		if (leDoctorant().toIndividuFwkpers().dNaissance() == null) {
			setIsModification(true);
			setIsEditing(false);
			editingDoctorant = leDoctorant();
			if (editingDoctorant.toIndividuFwkpers().toPaysNaissance() == null) {
				editingDoctorant.toIndividuFwkpers().setToPaysNaissanceRelationship(EOPays.getPaysDefaut(edcDoctorant()));
			}
			if (editingDoctorant.toIndividuFwkpers().toPaysNationalite() == null) {
				editingDoctorant.toIndividuFwkpers().setToPaysNationaliteRelationship(EOPays.getPaysDefaut(edcDoctorant()));
			}
			editingDoctorant.toIndividuFwkpers().setPersIdModification(session().applicationUser().getPersId());
		}
		return isModification;
	}

	public void setEdcDoctorant(EOEditingContext edcDoctorant) {
		this.edcDoctorant = edcDoctorant;
	}

	public EOEditingContext edcDoctorant() {
		if (edcDoctorant == null) {
			edcDoctorant = edc();
		}
		return edcDoctorant;
	}

	public void setEditingDoctorant(EODoctorant editingDoctorant) {
		this.editingDoctorant = editingDoctorant;
	}

	public EODoctorant editingDoctorant() {
		return editingDoctorant;
	}

	public void setLeDoctorant(EODoctorant leDoctorant) {
		this.leDoctorant = leDoctorant;
	}

	public EODoctorant leDoctorant() {
		return (EODoctorant) valueForBinding(B_leDoctorant);
	}

	public void setIsEditing(Boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Boolean isEditing() {
		return isEditing;
	}
	
	public Boolean showEtablissement() {
		if (leDoctorant().toPaysDiplome() == null || leDoctorant().toPaysDiplome().llPays() == null) {
			return false;
		}
		return leDoctorant().toPaysDiplome().llPays().equals("FRANCE");
	}

}