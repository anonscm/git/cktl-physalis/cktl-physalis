package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.CotutelleService;
import org.cocktail.fwkcktlrecherche.server.util.ContratUtilities;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Onglet cotutelle
 */
public class CoTutelle extends PhysalisWOComponent {

    private static final long serialVersionUID = -5703200420486612219L;
	private static final String B_laThese = "laThese";
	private static final String BINDING_IS_MODIFICATION_BOTTOM = "isModificationBottom";
	private WODisplayGroup dgCotutelle;
	private WODisplayGroup dgPresenceEtablissement;
	private EORepartAssociation selectedRepartAssociationCotutelle;
	private EORepartAssociation editingRepartAssociationCotutelle;
	private EOStructure editingEtablissementCotutelle;
	private NSTimestamp editingDateSignatureCotutelle;
	private NSTimestamp editingDateDebutCotutelle;
	private NSTimestamp editingDateFinCotutelle;
	private String editingCommentaireCotutelle;
	private EOAdresse editingAdresse = null;
	private EOPays editingPays = null;
	private Boolean isEditionCotutelle = false;
	private Boolean isCotutelleModif = false;
	private ContratPartenaire selectedContratPartenaireCotutelle = null;
	private EORepartAssociation currentRepartAssociationEtablissement;
	private NSArray<EORepartAssociation> repartAssociationCotutelle;
	
	private EORepartAssociation editingRepartAssociationPresenceEtablissement;
	private NSTimestamp editingDateDebutPresenceEtablissement;
	private NSTimestamp editingDateFinPresenceEtablissement;
	private Boolean isEditionPresenceEtablissement = false;
	private Boolean isModifPresenceEtablissement = false;
	private EORepartAssociation selectedRepartAssociationPresenceEtablissement;
	private EORepartAssociation currentRepartAssociationPresenceEtablissement;
	private EOStructure editingPresenceEtablissement;
	private ContratPartenaire selectedContratPartenairePresenceEtablissement;
	private NSArray<EORepartAssociation> repartAssociationPresenceEtablissement;
	
	/**
	 * Constructeur
	 * @param context contexte
	 */
	public CoTutelle(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		repartAssociationCotutelle = null;
	    dgCotutelle = null;
	    repartAssociationPresenceEtablissement = null;
	    dgPresenceEtablissement = null;
	    super.appendToResponse(response, context);
	}

	public Boolean isAjoutCotutelleDisabled() {
		return isEditionCotutelle() || (listeEtablissementsCotutelle().size() > 0);
	}
	
	/**
	 * Action lancee au click sur le bouton ajouter de la cotutelle
	 * @return null
	 */
	public WOActionResults ajoutCotutelle() {
		setIsEditionCotutelle(true);
		setIsCotutelleModif(false);
		setIsSelectionEnable(false);
		setIsEditionPresenceEtablissement(false);
		setIsModifPresenceEtablissement(false);
		disableOnglets(true, false);
		editingEtablissementCotutelle = null;
		editingDateDebutCotutelle = laThese().toContrat().dateDebut();
		editingDateFinCotutelle = laThese().toContrat().dateFin();
		editingCommentaireCotutelle = null;
		editingPays = null;
		editingAdresse = null; // l'adresse est a null au départ elle sera gérée lors de la validation
		return doNothing();
	}

	/**
	 * Action lancee au click sur le bouton modifier de la cotutelle
	 * @return null
	 */
	public WOActionResults modifCotutelle() {
		setIsEditionCotutelle(true);
		setIsCotutelleModif(true);
		setIsSelectionEnable(false);
		setIsEditionPresenceEtablissement(false);
		setIsModifPresenceEtablissement(false);
		disableOnglets(true, false);
		editingRepartAssociationCotutelle = selectedRepartAssociationCotutelle;
		editingEtablissementCotutelle = (EOStructure) editingRepartAssociationCotutelle.toPersonneElt();
		editingDateSignatureCotutelle = selectedContratPartenaireCotutelle.cpDateSignature();
		editingDateDebutCotutelle = editingRepartAssociationCotutelle.rasDOuverture();
		editingDateFinCotutelle = editingRepartAssociationCotutelle.rasDFermeture();
		editingCommentaireCotutelle = editingRepartAssociationCotutelle.rasCommentaire();
		if (selectedRepartAssociationCotutelle.toPersonneElt().getAdressePrincipale() != null && selectedRepartAssociationCotutelle.toPersonneElt().getAdressePrincipale().toPays() != null) {
			editingPays = selectedRepartAssociationCotutelle.toPersonneElt().getAdressePrincipale().toPays();
		}
		return doNothing();
	}

	/**
	 * Action lancee au click sur le bouton supprimer de la cotutelle
	 * @return null
	 */
	public WOActionResults supprimeCotutelle() {
		for (EORepartAssociation raEtablissement : listePresenceEtablissementCotutelle()) {
			if (raEtablissement.persId().equals(getSelectedRepartAssociationCotutelle().persId())) {
				session().addSimpleErrorMessage("Erreur", "Il existe des présences pour cette établissement. Vous devez les enlever pour permettre la suppression de cette cotutelle.");
				return doNothing();
			}
		}
				
		try {
			boolean cotutelleEncadrement = hasDirecteurCotutelle();
			CotutelleService cs = CotutelleService.creerNouvelleInstance(edc(), getUtilisateurPersId());
			cs.supprimerCotutelle(laThese(), getSelectedRepartAssociationCotutelle(), Application.isModeDebug);
			edc().saveChanges();
			session().addSimpleSuccessMessage("Suppression", "La présence dans l'établissement de cotutelle a bien été supprimé");
			if (cotutelleEncadrement) {
				session().addSimpleInfoMessage("Remarque", "L'attribut cotutelle rattachée à un directeur a été automatiquement supprimé dans l'onglet encadrement.");
			}
			selectedRepartAssociationCotutelle = null;
			repartAssociationCotutelle = null;
			dgCotutelle = null;
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.toString());
			e.printStackTrace();
		}

		return doNothing();
	}

	private boolean hasDirecteurCotutelle() {
	    NSArray<EODirecteurThese> directeurs = laThese().toDirecteursThese();
		for (EODirecteurThese directeur : directeurs) {
			if (directeur.isDirecteurCotutelle()) {
				return true;
			}
		}
	    return false;
    }

	/**
	 * Action lancee au click sur le bouton valider de la cotutelle
	 * @return null
	 */
	public WOActionResults validerCotutelle() {
		
		if (controleSaisieEtablissementCotutelle() > 0) {
			return doNothing();
		}

		if (!isCotutelleModif()) {

			ContratPartenaire cp = null;

			try {
				cp = creerEtablissementCotutelle(getEditingEtablissementCotutelle(), getEditingDateDebutCotutelle(), getEditingDateFinCotutelle(), getEditingCommentaireCotutelle());
				cp.setCpDateSignature(editingDateSignatureCotutelle);
			} catch (Exception e) {
				e.printStackTrace();
				session().addSimpleErrorMessage("Erreur", e.getMessage());
			}
		} else {
			editingRepartAssociationCotutelle.setRasDOuverture(editingDateDebutCotutelle);
			editingRepartAssociationCotutelle.setRasDFermeture(editingDateFinCotutelle);
			editingRepartAssociationCotutelle.setRasCommentaire(editingCommentaireCotutelle);
			getSelectedContratPartenaireCotutelle().setCpDateSignature(editingDateSignatureCotutelle);
		}

		// gestion du pays de l'adresse principale
		// on récupère l'adresse principale de l'etablissement sélectionné
		setEditingAdresse(getEditingEtablissementCotutelle().getAdressePrincipale());
		if (getEditingAdresse() != null) {
			// si changement de pays dans cotutelleForm (editingPays) on
			// repercute sur l'adresse principale
			if (getEditingAdresse().toPays() != getEditingPays()) {
				getEditingAdresse().setToPaysRelationship(getEditingPays());
			}
		} else {
			session.addSimpleInfoMessage("Probleme", "il faut ajouter une adresse principale");
		}

		try {
			edc().saveChanges();
			setIsCotutelleModif(false);
			setIsEditionCotutelle(false);
			setIsEditionPresenceEtablissement(false);
			setIsModifPresenceEtablissement(false);
			disableOnglets(false, false);
			session().addSimpleSuccessMessage("Confirmation", "La cotutelle est enregistrée");
			setIsSelectionEnable(true);
			repartAssociationCotutelle = null;
			dgCotutelle = null;
		} catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Erreur", e.getMessage());
		}

		return doNothing();
	}

	private int controleSaisieEtablissementCotutelle() {
	    int nbErreurs = 0;
		
		if (getEditingEtablissementCotutelle() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.etablissementManquant"));
		}
		
		if (getEditingPays() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.paysManquant"));
		}
		
		if (getEditingDateDebutCotutelle() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.dateDebutManquante"));
		}
		
		if (getEditingDateFinCotutelle() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.dateFinManquante"));
		}
	    return nbErreurs;
    }
	
	/**
	 * Ajoute partenariat de type etablissement de cotutelle 
	 * @param etablissement
	 * @param dateDebut
	 * @param dateFin
	 * @param commentaire
	 * @return
	 * @throws Exception
	 */
	private ContratPartenaire creerEtablissementCotutelle(EOStructure etablissement, NSTimestamp dateDebut, NSTimestamp dateFin, String commentaire) throws Exception {
		FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
		FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
		ContratPartenaire cp;

		cp_exist.setContrat(laThese().toContrat());
		cp_exist.setPartenaire(etablissement);
		cp = (ContratPartenaire) cp_exist.find().lastObject();

		// si le contrat partenaire n'existe pas, on le crée
		// sinon on ajoute le role au partenaire
		if (cp == null) {
			cp = fc.creerContratPartenaire(laThese().toContrat(), getEditingEtablissementCotutelle(), new NSArray<EOAssociation>(factoryAssociation()
					.etablissementCotutelleAssociation(edc())), getEditingDateDebutCotutelle(), getEditingDateFinCotutelle(), Boolean.FALSE, null, getEditingCommentaireCotutelle());
		} else {
			fc.ajouterLeRolePourLesDates(cp, factoryAssociation().etablissementCotutelleAssociation(edc()), dateDebut, dateFin);
		}
		return cp;
	}

	/**
	 * Action lancee au click sur le bouton annuler de la cotutelle
	 * @return null
	 */
	public WOActionResults annulerCotutelle() {
		edc().revert();
		setIsEditionCotutelle(false);
		setIsCotutelleModif(false);
		setIsSelectionEnable(true);
		setIsEditionPresenceEtablissement(false);
		setIsModifPresenceEtablissement(false);
		disableOnglets(false, false);
		return doNothing();
	}
	
	/**
	 * Classe DisplayGroup pour la cotutelle
	 */
	public class DgDelegateCotutelle {
		/**
		 * Changement de selection
		 * @param group group
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			selectedRepartAssociationCotutelle = (EORepartAssociation) dgCotutelle.selectedObject();
			if (selectedRepartAssociationCotutelle != null) {
				EOQualifier qualCP = ERXQ.and(ERXQ.equals(EOContratPartenaire.PERS_ID_KEY, getSelectedRepartAssociationCotutelle().persId()),
						ERXQ.equals(EOContratPartenaire.CONTRAT_KEY, laThese().toContrat()));

				selectedContratPartenaireCotutelle = ContratPartenaire.fetch(edc(), qualCP);
			} else {
				selectedContratPartenaireCotutelle = null;
			}
		}
	}

	/**
	 * DisplayGroup cotutelle
	 * @return dg
	 */
	public WODisplayGroup dgCotutelle() {
		if (dgCotutelle == null) {
			dgCotutelle = new WODisplayGroup();
			dgCotutelle.setDelegate(new DgDelegateCotutelle());
			dgCotutelle.setObjectArray(listeEtablissementsCotutelle());
		}

		return dgCotutelle;
	}
	
	/**
	 * Retourne la date de signature d'un contrat partenaire
	 * @return la date de signature
	 */
	public NSTimestamp dateSignature() {
		EOQualifier qualCP = ERXQ.and(
								ERXQ.equals(EOContratPartenaire.PERS_ID_KEY, getCurrentRepartAssociationEtablissement().persId()),
								ERXQ.equals(EOContratPartenaire.CONTRAT_KEY, laThese().toContrat())
							);
		ContratPartenaire cp = ContratPartenaire.fetch(edc(), qualCP);
		if (cp == null) {
			return null;
		}
		return cp.cpDateSignature();
	}
	
	/* Presence établissement */
	
	/**
	 * Display groupe présence établissement
	 * @return dg
	 */
	public WODisplayGroup dgPresenceEtablissement() {
		if (dgPresenceEtablissement == null) {
			dgPresenceEtablissement = new WODisplayGroup();
			dgPresenceEtablissement.setDelegate(new DgDelegatePresenceEtablissement());
			dgPresenceEtablissement.setSortOrderings(new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EORepartAssociation.RAS_D_OUVERTURE_KEY, EOSortOrdering.CompareAscending)));
			dgPresenceEtablissement.setObjectArray(listePresenceEtablissementCotutelle());
			dgPresenceEtablissement.fetch();
		}

		return dgPresenceEtablissement;
	}
	
	/**
	 * Classe DisplayGroup pour le tableau de présence dans l'établissement
	 */
	public class DgDelegatePresenceEtablissement {
		/**
		 * Lors du changement de selection
		 * @param group group
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (dgPresenceEtablissement.selectedObject() != null) {
				selectedRepartAssociationPresenceEtablissement = (EORepartAssociation) dgPresenceEtablissement.selectedObject();
			} else {
				selectedRepartAssociationPresenceEtablissement = null;
			}
			if (selectedRepartAssociationPresenceEtablissement != null) {
				EOQualifier qualCP = ERXQ.and(ERXQ.equals(EOContratPartenaire.PERS_ID_KEY, getSelectedRepartAssociationPresenceEtablissement().persId()),
						ERXQ.equals(EOContratPartenaire.CONTRAT_KEY, laThese().toContrat()));

				selectedContratPartenairePresenceEtablissement = ContratPartenaire.fetch(edc(), qualCP);
			} else {
				selectedContratPartenairePresenceEtablissement = null;
			}
		}
	}
	
	/**
	 * Action lancee au click sur le bouton ajout présence établissement
	 * @return null
	 */
	public WOActionResults ajoutPresenceEtablissement() {
		setIsEditionCotutelle(false);
		setIsCotutelleModif(false);
		setIsEditionPresenceEtablissement(true);
		setIsModifPresenceEtablissement(false);
		disableOnglets(true, false);
		editingEtablissementCotutelle = null;
		editingDateDebutPresenceEtablissement = dateDebutPresenceCalculee();
		editingDateFinPresenceEtablissement = laThese().toContrat().dateFin();
		
		return doNothing();
	}
	
	/**
	 * Action lancee au click sur le bouton modifier présence établissement
	 * @return null
	 */
	public WOActionResults modifiePresenceEtablissement() {
		setIsEditionCotutelle(false);
		setIsCotutelleModif(false);
		setIsEditionPresenceEtablissement(true);
		setIsModifPresenceEtablissement(true);
		disableOnglets(true, false);
		
		editingRepartAssociationPresenceEtablissement = selectedRepartAssociationPresenceEtablissement;
		editingPresenceEtablissement = (EOStructure) editingRepartAssociationPresenceEtablissement.toPersonneElt();
		editingDateDebutPresenceEtablissement = editingRepartAssociationPresenceEtablissement.rasDOuverture();
		editingDateFinPresenceEtablissement = editingRepartAssociationPresenceEtablissement.rasDFermeture();
		
		return doNothing();
	}
	
	/**
	 * Action lancee au click sur le bouton supprimer présence étalissement
	 * @return null
	 */
	public WOActionResults supprimePresenceEtablissement() {
		
		try {
			CotutelleService cs = CotutelleService.creerNouvelleInstance(edc(), getUtilisateurPersId());
			cs.supprimerPresenceEtablissement(laThese(), selectedRepartAssociationPresenceEtablissement);
	        edc().saveChanges();
	        session().addSimpleSuccessMessage("Suppression", "L'établissement de cotutelle a bien été supprimé");
			selectedRepartAssociationPresenceEtablissement = null;
			repartAssociationPresenceEtablissement = null;
			dgPresenceEtablissement = null;
        } catch (Exception e) {
        	session().addSimpleErrorMessage("Erreur", e.toString());
			e.printStackTrace();
        }
		
		return doNothing();
	}
	
	/**
	 * Action lancee au click sur le bouton valider présence étalissement
	 * @return null
	 */
	public WOActionResults validerPresenceEtablissement() {
		
		if (controleSaisiePresenceEtablissementCotutelle() > 0) {
			return doNothing();
		}
		
		if (listePresenceEtablissementCotutelle().size() > 0) {
			NSArray<EORepartAssociation> etablissements = listePresenceEtablissementCotutelle();
			if (isModifPresenceEtablissement() && editingRepartAssociationPresenceEtablissement != null && etablissements.contains(editingRepartAssociationPresenceEtablissement)) {
				etablissements.remove(editingRepartAssociationPresenceEtablissement);
			}
			for (EORepartAssociation etablissement : etablissements) {
				if ((editingDateDebutPresenceEtablissement.after(etablissement.rasDOuverture()) && editingDateDebutPresenceEtablissement.before(etablissement.rasDFermeture())) 
						|| (editingDateFinPresenceEtablissement.after(etablissement.rasDOuverture()) && editingDateFinPresenceEtablissement.before(etablissement.rasDFermeture())) 
						|| (editingDateDebutPresenceEtablissement.before(etablissement.rasDOuverture()) && editingDateFinPresenceEtablissement.after(etablissement.rasDFermeture()))
						|| editingDateDebutPresenceEtablissement.equals(etablissement.rasDOuverture()) || editingDateDebutPresenceEtablissement.equals(etablissement.rasDFermeture())
						|| editingDateFinPresenceEtablissement.equals(etablissement.rasDOuverture()) || editingDateFinPresenceEtablissement.equals(etablissement.rasDFermeture())) {
					session().addSimpleInfoMessage("Attention", "Le doctorant est présent dans plusieurs établissements en même temps.");
				}
			}
		}
		
		if (!isModifPresenceEtablissement()) {
			ContratUtilities cu = ContratUtilities.creerNouvelleInstance(edc());
			cu.ajouterPartenaireAvecRoleEtDatesAuContrat(getEditingEtablissementCotutelle(), factoryAssociation().etablissementAccueil(edc()), laThese().toContrat(), 
					getEditingDateDebutPresenceEtablissement(), getEditingDateFinPresenceEtablissement(), getUtilisateurPersId());
		} else {
			editingRepartAssociationPresenceEtablissement.setRasDOuverture(editingDateDebutPresenceEtablissement);
			editingRepartAssociationPresenceEtablissement.setRasDFermeture(editingDateFinPresenceEtablissement);
		}
		
		try {
			edc().saveChanges();
			setIsCotutelleModif(false);
			setIsEditionCotutelle(false);
			setIsEditionPresenceEtablissement(false);
			setIsModifPresenceEtablissement(false);
			session().addSimpleSuccessMessage("Confirmation", "La présence dans l'établissement est enregistrée");
			setIsSelectionEnable(true);
			disableOnglets(false, false);
			repartAssociationPresenceEtablissement = null;
			dgPresenceEtablissement = null;
		} catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Erreur", e.getMessage());
		}

		return doNothing();
	}
	


	private int controleSaisiePresenceEtablissementCotutelle() {
	    int nbErreurs = 0;
		
		if (getEditingEtablissementCotutelle() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.etablissementManquant"));
		}
		
		if (getEditingDateDebutPresenceEtablissement() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.dateDebutManquante"));
		}
		
		if (getEditingDateFinPresenceEtablissement() == null) {
			nbErreurs++;
			session().addSimpleErrorMessage(localisation("application.erreur"), localisation("cotutelle.dateFinManquante"));
		}
		
		if (getEditingDateDebutPresenceEtablissement() != null && getEditingDateFinPresenceEtablissement() != null) {
			if (getEditingDateDebutPresenceEtablissement().before(laThese().toContrat().dateDebut()) || getEditingDateFinPresenceEtablissement().after(laThese().toContrat().dateFin())) {
				nbErreurs++;
				session().addSimpleErrorMessage(localisation("application.erreur"), String.format(localisation("cotutelle.datesPresenceEtablissementIncoherentes"), 
						laThese().toContrat().dateDebut().toString(), laThese().toContrat().dateFin().toString()));
			}
		}
		
	    return nbErreurs;
    }
	
	/**
	 * Action lancee au click sur le bouton annuler presence etablissement
	 * @return null
	 */
	public WOActionResults annulerPresenceEtablissement() {
		edc().revert();
		setIsEditionCotutelle(false);
		setIsCotutelleModif(false);
		setIsEditionPresenceEtablissement(false);
		setIsModifPresenceEtablissement(false);
		disableOnglets(false, false);
		return doNothing();
	}
	
	/**
	 * Retourne la liste des etablissements dans lequel le doctorant effectuera sa these (etablissement origine + cotutelle)
	 * @return la liste des etablissements
	 */
	public NSArray<EOStructure> listEtablissements() {
		NSMutableArray<EOStructure> etablissements = new NSMutableArray<EOStructure>();
		NSArray<EORepartAssociation> ras = listeEtablissementsCotutelle();
		for (EORepartAssociation ra : ras) {
			etablissements.add((EOStructure) ra.toPersonne());
		}
		
		etablissements.add(laThese().toContrat().etablissement());
		
		return etablissements.immutableClone();
	}
	
	/**
	 * Calcule la date de début de présence dans l'établissement en fonction des présences dans les autres établissements
	 * @return la date calculée
	 */
	public NSTimestamp dateDebutPresenceCalculee() {
		NSArray<EORepartAssociation> listePresence = listePresenceEtablissementCotutelle();
		int taille = listePresence.size();
		NSTimestamp dateFinMax;
		
		if (taille == 0) {
			return laThese().toContrat().dateDebut();
		}
		
		dateFinMax = listePresence.get(0).rasDFermeture();
		
		for (EORepartAssociation ra : listePresence) {
			if (ra.rasDFermeture().after(dateFinMax)) {
				dateFinMax = ra.rasDFermeture();
			}
		}
		
		if (dateFinMax.equals(laThese().toContrat().dateFin())) {
			return laThese().toContrat().dateDebut();
		}
		
		return dateFinMax.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
	}
	
	// Containers 
	public String getAucCotutelleId() {
		return getComponentId() + "aucCotutelleId";
	}

	public String getAucBoutonCotutelleId() {
		return getComponentId() + "aucBoutonCotutelleId";
	}
	
	public String getAucCotutelleFormId() {
		return getComponentId() + "_aucCotutelleForm";
	}
	
	// fonctions privées
	
	private NSArray<EORepartAssociation> listePresenceEtablissementCotutelle() {
		if (repartAssociationPresenceEtablissement == null) {
			CotutelleService cs = CotutelleService.creerNouvelleInstance(edc(), getUtilisateurPersId());
			repartAssociationPresenceEtablissement = cs.listePresenceEtablissementCotutelle(laThese());
		}
		return repartAssociationPresenceEtablissement;
	}
	
	private NSArray<EORepartAssociation> listeEtablissementsCotutelle() {
		if (repartAssociationCotutelle == null) {
			CotutelleService cs = CotutelleService.creerNouvelleInstance(edc(), getUtilisateurPersId());
			repartAssociationCotutelle = cs.listeEtablissementsCotutelle(laThese());
		}
		return repartAssociationCotutelle;
	}

	// get / set

	/**
	 * getter de la these
	 * @return la these
	 */
	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	/**
	 * Setter de la these
	 * @param laThese la these a setter
	 */
	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, B_laThese);
	}
	
	public void setSelectedRepartAssociationCotutelle(EORepartAssociation selectedRepartAssociationCotutelle) {
		this.selectedRepartAssociationCotutelle = selectedRepartAssociationCotutelle;
	}

	public EORepartAssociation getSelectedRepartAssociationCotutelle() {
		return selectedRepartAssociationCotutelle;
	}

	public EORepartAssociation getEditingRepartAssociationCotutelle() {
		return editingRepartAssociationCotutelle;
	}

	public void setEditingRepartAssociationCotutelle(EORepartAssociation editingRepartAssociationCotutelle) {
		this.editingRepartAssociationCotutelle = editingRepartAssociationCotutelle;
	}

	public void setEditingAdresse(EOAdresse editingAdresse) {
		this.editingAdresse = editingAdresse;
	}

	public EOAdresse getEditingAdresse() {
		return editingAdresse;
	}

	public void setEditingPays(EOPays editingPays) {
		this.editingPays = editingPays;
	}

	public EOPays getEditingPays() {
		return editingPays;
	}

	public void setEditingEtablissementCotutelle(EOStructure editingEtablissementCotutelle) {
		this.editingEtablissementCotutelle = editingEtablissementCotutelle;
	}

	public EOStructure getEditingEtablissementCotutelle() {
		return editingEtablissementCotutelle;
	}

	public void setEditingDateDebutCotutelle(NSTimestamp editingDateDebutCotutelle) {
		this.editingDateDebutCotutelle = editingDateDebutCotutelle;
	}

	public NSTimestamp getEditingDateDebutCotutelle() {
		return editingDateDebutCotutelle;
	}

	public void setEditingDateFinCotutelle(NSTimestamp editingDateFinCotutelle) {
		this.editingDateFinCotutelle = editingDateFinCotutelle;
	}

	public NSTimestamp getEditingDateFinCotutelle() {
		return editingDateFinCotutelle;
	}

	public void setEditingCommentaireCotutelle(String editingCommentaireCotutelle) {
		this.editingCommentaireCotutelle = editingCommentaireCotutelle;
	}

	public String getEditingCommentaireCotutelle() {
		return editingCommentaireCotutelle;
	}

	public void setIsEditionCotutelle(Boolean isEditionCotutelle) {
		this.isEditionCotutelle = isEditionCotutelle;
	}

	public Boolean isEditionCotutelle() {
		return isEditionCotutelle;
	}

	public void setIsCotutelleModif(Boolean isCotutelleModif) {
		this.isCotutelleModif = isCotutelleModif;
	}

	public Boolean isCotutelleModif() {
		return isCotutelleModif;
	}

	public ContratPartenaire getSelectedContratPartenaireCotutelle() {
		return selectedContratPartenaireCotutelle;
	}

	public void setSelectedContratPartenaireCotutelle(ContratPartenaire selectedContratPartenaireCotutelle) {
		this.selectedContratPartenaireCotutelle = selectedContratPartenaireCotutelle;
	}

	public EORepartAssociation getCurrentRepartAssociationEtablissement() {
	    return currentRepartAssociationEtablissement;
    }

	public void setCurrentRepartAssociationEtablissement(EORepartAssociation currentRepartAssociationEtablissement) {
	    this.currentRepartAssociationEtablissement = currentRepartAssociationEtablissement;
    }

	public NSTimestamp getEditingDateSignatureCotutelle() {
	    return editingDateSignatureCotutelle;
    }

	public void setEditingDateSignatureCotutelle(NSTimestamp editingDateSignatureCotutelle) {
	    this.editingDateSignatureCotutelle = editingDateSignatureCotutelle;
    }
	
	public EORepartAssociation getSelectedRepartAssociationPresenceEtablissement() {
		return selectedRepartAssociationPresenceEtablissement;
	}

	public void setSelectedRepartAssociationPresenceEtablissement(EORepartAssociation selectedRepartAssociationPresenceEtablissement) {
		this.selectedRepartAssociationPresenceEtablissement = selectedRepartAssociationPresenceEtablissement;
	}

	public EORepartAssociation getCurrentRepartAssociationPresenceEtablissement() {
	    return currentRepartAssociationPresenceEtablissement;
    }

	public void setCurrentRepartAssociationPresenceEtablissement(EORepartAssociation currentRepartAssociationPresenceEtablissement) {
	    this.currentRepartAssociationPresenceEtablissement = currentRepartAssociationPresenceEtablissement;
    }
	
	public Boolean isEditionPresenceEtablissement() {
	    return isEditionPresenceEtablissement;
    }

	public void setIsEditionPresenceEtablissement(Boolean isEditionPresenceEtablissement) {
	    this.isEditionPresenceEtablissement = isEditionPresenceEtablissement;
    }

	public Boolean isModifPresenceEtablissement() {
	    return isModifPresenceEtablissement;
    }

	public void setIsModifPresenceEtablissement(Boolean isModifPresenceEtablissement) {
	    this.isModifPresenceEtablissement = isModifPresenceEtablissement;
    }

	public EORepartAssociation getEditingRepartAssociationPresenceEtablissement() {
	    return editingRepartAssociationPresenceEtablissement;
    }

	public void setEditingRepartAssociationPresenceEtablissement(EORepartAssociation editingRepartAssociationPresenceEtablissement) {
	    this.editingRepartAssociationPresenceEtablissement = editingRepartAssociationPresenceEtablissement;
    }

	public NSTimestamp getEditingDateDebutPresenceEtablissement() {
	    return editingDateDebutPresenceEtablissement;
    }

	public void setEditingDateDebutPresenceEtablissement(NSTimestamp editingDateDebutPresenceEtablissement) {
	    this.editingDateDebutPresenceEtablissement = editingDateDebutPresenceEtablissement;
    }

	public NSTimestamp getEditingDateFinPresenceEtablissement() {
	    return editingDateFinPresenceEtablissement;
    }

	public void setEditingDateFinPresenceEtablissement(NSTimestamp editingDateFinPresenceEtablissement) {
	    this.editingDateFinPresenceEtablissement = editingDateFinPresenceEtablissement;
    }

	public EOStructure getEditingPresenceEtablissement() {
	    return editingPresenceEtablissement;
    }

	public void setEditingPresenceEtablissement(EOStructure editingPresenceEtablissement) {
	    this.editingPresenceEtablissement = editingPresenceEtablissement;
    }
	
	public ContratPartenaire getSelectedContratPartenairePresenceEtablissement() {
		return selectedContratPartenairePresenceEtablissement;
	}
	
	public void setSelectedContratPartenairePresenceEtablissement(ContratPartenaire selectedContratPartenairePresenceEtablissement) {
		this.selectedContratPartenairePresenceEtablissement = selectedContratPartenairePresenceEtablissement;
	}
	
}