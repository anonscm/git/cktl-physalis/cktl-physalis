package org.cocktail.physalisw.serveur.components;

import java.util.Date;

import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;

public class IntervenantThese extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8324576334653225492L;
	private NSArray<EOCivilite> civilites = null;
	private EOCivilite uneCivilite;
	private static final String B_laThese = "laThese";
	private static final String B_intervenant = "editingIntervenant";
	private static final String B_rasIntervenant = "editingRasIntervenant";
	private static final String B_isEditing = "isEditing";
	private static final String B_isBoutonValiderDisable = "isBoutonValiderDisable";
	private static final String B_isAffichage = "isAffichage";
	private static final String B_typeMembre = "typeMembre";
	private static final String B_actionSelectionner = "selectionner";
	private static final String B_dateDebut = "dateDebut";
	private static final String B_dateFin = "dateFin";
	private static final String B_commentaire = "commentaire";
	private static final String B_isDatesVisibles = "isDatesVisibles";
	private static final String B_classBox = "classBox";
	private static final String TYPE_MEMBRE_JURY = "JURY";
	private static final String TYPE_MEMBRE_RAPPORTEUR = "RAPPORTEUR";
	private Boolean isEditingAdresse = false;
	private Boolean isEditing;
	private Boolean isRechercheIndividu = false;
	private Boolean isAffichage = false;

	public IntervenantThese(WOContext context) {
		super(context);
	}

	public String aucIntervenantFormId() {
		return getComponentId() + "_aucIntervenantFormId";
	}

	/**
	 * @return the civilites
	 */

	public NSArray<EOCivilite> civilites() {
		if (civilites == null) {
			EOSortOrdering sexeOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.SEXE_KEY, EOSortOrdering.CompareAscending);
			EOSortOrdering libelleLongOrdering = EOSortOrdering.sortOrderingWithKey(EOCivilite.L_CIVILITE_KEY, EOSortOrdering.CompareAscending);
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(new EOSortOrdering[] { sexeOrdering, libelleLongOrdering });
			civilites = EOCivilite.fetchAll(edc(), sortOrderings);
		}
		return civilites;
	}
	
	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}
	
	public EOIndividu editingIntervenant() {
		return (EOIndividu) valueForBinding(B_intervenant);
	}

	public void setEditingIntervenant(EOIndividu editingIntervenant) {
		setValueForBinding(editingIntervenant, B_intervenant);
	}

	public NSTimestamp dateDebut() {
		return (NSTimestamp) valueForBinding(B_dateDebut);
	}

	public void setDateDebut(NSTimestamp dateDebut) {
		setValueForBinding(dateDebut, B_dateDebut);
	}

	public NSTimestamp dateFin() {
		return (NSTimestamp) valueForBinding(B_dateFin);
	}

	public void setDateFin(NSTimestamp dateFin) {
		setValueForBinding(dateFin, B_dateFin);
	}

	public String commentaire() {
		return (String) valueForBinding(B_commentaire);
	}

	public void setCommentaire(String commentaire) {
		setValueForBinding(commentaire, B_commentaire);
	}

	public Boolean isDatesVisibles() {
		return (Boolean) valueForBinding(B_isDatesVisibles);
	}

	public String typeMembre() {
		return (String) valueForBinding(B_typeMembre);
	}

	public String classBox() {
		/*
		 * if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) { return
		 * "boxtitreOrange"; } else { return "boxtitre"; }
		 */
		if (hasBinding(B_classBox)) {
			return (String) valueForBinding(B_classBox);
		}
		return "boxtitre";
	}

	public void setUneCivilite(EOCivilite uneCivilite) {
		this.uneCivilite = uneCivilite;
	}

	public EOCivilite uneCivilite() {
		return uneCivilite;
	}

	public void setIsEditing(Boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Boolean isEditing() {
		return isEditing;
	}

	public Boolean isBoutonValiderDisable() {
		return (Boolean) valueForBinding(B_isBoutonValiderDisable);
	}

	public void setIsBoutonValiderDisable(Boolean isBoutonValiderDisable) {
		setValueForBinding(isBoutonValiderDisable, B_isBoutonValiderDisable);
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
	}

	public void setIsEditingAdresse(Boolean isEditingAdresse) {
		if (isEditingAdresse)
			setIsBoutonValiderDisable(true);
		else
			setIsBoutonValiderDisable(false);
		this.isEditingAdresse = isEditingAdresse;
	}

	public Boolean isEditingAdresse() {
		return isEditingAdresse;
	}

	public void setIsRechercheIndividu(Boolean isRechercheIndividu) {
		if (isRechercheIndividu & !isBoutonValiderDisable())
			setIsBoutonValiderDisable(true);
		if (!isRechercheIndividu & isBoutonValiderDisable())
			setIsBoutonValiderDisable(false);

		this.isRechercheIndividu = isRechercheIndividu;
	}

	public Boolean isRechercheIndividu() {
		// if (!isRechercheIndividu && editingCoEncadrant() == null) {
		if (editingIntervenant() == null) {
			setIsRechercheIndividu(true);
		}
		return isRechercheIndividu;
	}

	public WOActionResults onCreerPersonne() {
		setIsRechercheIndividu(false);
		editingIntervenant().setToCiviliteRelationship(civilites().get(0));
		return null;
	}

	public WOActionResults onModifierPersonne() {
		setIsRechercheIndividu(false);
		return null;
	}

	public WOActionResults onSelectionnerPersonne() {
		//AjaxUpdateContainer.updateContainerWithID("AUCBoutonSelectionCoencadrant", context());
		setIsBoutonValiderDisable(true);
		return null;
	}

	public WOActionResults selectionnerIntervenant() {
		WOActionResults result = null;
		
		if (laThese() != null && editingIntervenant().noIndividu().equals(laThese().toDoctorant().toIndividuFwkpers().noIndividu())) {
			if (TYPE_MEMBRE_JURY.equals(typeMembre())) {
				session().addSimpleErrorMessage("Erreur", "Le doctorant ne peut être un membre du jury.");
			} else if (TYPE_MEMBRE_RAPPORTEUR.equals(typeMembre())) {
				session().addSimpleErrorMessage("Erreur", "Le doctorant ne peut être un rapporteur.");
			}
			return result;
		}
		
		if (hasBinding(B_actionSelectionner)) {
			result = performParentAction((String) valueForBinding(B_actionSelectionner));
		}
		setIsRechercheIndividu(false);
		return result;
	}

	public String libelleBoutonSelectionner() {
		return new String("Sélectionner " + editingIntervenant().nomUsuel() + " " + editingIntervenant().prenom());
	}

	public void setIsAffichage(Boolean isAffichage) {
		this.isAffichage = isAffichage;
	}

	public Boolean isAffichage() {
		if (hasBinding(B_isAffichage)) {
			isAffichage = (Boolean) valueForBinding(B_isAffichage);
		}
		return isAffichage;
	}

	public EOQualifier qualifierTypeAdresse() {
		// EOQualifier qual = ERXQ.and(EOTypeAdresse.QUAL_TADR_CODE_LIVR,)
		return EOTypeAdresse.QUAL_TADR_CODE_PRO;
	}

	public EOQualifier qualifierTypeTel() {
		return EOTypeTel.QUAL_C_TYPE_TEL_PRF;
	}

	public Boolean getWantReset() {
	    return true;
    }

	public void setWantReset(Boolean wantReset) {
    }
	
	public boolean peutModifierNomPrenomCiviliteMembreJury() {
		if (!getIsGrhInUse()) {
			return true;
		}
		
		if (editingIntervenant() != null) {
			EOQualifier qualifier = EOAffectation.TO_INDIVIDU.eq(editingIntervenant())
					.and(EOAffectation.D_DEB_AFFECTATION.lessThanOrEqualTo(new NSTimestamp()))
					.and(
						EOAffectation.D_FIN_AFFECTATION.isNull()
						.or(EOAffectation.D_FIN_AFFECTATION.greaterThanOrEqualTo(new NSTimestamp()))
					)
					.and(EOAffectation.TEM_VALIDE.eq("O"));
			
			NSArray<EOAffectation> affectations = EOAffectation.fetch(edc(), qualifier, null);
			
			if (affectations.count() > 0) {
				return false;
			}
		}
		
		return true;
	}
}