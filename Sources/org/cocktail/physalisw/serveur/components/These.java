package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.cocowork.server.metier.convention.TypeAvenant;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderModeGestion;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderTypeContrat;
import org.cocktail.cocowork.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantFinancement;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.EOTheseDomaine;
import org.cocktail.fwkcktlrecherche.server.metier.EOTypeFinancement;
import org.cocktail.fwkcktlrecherche.server.metier.service.CotutelleService;
import org.cocktail.fwkcktlrecherche.server.metier.service.DoctorantTheseService;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.validation.ERXValidationException;

/**
 * Page these
 */
public class These extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 6954002024304641564L;
	public static final String ID_INTERNE_TYPE_CONVENTION_THESE = "CONV_THESE";
	public static final String LC_MODE_GESTION_THESE = "CS";
	private static final String BINDING_LE_DOCTORANT = "leDoctorant";
	private static final String BINDING_IS_MODIFICATION = "isModification";
	private static final String CONTRAT_ETABLISSEMENT_REQUIS = "EtablissementRequis";
	private WODisplayGroup dgDoctorantThese;
	private WODisplayGroup dgAvenantThese;
	private EODoctorantThese laThese;
	private EODoctorantThese editingThese;
	private Boolean isModifThese = false;
	private Boolean isTheseEditable = false;
	private Boolean isDetailThese = false;
	private final DgDelegate dgDelegate = new DgDelegate();
	private EOStructure selectedEcoleDoctorale = null;
	private EOStructure editingEcoleDoctorale = null;
	private Boolean isTheseFetched = false;
	private EOStructure etablissementGestionnaire = null;
	private TypeClassificationContrat typeClassification = null;
	private Integer persIdEcoleDoctorale = null;
	private EOTypeFinancement defaultTypeSansFinancement = null;
	private String theseTitre = null;
	private EOTheseDomaine theseDomaine = null;
	private NSTimestamp theseDateDebutThese = null;
	private NSTimestamp theseDateFinThese = null;
	private NSTimestamp theseDatePrevueSoutenance = null;
	private String theseContractuel = null;
	private String theseReference = null;
	private EOProjetScientifique theseProjetScientifique = null;
	private Boolean theseIsCotutelle;
	private String theseResume = null;
	private NSTimestamp dateDebutAvenant = null;
	private NSTimestamp dateFinAvenant = null;
	private String titreAvenant = null;
	private String commentaireAvenant = null;
	private Boolean isEditionAvenant = false;
	private Boolean isModificationAvenant = false;
	private Avenant selectedAvenant = null;
	private NSTimestamp theseDateFinAnticipee = null;
	private String theseMotifFinAnticipee = null;
	private String theseCodeSiseDiplome = null;
	private EODomaineScientifique theseDomaineScientifique = null;
	private boolean isModificationTop = false;
	private boolean isModificationBottom = false;
	
	/**
	 * Constructeur
	 * @param context contexte
	 */
	public These(WOContext context) {
		super(context);
	}

	/**
	 * @param anEc editingContext
	 * @return liste des domaines d'une these
	 */
	public NSArray<EOTheseDomaine> listeTheseDomaines(EOEditingContext anEc) {
		return EOTheseDomaine.fetchAllTheseDomaines(anEc, null);
	}

	/**
	 * @param ec editingContext
	 * @return le type de contrat pour une convention de these
	 */
	public static TypeContrat findTypeContratThese(EOEditingContext ec) {
		FinderTypeContrat ftc = new FinderTypeContrat(ec);
		TypeContrat tc = null;
		try {
			tc = ftc.findWithTyconIdInterne(ID_INTERNE_TYPE_CONVENTION_THESE);
		} catch (ExceptionFinder e) {
			e.printStackTrace();
		}
		return tc;
	}

	/**
	 * @param ec editingContext
	 * @return le mode de gestion d'une convention de these
	 */
	public static ModeGestion findModeGestionThese(EOEditingContext ec) {
		FinderModeGestion fmg = new FinderModeGestion(ec);
		ModeGestion mg = null;
		try {
			@SuppressWarnings("unchecked")
            NSArray<ModeGestion> data = fmg.findWithLibelleCourt(LC_MODE_GESTION_THESE);
			if (data.count() > 0) {
				mg = data.lastObject();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mg;
	}

	/**
	 * Action effectuée lors du click sur le bouton ajouter these
	 * @return null
	 */
	public WOActionResults addThese() {

		theseTitre = null;
		theseDomaine = null;
		theseContractuel = null;
		theseReference = null;
		theseProjetScientifique = null;
		theseIsCotutelle = false;
		theseResume = null;
		editingEcoleDoctorale = null;
		setIsTheseEditable(true);
		setIsModifThese(false);
		setIsSelectionEnable(false);
		theseDateFinAnticipee = null;
		theseMotifFinAnticipee = null;
		theseDomaineScientifique = null;
		theseCodeSiseDiplome = null;

		return doNothing();
	}

	/**
	 * Action effectuée lors du click sur le bouton modifier thèse
	 * @return null
	 */
	public WOActionResults modifyThese() {
		
		setIsTheseEditable(true);
		setIsModifThese(true);
		setIsSelectionEnable(false);

		if (laThese() != null) {
			editingThese = laThese();
			persIdEcoleDoctorale = selectedEcoleDoctorale().persId();
			editingEcoleDoctorale = selectedEcoleDoctorale();
			theseDateDebutThese = editingThese.toContrat().dateDebut();
			theseDateFinThese = editingThese.toContrat().dateFin();
			theseTitre = editingThese.toContrat().conObjet();
			theseDomaine = editingThese.toTheseDomaine();
			theseDatePrevueSoutenance = editingThese.datePrevSoutenance();
			theseContractuel = editingThese.contractuel();
			theseReference = editingThese.toContrat().conReferenceExterne();
			theseProjetScientifique = editingThese.toProjetScientifique();
			theseIsCotutelle = editingThese.isCotutelle();
			theseResume = editingThese.resumeThese();
			theseDateFinAnticipee = editingThese.dateFinAnticipee();
			theseMotifFinAnticipee = editingThese.motifFinAnticipee();
			theseCodeSiseDiplome = editingThese.codeSiseDiplome();
			theseDomaineScientifique = editingThese.toContrat().avenantZero().domaineScientifique();
		}
		return null;
	}
	
	/**
	 * Supprime une thèse
	 * @return null
	 */
	public WOActionResults supprimerThese() {
		if (laThese() != null) {
			try {
				DoctorantTheseService dts = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
				if (dts.supprimerTouteLaThese(laThese())) {
					edc().saveChanges();
					session().addSimpleSuccessMessage("Succès", "La thèse a été supprimée.");
					setLaThese(null);
					setSelectedEcoleDoctorale(null);
					dgDoctorantThese = null;
					dgAvenantThese = null;
				} else {
					edc().revert();
					session().addSimpleErrorMessage("Erreur", "La thèse n'a pas été supprimée.");
				}
			} catch (Exception e) {
				session().addSimpleErrorMessage("Erreur", e.getMessage()); //deleteRowDescribedByQualifierEntity -- er.extensions.jdbc.ERXJDBCAdaptor$Channel: method failed to delete row in database
				edc().revert();
				e.printStackTrace();
				return doNothing();
			} 
		}
		
		return doNothing();
	}

	/**
	 * Modifie isDetailThese
	 * @return null
	 */
	public WOActionResults detailThese() {
		if (isDetailThese) {
			setIsDetailThese(false);
		} else {
			setIsDetailThese(true);
		}
		return doNothing();
	}

	/**
	 * Enregistrement de la thèse
	 * @return null
	 */
	public WOActionResults validerThese() {

		try {
			if (controlesSaisieValidationThese() > 0) {
				return doNothing();
			}

			if (isModifThese) {
				modifierPartenaire();
			} else { 
				ajoutPartenaire();
			}
			
			hasDirecteurTheseCotutelle();

			edc().saveChanges();
			setIsTheseEditable(false);
			setIsSelectionEnable(true);
			dgDoctorantThese.setObjectArray(leDoctorant().toDoctorantTheses());
			setSelectedEcoleDoctorale(laThese().toContrat().structuresPartenairesForAssociation(roleEcoleDoctorale()).lastObject());
			session().addSimpleSuccessMessage("Confirmation", "Les données ont bien été enregistrées");
		} catch (ERXValidationException e) {
			if (CONTRAT_ETABLISSEMENT_REQUIS.equals(e.method())) {
				session().addSimpleErrorMessage("Erreur", "Le paramètre \"org.cocktail.physalis. C_STRUCTURE_ETAB_GESTIONNAIRE _THESE\" est-il correctement renseigné ? (Voir manuel de configuration).");
			}
			session().addSimpleErrorMessage("Erreur", "La thèse n'a pas été enregistrée : " + e.getMessage());
			edc().revert();
			e.printStackTrace();
			return doNothing();
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", "La thèse n'a pas été enregistrée : " + e.getMessage());
			edc().revert();
			e.printStackTrace();
			return doNothing();
		}

		return doNothing();
	}

	private Boolean hasDirecteurTheseCotutelle() {
	    NSArray<EODirecteurThese> directeurs = editingThese.toDirecteursThese();
	    for (EODirecteurThese directeur : directeurs) {
	    	if (directeur.isDirecteurCotutelle() && editingThese.cotutelle().equals("N")) {
	    		editingThese.setCotutelle("O");
	        	session().addSimpleInfoMessage("Remarque", "L'attribut cotutelle a été automatiquement coché dans les informations de la thèse car il existe un directeur de thèse en cotutelle.");
	        	return true;
	    	}
	    }
	    return false;
    }

	private Integer controlesSaisieValidationThese() {
		
		Integer nbErreurs = 0;
		
	    if (theseDateDebutThese == null || theseDateFinThese == null) {
	    	session().addSimpleErrorMessage("Erreur", "Vous devez saisir les dates de début et de fin de thèse.");
	    	nbErreurs++;
	    } else {
	    	if (theseDateFinThese.before(theseDateDebutThese)) {
	    		session.addSimpleErrorMessage("Erreur", "La date de début doit être avant la date de fin.");
	    		nbErreurs++;
	    	}
	    }

	    if (theseTitre == null) {
	    	session().addSimpleErrorMessage("Erreur", "Vous devez saisir le titre de la thèse.");
	    	nbErreurs++;
	    }

	    if (theseDomaine == null) {
	    	session().addSimpleErrorMessage("Erreur", "Vous devez choisir un domaine de thèse.");
	    	nbErreurs++;
	    }

	    if (theseDomaineScientifique == null) {
	    	session().addSimpleErrorMessage("Erreur", "Vous devez choisir un domaine scientifique.");
	    	nbErreurs++;
	    }
	    
	    if (theseDateFinAnticipee != null && theseMotifFinAnticipee == null) {
	    	session().addSimpleErrorMessage("Erreur", "Vous devez renseigner un motif d'abandon.");
	    	nbErreurs++;
	    }
	    
	    // Si on décoche la cotutelle
	    if (theseIsCotutelle != null && laThese() != null && !theseIsCotutelle && laThese().isCotutelle()) {
	    	CotutelleService cs = CotutelleService.creerNouvelleInstance(edc(), getUtilisateurPersId());
	    	if (cs.listeEtablissementsCotutelle(laThese()).size() > 0) {
	    		session().addSimpleErrorMessage("Erreur", "Vous devez supprimer les informations de l'onglet cotutelle avant de pouvoir décocher le bouton de cotutelle.");
	    		nbErreurs++;
	    	}
	    }
	    
	    return nbErreurs;
    }

	private void modifierPartenaire() throws Exception {
	    FactoryContratPartenaire fcp;
	    ContratPartenaire editingContratPartenaire = editingThese.toContrat().partenairesForAssociation(roleEcoleDoctorale()).lastObject();
	    if (changementEcoleDoctorale()) {
	    	fcp = new FactoryContratPartenaire(edc(), Application.isModeDebug);
	    	fcp.supprimerLeRole(editingContratPartenaire, roleEcoleDoctorale());
	    	fcp.supprimerContratPartenaire(editingContratPartenaire, getUtilisateurPersId());
	    	fcp.creerContratPartenaire(editingThese.toContrat(), editingEcoleDoctorale, new NSArray<EOAssociation>(roleEcoleDoctorale()), theseDateDebutThese,
	    			theseDateFinThese, Boolean.FALSE, null, null);
	    } else {
	    	editingContratPartenaire.setCpDateSignature(theseDateDebutThese);
	    }

	    // A REVOIR POUR INTEGRER LES AVENANTS
	    editingThese.toContrat().setDateDebut(theseDateDebutThese);
	    editingThese.toContrat().setDateFin(theseDateFinThese);
	    editingThese.toContrat().avenantZero().setAvtDateDeb(theseDateDebutThese);
	    editingThese.toContrat().avenantZero().setAvtDateFin(theseDateFinThese);
	    editingThese.toContrat().avenantZero().setDomaineScientifiqueRelationship(theseDomaineScientifique);

	    editingThese.setToTheseDomaineRelationship(theseDomaine);
	    editingThese.setDatePrevSoutenance(theseDatePrevueSoutenance);
	    editingThese.setContractuel(theseContractuel);
	    editingThese.toContrat().setConReferenceExterne(theseReference);
	    editingThese.setToProjetScientifiqueRelationship(theseProjetScientifique);
	    if (theseIsCotutelle()) {
	    	editingThese.setCotutelle("O");
	    } else {
	    	editingThese.setCotutelle("N");
	    }
	    editingThese.toContrat().setConObjet(theseTitre);
	    editingThese.setResumeThese(theseResume);
	    editingThese.setDateFinAnticipee(theseDateFinAnticipee);
	    if (theseDateFinAnticipee != null) {
	    	DoctorantTheseService dts = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
	    	if (dts.modifierDatesLorsDUnAbandon(editingThese, theseDateFinAnticipee)) {
	    		session().addSimpleInfoMessage("Remarque", "Les dates de fin ont été changées.");
	    	} else {
	    		session().addSimpleInfoMessage("Remarque", "Les dates de fin n'ont pas été changées.");
	    	}
	    }
	    editingThese.setMotifFinAnticipee(theseMotifFinAnticipee);
	    editingThese.setCodeSiseDiplome(theseCodeSiseDiplome);
    }

	private boolean changementEcoleDoctorale() {
	    return editingEcoleDoctorale.persId() != persIdEcoleDoctorale;
    }

	private void ajoutPartenaire() throws Exception {
	    FactoryConvention fc;
	    PersonneApplicationUser persAppUser = new PersonneApplicationUser(edc(), session().applicationUser().getPersId());
	    Contrat contrat = null;

	    fc = getFactoryConvention();
	    contrat = fc.creerConventionVierge(persAppUser.getUtilisateur(), etablissementGestionnaire());
	    contrat.setUtilisateurCreationRelationship(persAppUser.getUtilisateur());
	    contrat.setUtilisateurModifRelationship(persAppUser.getUtilisateur());
	    contrat.setUtilisateurValidAdmRelationship(persAppUser.getUtilisateur());
	    contrat.setExerciceCocktailRelationship(new FinderExerciceCocktail(edc()).findExerciceCocktailCourant());
	    contrat.setDateDebut(theseDateDebutThese);
	    contrat.setDateFin(theseDateFinThese);
	    contrat.setTypeClassificationContratRelationship(typeClassification());
	    contrat.setEtablissementRelationship(etablissementGestionnaire(), persAppUser.getPersId());
	    contrat.setCentreResponsabiliteRelationship(etablissementGestionnaire(), persAppUser.getPersId());
	    contrat.setTypeContratRelationship(findTypeContratThese(edc()));
	    contrat.setConDateValidAdm(theseDateDebutThese);
	    contrat.setConObjet(theseTitre);
	    contrat.avenantZero().setAvtObjet(theseTitre);
	    contrat.avenantZero().setAvtObjetCourt("Initial");
	    contrat.avenantZero().setAvtDateDebExec(theseDateDebutThese);
	    contrat.avenantZero().setAvtDateFinExec(theseDateFinThese);
	    contrat.avenantZero().setAvtDateSignature(theseDateDebutThese);
	    contrat.avenantZero().setModeGestionRelationship(findModeGestionThese(edc()));
	    contrat.avenantZero().setDomaineScientifiqueRelationship(theseDomaineScientifique);
	    editingThese = EODoctorantThese.createDoctorantThese(edc(), contrat, leDoctorant(), null);
	    editingThese.setDatePrevSoutenance(theseDatePrevueSoutenance);
	    editingThese.setToTheseDomaineRelationship(theseDomaine);
	    editingThese.setContractuel(theseContractuel);
	    editingThese.setResumeThese(theseResume);
	    editingThese.setDateFinAnticipee(theseDateFinAnticipee);
	    
	    if (theseDateFinAnticipee != null) {
	    	DoctorantTheseService dts = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
	    	if (dts.modifierDatesLorsDUnAbandon(editingThese, theseDateFinAnticipee)) {
	    		session().addSimpleInfoMessage("Remarque", "Les dates de fin ont été changées.");
	    	} else {
	    		session().addSimpleInfoMessage("Remarque", "Les dates de fin n'ont pas été changées.");
	    	}
	    }
	    
	    editingThese.setMotifFinAnticipee(theseMotifFinAnticipee);
	    editingThese.setCodeSiseDiplome(theseCodeSiseDiplome);
	    
	    if (theseIsCotutelle) {
	    	editingThese.setCotutelle("O");
	    } else {
	    	editingThese.setCotutelle("N");
	    }
	    fc.ajouterPartenaire(editingThese.toContrat(), editingEcoleDoctorale, new NSArray<EOAssociation>(roleEcoleDoctorale()), Boolean.FALSE, null);
	    EODoctorantFinancement.createDoctorantFinancement(edc(), contrat.avenantZero(), defaultTypeFinancement());
	    
    }
	
	
	
	private FactoryConvention getFactoryConvention() {
	    return new FactoryConvention(edc(), Application.isModeDebug);
    }

	/**
	 * Click sur le bouton annuler modifications these
	 * @return null
	 */
	public WOActionResults annulerThese() {
		try {
			edc().revert();
			setIsTheseEditable(false);
			setIsSelectionEnable(true);
			setIsModifThese(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}

		return doNothing();
	}

	/**
	 * Action lancée au moment du click sur le bouton ajouter avenant
	 * @return null
	 */
	public WOActionResults ajouterAvenantThese() {
		setIsEditionAvenant(true);
		isDetailThese = false;
		dateDebutAvenant = laThese.dateFin().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
		dateFinAvenant = null;
		titreAvenant = null;
		commentaireAvenant = null;
		setIsSelectionEnable(false);

		return doNothing();
	}
	
	/**
	 * Action lancée au moment du click sur le bouton modifier de l'avenant
	 * @return null
	 */
	public WOActionResults modifierAvenant() {
		setIsModificationAvenant(true);
		isDetailThese = false;
		dateDebutAvenant = selectedAvenant.avtDateDeb();
		dateFinAvenant = selectedAvenant.avtDateFin();
		titreAvenant = selectedAvenant.avtObjetCourt();
		commentaireAvenant = selectedAvenant.avtObservations();
		setIsSelectionEnable(false);
		
		return doNothing();
	}

	/**
	 * Supprime un avenant
	 * @return null
	 * @throws Exception exception
	 */
	public WOActionResults supprimerAvenant() throws Exception {
		try {
			if (selectedAvenant.isAvenantZero()) {
				session.addSimpleErrorMessage("Impossible", "L'avenant initial ne peut être supprimé");
				return null;
			} else {
				FactoryAvenant fa = new FactoryAvenant(edc(), Application.isModeDebug);
				fa.supprimerAvenant(selectedAvenant);
				edc().saveChanges();
				selectedAvenant = null;
				session().addSimpleSuccessMessage("Suppression", "L'avenant a bien été supprimé");
				edc().refreshObject(laThese);
				dgAvenantThese().setObjectArray(listeAvenants());
				dgAvenantThese.setSelectedObject(laThese().avenantZero());
				selectedAvenant = laThese.avenantZero();
			}
		} catch (Exception e) {
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}

		return doNothing();
	}

	/**
	 * @return la liste des avenants valides et non supprimes de la these 
	 */
	@SuppressWarnings("unchecked")
    public NSArray<Avenant> listeAvenants() {
		return laThese().toContrat().avenantsDontInitialNonSupprimesEtValides();
	}

	/**
	 * Enregistrement de l'avenant
	 * @return null
	 */
	public WOActionResults validerAvenant() {

		if (controlesSaisieValidationAvenant() > 0) {
			return doNothing();
		}

		try {
			PersonneApplicationUser persAppUser = new PersonneApplicationUser(edc(), session().applicationUser().getPersId());
			FinderModeGestion fmg = new FinderModeGestion(edc());
			ModeGestion modeGestion = (ModeGestion) fmg.findWithLibelleCourt("CIF").lastObject();
			TypeAvenant typeAvenant = TypeAvenant.TypeAvenantWithCode(edc(), TypeAvenant.CODE_TYPE_AVENANT_ADMINISTRATIF);

			FactoryAvenant fa = new FactoryAvenant(edc(), Application.isModeDebug);
			selectedAvenant = fa.creerAvenantVierge(laThese.toContrat(), null, typeAvenant, persAppUser.getUtilisateur());
			selectedAvenant.setAvtObjet(titreAvenant);
			selectedAvenant.setAvtObjetCourt(titreAvenant);
			selectedAvenant.setAvtObservations(commentaireAvenant);
			selectedAvenant.setAvtDateDeb(dateDebutAvenant);
			selectedAvenant.setAvtDateFin(dateFinAvenant);
			selectedAvenant.setAvtDateValidAdm(dateDebutAvenant);
			selectedAvenant.setAvtDateSignature(dateDebutAvenant);
			selectedAvenant.setModeGestion(modeGestion);
			EODoctorantFinancement.createDoctorantFinancement(edc(), selectedAvenant, defaultTypeFinancement());
			edc().saveChanges();
			session.addSimpleSuccessMessage("Ajout", "L'avenant a bien été créé");
			session.addSimpleInfoMessage("Attention", "Veuillez modifier (si nécessaire) les dates d'encadrement de la thèse.");
			setIsEditionAvenant(false);
			setIsSelectionEnable(true);
			dgAvenantThese().setObjectArray(listeAvenants());
			dgAvenantThese.setSelectedObject(selectedAvenant);
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	private Integer controlesSaisieValidationAvenant() {
		Integer nbErreurs = 0;
		
	    if (dateDebutAvenant == null || dateFinAvenant == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez saisir les dates de début et de fin de l'avenant.");
			nbErreurs++;
		} else {
			if (dateFinAvenant.before(dateDebutAvenant)) {
				session.addSimpleErrorMessage("Erreur", "La date de début doit être avant la date de fin");
				nbErreurs++;
			} else {
				if (isEditionAvenant() && dateDebutAvenant.before(laThese.toContrat().dateFin())) {
					session.addSimpleErrorMessage("Erreur", "La date de début de l'avenant doit se situer après la date de fin actuelle ("
							+ laThese.toContrat().dateFin().toString() + ")");
					nbErreurs++;
				}
			}
		}
	    
	    return nbErreurs;
    }
	
	/**
	 * Enregistrement de la modification de l'avenant
	 * @return null
	 */
	public WOActionResults validerModificationsAvenant() {

		if (controlesSaisieValidationAvenant() > 0) {
			return null;
		}

		try {
			selectedAvenant.setAvtObjet(titreAvenant);
			selectedAvenant.setAvtObjetCourt(titreAvenant);
			selectedAvenant.setAvtObservations(commentaireAvenant);
			selectedAvenant.setAvtDateDeb(dateDebutAvenant);
			selectedAvenant.setAvtDateFin(dateFinAvenant);
			selectedAvenant.setAvtDateValidAdm(dateDebutAvenant);
			selectedAvenant.setAvtDateSignature(dateDebutAvenant);
			selectedAvenant.setAvtDateModif(new NSTimestamp());
			
			edc().saveChanges();
			session.addSimpleSuccessMessage("Ajout", "L'avenant a bien été modifié");
			setIsModificationAvenant(false);
			setIsSelectionEnable(true);
			dgAvenantThese().setObjectArray(listeAvenants());
			dgAvenantThese.setSelectedObject(selectedAvenant);
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Action sur le bouton annuler de l'avenant
	 * @return null
	 */
	public WOActionResults annulerAvenant() {
		try {
			edc().revert();
			setIsEditionAvenant(false);
			setIsModificationAvenant(false);
			setIsSelectionEnable(true);
		} catch (Exception e) {
			session().addSimpleErrorMessage("ERREUR", e.getMessage());
			e.printStackTrace();
		}

		return null;
	}
	
	public Boolean isTitreAvenantDisabled() {
		return (isModificationAvenant() && selectedAvenant().isAvenantZero());
	}

	/**
	 * Class Delegate
	 */
	public class DgDelegate {
		/**
		 * Changement de selection dans le tableau
		 * @param group groupe
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (group == dgDoctorantThese) {
				laThese = (EODoctorantThese) dgDoctorantThese.selectedObject();
				setSelectedEcoleDoctorale(laThese().toContrat().structuresPartenairesForAssociation(roleEcoleDoctorale()).lastObject());
				dgAvenantThese().setObjectArray(listeAvenants());
			}
			if (group == dgAvenantThese) {
				selectedAvenant = (Avenant) dgAvenantThese.selectedObject();
			}
		}
	}

	/**
	 * @return Display group des theses
	 */
	public WODisplayGroup dgDoctorantThese() {
		if (dgDoctorantThese == null) {
			dgDoctorantThese = new WODisplayGroup();
			dgDoctorantThese.setDelegate(dgDelegate);
		}
		if (!isTheseFetched) {
			setIsTheseFetched(true);
			dgDoctorantThese.setObjectArray(leDoctorant().toDoctorantTheses());

		}
		return dgDoctorantThese;
	}

	/**
	 * @return Display group des avenants
	 */
	public WODisplayGroup dgAvenantThese() {
		if (dgAvenantThese == null) {
			dgAvenantThese = new WODisplayGroup();
			dgAvenantThese.setDelegate(dgDelegate);
			dgAvenantThese.setSortOrderings(new NSArray<EOSortOrdering>(Avenant.SORT_INDEX_ASC));
		}
		return dgAvenantThese;
	}
	
	/**
	 * @return container
	 */
	public String aucTheseId() {
		return getComponentId() + "_aucThese";
	}

	/**
	 * @return container
	 */
	public String aucAvenantId() {
		return getComponentId() + "_aucAvenant";
	}

	/**
	 * @return container
	 */
	public String aucAvenantDetailId() {
		return getComponentId() + "_aucAvenantDetailId";
	}

	/**
	 * update containers
	 * @return containers
	 */
	public NSArray<String> onUpdateAvenant() {
		NSArray<String> containers = new NSMutableArray<String>();
		containers.add(aucGestionTheseId());
		return containers;
	}

	/**
	 * @return container
	 */
	public String aucBoutonsThese() {
		return getComponentId() + "_aucBoutonsThese";
	}

	/**
	 * @return container
	 */
	public String aucTheseFormId() {
		return getComponentId() + "_aucTheseForm";
	}

	/**
	 * @return container
	 */
	public String aucGestionTheseId() {
		return getComponentId() + "_aucGestionTheseId";
	}

	/**
	 * update containers
	 * @return containers
	 */
	public NSArray<String> onUpdateDoctorantThese() {
		NSArray<String> containers = new NSMutableArray<String>();
		containers.add(aucAvenantId());
		containers.add(aucTheseFormId());
		containers.add(aucGestionTheseId());
		return containers;
	}

	public EODoctorantThese getEditingDoctorantThese() {
		return editingThese;
	}

	public void setEditingDoctorantThese(EODoctorantThese editingDoctorantThese) {
		this.editingThese = editingDoctorantThese;
	}

	/**
	 * @return l'ecole doctorale selectionnee
	 */
	public EOStructure selectedEcoleDoctorale() {
		return selectedEcoleDoctorale;
	}

	public void setSelectedEcoleDoctorale(EOStructure selectedEcoleDoctorale) {
		this.selectedEcoleDoctorale = selectedEcoleDoctorale;
	}

	public EOStructure getEditingEcoleDoctorale() {
		return editingEcoleDoctorale;
	}

	public void setEditingEcoleDoctorale(EOStructure editingEcoleDoctorale) {
		this.editingEcoleDoctorale = editingEcoleDoctorale;
	}

	public void setIsModifThese(Boolean isModifThese) {
		this.isModifThese = isModifThese;
	}

	public Boolean isModifThese() {
		return isModifThese;
	}

	public void setIsTheseEditable(Boolean isTheseEditable) {
		this.isTheseEditable = isTheseEditable;
		setIsModificationTop(isTheseEditable);
	}

	public Boolean isTheseEditable() {
		return isTheseEditable;
	}

	public boolean isTheseSelectable() {
		return !isTheseEditable;
	}

	/**
	 * @return le doctorant grace au binding passé
	 */
	public EODoctorant leDoctorant() {
		return (EODoctorant) valueForBinding(BINDING_LE_DOCTORANT);
	}

	public void setLaThese(EODoctorantThese laThese) {
		this.laThese = laThese;
	}

	/**
	 * @return la these
	 */
	public EODoctorantThese laThese() {
		return laThese;
	}

	public void setIsTheseFetched(Boolean isTheseFetched) {
		this.isTheseFetched = isTheseFetched;
	}

	/**
	 * @return est ce que la these a été fetchée
	 */
	public Boolean theseFetched() {
		return isTheseFetched;
	}

	public void setIsDetailThese(Boolean isDetailThese) {
		this.isDetailThese = isDetailThese;
	}

	public Boolean isDetailThese() {
		return isDetailThese;
	}

	public void setEtablissementGestionnaire(EOStructure etablissementGestionnaire) {
		this.etablissementGestionnaire = etablissementGestionnaire;
	}

	/**
	 * @return etablissement gestionnaire
	 */
	public EOStructure etablissementGestionnaire() {
		if (etablissementGestionnaire == null) {
			EOQualifier qual = ERXQ.equals(EOStructure.C_STRUCTURE_KEY,
					EOGrhumParametres.parametrePourCle(edc(), ParametresRecherche.PARAM_C_STRUCTURE_ETAB_GESTIONNAIRE_THESE));
			etablissementGestionnaire = EOStructure.fetchByQualifier(edc(), qual);
		}

		return etablissementGestionnaire;
	}

	/**
	 * @return role ecole doctorale
	 */
	public EOAssociation roleEcoleDoctorale() {
		return factoryAssociation().ecoleDoctoraleAssociation(edc());
	}

	public void setTypeClassification(TypeClassificationContrat typeClassification) {
		this.typeClassification = typeClassification;
	}

	/**
	 * @return type classification
	 */
	public TypeClassificationContrat typeClassification() {
		if (typeClassification == null) {
			typeClassification = TypeClassificationContrat.defaultTypeClassificationContrat(edc());
		}
		return typeClassification;
	}

	public void setPersIdEcoleDoctorale(Integer persIdEcoleDoctorale) {
		this.persIdEcoleDoctorale = persIdEcoleDoctorale;
	}

	/**
	 * @return persId ecole doctorale
	 */
	public Integer persIdEcoleDoctorale() {
		return persIdEcoleDoctorale;
	}

	/**
	 * @return type de financement par defaut
	 */
	public EOTypeFinancement defaultTypeFinancement() {
		if (defaultTypeSansFinancement == null) {
			defaultTypeSansFinancement = EOTypeFinancement.fetchTypeFinancement(edc(), EOTypeFinancement.TYPE_FINANCEMENT_KEY, "-");
		}
		return defaultTypeSansFinancement;
	}

	public void setTheseTitre(String theseTitre) {
		this.theseTitre = theseTitre;
	}

	/**
	 * @return titre de la these
	 */
	public String theseTitre() {
		return theseTitre;
	}

	public void setTheseDomaine(EOTheseDomaine theseDomaine) {
		this.theseDomaine = theseDomaine;
	}

	/**
	 * @return domaine de la these
	 */
	public EOTheseDomaine theseDomaine() {
		return theseDomaine;
	}

	public void setTheseDateDebutThese(NSTimestamp theseDateDebutThese) {
		this.theseDateDebutThese = theseDateDebutThese;
	}

	/**
	 * @return date debut these
	 */
	public NSTimestamp theseDateDebutThese() {
		return theseDateDebutThese;
	}

	public void setTheseDateFinThese(NSTimestamp theseDateFinThese) {
		this.theseDateFinThese = theseDateFinThese;
	}

	/**
	 * @return date fin these
	 */
	public NSTimestamp theseDateFinThese() {
		return theseDateFinThese;
	}

	public void setTheseDatePrevueSoutenance(NSTimestamp theseDatePrevueSoutenance) {
		this.theseDatePrevueSoutenance = theseDatePrevueSoutenance;
	}
	
	public void setTheseContractuel(String theseContractuel) {
		this.theseContractuel = theseContractuel;
	}

	/**
	 * @return these contractuel
	 */
	public String theseContractuel() {
		return theseContractuel;
	}

	public void setTheseReference(String theseReference) {
		this.theseReference = theseReference;
	}

	/**
	 * @return reference de la these
	 */
	public String theseReference() {
		return theseReference;
	}

	public void setTheseProjetScientifique(EOProjetScientifique theseProjetScientifique) {
		this.theseProjetScientifique = theseProjetScientifique;
	}

	/**
	 * @return projet scientifique
	 */
	public EOProjetScientifique theseProjetScientifique() {
		return theseProjetScientifique;
	}

	public void setTheseIsCotutelle(Boolean theseIsCotutelle) {
		this.theseIsCotutelle = theseIsCotutelle;
	}

	/**
	 * @return boolean cotutelle
	 */
	public Boolean theseIsCotutelle() {
		return theseIsCotutelle;
	}
	
	public void setTheseResume(String theseResume) {
		this.theseResume = theseResume;
	}

	/**
	 * @return resumé de these
	 */
	public String theseResume() {
		return theseResume;
	}

	public void setDateDebutAvenant(NSTimestamp dateDebutAvenant) {
		this.dateDebutAvenant = dateDebutAvenant;
	}

	/**
	 * @return date debut avenant
	 */
	public NSTimestamp dateDebutAvenant() {
		return dateDebutAvenant;
	}

	public void setDateFinAvenant(NSTimestamp dateFinAvenant) {
		this.dateFinAvenant = dateFinAvenant;
	}

	/**
	 * @return date fin avenant
	 */
	public NSTimestamp dateFinAvenant() {
		return dateFinAvenant;
	}

	public void setIsEditionAvenant(Boolean isEditionAvenant) {
		this.isEditionAvenant = isEditionAvenant;
		setIsModificationTop(isEditionAvenant);
	}

	public Boolean isEditionAvenant() {
		return isEditionAvenant;
	}

	public void setIsModificationAvenant(Boolean isModificationAvenant) {
		this.isModificationAvenant = isModificationAvenant;
		setIsModificationTop(isModificationAvenant);
	}

	public Boolean isModificationAvenant() {
		return isModificationAvenant;
	}

	public void setSelectedAvenant(Avenant selectedAvenant) {
		this.selectedAvenant = selectedAvenant;
	}

	/**
	 * @return avenant selectionné
	 */
	public Avenant selectedAvenant() {
		return selectedAvenant;
	}

	public void setTitreAvenant(String titreAvenant) {
		this.titreAvenant = titreAvenant;
	}

	/**
	 * @return titre avenant
	 */
	public String titreAvenant() {
		return titreAvenant;
	}

	public String getCommentaireAvenant() {
	    return commentaireAvenant;
    }

	public void setCommentaireAvenant(String commentaireAvenant) {
	    this.commentaireAvenant = commentaireAvenant;
    }

	public Boolean isBoutonDisabled() {
		return isTheseEditable || isEditionAvenant || isModificationAvenant;
	}

	public Boolean isBoutonSupprimerAvenantDisabled() {
		if (selectedAvenant == null) {
			return isBoutonDisabled();
		}
		return isBoutonDisabled() || selectedAvenant.isAvenantZero();
	}

	/**
	 * @return classe du bouton detail
	 */
	public String typeBoutonDetail() {
		if (isDetailThese) {
			return "up_triangle";
		} else {
			return "down_triangle";
		}
	}

	/**
	 * @return date de fin anticipee
	 */
	public NSTimestamp theseDateFinAnticipee() {
	    return theseDateFinAnticipee;
    }

	public void setTheseDateFinAnticipee(NSTimestamp theseDateFinAnticipee) {
	    this.theseDateFinAnticipee = theseDateFinAnticipee;
    }

	/**
	 * @return motif de fin anticipee
	 */
	public String theseMotifFinAnticipee() {
	    return theseMotifFinAnticipee;
    }

	public void setTheseMotifFinAnticipee(String theseMotifFinAnticipee) {
	    this.theseMotifFinAnticipee = theseMotifFinAnticipee;
    }

	/**
	 * @return domaine scientifique
	 */
	public EODomaineScientifique theseDomaineScientifique() {
	    return theseDomaineScientifique;
    }

	public void setTheseDomaineScientifique(EODomaineScientifique theseDomaineScientifique) {
	    this.theseDomaineScientifique = theseDomaineScientifique;
    }

	/**
	 * @return code SISE du diplome
	 */
	public String theseCodeSiseDiplome() {
	    return theseCodeSiseDiplome;
    }

	public void setTheseCodeSiseDiplome(String theseCodeSiseDiplome) {
	    this.theseCodeSiseDiplome = theseCodeSiseDiplome;
    }
	
	public void setIsModification(boolean isModification) {
		disableOnglets(isModificationTop() || isModificationBottom(), true);
	}
	
	public boolean isModification() {
		return valueForBooleanBinding(BINDING_IS_MODIFICATION, false);
	}
	
	public void setIsModificationTop(boolean isModificationTop) {
		this.isModificationTop = isModificationTop;
		disableOnglets(isModificationTop || isModificationBottom(), true);
	}
	
	public boolean isModificationTop() {
		return isModificationTop;
	}
	
	public void setIsModificationBottom(boolean isModificationBottom) {
		this.isModificationBottom = isModificationBottom;
		disableOnglets(isModificationTop() || isModificationBottom, true);
	}
	
	public boolean isModificationBottom() {
		return isModificationBottom;
	}
	
	public boolean areTabsDisabled() {
		return isBoutonDisabled();
	}
	
}
