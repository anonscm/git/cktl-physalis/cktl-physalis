package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

public class GestionDoctorant extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 7849379924169158956L;
	private Boolean ongletDoctorantSelected = true;
	private Boolean ongletScolariteSelected = false;
	private Boolean ongletTheseSelected = false;
	private EODoctorant leDoctorant = null;
	private EOEditingContext edcDoctorant = null;
	private boolean isModification = false;
	private boolean wantInit = true;

	public GestionDoctorant(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
	    if (wantInit) {
	    	session().setTabDisabled(false);
	    	wantInit = false;
	    }
	    super.appendToResponse(response, context);
	}

	/**
	 * @return the aucDoctorantId
	 */
	public String aucDoctorantId() {
		return "aucDoctorantId";
	}

	/*
	 * Id des onglets
	 */
	public String atpOngletDoctorantId() {
		return getComponentId() + "_ongletDoctorant";
	}

	public String atpOngletScolariteId() {
		return getComponentId() + "_ongletScolarite";
	}

	public String atpOngletTheseId() {
		return getComponentId() + "_ongletThese";
	}
	
	/*
	 * Gestion des onglets
	 */

	public Boolean ongletDoctorantSelected() {
		return ongletDoctorantSelected;
	}

	public void setOngletDoctorantSelected(Boolean ongletDoctorantSelected) {
		this.ongletDoctorantSelected = ongletDoctorantSelected;
	}

	public Boolean ongletScolariteSelected() {
		return ongletScolariteSelected;
	}

	public void setOngletScolariteSelected(Boolean ongletScolariteSelected) {
		this.ongletScolariteSelected = ongletScolariteSelected;
	}

	public Boolean ongletTheseSelected() {
		return ongletTheseSelected;
	}

	public void setOngletTheseSelected(Boolean ongletTheseSelected) {
		this.ongletTheseSelected = ongletTheseSelected;
	}

	public EODoctorant leDoctorant() {
		return leDoctorant;
	}

	public void setLeDoctorant(EODoctorant leDoctorant) {
		this.leDoctorant = leDoctorant;
	}

	public void setEdcDoctorant(EOEditingContext edcDoctorant) {
		this.edcDoctorant = edcDoctorant;
	}

	public EOEditingContext edcDoctorant() {
		return edcDoctorant;
	}
	
	public WOActionResults pagePrecedente() {
		return session().getPageIntermediaire();
	}
	
	public String libellePagePrecedente() {
		return session().getLibellePageIntermediaire();
	}

	public boolean isModification() {
	    return isModification;
    }

	public void setIsModification(boolean isModification) {
	    this.isModification = isModification;
    }
	
	public String getNomAffichageDoctorant() {
		if (leDoctorant() != null) {
			return leDoctorant().toIndividuFwkpers().toCivilite().cCivilite() + " " + leDoctorant().toIndividuFwkpers().getNomCompletAffichage().toUpperCase();
		}
		return "";
	}
}