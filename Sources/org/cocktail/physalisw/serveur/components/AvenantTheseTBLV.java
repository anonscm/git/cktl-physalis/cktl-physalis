package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.physalisw.serveur.components.common.PhysalisTableView;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class AvenantTheseTBLV extends PhysalisTableView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6260026670470566974L;
	private static final String LIBELLE_AVENANT = Avenant.AVT_OBJET_COURT_KEY;
	private static final String INDEX_AVENANT = Avenant.AVT_INDEX_KEY;
	static {
		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("No");
		col0.setOrderKeyPath(INDEX_AVENANT);
		col0.setRowCssClass("alignToCenter useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + INDEX_AVENANT, " ");
		col0.setAssociations(ass0);
		_colonnesMap.takeValueForKey(col0, INDEX_AVENANT);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Objet");
		col1.setOrderKeyPath(LIBELLE_AVENANT);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + LIBELLE_AVENANT, "Non renseigné");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, LIBELLE_AVENANT);
	}

	public AvenantTheseTBLV(WOContext context) {
		super(context);
	}

	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { INDEX_AVENANT, LIBELLE_AVENANT });
	}

}