package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class MembreJuryForm extends PhysalisWOComponent {

	private static final String B_MembreJury = "editingMembreJury";
	private static final String B_roleMembreJury = "roleMembreJury";
	private static final String B_isRoleEnable = "isRoleEnable";
	private static final String B_isAffichage = "isAffichage";
	private static final String B_typeMembre = "typeMembre";
	private static final String B_laThese = "laThese";
	private static final String BINDING_IS_ETABLISSEMENT_FRANCAIS = "isEtablissementFrancais";
	private static final String TYPE_MEMBRE_JURY = "JURY";
	private static final String TYPE_MEMBRE_RAPPORTEUR = "RAPPORTEUR";
	private Boolean isEnseignant = false;
	private Boolean isRapporteurSeance = false;
	private EOAssociation itemRoleJury;
	private EOAssociation presidentJury = null;
	private EOAssociation selectedRoleMembreJury = null;
	private EODoctorantThese laThese;
	// par defaut la partie role n'est pas visible
	private Boolean isRoleEnable = false;
	private Boolean isAffichage = false;
	private String typeMembre;
	private EOStructure selectedEtablissement;

	public MembreJuryForm(WOContext context) {
		super(context);
	}

	public String aucMembreJuryId() {
		return getComponentId() + "_aucMembreJuryId";
	}
	
	public String aucEtablissementId() {
		return getComponentId() + "_aucEtablissementId";
	}
	
	public String aucCorpsId() {
		return getComponentId() + "_aucCorpsId";
	}
	
	public EOMembreJuryThese editingMembreJury() {
		return (EOMembreJuryThese) valueForBinding(B_MembreJury);
	}

	public void setEditingMembreJury(EOMembreJuryThese editingMembreJury) {
		setValueForBinding(editingMembreJury, B_MembreJury);
	}

	public String typeMembre() {
		typeMembre = (String) valueForBinding(B_typeMembre);
		return typeMembre;
	}

	public String classBox() {
		if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
			return "boxtitreOrange";
		} else {
			return "boxtitre";
		}
	}
	
	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
		setValueForBinding(isEtablissementFrancais, BINDING_IS_ETABLISSEMENT_FRANCAIS);
	}

	public Boolean isEtablissementFrancais() {
		return booleanValueForBinding(BINDING_IS_ETABLISSEMENT_FRANCAIS, true);
	}

	public void setIsEtablissementEtranger(Boolean isEtablissementEtranger) {
	}

	public Boolean isEtablissementEtranger() {
		return !isEtablissementFrancais();
	}

	public Boolean isEnseignant() {
		String temEnseignant = null;
		if (editingMembreJury() != null) {
			temEnseignant = editingMembreJury().temEnseignant();
		}
		if (temEnseignant != null && temEnseignant.equals("O")) {
			isEnseignant = true;
		} else {
			isEnseignant = false;
		}
		return isEnseignant;
	}

	public Boolean isRapporteurSeance() {
		String rapporteurSeance = null;
		if (editingMembreJury() != null) {
			rapporteurSeance = editingMembreJury().rapporteurSeance();
		}
		if (rapporteurSeance != null && rapporteurSeance.equals("O")) {
			isRapporteurSeance = true;
		} else {
			isRapporteurSeance = false;
		}
		return isRapporteurSeance;
	}

	public void setIsEnseignant(Boolean isEnseignant) {
		this.isEnseignant = isEnseignant;
		if (isEnseignant) {
			editingMembreJury().setTemEnseignant("O");
		} else {
			editingMembreJury().setTemEnseignant("N");
		}
	}

	public Boolean theseHasPresident() {
		Boolean hasPresident = false;
		NSArray lesRepartPresidents = null;
		lesRepartPresidents = laThese().toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().presidentJuryAssociation(edc()));
		if (lesRepartPresidents != null) {
			if (lesRepartPresidents.count() > 0) {
				hasPresident = true;
			}
		}
		return hasPresident;
	}

	public NSArray<EOAssociation> listeRoleJury() {
		NSArray roles = null;
		if (isRoleEnable) {
			// on verifie le nombre de présidents du jury
			EOAssociation associationJury = factoryAssociation().rolesJuryAssociation(edc());
			if (associationJury != null) {
				roles = associationJury.getFils(edc());
				if (selectedRoleMembreJury() != factoryAssociation().presidentJuryAssociation(edc()) && theseHasPresident()) {
					roles.remove(factoryAssociation().presidentJuryAssociation(edc()));
				}

			}
		}
		return roles;
	}

	public void setItemRoleJury(EOAssociation itemRoleJury) {
		this.itemRoleJury = itemRoleJury;
	}

	public EOAssociation itemRoleJury() {
		return itemRoleJury;
	}

	public void setSelectedRoleMembreJury(EOAssociation selectedRoleMembreJury) {
		this.selectedRoleMembreJury = selectedRoleMembreJury;
		setValueForBinding(selectedRoleMembreJury, B_roleMembreJury);
	}
	
	public EOAssociation selectedRoleMembreJury() {
		if (hasBinding(B_roleMembreJury)) {
			selectedRoleMembreJury = (EOAssociation) valueForBinding(B_roleMembreJury);
		}
		return selectedRoleMembreJury;
	}
	
	public void setIsRoleEnable(Boolean isRoleEnable) {
		this.isRoleEnable = isRoleEnable;
	}
	
	public Boolean isRoleEnable() {
		isRoleEnable = (Boolean) valueForBinding(B_isRoleEnable);
		return isRoleEnable;
	}
	
	public void setIsAffichage(Boolean isAffichage) {
		this.isAffichage = isAffichage;
	}
	
	public Boolean isAffichage() {
		if (hasBinding(B_isAffichage)) {
			isAffichage = (Boolean) valueForBinding(B_isAffichage);
		}
		return isAffichage;
	}
	
	public EODoctorantThese laThese() {
		EODoctorantThese theThese = (EODoctorantThese) valueForBinding(B_laThese);
		if (laThese != theThese) {
			setLaThese(theThese);
		}
		return laThese;
	}
	
	public void setLaThese(EODoctorantThese laThese) {
		this.laThese = laThese;
	}
	
	public void setPresidentJury(EOAssociation presidentJury) {
		this.presidentJury = presidentJury;
	}
	
	public void setIsRapporteurSeance(Boolean isRapporteurSeance) {
		this.isRapporteurSeance = isRapporteurSeance;
		if (isRapporteurSeance) {
			editingMembreJury().setRapporteurSeance("O");
		} else {
			editingMembreJury().setRapporteurSeance("N");
		}
	}
	
	public Boolean wantReset() {
		return true;
	}
	
	public void setWantReset(Boolean wantReset) {
	}
	
	public EOStructure selectedEtablissement() {
		return selectedEtablissement;
	}
	
	public void setSelectedEtablissement(EOStructure selectedEtablissement) {
		this.selectedEtablissement = selectedEtablissement;
	}
	
	public Boolean isAjoutEtablissementDisabled() {
		return (selectedEtablissement == null);
	}
	
	public WOActionResults changeEtablissement() {
		editingMembreJury().setToRneRelationship(null);
		editingMembreJury().setLibelleStructExterne(null);
		return doNothing();
	}
	
	public WOActionResults supprimerCorps() {
		editingMembreJury().setToCorpsRelationship(null);
		return doNothing();
	}

}