package org.cocktail.physalisw.serveur.components;

import java.math.BigDecimal;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderContratPartenaire;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.service.CotutelleService;
import org.cocktail.fwkcktlrecherche.server.metier.service.DirecteurTheseService;
import org.cocktail.fwkcktlrecherche.server.metier.service.DoctorantTheseService;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Onglet encadrant d'une these
 */
public class Directeur extends PhysalisWOComponent {
	
	private static final long serialVersionUID = 1099009606048816548L;
	private static final Integer POURCENTAGE_ENCADREMENT_TOTAL = 100;
	private static final Integer NB_MAX_DIRECTEURS_THESE = 2;
	private static final String B_laThese = "laThese";
	private static final String B_rechargerDirecteur = "rechargerDirecteur";
	private static final String B_quotiteEncadrement = "quotiteEncadrement";
	private static final String B_showDateAutorisation = "showDateAutorisation";
	private static final String BINDING_IS_MODIFICATION_BOTTOM = "isModificationBottom";
	private EODoctorantThese laThese = null;
	private ERXDisplayGroup<EODirecteurThese> dgDirThese;
	private EODirecteurThese editingDirecteurThese = null;
	private EODirecteurThese selectedDirecteurThese = null;
	private EODirecteurThese currentDirecteurThese = null;
	public EOAssociation selectedRoleDirecteur = null;
	public EOAssociation editingRoleDirecteur = null;
	public EORepartAssociation selectedRepartAssocDirecteur;
	public EORepartAssociation editingRepartAssocDirecteur;
	private EORepartStructure editingRepartStructureDirecteur;
	private EOIndividu selectedEncadrant = null;
	private Boolean isModif = false;
	private Boolean isEditable = false;
	private Boolean isEtablissementFrancais = false;
	private Boolean isLaboratoireLocal = false;
	private NSTimestamp editingDateDebutEncadrement = null;
	private NSTimestamp editingDateFinEncadrement = null;
	private String editingCommentaireEncadrement = null;
	private BigDecimal editingQuotiteEncadrement = null;
	private Boolean isCotutelle = false;
	private Boolean wasCotutelle = false;
	private EOAdresse editingAdresse = null;
	private EOPays editingPays = null;
	private ContratPartenaire ancienneCotutelle = null;
	private Boolean showDateAutorisation = false;
	
	public Directeur(WOContext context) {
		super(context);
	}
	
	public String encadrementTableViewId() {
		return getComponentId() + "_encadrementTableViewId";
	}
	
	public String aucDirTheseId() {
		return getComponentId() + "_aucDirTheseId";
	}

	public class DgDelegate {
		
		public void displayGroupDidChangeSelection(WODisplayGroup displayGroup) {
			ERXDisplayGroup<EODirecteurThese> _displayGroup = (ERXDisplayGroup<EODirecteurThese>) displayGroup;
			if (_displayGroup.selectedObject() != null) {
				setSelectedDirecteurThese(_displayGroup.selectedObject());
			}
		}
		
	}
	
	public NSArray<EODirecteurThese> listeDirecteurs() {
		return laThese().toDirecteursThese(null, true);
	}

	public ERXDisplayGroup<EODirecteurThese> dgDirThese() {
		if (dgDirThese == null || rechargerDirecteur()) {
			dgDirThese = new ERXDisplayGroup<EODirecteurThese>();
			dgDirThese.setDelegate(new DgDelegate());
			dgDirThese.setSortOrderings(new NSArray<EOSortOrdering>(EODirecteurThese.SORT_NOM_DIRECTEUR_ASC));
			dgDirThese.setObjectArray(listeDirecteurs());
			setRechargerDirecteur(false);
		}

		return dgDirThese;
	}
	
	public Boolean hasDirecteur() {
		return listeDirecteurs().size() > 0;
	}

	public WOActionResults ajouterDirecteur() {

		editingDirecteurThese = EODirecteurThese.createDirecteurThese(edc(), null, laThese());
		isModif = false;
		setIsEditable(true);
		setIsSelectionEnable(false);
		setIsEtablissementFrancais(true);
		setIsLaboratoireLocal(true);
		setShowDateAutorisation(false);
		// par defaut
		selectedRoleDirecteur = null;
		selectedEncadrant = null;
		editingRepartAssocDirecteur = null;
		editingDateDebutEncadrement = laThese().dateDeb();
		editingDateFinEncadrement = laThese().dateFin();
		editingCommentaireEncadrement = null;
		
		if (calculSommeQuotite().compareTo(new BigDecimal(POURCENTAGE_ENCADREMENT_TOTAL)) >= 0) {
			setEditingQuotiteEncadrement(new BigDecimal(0));
		} else {
			BigDecimal quotite = new BigDecimal(POURCENTAGE_ENCADREMENT_TOTAL);
			quotite = quotite.subtract(calculSommeQuotite());
			setEditingQuotiteEncadrement(quotite);
		}
		isCotutelle = false;
		editingPays = null;
		// l'adresse est a null au départ elle sera gérée lors de la validation
		editingAdresse = null;
		
		return doErxRedirect(context());
	}

	public WOActionResults modifierDirecteur() {
		editingDirecteurThese = selectedDirecteurThese();
		isModif = true;
		setIsEditable(true);
		setIsSelectionEnable(false);
		setShowDateAutorisation(false);
		// isFicheDirTheseVisible = true;
		// PROBVLEME pas de topersonneElt;....
		editingRepartAssocDirecteur = editingDirecteurThese.repartAssociationDirecteur();
		editingRepartStructureDirecteur = editingDirecteurThese.repartStructureDirecteur();
		selectedRoleDirecteur = editingRepartAssocDirecteur.toAssociation();
		editingDateDebutEncadrement = editingRepartAssocDirecteur.rasDOuverture();
		editingDateFinEncadrement = editingRepartAssocDirecteur.rasDFermeture();
		editingQuotiteEncadrement = editingRepartAssocDirecteur.rasQuotite();
		editingCommentaireEncadrement = editingRepartAssocDirecteur.rasCommentaire();

		// EOQualifier qualDirTheseHabilite =
		// ERXQ.and(ERXQ.equals(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY,
		// editingDirecteurThese.toIndividuDirecteur()),
		// ERXQ.equals(EODirecteurTheseHabilite.TO_STRUCTURE_ED_FWKPERS_KEY,
		// laThese().ecoleDoctorale()));
//		EOQualifier qualDirTheseHabilite = ERXQ.equals(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY, editingDirecteurThese.toIndividuDirecteur());
//		selectedDirecteurTheseHabilite = EODirecteurTheseHabilite.fetchDirecteurTheseHabilite(edc(), qualDirTheseHabilite);

		if (editingDirecteurThese.toStructureEtabFwkPers() == null) {
			setIsEtablissementFrancais(true);
		} else {
			setIsEtablissementFrancais(false);
		}
		if (editingDirecteurThese.libelleLabo() == null) {
			setIsLaboratoireLocal(true);
		} else {
			setIsLaboratoireLocal(false);
		}
		isCotutelle = selectedDirecteurThese().isDirecteurCotutelle();
		wasCotutelle = isCotutelle;
		if (isCotutelle) {
			try {
				editingPays = selectedDirecteurThese().toStructureEtabFwkPers().getAdressePrincipale().toPays();
				FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
				cp_exist.setContrat(laThese().toContrat());
				cp_exist.setPartenaire(selectedDirecteurThese.toStructureEtabFwkPers());
				ancienneCotutelle = (ContratPartenaire) cp_exist.find().lastObject();
			} catch (ExceptionFinder e) {
				e.printStackTrace();
			}
		} else {
			editingPays = null;
			ancienneCotutelle = null;
		}
		editingAdresse = null;

		return doErxRedirect(context());

	}
	
	public WOActionResults supprimerDirecteur() {

		if (selectedDirecteurThese != null) {

			try {
				DoctorantTheseService dts = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
				dts.supprimerEncadrant(laThese(), selectedDirecteurThese(), Application.isModeDebug);
				
				edc().deleteObject(selectedDirecteurThese);
				edc().saveChanges();
				selectedDirecteurThese = null;
				session().addSimpleSuccessMessage("Suppresion", "L'encadrant a bien été supprimé");
				dgDirThese.setObjectArray(laThese().toDirecteursThese(null, true));
			} catch (Exception e) {
				edc().revert();
				session().addSimpleErrorMessage("Erreur", e.toString());
				e.printStackTrace();
			}
//			AjaxUpdateContainer.updateContainerWithID(aucDirTheseId(), context());
		}

		return null;
	}

	public WOActionResults validerModif() {
		FactoryContratPartenaire fcp = new FactoryContratPartenaire(edc(), Application.isModeDebug);
		editingRepartAssocDirecteur.setToAssociationRelationship(selectedRoleDirecteur);
		editingRepartAssocDirecteur.setRasDOuverture(editingDateDebutEncadrement);
		editingRepartAssocDirecteur.setRasDFermeture(editingDateFinEncadrement);
		editingRepartAssocDirecteur.setRasQuotite(editingQuotiteEncadrement);
		editingRepartAssocDirecteur.setRasCommentaire(editingCommentaireEncadrement);

		try {
			// si il y avait une cotutelle et qu'il n'y en a plus ou que
			// l'ancien etablissement de cotutelle est différent du nouveau on
			// supprime la cotutelle
			if (wasCotutelle) {
				if ((isCotutelle && ancienneCotutelle.partenaire() != editingDirecteurThese.toStructureEtabFwkPers()) || !isCotutelle) {
					// on supprime l'ancienne cotutelle					
					fcp.supprimerLeRole(ancienneCotutelle, factoryAssociation().etablissementCotutelleAssociation(edc()));
					fcp.supprimerContratPartenaire(ancienneCotutelle, getUtilisateurPersId());
				}
				if (!isCotutelle && laThese().isCotutelle()) {
					session().addSimpleInfoMessage("Attention", "La thèse est toujours considérée comme une cotutelle. (Voir détails de la thèse).");
				}
			}
			if (isCotutelle) {
				FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
				cp_exist.setContrat(laThese().toContrat());
				cp_exist.setPartenaire(editingDirecteurThese.toStructureEtabFwkPers());
				ContratPartenaire cpCotutelle = null;
				cpCotutelle = (ContratPartenaire) cp_exist.find().lastObject();
				if (cpCotutelle == null) {
					cpCotutelle = fcp.creerContratPartenaire(laThese().toContrat(), editingDirecteurThese.toStructureEtabFwkPers(), new NSArray<EOAssociation>(factoryAssociation()
							.etablissementCotutelleAssociation(edc())), editingDateDebutEncadrement(), editingDateFinEncadrement(), Boolean.FALSE, null, editingCommentaireEncadrement(), editingQuotiteEncadrement());
				} else {
					fcp.ajouterLeRolePourLesDates(cpCotutelle, factoryAssociation().etablissementCotutelleAssociation(edc()), editingDateDebutEncadrement(),
							editingDateFinEncadrement());
				}
				
				verificationCotutelleTheseEstCoche();
				
				verificationEtablissementCotutelleEstRenseigne(editingDirecteurThese, editingDateDebutEncadrement(), editingDateFinEncadrement());
				
				// gestion du pays de l'adresse principale
				// on récupère l'adresse principale de l'etablissement
				// sélectionné
				setEditingAdresse(editingDirecteurThese.toStructureEtabFwkPers().getAdressePrincipale());
				if (editingAdresse() != null) {
					// si changement de pays dans cotutelleForm (editingPays) on
					// repercute sur l'adresse principale
					if (editingAdresse().toPays() != editingPays()) {
						// editingAdresse().setToPaysRelationship(editingPays().localInstanceIn(edc()));
						editingAdresse().setToPaysRelationship(editingPays());
					}
				} else {
					session.addSimpleInfoMessage("Probleme", "il faut ajouter une adresse principale");
				}

			}

			edc().saveChanges();
			isModif = false;
			session().addSimpleSuccessMessage("Modification", "Modification enregistrée");
			setIsEditable(false);
			setIsSelectionEnable(true);
			dgDirThese.setObjectArray(laThese().toDirecteursThese(null, true));
//			AjaxUpdateContainer.updateContainerWithID(aucDirTheseId(), context());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e.toString());
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults validerAjout() {

		ContratPartenaire cp = null;
		try {
			FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
			cp_exist.setContrat(laThese().toContrat().localInstanceIn(edc()));
			cp_exist.setPartenaire(selectedEncadrant);
			cp = (ContratPartenaire) cp_exist.find().lastObject();

			// si le contrat partenaire n'existe pas, on le crée
			// sinon on ajoute le role au partenaire
			FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
			if (cp == null) {
				cp = fc.creerContratPartenaire(laThese().toContrat().localInstanceIn(edc()), selectedEncadrant, new NSArray<EOAssociation>(
						selectedRoleDirecteur), editingDateDebutEncadrement(), editingDateFinEncadrement(), Boolean.FALSE, null, editingCommentaireEncadrement(), editingQuotiteEncadrement());
			} else {
				fc.ajouterLeRolePourLesDatesEtQuotite(cp, selectedRoleDirecteur, editingDateDebutEncadrement(), editingDateFinEncadrement(), editingQuotiteEncadrement()); //TODO : ajouter commentaire sur le directeur
			}
			editingDirecteurThese().setToContratPartenaireCwRelationship(cp);
			// si la case Cotutelle est cochée, on ajoute l'etablissement en
			// cotutelle
			if (isCotutelle) {
				cp_exist.setPartenaire(editingDirecteurThese.toStructureEtabFwkPers());
				ContratPartenaire cpCotutelle = null;
				cpCotutelle = (ContratPartenaire) cp_exist.find().lastObject();
				if (cpCotutelle == null) {
					cpCotutelle = fc.creerContratPartenaire(laThese().toContrat(), editingDirecteurThese.toStructureEtabFwkPers(), new NSArray<EOAssociation>(factoryAssociation()
							.etablissementCotutelleAssociation(edc())), editingDateDebutEncadrement(), editingDateFinEncadrement(), Boolean.FALSE, null, editingCommentaireEncadrement(), editingQuotiteEncadrement());
				} else {
					fc.ajouterLeRolePourLesDatesEtQuotite(cpCotutelle, factoryAssociation().etablissementCotutelleAssociation(edc()), editingDateDebutEncadrement(),
							editingDateFinEncadrement(), editingQuotiteEncadrement());
				}
				
				verificationCotutelleTheseEstCoche();
				
				verificationEtablissementCotutelleEstRenseigne(editingDirecteurThese, editingDateDebutEncadrement(), editingDateFinEncadrement());
				
				// gestion du pays de l'adresse principale
				// on récupère l'adresse principale de l'etablissement
				// sélectionné
				setEditingAdresse(editingDirecteurThese.toStructureEtabFwkPers().getAdressePrincipale());
				if (editingAdresse() != null) {
					// si changement de pays dans cotutelleForm (editingPays) on
					// repercute sur l'adresse principale
					if (editingAdresse().toPays() != editingPays()) {
						// editingAdresse().setToPaysRelationship(editingPays().localInstanceIn(edc()));
						editingAdresse().setToPaysRelationship(editingPays());
					}
				} else {
					session.addSimpleInfoMessage("Probleme", "il faut ajouter une adresse principale");
				}

			}

			edc().saveChanges();
			session().addSimpleSuccessMessage("Ajout", "Encadrant enregistré");
			isModif = false;
			setIsEditable(false);
			setIsSelectionEnable(true);
			dgDirThese.setObjectArray(laThese().toDirecteursThese(null, true));
//			AjaxUpdateContainer.updateContainerWithID(aucDirTheseId(), context());
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e.toString());
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Rempli l'onglet cotutelle avec l'info de l'établissement de cotutelle
	 * @param directeurThese
	 * @param dateDebut
	 * @param dateFin
	 */
	private void verificationEtablissementCotutelleEstRenseigne(EODirecteurThese directeurThese, NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (isCotutelle() && directeurThese != null && directeurThese.toStructureEtabFwkPers() != null && dateDebut != null && dateFin != null) {
			CotutelleService cotutelleService = CotutelleService.creerNouvelleInstance(edc(), getUtilisateurPersId());
			NSArray<EORepartAssociation> lesRepartAssociationEtablissement = cotutelleService.listeEtablissementsCotutelle(laThese());
			if (lesRepartAssociationEtablissement.size() == 0) {
				ajoutEtablissementCotutelle(directeurThese, dateDebut, dateFin);
//			} else if (lesRepartAssociationEtablissement.size() == 1) {
//				if (!directeurThese.toStructureEtabFwkPers().equals(lesRepartAssociationEtablissement.get(0).toPersonne())) {
//					ajoutEtablissementCotutelle(directeurThese, dateDebut, dateFin);
//				}
			}
		}
	}

	private void ajoutEtablissementCotutelle(EODirecteurThese directeurThese, NSTimestamp dateDebut, NSTimestamp dateFin) {
	    ContratPartenaire cp = null;
	    FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
	    try {
	    	cp_exist.setContrat(laThese().toContrat());
	    	cp_exist.setPartenaire(directeurThese.toStructureEtabFwkPers());
	        cp = (ContratPartenaire) cp_exist.find().lastObject();
	        if (cp == null) {
	        	FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
	    		cp = fc.creerContratPartenaire(laThese().toContrat(), directeurThese.toStructureEtabFwkPers(), new NSArray<EOAssociation>(factoryAssociation()
	    				.etablissementCotutelleAssociation(edc())), dateDebut, dateFin, Boolean.FALSE, null, null);
	    		session().addSimpleInfoMessage("Remarque", "L'établissement de cotutelle a été automatiquement rajouté dans l'onglet cotutelle.");
	        }
	    } catch (Exception e) {
	        session().addSimpleErrorMessage("Erreur", e.toString());
	        e.printStackTrace();
	    }
    }
	
	private void verificationCotutelleTheseEstCoche() {
	    if (!laThese().isCotutelle()) {
	    	laThese().setCotutelle("O");
	    	session().addSimpleInfoMessage("Remarque", "L'attribut cotutelle a été automatiquement coché dans les informations de la thèse.");
	    }
    }
	
	private boolean isAjoutEncadrantPossible(EOIndividu encadrant) {
		
		NSArray<EODirecteurThese> listeDirecteurs = listeDirecteurs();
		for (EODirecteurThese directeur : listeDirecteurs) {
			if (directeur.toIndividuDirecteur().persId() == encadrant.persId()) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Valide les données du directeur (selectedEncadrant lors de l'ajout ou editingDirecteur lors d'une modification)
	 * @return null
	 */
	public WOActionResults validerDirecteurThese() {
		Integer nbErreurs = 0;
		
		if (selectedEncadrant == null && !isModif()) {
			session().addSimpleErrorMessage("Erreur", "Vous devez choisir un encadrant.");
			nbErreurs++;
		}
		
		if (isModif()) {
			if (editingDirecteurThese().toIndividuDirecteur().noIndividu() == laThese().toDoctorant().toIndividuFwkpers().noIndividu()) {
				session().addSimpleErrorMessage("Erreur", "Le doctorant ne peut pas être son propre encadrant.");
				nbErreurs++;
			}
		} else {
			if (selectedEncadrant.noIndividu() == laThese().toDoctorant().toIndividuFwkpers().noIndividu()) {
				session().addSimpleErrorMessage("Erreur", "Le doctorant ne peut pas être son propre encadrant.");
				nbErreurs++;
			}
		}
		
		if (!isModif() && selectedEncadrant != null && !isAjoutEncadrantPossible(selectedEncadrant)) {
			session().addSimpleErrorMessage("Erreur", "Vous avez d&eacute;j&agrave; ajout&eacute; cet encadrant.");
			nbErreurs++;
		}

		if (selectedRoleDirecteur == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez choisir un role pour cet encadrant");
			nbErreurs++;
		}

		if ((!isEtablissementFrancais() && editingDirecteurThese().toStructureEtabFwkPers() == null)
				|| (isEtablissementFrancais() && editingDirecteurThese().toRneFwkpers() == null)) {
			session.addSimpleErrorMessage("Erreur", "Vous devez indiquer un établissement");
			nbErreurs++;
		}

		if ((!isLaboratoireLocal() && editingDirecteurThese().libelleLabo() == null) || (isLaboratoireLocal() && editingDirecteurThese().toStructureLaboFwkpers() == null)) {
			session.addSimpleErrorMessage("Erreur", "Vous devez indiquer un laboratoire");
			nbErreurs++;
		}

		if (editingDateDebutEncadrement == null || editingDateFinEncadrement == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez saisir les dates de début et de fin d'encadrement.");
			nbErreurs++;
		} else {
			if (editingDateFinEncadrement.before(editingDateDebutEncadrement)) {
				session.addSimpleErrorMessage("Erreur", "La date de début doit être avant la date de fin");
				nbErreurs++;
			} else {
				if (!laThese.isDateCoherente(editingDateDebutEncadrement, editingDateFinEncadrement)) {
					session.addSimpleErrorMessage("Erreur", "Les dates de début et de fin d'encadrement doivent être comprises dans l'intervale de la thèse (du "
							+ laThese.toContrat().dateDebut().toString() + " au " + laThese.toContrat().dateFin().toString() + ")");
					nbErreurs++;
				}
			}
		}
		
		if (showDateAutorisation() && editingDirecteurThese.dateAutorisation() == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez indiquer la date d'autorisation pour que "
					+ editingDirecteurThese().toIndividuDirecteur().getNomAndPrenom() + " puisse diriger cette th&egrave;se.");
			nbErreurs++;
		}

		if (isEtablissementFrancais() && editingDirecteurThese().toStructureEtabFwkPers() != null) {
			editingDirecteurThese().setToStructureEtabFwkPersRelationship(null);
		}

		if (!isEtablissementFrancais() && editingDirecteurThese().toRneFwkpers() != null) {
			editingDirecteurThese().setToRneFwkpersRelationship(null);
		}

		if (isLaboratoireLocal() && editingDirecteurThese().libelleLabo() != null) {
			editingDirecteurThese().setLibelleLabo(null);
		}

		if (!isLaboratoireLocal() && editingDirecteurThese().toStructureLaboFwkpers() != null) {
			editingDirecteurThese().setToStructureLaboFwkpersRelationship(null);
		}
		
		if (hasRoleDirecteurThese() && editingQuotiteEncadrement() != null) {
			if (editingQuotiteEncadrement().compareTo(new BigDecimal(POURCENTAGE_ENCADREMENT_TOTAL)) > 0 || editingQuotiteEncadrement().compareTo(new BigDecimal(0.01)) < 0) {
				session().addSimpleErrorMessage("Erreur", "La quotité doit être comprise entre 0 et " + POURCENTAGE_ENCADREMENT_TOTAL);
				nbErreurs++;
			}
			
			DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
			BigDecimal totalQuotite = dts.calculSommeQuotiteThese(laThese, editingDirecteurThese, editingDateDebutEncadrement, editingDateFinEncadrement);
			totalQuotite = totalQuotite.add(dts.calculQuotite(laThese, editingQuotiteEncadrement, editingDateDebutEncadrement, editingDateFinEncadrement));
			
			if (totalQuotite.compareTo(new BigDecimal(POURCENTAGE_ENCADREMENT_TOTAL)) > 0) {
				session().addSimpleErrorMessage("Erreur", "La somme des quotités dépasse " + POURCENTAGE_ENCADREMENT_TOTAL + "% à cette période (" + totalQuotite.floatValue() + "%)");
				nbErreurs++;
			}
		} else {
			setEditingQuotiteEncadrement(new BigDecimal(0));
		}
		
		if (hasRoleEncadrantThese()) {
			setEditingQuotiteEncadrement(null);
		} else if (hasRoleDirecteurThese()) {
			if (!isQuotiteSuffisante(editingQuotiteEncadrement(), laThese().ecoleDoctorale())) {
				session().addSimpleInfoMessage("Attention", "La quotité pour le directeur de thèse est insuffisante (" 
						+ quotiteMinPourEcoleDoctorale(laThese().ecoleDoctorale()) + "% minimum).");
			}
			EOIndividu directeur;
			if (isModif()) {
				directeur = editingDirecteurThese().toIndividuDirecteur();
			} else {
				directeur = selectedEncadrant();
			}
			isTropDeTheseEncadrees(directeur, laThese().ecoleDoctorale(), editingDateDebutEncadrement(), editingDateFinEncadrement());
		}
		
		if (!isModif() && FactoryAssociation.shared().directeurTheseAssociation(edc()).equals(selectedRoleDirecteur)) {
			if (laThese().nbDirecteurs(editingDateDebutEncadrement(), editingDateFinEncadrement()) >= NB_MAX_DIRECTEURS_THESE) {
				session().addSimpleInfoMessage("Attention", "Le nombre maximum de directeurs (" + NB_MAX_DIRECTEURS_THESE + ") pour une thèse est déjà atteint (Article 17 de l’arrêté du 7 août 2006)");
			}
		}

		if (nbErreurs > 0) {
			return doNothing();
		}
		
		if (isModif()) {
			return validerModif();
		} else {
			return validerAjout();
		}
	}

	private boolean hasRoleEncadrantThese() {
	    return factoryAssociation().coEncadrantTheseAssociation(edc()).equals(selectedRoleDirecteur);
    }

	private boolean hasRoleDirecteurThese() {
	    return factoryAssociation().directeurTheseAssociation(edc()).equals(selectedRoleDirecteur);
    }

	/**
	 * Retourne l'individu avec le role de directeur de these de la these donnee qui a la plus grosse quotite d'encadrement
	 * @param laThese La these selectionnee
	 * @return le directeur de these principal pour la these
	 */
	public EODirecteurThese getDirecteurThesePrincipalThese(EODoctorantThese laThese) {
		EODirecteurThese directeur = null;
		BigDecimal quotiteMax = new BigDecimal(0);
		EOAssociation associationDirecteurThese = factoryAssociation().directeurTheseAssociation(edc());
		
		if (laThese.toDirecteursThese() == null) {
			return null;
		}
		
		for (EODirecteurThese dir : laThese.toDirecteursThese()) {
			if (dir.repartAssociationDirecteur() != null && dir.repartAssociationDirecteur().rasQuotite() != null && dir.repartAssociationDirecteur().toAssociation() == associationDirecteurThese) {
				if (quotiteMax.compareTo(dir.repartAssociationDirecteur().rasQuotite()) < 0) {
					directeur = dir;
				}
			}
		}
		
		return directeur;
	}
	
	/**
	 * Retourne la quotité maximale des directeurs de la these donnee
	 * @param laThese La these selectionnee
	 * @return La quotite maximale d'encadrement par un directeur de la these
	 */
	public BigDecimal getQuotiteMaxDirecteurThese(EODoctorantThese laThese) {
		
		EODirecteurThese directeur = getDirecteurThesePrincipalThese(laThese);
		
		if (directeur == null) {
			return new BigDecimal(0);
		}
		
		if (directeur.repartAssociationDirecteur().rasQuotite() == null) {
			return new BigDecimal(0);
		}
		
		return directeur.repartAssociationDirecteur().rasQuotite();
	}

	/**
	 * Calcule la somme des quotités des encadrants d'une these
	 * @return la somme des quotités
	 */
	private BigDecimal calculSommeQuotite() {
		BigDecimal totalQuotite = new BigDecimal(0);
	    NSArray<EODirecteurThese> listeDirecteurs = listeDirecteurs();
	    for (EODirecteurThese currentDirecteur : listeDirecteurs) {
	    	if (currentDirecteur != editingDirecteurThese() && currentDirecteur.repartAssociationDirecteur() != null && currentDirecteur.repartAssociationDirecteur().rasQuotite() != null) {
	    		totalQuotite = totalQuotite.add(currentDirecteur.repartAssociationDirecteur().rasQuotite());
	    	}
	    }
	    return totalQuotite;
    }
	
	/**
	 * Vérifie que le nombre de theses encadrées par le directeur ne dépasse pas le nombre maxi autorisé pour son ED.
	 * Si le nombre est dépassé, un message d'information est envoyé
	 * @param leDirecteur l'individu avec comme role directeur
	 * @param ecoleDoctorale l'école doctorale de rattachement
	 * @param dateDebut date de début de l'encadrement
	 * @param dateFin date de fin de l'encadrement
	 * @return true si le nb de theses est dépassé
	 */
	private Boolean isTropDeTheseEncadrees(EOIndividu leDirecteur, EOStructure ecoleDoctorale, NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (leDirecteur == null || ecoleDoctorale == null) {
			return false;
		}
		
		EOQualifier qual = ERXQ.and(
				ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().directeurTheseAssociation(edc())),
				ERXQ.equals(EORepartAssociation.PERS_ID_KEY, leDirecteur.persId()),
				ERXQ.or(
						ERXQ.and(
								ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, dateDebut),
								ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, dateDebut)),
						ERXQ.and(ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, dateFin),
								ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, dateFin))));
		NSArray<EORepartAssociation> toutesLesThesesEncadrees = EORepartAssociation.fetchAll(edc(), qual);
		NSMutableArray<EORepartAssociation> toutesLesTheseDirigees = new NSMutableArray<EORepartAssociation>();
		toutesLesTheseDirigees.addAll(toutesLesThesesEncadrees);
		
		for (EORepartAssociation repartTheseEncadreeParCeDirecteur : toutesLesThesesEncadrees) {
			EOQualifier qualThese = ERXQ.and(
					ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().directeurTheseAssociation(edc())),
					ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY + "." + EOStructure.C_STRUCTURE_KEY, repartTheseEncadreeParCeDirecteur.toStructure().cStructure())
					);
			NSArray<EORepartAssociation> tousLesEncadrantsTheseCourante = EORepartAssociation.fetchAll(edc(), qualThese);
			for (EORepartAssociation repartTheseEncadrant : tousLesEncadrantsTheseCourante) {
	            // si on a un directeur avec un encadrement superieur alors c'est lui le directeur de la these
	            if (repartTheseEncadrant.rasQuotite() != null && repartTheseEncadreeParCeDirecteur.rasQuotite() != null && repartTheseEncadrant.rasQuotite().compareTo(repartTheseEncadreeParCeDirecteur.rasQuotite()) > 0) { 
	            	toutesLesTheseDirigees.remove(repartTheseEncadreeParCeDirecteur);
	            }
            }
		}
		
		Integer nbTheseEncadreeMaximum = Integer.parseInt(EOGrhumParametres.parametrePourCle(edc(), ParametresRecherche.PARAM_ECOLE_DOCTORALE_NB_THESE
				+ ecoleDoctorale.cStructure()));
		if (!toutesLesTheseDirigees.isEmpty() && toutesLesTheseDirigees != null) {
			if (toutesLesTheseDirigees.count() >= nbTheseEncadreeMaximum) {
				session().addSimpleInfoMessage("Attention", leDirecteur.getNomCompletAffichage() + " encadre déjà " + toutesLesTheseDirigees.count()
								+ " thèses alors que le maximum autorisé pour l'école doctorale " + ecoleDoctorale.lcStructure() + " est de "
								+ nbTheseEncadreeMaximum + " thèses");
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Retourne true si la quotité d'encadrement pour un directeur de these est suffisante (quotité variable selon l'ecole doctorale de rattachement)
	 * @param directeur
	 * @param quotiteDirecteur
	 * @return
	 */
	private Boolean isQuotiteSuffisante(EODirecteurThese directeur, BigDecimal quotiteDirecteur) {
		EOStructure structureED = getEcoleDoctoraleDirecteurThese(directeur);
		
		if (structureED != null) {
			return isQuotiteSuffisante(quotiteDirecteur, structureED);
		}
			
		return false;		
	}

	/**
	 * Retourne l'ecole doctorale dont le directeur est membre
	 * @param laboratoire
	 * @return
	 */
	private EOStructure getEcoleDoctoraleDirecteurThese(EODirecteurThese directeur) {
		
		EOStructure laboratoire = directeur.toStructureLaboFwkpers();
		if (laboratoire == null) {
			return null;
		}
			
	    EOQualifier qualDoct = ERXQ.and(ERXQ.contains(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "ED"),
	    		ERXQ.equals(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TEM_VALIDE_KEY, EOStructure.TEM_VALIDE_OUI));
	    @SuppressWarnings("unchecked")
	    NSArray<EORepartStructure> groupes = laboratoire.getRepartStructuresAffectes(new PersonneApplicationUser(edc(), EOFournis.TYAP_STRID_ANNUAIRE, getUtilisateurPersId()), qualDoct);
	    EOStructure structureED = null;
	    
	    for (EORepartStructure groupe : groupes) {
	    	for (EORepartTypeGroupe rtg : groupe.toStructureGroupe().toRepartTypeGroupes()) {
	    		if (rtg.tgrpCode().equals("ED")) {
	    			structureED = groupe.toStructureGroupe();
	    			break;
	    		}
	    	}
	    }
	    
	    return structureED;
    }
	
	/**
	 * Retourne true si la quotité d'encadrement pour un directeur de these habilité est suffisante (quotité variable selon l'ecole doctorale de rattachement)
	 * @param leDirecteur
	 * @param quotiteDirecteur
	 * @return
	 */
	private Boolean isQuotiteSuffisante(EODirecteurTheseHabilite leDirecteur, BigDecimal quotiteDirecteur) {
		return isQuotiteSuffisante(quotiteDirecteur, leDirecteur.toStructureEdFwkpers());
	}
	
	/**
	 * Retourne true si la quotité d'encadrement pour un directeur de these est suffisante pour une ecole doctorale 
	 * @param quotiteDirecteur
	 * @param structureED
	 * @return
	 */
	private Boolean isQuotiteSuffisante(BigDecimal quotiteDirecteur, EOStructure structureED) {
	    if (quotiteDirecteur.compareTo(new BigDecimal(quotiteMinPourEcoleDoctorale(structureED))) >= 0) {
	    	return true;
	    }
	    return false;
    }

	/**
	 * Retourne la quotité minimale paramétrée par Ecole Doctorale
	 * @param ecoleDoctorale
	 * @return
	 */
	private Double quotiteMinPourEcoleDoctorale(EOStructure ecoleDoctorale) {
		if (ecoleDoctorale == null) {
			return 0D;
		}
	    return Double.parseDouble(EOGrhumParametres.parametrePourCle(edc(), ParametresRecherche.PARAM_ECOLE_DOCTORALE_QUOTITE + ecoleDoctorale.cStructure()));
    }

	public WOActionResults annulerDirecteurThese() {
		try {
			edc().revert();
			isModif = false;
			setIsEditable(false);
			setIsSelectionEnable(true);
			setShowDateAutorisation(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public EODirecteurThese editingDirecteurThese() {
		return editingDirecteurThese;
	}

	public void setEditingDirecteurThese(EODirecteurThese editingDirecteurThese) {
		this.editingDirecteurThese = editingDirecteurThese;
	}

	public void setIsModif(Boolean isModif) {
		this.isModif = isModif;
	}

	public Boolean isModif() {
		return isModif;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
		disableOnglets(isEditable, false);
	}

	public Boolean isEditable() {
		return isEditable;
	}

	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
		this.isEtablissementFrancais = isEtablissementFrancais;
	}

	public Boolean isEtablissementFrancais() {
		return isEtablissementFrancais;
	}

	public void setIsLaboratoireLocal(Boolean isLaboratoireLocal) {
		this.isLaboratoireLocal = isLaboratoireLocal;
	}

	public Boolean isLaboratoireLocal() {
		return isLaboratoireLocal;
	}

	public EODirecteurThese selectedDirecteurThese() {
		return selectedDirecteurThese;
	}

	public void setSelectedDirecteurThese(EODirecteurThese selectedDirecteurThese) {
		this.selectedDirecteurThese = selectedDirecteurThese;
	}

	public EODirecteurThese getCurrentDirecteurThese() {
	    return currentDirecteurThese;
    }

	public void setCurrentDirecteurThese(EODirecteurThese currentDirecteurThese) {
	    this.currentDirecteurThese = currentDirecteurThese;
    }

	public EODoctorantThese laThese() {
		if (laThese != (EODoctorantThese) valueForBinding(B_laThese)) {
			laThese = (EODoctorantThese) valueForBinding(B_laThese);
			setRechargerDirecteur(true);
		}
		return (EODoctorantThese) valueForBinding(B_laThese);
	}

	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, B_laThese);
	}

	public Boolean rechargerDirecteur() {
		return (Boolean) valueForBinding(B_rechargerDirecteur);
	}

	public void setRechargerDirecteur(Boolean rechargerDirecteur) {
		setValueForBinding(rechargerDirecteur, B_rechargerDirecteur);
	}

	public EOIndividu selectedEncadrant() {
		return selectedEncadrant;
	}

	public void setSelectedEncadrant(EOIndividu selectedEncadrant) {
		this.selectedEncadrant = selectedEncadrant;
	}

	public void setEditingRepartStructureDirecteur(EORepartStructure editingRepartStructureDirecteur) {
		this.editingRepartStructureDirecteur = editingRepartStructureDirecteur;
	}

	public EORepartStructure editingRepartStructureDirecteur() {
		return editingRepartStructureDirecteur;
	}

	/**
	 * Vérifie que le nombre de directeurs ne dépasse pas la limite
	 * @return true si le nombre de directeurs est depassé
	 */
	public Boolean isTropDirecteur() {
		Boolean retour = null;
		NSTimestamp dateDebut = null;
		NSTimestamp dateFin = null;

		if (editingRepartAssocDirecteur != null) {
			dateDebut = editingRepartAssocDirecteur.rasDOuverture();
			dateFin = editingRepartAssocDirecteur.rasDFermeture();
		}
		NSArray<EORepartAssociation> lesRepartAssocDirecteur = null;
		lesRepartAssocDirecteur = laThese().toContrat().repartAssociationPartenairesForAssociationAuxDates(factoryAssociation().directionTheseAssociation(edc()), dateDebut,
				dateFin);
		if (lesRepartAssocDirecteur != null) {
			if (lesRepartAssocDirecteur.count() >= NB_MAX_DIRECTEURS_THESE) {
				retour = true;
			} else {
				retour = false;
			}
		} else {
			retour = false;
		}
		return retour;
	}

	public String onClickBefore() {
		String jsRetour = null;
		if (isTropDirecteur()) {
			jsRetour = "confirm('Il existe déjà au moins " + NB_MAX_DIRECTEURS_THESE + " directeurs pour cette thèse à cette période. Voulez-vous continuer ?')";
		} else {
			jsRetour = "";
		}
		return null;
	}

	public void setEditingDateDebutEncadrement(NSTimestamp editingDateDebutEncadrement) {
		this.editingDateDebutEncadrement = editingDateDebutEncadrement;
	}

	public NSTimestamp editingDateDebutEncadrement() {
		return editingDateDebutEncadrement;
	}

	public void setEditingDateFinEncadrement(NSTimestamp editingDateFinEncadrement) {
		this.editingDateFinEncadrement = editingDateFinEncadrement;
	}

	public NSTimestamp editingDateFinEncadrement() {
		return editingDateFinEncadrement;
	}

	public void setEditingCommentaireEncadrement(String editingCommentaireEncadrement) {
		this.editingCommentaireEncadrement = editingCommentaireEncadrement;
	}

	public String editingCommentaireEncadrement() {
		return editingCommentaireEncadrement;
	}

	public void setIsCotutelle(Boolean isCotutelle) {
		this.isCotutelle = isCotutelle;
	}

	public Boolean isCotutelle() {
		return isCotutelle;
	}

	public void setEditingAdresse(EOAdresse editingAdresse) {
		this.editingAdresse = editingAdresse;
	}

	public EOAdresse editingAdresse() {
		return editingAdresse;
	}

	public void setEditingPays(EOPays editingPays) {
		this.editingPays = editingPays;
	}

	public EOPays editingPays() {
		return editingPays;
	}

	public void setWasCotutelle(Boolean wasCotutelle) {
		this.wasCotutelle = wasCotutelle;
	}

	public Boolean wasCotutelle() {
		return wasCotutelle;
	}

	public void setAncienneCotutelle(ContratPartenaire ancienneCotutelle) {
		this.ancienneCotutelle = ancienneCotutelle;
	}

	public ContratPartenaire ancienneCotutelle() {
		return ancienneCotutelle;
	}

	public BigDecimal editingQuotiteEncadrement() {
		return editingQuotiteEncadrement;
    }

	public void setEditingQuotiteEncadrement(BigDecimal editingQuotiteEncadrement) {
		this.editingQuotiteEncadrement = editingQuotiteEncadrement;
    }
	
	public Boolean showDateAutorisation() {
		return showDateAutorisation;
    }

	public void setShowDateAutorisation(Boolean showDateAutorisation) {
		this.showDateAutorisation = showDateAutorisation;
	    setValueForBinding(showDateAutorisation, B_showDateAutorisation);
    }
	
}