package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class TheseAffectationForm extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2890934784880134323L;
	private static final String B_repartAssociation = "repartAssociation";
	private static final String B_editingLaboratoire = "editingLaboratoire";
	private static final String B_editingDateDebutAffectation = "editingDateDebutAffectation";
	private static final String B_editingDateFinAffectation = "editingDateFinAffectation";
	private static final String B_editingCommentaireAffectation = "editingCommentaireAffectation";
	private static final String B_ecoleDoctorale = "ecoleDoctorale";
	private static final String BINDING_INDIVIDU = "individu";
	private static final String BINDING_LA_THESE = "laThese";

	// isModif : pas de modif du labo (affichage), uniquement dates et
	// commentaires
	private static final String B_isModif = "isModif";

	private EOStructure itemStructureLabo;
	private EOStructure selectedStructureLabo;

	public NSTimestamp dateDebut, dateFin;
	public String observations;

	public TheseAffectationForm(WOContext context) {
		super(context);
	}

	public String getTheseAffectationContainerId() {
		return getComponentId() + "_theseAffectationContainerId";
	}
	
	public EOStructure ecoleDoctorale() {
		return (EOStructure) valueForBinding(B_ecoleDoctorale);
	}

	public EOIndividu getIndividu() {
		return (EOIndividu) valueForBinding(BINDING_INDIVIDU);
	}

	public EOStructure editingLaboratoire() {
		return (EOStructure) valueForBinding(B_editingLaboratoire);
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(BINDING_LA_THESE);
	}

	public void setEditingLaboratoire(EOStructure editingLaboratoire) {
		//recherche si la relation entre le doctorant et le labo existe ds sangria
		NSArray<EORepartAssociation> ra_exist = repartAssociationsIndividuRoleDoctorantLaboratoire(getIndividu(), editingLaboratoire);
		
		if (ra_exist != null && ra_exist.count() == 1) {
			setEditingDateDebutAffectation(ra_exist.get(0).rasDOuverture());
			setEditingDateFinAffectation(ra_exist.get(0).rasDFermeture());
			session().addSimpleInfoMessage("Remarque", "Les dates d'encadrement ont été modifiées car il existe déjà une relation entre ce laboratoire et le doctorant.");
		} else {
			setEditingDateDebutAffectation(laThese().dateDeb());
			setEditingDateFinAffectation(laThese().dateFin());
		}
		
		setValueForBinding(editingLaboratoire, B_editingLaboratoire);
	}

	public NSTimestamp editingDateDebutAffectation() {
		return (NSTimestamp) valueForBinding(B_editingDateDebutAffectation);
	}

	public void setEditingDateDebutAffectation(NSTimestamp editingDateDebutAffectation) {
		setValueForBinding(editingDateDebutAffectation, B_editingDateDebutAffectation);
	}

	public NSTimestamp editingDateFinAffectation() {
		return (NSTimestamp) valueForBinding(B_editingDateFinAffectation);
	}

	public void setEditingDateFinAffectation(NSTimestamp editingDateFinAffectation) {
		setValueForBinding(editingDateFinAffectation, B_editingDateFinAffectation);
	}

	public String editingCommentaireAffectation() {
		return (String) valueForBinding(B_editingCommentaireAffectation);
	}

	public void setEditingCommentaireAffectation(String editingCommentaireAffectation) {
		setValueForBinding(editingCommentaireAffectation, B_editingCommentaireAffectation);
	}

	public void setItemStructureLabo(EOStructure itemStructureLabo) {
		this.itemStructureLabo = itemStructureLabo;
	}

	public EOStructure itemStructureLabo() {
		return itemStructureLabo;
	}

	public EORepartAssociation editingRepartAssociation() {
		return (EORepartAssociation) valueForBinding(B_repartAssociation);
	}

	public Boolean isModif() {
		return (Boolean) valueForBinding(B_isModif);
	}

	// @SuppressWarnings("unchecked")
	public NSArray<EOStructure> listeStructuresLabo() {
		/* LAbo et Structure Recherche
		 * EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, ecoleDoctorale().cStructure()), ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY,
				factoryAssociation().membreAssociation(edc())), ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, editingDateDebutAffectation()), ERXQ.or(
				ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, editingDateFinAffectation()), ERXQ.isNull(EORepartAssociation.RAS_D_FERMETURE_KEY)), ERXQ.in(
				EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.' + EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, new NSArray<String>(
						EOTypeGroupe.TGRP_CODE_LA, EOTypeGroupe.TGRP_CODE_SR)));
						*/
		/* Suppression de la restriction sur la date du labo
		EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, ecoleDoctorale().cStructure()), ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY,
				factoryAssociation().membreAssociation(edc())), ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, editingDateDebutAffectation()), ERXQ.or(
				ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, editingDateFinAffectation()), ERXQ.isNull(EORepartAssociation.RAS_D_FERMETURE_KEY)), ERXQ.equals(
				EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.' + EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, 
						EOTypeGroupe.TGRP_CODE_LA));
		 */
		EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, ecoleDoctorale().cStructure()), ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY,
				factoryAssociation().membreAssociation(edc())), ERXQ.equals(
				EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.' + EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, 
						EOTypeGroupe.TGRP_CODE_LA));
		NSArray<EORepartAssociation> lesRepartAssoLabo = ERXArrayUtilities.arrayWithoutDuplicates(EORepartAssociation.fetchAll(edc(), qualLabos));
		if (lesRepartAssoLabo == null)
			return null;
		else
			return (NSArray) lesRepartAssoLabo.valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	}

	public EOStructure getSelectedStructureLabo() {
		if (editingRepartAssociation() != null) {
			EOStructure laStructure = EOStructure.fetchFirstByQualifier(edc(), ERXQ.equals(EOStructure.PERS_ID_KEY, editingRepartAssociation().persId()));
			return laStructure;
		} else
			return null;
	}

	public void setSelectedStructureLabo(EOStructure selectedStructureLabo) {
		this.selectedStructureLabo = selectedStructureLabo;
		if (editingRepartAssociation() != null) {
			editingRepartAssociation().setToPersonne(selectedStructureLabo);
		}

	}

}