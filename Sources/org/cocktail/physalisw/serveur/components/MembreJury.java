package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderContratPartenaire;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.fwkcktlrecherche.server.metier.service.DoctorantTheseService;
import org.cocktail.physalisw.serveur.Application;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Composant gerant les membres de jurys (rapporteurs et membres de jury)
 */
public class MembreJury extends PhysalisWOComponent {

	private static final long serialVersionUID = 5701282612479693763L;
	private WODisplayGroup dgMembre;
	private EORepartAssociation selectedRepartAssociationMembre;
	private static final String BINDING_LA_THESE = "laThese";
	private static final String BINDING_TYPE_MEMBRE = "typeMembre";
//	private static final String BINDING_COLONNES_TV = "colonnesTV";
//	private static final String BINDING_HAUTEUR_TV = "hauteurTV";
	private static final String BINDING_IS_EDITION_MEMBRE = "isEditionMembre";
	private static final String BINDING_RECHARGER = "recharger";
//	private static final String BINDING_CLASS_COULEUR = "classCouleur";
//	private static final String BINDING_IS_MODIFICATION_BOTTOM = "isModificationBottom";
	private static final String TYPE_MEMBRE_JURY = "JURY";
	private static final String TYPE_MEMBRE_RAPPORTEUR = "RAPPORTEUR";
	private static final int NOMBRE_MEMBRES_JURY_MIN = 3;
	private static final int NOMBRE_MEMBRES_JURY_MAX = 8;

	private String colonnesTV = null;
	private String hauteurTV = null;
	private String titre = null;
	private String typeMembre;
	private Boolean isEditionMembre = false;
	private Boolean isValiderMembreDisable = false;
	private Boolean isModificationMembre = false;
	private Boolean isDetailMembre = false;
	private EODoctorantThese laThese = null;
	private Boolean isEtablissementFrancais = false;

	private EOMembreJuryThese editingMembre;
	private EOMembreJuryThese selectedMembre;
	private EORepartAssociation editingRepartAssociationMembre;
	private EOIndividu editingIndividuMembre;
	private EOAssociation selectedRoleMembre = null;
	private ContratPartenaire selectedContratPartenaire = null;
	private NSMutableArray<String> erreursValiditeJury = new NSMutableArray<String>();
	private NSMutableArray<String> erreursValiditeRapporteur = new NSMutableArray<String>();

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			selectedMembre = (EOMembreJuryThese) dgMembre.selectedObject();
			if (selectedMembre != null) {
				selectedContratPartenaire = selectedMembre.toContratPartenaire();
				if (isTypeRapporteur()) {
					selectedRepartAssociationMembre = selectedMembre.repartAssociationRapporteur();
				} else {
					selectedRepartAssociationMembre = selectedMembre.repartAssociationJury();
				}
				selectedRoleMembre = selectedRepartAssociationMembre.toAssociation();
			}
		}
	}

	public WODisplayGroup dgMembre() {
		if (dgMembre == null) {
			dgMembre = new WODisplayGroup();
			dgMembre.setDelegate(new DgDelegate());

			dgMembre.setSortOrderings(new NSArray<EOSortOrdering>(EOMembreJuryThese.SORT_NOM_MEMBRE_ASC));
			dgMembre.setObjectArray(listeMembresJury());
			dgMembre.setSelectsFirstObjectAfterFetch(true);
		}

		if (recharger()) {
			setRecharger(false);
			dgMembre.setObjectArray(listeMembresJury());
			dgMembre.setSelectsFirstObjectAfterFetch(true);
		}

		return dgMembre;
	}

	public NSArray<EOAssociation> associationsMembres() {
		NSArray<EOAssociation> associations = null;
		if (typeMembre().equals(TYPE_MEMBRE_JURY)) {
			associations = factoryAssociation().associationsFilleDe(factoryAssociation().rolesJuryAssociation(edc()), edc());
		} else { // TYPE_MEMBRE_RAPPORTEUR
			associations = new NSArray<EOAssociation>(factoryAssociation().rapporteurTheseAssociation(edc()));
		}
		return associations;
	}

	public NSArray<EOMembreJuryThese> listeMembresJury() {
		EOQualifier qualMjt = ERXQ.and(ERXQ.equals(EOMembreJuryThese.TO_DOCTORANT_THESE_KEY, laThese()),
				ERXQ.in(EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY, laThese().toContrat().partenairesForAssociations(associationsMembres())));
		return EOMembreJuryThese.fetchMembreJuryTheses(edc(), qualMjt, null);
	}

	public WOActionResults ajoutMembre() {
		setIsEditionMembre(true);
		setIsSelectionEnable(false);
		setIsValiderMembreDisable(true);
		setIsModificationMembre(false);
		setIsEtablissementFrancais(true);

		setEditingMembre(EOMembreJuryThese.createMembreJuryThese(edc(), new String("N"), new String("O"), null, laThese()));
		setEditingRepartAssociationMembre(null);
		setEditingIndividuMembre(null);
		setSelectedRoleMembre(null);
		selectedContratPartenaire = null;
		return null;
	}

	public WOActionResults modifMembre() {
		setIsEditionMembre(true);
		setIsModificationMembre(true);
		setIsSelectionEnable(false);
		setIsValiderMembreDisable(false);

		editingMembre = selectedMembre();
		
		if (editingMembre.libelleStructExterne() == null) {
			setIsEtablissementFrancais(true);
		} else {
			setIsEtablissementFrancais(false);
		}
		
		editingRepartAssociationMembre = selectedRepartAssociationMembre();
		editingIndividuMembre = (EOIndividu) editingRepartAssociationMembre.toPersonneElt();
		editingIndividuMembre.setPersIdModification(session().applicationUser().getPersId());
		selectedRoleMembre = editingRepartAssociationMembre.toAssociation();
		return null;
	}

	public WOActionResults supprimeMembre() {
		try {
			DoctorantTheseService dts = DoctorantTheseService.creerNouvelleInstance(edc(), getUtilisateurPersId());
			dts.supprimerMembreJury(laThese(), selectedMembre, TYPE_MEMBRE_RAPPORTEUR.equals(typeMembre()));
			edc().saveChanges();
			selectedMembre = null;
			session().addSimpleSuccessMessage("Suppresion", "Le membre a bien été supprimé");
			dgMembre().setObjectArray(listeMembresJury());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults afficherDetail() {
		if (isDetailMembre) {
			setIsDetailMembre(false);
		} else {
			setIsDetailMembre(true);
		}
		return null;
	}

	/**
	 * Un rapporteur a qui on ajoute le role de membre du jury
	 * @return
	 */
	public WOActionResults ajoutRapporteurJury() {
		try {
			FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
			if (!fc.partenaireARolesDansContrat(selectedContratPartenaire.partenaire(),
					factoryAssociation().associationsFilleDe(factoryAssociation().rolesJuryAssociation(edc()), edc()), selectedContratPartenaire.contrat())) {
				fc.ajouterLeRole(selectedContratPartenaire, factoryAssociation().membreJuryAssociation(edc()));
				edc().saveChanges();
				setRecharger(true);
				session().addSimpleSuccessMessage("Ajout", "La personne a été ajoutée au jury comme membre");
			} else {
				session().addSimpleErrorMessage("Impossible", "La personne est déjà membre du jury !");
			}

		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults validerMembre() {
		EORepartAssociation leRepartAssociation = null;
		ContratPartenaire cp = null;
		Integer nbErreur = 0;
		
		if (laThese().toDoctorant().toIndividuFwkpers().noIndividu().equals(editingIndividuMembre.noIndividu())) {
			session().addSimpleErrorMessage("Erreur", "Le doctorant ne peut être un membre du jury.");
			nbErreur++;
		}
		
		if (editingMembre.toCorps() == null && editingMembre.titreSpecial() == null) {
			session.addSimpleErrorMessage("Erreur", "Vous devez renseigner le corps ou le titre de la personne");
			nbErreur++;
		}

		if (nbErreur > 0) {
			return null;
		}

		if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
			selectedRoleMembre = factoryAssociation().rapporteurTheseAssociation(edc());
		}
		try {
			if (!isModificationMembre()) { // si ajout
				// tester si le cp existe déjà
				// si oui on n'ajoute que l'assoc (repart_assoc et
				// repart_structure)
				// sinon creation du cp avec role
				FinderContratPartenaire cp_exist = new FinderContratPartenaire(edc());
				cp_exist.setContrat(laThese().toContrat());
				cp_exist.setPartenaire(editingIndividuMembre());
				cp = (ContratPartenaire) cp_exist.find().lastObject();

				// si le contrat partenaire n'existe pas, on le crée
				// sinon on ajoute le role au partenaire
				FactoryContratPartenaire fc = new FactoryContratPartenaire(edc(), Application.isModeDebug);
				if (cp == null) {
					cp = fc.creerContratPartenaire(laThese().toContrat(), editingIndividuMembre(), new NSArray<EOAssociation>(selectedRoleMembre), Boolean.FALSE, null);
				} else {
					fc.ajouterLeRole(cp, selectedRoleMembre);
					// on cherche si il est deja membre du jury ou rapporteur si
					// oui on ne crée pas le membrejurythese sinon on le crée
					EOMembreJuryThese mjt = EOMembreJuryThese.fetchMembreJuryThese(edc(), EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY, cp);
					if (mjt != null) {
						edc().deleteObject(editingMembre);
						editingMembre = mjt;
					}
				}
				editingMembre().setToContratPartenaireRelationship(cp);
			} else {
				editingRepartAssociationMembre().setToAssociationRelationship(selectedRoleMembre);
			}
			edc().saveChanges();
			if (!isModificationMembre()) {
				session().addSimpleSuccessMessage("Ajout", "Personne enregistrée");
			} else {
				session().addSimpleSuccessMessage("Modification", "Modifications enregistrées");
			}
			setIsEditionMembre(false);
			setIsSelectionEnable(true);
			dgMembre().setObjectArray(listeMembresJury());
			if (isModificationMembre()) {
				setRecharger(true);
			}
			setIsModificationMembre(false);
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
			e.printStackTrace();
		}
		return null;
	}

	public WOActionResults annulerMembre() {
		setIsEditionMembre(false);
		setIsSelectionEnable(true);
		setIsValiderMembreDisable(true);
		edc().revert();
		return null;
	}

	public MembreJury(WOContext context) {
		super(context);
	}

	public String aucMembreFormId() {
		return getComponentId() + "_aucMembreFormId";
	}

	public String membreJuryTableViewId() {
		return getComponentId() + "_membreJuryTableViewId";
	}
	
	public String formMembreId() {
		return getComponentId() + "_formMembreId";
	}

	public EODoctorantThese laThese() {
		if (laThese != (EODoctorantThese) valueForBinding(BINDING_LA_THESE)) {
			laThese = (EODoctorantThese) valueForBinding(BINDING_LA_THESE);
			dgMembre = null;
		}
		return (EODoctorantThese) valueForBinding(BINDING_LA_THESE);
	}
	
	public void setLaThese(EODoctorantThese laThese) {
		setValueForBinding(laThese, BINDING_LA_THESE);
	}

	public String typeMembre() {
		typeMembre = (String) valueForBinding(BINDING_TYPE_MEMBRE);
		return typeMembre;
	}
	
	public ContratPartenaire contratPartenaire() {
		if (selectedContratPartenaire == null) {
			try {
				FinderContratPartenaire leCp = new FinderContratPartenaire(edc());
				leCp.setContrat(laThese().toContrat());
				leCp.setPartenaire(selectedRepartAssociationMembre.toPersonneElt());

				selectedContratPartenaire = (ContratPartenaire) leCp.find().lastObject();
			} catch (ExceptionFinder e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return selectedContratPartenaire;
	}

	public void setSelectedRepartAssociationMembre(EORepartAssociation selectedRepartAssociationMembre) {
		// ContratPartenaire cp = null;
		this.selectedRepartAssociationMembre = selectedRepartAssociationMembre;
		/*
		 * if (selectedRepartAssociationMembre != null) { // oncherche le membre
		 * du jury correspondant au contrat partenaire selectedMembre =
		 * EOMembreJuryThese.fetchMembreJuryThese(edc(),
		 * EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY, selec);
		 * selectedRoleMembre = selectedRepartAssociationMembre.toAssociation();
		 * }
		 */
	}

	public EORepartAssociation selectedRepartAssociationMembre() {
		/*
		 * if (selectedRepartAssociationMembre != null) { // oncherche le membre
		 * du jury correspondant au ras_id
		 * setSelectedMembre(EOMembreJuryThese.fetchMembreJuryThese(edc(),
		 * EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY, //)); }
		 */
		return selectedRepartAssociationMembre;
	}

	public String classBox() {
		if (TYPE_MEMBRE_RAPPORTEUR.equals(typeMembre())) {
			return "boxtitreOrange";
		} else {
			return "boxtitre";
		}
	}

	public String colonnesTV() {
		if (colonnesTV == null) {
			if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
				colonnesTV = "NOM_MEMBRE,ETABLISSEMENT";
			} else {
				colonnesTV = "NOM_MEMBRE,ETABLISSEMENT,ROLE,RAPPORTEUR_SEANCE";
			}
		}
		return colonnesTV;
	}

	public String hauteurTV() {
		if (hauteurTV == null) {
			if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
				hauteurTV = "64px";
			} else {
				hauteurTV = "100px";
			}
		}
		return hauteurTV;
	}

	public void setIsEditionMembre(Boolean isEditionMembre) {
		setValueForBinding(isEditionMembre, BINDING_IS_EDITION_MEMBRE);
		disableOnglets(isEditionMembre, false);
		this.isEditionMembre = isEditionMembre;
	}

	public Boolean isEditionMembre() {
		isEditionMembre = (Boolean) valueForBinding(BINDING_IS_EDITION_MEMBRE);
		return isEditionMembre;
	}

	public Boolean recharger() {
		return (Boolean) valueForBinding(BINDING_RECHARGER);
	}

	public void setRecharger(Boolean recharger) {
		setValueForBinding(recharger, BINDING_RECHARGER);
	}

	public Boolean isRoleEnable() {
		if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
			return false;
		} else {
			return true;
		}
	}

	public void setIsValiderMembreDisable(Boolean isValiderMembreDisable) {
		this.isValiderMembreDisable = isValiderMembreDisable;
	}

	public Boolean isValiderMembreDisable() {
		return isValiderMembreDisable;
	}

	public void setIsModificationMembre(Boolean isModificationMembre) {
		this.isModificationMembre = isModificationMembre;
	}

	public Boolean isModificationMembre() {
		return isModificationMembre;
	}

	public void setEditingMembre(EOMembreJuryThese editingMembre) {
		this.editingMembre = editingMembre;
	}

	public EOMembreJuryThese editingMembre() {
		return editingMembre;
	}

	public void setEditingRepartAssociationMembre(EORepartAssociation editingRepartAssociationMembre) {
		this.editingRepartAssociationMembre = editingRepartAssociationMembre;
	}

	public EORepartAssociation editingRepartAssociationMembre() {
		return editingRepartAssociationMembre;
	}

	public void setEditingIndividuMembre(EOIndividu editingIndividuMembre) {
		this.editingIndividuMembre = editingIndividuMembre;
	}

	public EOIndividu editingIndividuMembre() {
		return editingIndividuMembre;
	}

	public void setSelectedMembre(EOMembreJuryThese selectedMembre) {
		this.selectedMembre = selectedMembre;
	}

	public EOMembreJuryThese selectedMembre() {
		return selectedMembre;
	}

	public void setIsDetailMembre(Boolean isDetailMembre) {
		this.isDetailMembre = isDetailMembre;
	}

	public Boolean isDetailMembre() {
		return isDetailMembre;
	}

	public void setSelectedRoleMembre(EOAssociation selectedRoleMembre) {
		this.selectedRoleMembre = selectedRoleMembre;
	}

	public EOAssociation selectedRoleMembre() {
		return selectedRoleMembre;
	}

	public Boolean isTypeRapporteur() {
		if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
			return true;
		} else {
			return false;
		}
	}

	public String cssClass() {
		if (isTypeRapporteur()) {
			return "cktlajaxtableviewOrange ";
		} else {
			return "cktlajaxtableview ";
		}
	}

	public String titre() {
		if (titre == null) {
			if (typeMembre().equals(TYPE_MEMBRE_RAPPORTEUR)) {
				titre = "Rapporteurs";
			} else {
				titre = "Membres du jury";
			}
		}
		return titre;
	}

	public void onSelectionnerIndividu() {
		try {
			
			if (editingIndividuMembre.noIndividu().equals(laThese().toDoctorant().toIndividuFwkpers().noIndividu())) {
				session().addSimpleErrorMessage("Erreur", "Le doctorant ne peut être un membre du jury.");
			}
			
			// Récupération du grade
			IndividusGradesService individusGradesService = IndividusGradesService.creerNouvelleInstance(edc());
			EOGrade grade = individusGradesService.gradePourIndividu(editingIndividuMembre, new NSTimestamp());
			
			if (grade != null) {
				editingMembre.setToCorpsRelationship(grade.toCorps());
				editingMembre.setTemEnseignant(grade.toCorps().toTypePopulation().temEnseignant());
			}
			
			NSArray<EOStructure> etablissements = editingIndividuMembre.getEtablissementsAffectation(null);
			for (EOStructure etablissement : etablissements) {
				if (etablissement.toRne() != null) {
					editingMembre.setToRneRelationship(etablissement.toRne());
				}
			}
			
//			String cGrade = SPgetCGradeAgent.getCGradeForAgent(edc(), editingIndividuMembre.noIndividu());
//			if (cGrade != null && cGrade != "") {
//				EOGrade leGrade = EOGrade.fetchByKeyValue(edc(), EOGrade.C_GRADE_KEY, cGrade);
//				System.err.println(leGrade.toCorps().toTypePopulation().temEnseignant());
//				System.err.println(leGrade.toCorps().libelle());
//			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String typeBoutonDetail() {
		if (isDetailMembre) {
			return "up_triangle";
		} else {
			return "down_triangle";
		}
	}

	public String typeBoutonEtat() {
		if (isEtatValide()) {
			return "ok";
		} else {
			return "ko";
		}
	}

	public String titleEtat() {
		return "Controle de la validité des " + titre();
	}

	public WOActionResults afficherEtat() {
		if (isTypeRapporteur()) {
			afficherEtatRapporteur();
		} else {
			afficherEtatJury();
		}
		return null;
	}

	public void afficherEtatRapporteur() {
		if (erreursValiditeRapporteur.count() > 0) {
			for (String uneErreur : erreursValiditeRapporteur) {
				session.addSimpleErrorMessage("Rapporteurs", uneErreur);
			}
		} else {
			session.addSimpleInfoMessage("Rapporteurs", "La liste des rapporteurs est valide");
		}
	}

	public void afficherEtatJury() {
		if (erreursValiditeJury.count() > 0) {
			for (String uneErreur : erreursValiditeJury) {
				session.addSimpleErrorMessage("Composition du Jury", uneErreur);
			}
		} else {
			session.addSimpleInfoMessage("Composition du Jury", "La composition du jury est valide");
		}
	}

	public Boolean isEtatValide() {
		if (isTypeRapporteur()) {
			return isEtatRapporteurValide();
		} else {
			return isEtatJuryValide();
		}
	}

	public Boolean isEtatRapporteurValide() {
		erreursValiditeRapporteur = new NSMutableArray<String>();

		// il faut au moins 2 rapporteurs
		Integer nbRapporteurs = 0;
		Boolean erreurNbRapporteur = true;
		if (dgMembre().allObjects() != null) {
			nbRapporteurs = dgMembre().allObjects().count();
			if (nbRapporteurs >= 2) {
				erreurNbRapporteur = false;
			}
		}

		if (erreurNbRapporteur) {
			erreursValiditeRapporteur.add("Il faut au moins 2 rapporteurs");
		}
		
		// un directeur de these ne peut pas être rapporteur
		if (nbRapporteurs > 0) {
			NSArray<EORepartAssociation> lesDirecteurs = null;
			lesDirecteurs = laThese().toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().directeurTheseAssociation(edc()));
			NSArray<ContratPartenaire> lesCps = (NSArray<ContratPartenaire>) dgMembre.allObjects().valueForKey(EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY);
			EOQualifier qualDirecteurEtRapporteur = ERXQ.and(ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().directeurTheseAssociation(edc())),
					ERXQ.in(EORepartAssociation.PERS_ID_KEY, (NSArray<Integer>) lesCps.valueForKey(ContratPartenaire.PERS_ID_KEY)),
					ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, laThese().toContrat().groupePartenaire()));
			NSArray<EORepartAssociation> lesDirecteursEtRapporteur = EORepartAssociation.fetchAll(edc(), qualDirecteurEtRapporteur);
			if (lesDirecteursEtRapporteur != null && !lesDirecteursEtRapporteur.isEmpty()) {
				erreursValiditeRapporteur.add(lesDirecteursEtRapporteur.lastObject().toPersonneElt().getNomCompletAffichage()
						+ " ne peut pas être directeur de la thèse et rapporteur");
			}

		}

		if (erreursValiditeRapporteur.count() > 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Controle de la validité de la composition du jury
	 */
	public Boolean isEtatJuryValide() {
		erreursValiditeJury = new NSMutableArray<String>();
		Integer nbMembres = 0;

		// le nombre de membre du jury doit être compris entre 3 et 8
		Boolean erreurNb = true;
		if (dgMembre().allObjects() != null) {
			nbMembres = dgMembre().allObjects().count();
			if (nbMembres >= NOMBRE_MEMBRES_JURY_MIN && nbMembres <= NOMBRE_MEMBRES_JURY_MAX) {
				erreurNb = false;
			}
		}

		if (erreurNb) {
			erreursValiditeJury.add("Le jury doit être composé de " + NOMBRE_MEMBRES_JURY_MIN + " à " + NOMBRE_MEMBRES_JURY_MAX + " membres");
		}

		// la moitié des membres doit être de type enseignant
		Boolean erreurEns = true;
		EOQualifier qualEnseignant = ERXQ.equals(EOMembreJuryThese.TEM_ENSEIGNANT_KEY, "O");
		NSArray<EOMembreJuryThese> lesEnseignants = laThese().toMembreJuryTheses(qualEnseignant);
		if (lesEnseignants != null) {
			if (lesEnseignants.count() >= (nbMembres / 2)) {
				erreurEns = false;
			}
		}

		if (erreurEns) {
			erreursValiditeJury.add("La moitié des membres du jury doit être de type enseignant");
		}
		
		// Le jury doit avoir un et un seul président
		Boolean erreurPresident = true;
		NSArray<EORepartAssociation> lesPresidents = null;
		lesPresidents = laThese().toContrat().repartAssociationPartenairesForAssociation(factoryAssociation().presidentJuryAssociation(edc()));
		if (lesPresidents != null) {
			Integer nbPresident = lesPresidents.count();
			if (nbPresident == 1) {
				erreurPresident = false;
			}
		}
		if (erreurPresident) {
			erreursValiditeJury.add("Le jury doit avoir un président");
		}

		// un directeur de these ne peut pas être président
		if (!erreurPresident) {
			EOQualifier qualDirecteurEtPresident = ERXQ.and(ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, factoryAssociation().directeurTheseAssociation(edc())),
					ERXQ.equals(EORepartAssociation.PERS_ID_KEY, lesPresidents.lastObject().persId()),
					ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, laThese().toContrat().groupePartenaire()));
			NSArray<EORepartAssociation> lesDirecteursEtPresident = EORepartAssociation.fetchAll(edc(), qualDirecteurEtPresident);

			if (lesDirecteursEtPresident != null && !lesDirecteursEtPresident.isEmpty()) {
				erreursValiditeJury.add(lesDirecteursEtPresident.lastObject().toPersonneElt().getNomCompletAffichage()
						+ " ne peut pas être directeur de la thèse et président du jury");
			}

		}

		if (erreursValiditeJury.count() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public Boolean getIsEtablissementFrancais() {
	    return isEtablissementFrancais;
    }

	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
	    this.isEtablissementFrancais = isEtablissementFrancais;
    }
	
}