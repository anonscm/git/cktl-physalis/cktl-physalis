package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.physalisw.serveur.components.common.PhysalisTableView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class ContactTBLV extends PhysalisTableView {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2755679487877940496L;
	private static final String NOM = ContratPartContact.PERSONNE_KEY + ".getNomPrenomAffichage";

	static {

		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Encadrant");
		col0.setOrderKeyPath(NOM);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(
				OBJ_KEY + "." + NOM, "emptyValue");
		col0.setAssociations(ass0);
		_colonnesMap.takeValueForKey(col0, NOM);

	}
	
	public ContactTBLV(WOContext context) {
        super(context);
    }
    
	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] { NOM });
	}

	public WOActionResults commitSave() {
		return null;
	}

}