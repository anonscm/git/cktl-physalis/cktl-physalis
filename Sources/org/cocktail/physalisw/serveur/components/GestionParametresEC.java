package org.cocktail.physalisw.serveur.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering;

public class GestionParametresEC extends PhysalisWOComponent {

	private static final long serialVersionUID = -4347942449517478704L;

	private WODisplayGroup displayGroupTableEcoleDoctorale;
	private WODisplayGroup displayGroupTableParametres;
	
	private NSMutableArray<EcoleDoctorale> ecolesDoctorales;
	private EcoleDoctorale currentEcoleDoctorale;
	private EcoleDoctorale selectedEcoleDoctorale;
	
	private NSArray<EOGrhumParametres> grhumParameters;	
	private EOGrhumParametres currentParameter;
	private EOGrhumParametres selectedParameter;
	
	private String valeurParametre;

	public GestionParametresEC(WOContext context) {
        super(context);
    }
	
	public String getModifierNombreThesesMaxWindowId() {
		return getComponentId() + "_ModifierNombreThesesMaxWindowId";
	}
	
	public String getParametresTableViewId() {
		return getComponentId() + "_parametresTableViewId";
	}
	
	public String getEcolesDoctoralesTableViewId() {
		return getComponentId() + "_ecolesDoctoralesTableViewId";
	}
	
	public EOGrhumParametres getCurrentParameter() {
		return currentParameter;
	}

	public void setCurrentParameter(EOGrhumParametres currentParameter) {
		this.currentParameter = currentParameter;
	}

	public EOGrhumParametres getSelectedParameter() {
		return selectedParameter;
	}

	public void setSelectedParameter(EOGrhumParametres selectedParameter) {
		this.selectedParameter = selectedParameter;
	}
	
	public EcoleDoctorale getCurrentEcoleDoctorale() {
		return currentEcoleDoctorale;
	}

	public void setCurrentEcoleDoctorale(EcoleDoctorale currentEcoleDoctorale) {
		this.currentEcoleDoctorale = currentEcoleDoctorale;
	}

	public EcoleDoctorale getSelectedEcoleDoctorale() {
		return selectedEcoleDoctorale;
	}

	public void setSelectedEcoleDoctorale(EcoleDoctorale selectedEcoleDoctorale) {
		this.selectedEcoleDoctorale = selectedEcoleDoctorale;
	}

	public String getValeurParametre() {
		return valeurParametre;
	}

	public void setValeurParametre(String valeurParametre) {
		this.valeurParametre = valeurParametre;
	}
	
	public String getValeurNombre() {
		return valeurParametre;
	}
	
	public void setValeurNombre(BigDecimal valeur) {
		setValeurParametre(valeur.toString());
	}
	
	public String currentNomParametre() {
		String currentParamKey = getCurrentParameter().paramKey();
		return ParametresRecherche.getLibelleForParamKey(currentParamKey);
	}
	
	public String selectedNomParametre() {
		return ParametresRecherche.getLibelleForParamKey(getSelectedParameter().paramKey());
	}
	
	public Boolean isNumber() {
		EOGrhumParametresType paramTypeNumero = EOGrhumParametresType.fetchByQualifier(edc(), EOGrhumParametresType.TYPE_ID_INTERNE.eq("VALEUR_NUMERIQUE"));
		return getSelectedParameter().paramTypeId().equals(paramTypeNumero.typeID());		
	}
	
	public NSArray<EcoleDoctorale> getEcolesDoctorales() {
		if (ecolesDoctorales == null) {
			NSArray<EOSortOrdering> sortOrderings = new NSArray<EOSortOrdering>(ERXSortOrdering.sortOrderingWithKey(EOGrhumParametres.PARAM_KEY_KEY, EOSortOrdering.CompareAscending));
			grhumParameters = EOGrhumParametres.fetchAll(edc(), ERXQ.startsWith("paramKey", ParametresRecherche.PARAM_PREFIXE_ECOLE_DOCTORALE), sortOrderings);
			
			int nbParametres = grhumParameters.size();
			int nb = nbParametres;
			ecolesDoctorales = new NSMutableArray<GestionParametresEC.EcoleDoctorale>();
			
			for (int i = 0; i < nbParametres; i++) {
				EcoleDoctorale ecole = new EcoleDoctorale();
				
				if (grhumParameters.get(i).paramKey().contains(ParametresRecherche.PARAM_ECOLE_DOCTORALE_NB_THESE)) {
					String numEcoleDoctorale = grhumParameters.get(i).paramKey().substring(ParametresRecherche.PARAM_ECOLE_DOCTORALE_NB_THESE.length());

					if (!StringCtrl.isEmpty(numEcoleDoctorale)) {

						ecole.setCode(numEcoleDoctorale);
						ecole.setNom(EOStructure.structurePourCode(edc(), numEcoleDoctorale).libelle());
						
						for (int j = 0; j < nb; j++) {
							if (grhumParameters.get(j).paramKey().endsWith(ecole.getCode())) {
								ecole.addParametre(grhumParameters.get(j));
							}
						}
						
						ecolesDoctorales.add(ecole);
					}
				}
				
			}
		}
		return ecolesDoctorales.immutableClone();
	}
	
	public WODisplayGroup getDisplayGroupTableEcoleDoctorale() {
		if (displayGroupTableEcoleDoctorale == null) {
			displayGroupTableEcoleDoctorale = new ERXDisplayGroup<EcoleDoctorale>();
			displayGroupTableEcoleDoctorale.setDelegate(new EcoleDoctoraleDisplayGroupDelegate());
			displayGroupTableEcoleDoctorale.setObjectArray(getEcolesDoctorales());
		}
		return displayGroupTableEcoleDoctorale;
	}
	
	public WODisplayGroup getDisplayGroupTableParametres() {
		if (displayGroupTableParametres == null) {
			displayGroupTableParametres = new ERXDisplayGroup<EOGrhumParametres>();
			displayGroupTableParametres.setDelegate(new ParameterDisplayGroupDelegate());
			displayGroupTableParametres.setObjectArray(getSelectedEcoleDoctorale().getParametres().immutableClone());
		}
		return displayGroupTableParametres;
	}
	
	public WOActionResults enregistrerModifications() {
		
		getSelectedParameter().setParamValue(getValeurParametre(), getUtilisateurPersId());
		
		try {
			edc().saveChanges();
			session().addSimpleSuccessMessage("Confirmation", "La modification a &eacute;t&eacute; enregistr&eacute;e");
			setValeurParametre(null);
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", "Erreur lors de l'enregistrement : " + e);
			edc().revert();
			return doNothing();
		}
		
		return doNothing();
	}
	
	public WOActionResults annulerModifications() {
		edc().revert();
		setValeurParametre(null);
		return doNothing();
	}

	public class EcoleDoctoraleDisplayGroupDelegate {
		
		public void displayGroupDidChangeSelection(WODisplayGroup displayGroup) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EcoleDoctorale> _displayGroup = (ERXDisplayGroup<EcoleDoctorale>) displayGroup;
			if (_displayGroup.selectedObject() != null) {
				setSelectedEcoleDoctorale(_displayGroup.selectedObject());
				displayGroupTableParametres = null;
			} else {
				setSelectedEcoleDoctorale(null);
				displayGroupTableParametres = null;
			}
		}
	}
	
	public class ParameterDisplayGroupDelegate {
		
		public void displayGroupDidChangeSelection(WODisplayGroup displayGroup) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EOGrhumParametres> _displayGroup = (ERXDisplayGroup<EOGrhumParametres>) displayGroup;
			if (_displayGroup.selectedObject() != null) {
					setSelectedParameter(_displayGroup.selectedObject());
			}
		}
	}
	
	/**
	 * Classe stockant tous les elements d'une ecole doctorale : nom, identifiant et sa liste de parametres
	 */
	public class EcoleDoctorale {

		private String code;
		private String nom;
		private NSMutableArray<EOGrhumParametres> parametres;
		
		public EcoleDoctorale() {
			parametres = new NSMutableArray<EOGrhumParametres>();
		}
		
		public String getCode() {
			return code;
		}
		
		public void setCode(String code) {
			this.code = code;
		}
		
		public String getNom() {
			return nom;
		}
		
		public void setNom(String nom) {
			this.nom = nom;
		}
		
		public NSMutableArray<EOGrhumParametres> getParametres() {
			return parametres;
		}
		
		public void setParametres(NSMutableArray<EOGrhumParametres> parametres) {
			this.parametres = parametres;
		}
		
		public void addParametre(EOGrhumParametres parametre) {
			parametres.add(parametre);
		}
		
	}

}