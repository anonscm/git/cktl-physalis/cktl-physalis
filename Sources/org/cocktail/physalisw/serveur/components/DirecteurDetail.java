package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;

public class DirecteurDetail extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8532661723488305430L;
	public static final String B_directeurThese = "directeurThese";
 
	public DirecteurDetail(WOContext context) {
        super(context);
    }
    
	public EODirecteurThese dirThese() {
		return (EODirecteurThese)valueForBinding(B_directeurThese);
	}

}