package org.cocktail.physalisw.serveur.components;

import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTVCheckBoxCell;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOMembreJuryThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisTableView;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class MembreJuryTBLV extends PhysalisTableView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8948210354477642005L;
	private static final String NOM_MEMBRE = EOMembreJuryThese.TO_CONTRAT_PARTENAIRE_KEY + "." + ContratPartenaire.PARTENAIRE_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
	private static final String ETABLISSEMENT = EOMembreJuryThese.LIBELLE_ETABLISSEMENT;
	private static final String ROLE = EOMembreJuryThese.LIBELLE_ROLE_MEMBRE;
	private static final String RAPPORTEUR_SEANCE = EOMembreJuryThese.IS_RAPPORTEUR_SEANCE;
	
	static {

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Nom");
		col1.setOrderKeyPath(NOM_MEMBRE);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + NOM_MEMBRE, " ");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, "NOM_MEMBRE");
		
		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Etablissement");
		col4.setOrderKeyPath(ETABLISSEMENT);
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + ETABLISSEMENT, "--");
		col4.setAssociations(ass4);
		_colonnesMap.takeValueForKey(col4, "ETABLISSEMENT");

		CktlAjaxTableViewColumn col2 = new CktlAjaxTableViewColumn();
		col2.setLibelle("Rôle");
		col2.setOrderKeyPath(ROLE);
		col2.setRowCssClass("alignToLeft useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass2 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + ROLE, " ");
		col2.setAssociations(ass2);
		_colonnesMap.takeValueForKey(col2, "ROLE");

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Rapporteur de séance");
		col3.setComponent(CktlAjaxTVCheckBoxCell.class.getName());
		col3.setOrderKeyPath(RAPPORTEUR_SEANCE);
		col3.setRowCssClass("alignToCenter useMinWidth wrap");
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + RAPPORTEUR_SEANCE, " ");
		ass3.setObjectForKey(OBJ_KEY + "." + RAPPORTEUR_SEANCE, "checked");
		col3.setAssociations(ass3);
		_colonnesMap.takeValueForKey(col3, "RAPPORTEUR_SEANCE");

	}

	public MembreJuryTBLV(WOContext context) {
		super(context);
	}

	@Override
	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		return new NSArray<String>(new String[] {"NOM_MEMBRE", "ETABLISSEMENT", "ROLE", "RAPPORTEUR_SEANCE"});
	}

}