package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlrecherche.server.metier.EODoctorant;
import org.cocktail.fwkcktlrecherche.server.metier.EODoctorantThese;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;

public class GestionThese extends PhysalisWOComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 506790556669706666L;
	private static final String B_leDoctorant = "leDoctorant";
	private static final String B_laThese = "laThese";
	private static final String BINDING_IS_MODIFICATION_TOP = "isModificationTop";
	private static final String BINDING_IS_MODIFICATION_BOTTOM = "isModificationBottom";
	private Boolean ongletLaboratoireSelected = true;
	private Boolean ongletDirecteurSelected = false;
	private Boolean ongletCotutelleSelected = false;
	private Boolean ongletFinancementSelected = false;
	private Boolean ongletSoutenanceSelected = false;
	private Boolean ongletApresTheseSelected = false;
	private Boolean ongletDocumentsSelected = false;
	private Boolean rechargerDirecteur = true;
	private Boolean isCotutelle = false;

	public GestionThese(WOContext context) {
		super(context);
	}

	public EODoctorant leDoctorant() {
		return (EODoctorant) valueForBinding(B_leDoctorant);
	}

	public EODoctorantThese laThese() {
		return (EODoctorantThese) valueForBinding(B_laThese);
	}
	
	public boolean isModification() {
		return isModificationTop() || isModificationBottom();
	}
	
	public void setIsModification() {
	}
	
	public boolean isModificationTop() {
		return valueForBooleanBinding(BINDING_IS_MODIFICATION_TOP, false);
	}
	
	public void setIsModificationTop(boolean isModification) {
		setValueForBinding(isModification, BINDING_IS_MODIFICATION_TOP);
	}
	
	public boolean isModificationBottom() {
		return valueForBooleanBinding(BINDING_IS_MODIFICATION_BOTTOM, false);
	}
	
	public void setIsModificationBottom(boolean isModification) {
		setValueForBinding(isModification, BINDING_IS_MODIFICATION_BOTTOM);
	}
	
	public String atpOngletCoTutelleId() {
		return getComponentId() + "_ongletCoTutelle";
	}
	
	public String atpOngletLaboratoireId() {
		return getComponentId() + "_ongletLaboratoire";
	}

	public String atpOngletDirecteurId() {
		return getComponentId() + "_ongletDirecteur";
	}

	public String atpOngletCotutelleId() {
		return getComponentId() + "_ongletCotutelle";
	}

	public String atpOngletFinancementId() {
		return getComponentId() + "_ongletFinancement";
	}

	public String atpOngletSoutenanceId() {
		return getComponentId() + "_ongletSoutenance";
	}

	public String atpOngletApresTheseId() {
		return getComponentId() + "_ongletApresThese";
	}

	public String atpOngletDocumentsId() {
		return getComponentId() + "_ongletDocuments";
	}

	public Boolean ongletLaboratoireSelected() {
		return ongletLaboratoireSelected;
	}

	public void setOngletLaboratoireSelected(Boolean ongletLaboratoireSelected) {
		this.ongletLaboratoireSelected = ongletLaboratoireSelected;
	}

	public Boolean ongletDirecteurSelected() {
		return ongletDirecteurSelected;
	}

	public void setOngletDirecteurSelected(Boolean ongletDirecteurSelected) {
		this.ongletDirecteurSelected = ongletDirecteurSelected;
	}

	public Boolean ongletCotutelleSelected() {
		return ongletCotutelleSelected;
	}

	public void setOngletCotutelleSelected(Boolean ongletCotutelleSelected) {
		this.ongletCotutelleSelected = ongletCotutelleSelected;
	}

	public Boolean ongletFinancementSelected() {
		return ongletFinancementSelected;
	}

	public void setOngletFinancementSelected(Boolean ongletFinancementSelected) {
		this.ongletFinancementSelected = ongletFinancementSelected;
	}

	public Boolean ongletSoutenanceSelected() {
		return ongletSoutenanceSelected;
	}

	public void setOngletSoutenanceSelected(Boolean ongletSoutenanceSelected) {
		this.ongletSoutenanceSelected = ongletSoutenanceSelected;
	}

	public void setOngletApresTheseSelected(Boolean ongletApresTheseSelected) {
		this.ongletApresTheseSelected = ongletApresTheseSelected;
	}

	public Boolean ongletApresTheseSelected() {
		return ongletApresTheseSelected;
	}

	public void setOngletDocumentsSelected(Boolean ongletDocumentsSelected) {
		this.ongletDocumentsSelected = ongletDocumentsSelected;
	}

	public Boolean ongletDocumentsSelected() {
		return ongletDocumentsSelected;
	}

	public void setRechargerDirecteur(Boolean rechargerDirecteur) {
		this.rechargerDirecteur = rechargerDirecteur;
	}

	public Boolean rechargerDirecteur() {
		return rechargerDirecteur;
	}
	
	public boolean isTabCotutelleDisabled() {
		return laThese().isTabCotutelleDisabled() || session().isTabDisabled();
	}
	
	public boolean isTabSoutenanceDisabled() {
		return laThese().dateFinAnticipee() != null || session().isTabDisabled();
	}
	
	public boolean isTabDevenirDocteurDisabled() {
		return laThese().dateFinAnticipee() != null || session().isTabDisabled();
	}

}