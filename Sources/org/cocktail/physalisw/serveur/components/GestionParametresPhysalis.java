package org.cocktail.physalisw.serveur.components;

import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;

public class GestionParametresPhysalis extends PhysalisWOComponent {

	private static final long serialVersionUID = -9076523833959935253L;

	public GestionParametresPhysalis(WOContext context) {
        super(context);
    }
}