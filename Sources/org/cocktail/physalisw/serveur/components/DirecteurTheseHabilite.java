package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.fwkcktlrecherche.server.metier.service.DirecteurTheseService;
import org.cocktail.fwkcktlrecherche.server.tasks.MettreAJourDirecteursTheseHabilitesTask;
import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class DirecteurTheseHabilite extends PhysalisWOComponent {

	private static final long serialVersionUID = -1260192865754144671L;

	private WODisplayGroup dgDirTheseHab;
	private EODirecteurTheseHabilite editingDirTheseHab = null;
	private EODirecteurTheseHabilite selectedDirTheseHab = null;
	private EOIndividu editingIndividuDirecteur = null;
	private EOEditingContext edcDirTheseHab = null;
	private boolean isAjout = false;
	private boolean isEditionDirecteur = false;
	private Boolean isLaboratoireLocal = false;
	private Boolean isEtablissementFrancais = false;
	private Boolean isListeDirecteursChargee = false;
	private Boolean isValiderDirecteurDisable = false;
	
	private final DgDelegate dgDelegate = new DgDelegate();

	public DirecteurTheseHabilite(WOContext context) {
		super(context);
	}
	
	public NSArray<EODirecteurTheseHabilite> listeDirecteurs() {
		EOQualifier qualifier = ERXQ.equals(EODirecteurTheseHabilite.TEM_VALIDE_KEY, "O");
		return EODirecteurTheseHabilite.fetchDirecteurTheseHabilites(edcDirTheseHab(), qualifier, new NSArray<EOSortOrdering>(
				EODirecteurTheseHabilite.SORT_NOM));
	}

	private void majListeDirecteurs() {
		dgDirTheseHab.setObjectArray(listeDirecteurs());
	}

	public WOActionResults ajouterDirecteur() {
		EOIndividu indUser = EOIndividu.individuWithPersId(edcDirTheseHab(), getApplicationUser().getPersId());
		editingDirTheseHab = EODirecteurTheseHabilite.createDirecteurTheseHabilite(edcDirTheseHab(), "O", null, indUser, null);
		setIsEditionDirecteur(true);
		setIsValiderDirecteurDisable(true);
		setIsSelectionEnable(false);
		setIsAjout(true);
		setIsLaboratoireLocal(true);
		setIsEtablissementFrancais(true);
		setEditingIndividuDirecteur(null);
		return null;
	}

	public WOActionResults remDirTheseHab() {
		if (selectedDirTheseHab != null) {
			editingDirTheseHab = selectedDirTheseHab.localInstanceIn(edcDirTheseHab());
			editingDirTheseHab.setTemValide("N");
			try {
				edcDirTheseHab().saveChanges();
				session.addSimpleSuccessMessage("Confirmation", "Ce directeur est maintenant invalide");
				majListeDirecteurs();
			} catch (Exception e) {
				e.printStackTrace();
				session.addSimpleErrorMessage("Erreur", e.toString());
			}
		}
		return null;
	}

	public WOActionResults modDirTheseHab() {
		editingDirTheseHab = selectedDirTheseHab;
		editingDirTheseHab.toIndividuFwkpers().setPersIdModification(getUtilisateurPersId());
		editingIndividuDirecteur = selectedDirTheseHab.toIndividuFwkpers();
		setIsEditionDirecteur(true);
		setIsAjout(false);
		setIsValiderDirecteurDisable(false);
		setIsLaboratoireLocal(editingDirTheseHab.isLaboratoireLocal());
		setIsEtablissementFrancais(editingDirTheseHab.isEtablissementFrancais());
		return null;
	}

	public WOActionResults validerFiche() {
		try {
			if (isLaboratoireLocal()) {
				editingDirTheseHab.setLaboExterneDth(null);
			} else {
				editingDirTheseHab.setToStructureLaboFwkpersRelationship(null);
			}

			if (isEtablissementFrancais()) {
				editingDirTheseHab.setToStructureEtabFwkPersRelationship(null);
			} else {
				editingDirTheseHab.setToRneFwkPersRelationship(null);
				editingDirTheseHab.setToStructureEdFwkpersRelationship(null);
			}
			
			if (editingDirTheseHab.toIndividuFwkpers() == null) {
				editingDirTheseHab.setToIndividuFwkpersRelationship(editingIndividuDirecteur);
			}
			
			edcDirTheseHab().saveChanges();
			this.isEditionDirecteur = false;
			this.isAjout = false;
			session.addSimpleSuccessMessage("Confirmation", "Les informations sont enregistrées");
			majListeDirecteurs();
			selectedDirTheseHab = editingDirTheseHab;
			dgDirTheseHab.setSelectedObject(selectedDirTheseHab);
		} catch (Exception e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e);
		}

		return null;
	}

	public WOActionResults annulerFiche() {
		edcDirTheseHab().revert();
		this.isEditionDirecteur = false;
		this.isAjout = false;
		return null;
	}

	public WODisplayGroup dgDirTheseHab() {
		if (dgDirTheseHab == null) {
			dgDirTheseHab = new WODisplayGroup();
			dgDirTheseHab.setDelegate(dgDelegate);
			dgDirTheseHab.setSelectsFirstObjectAfterFetch(true);

		}
		if (!isListeDirecteursChargee) {
			majListeDirecteurs();
			setIsListeDirecteursChargee(true);
		}
		return dgDirTheseHab;
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (group == dgDirTheseHab) {
				selectedDirTheseHab = (EODirecteurTheseHabilite) dgDirTheseHab.selectedObject();
			}
		}
	}

	public void onSelectionnerIndividuDirecteur() {
		EOQualifier qual = ERXQ.and(ERXQ.equals(EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY, editingIndividuDirecteur),
				ERXQ.equals(EODirecteurTheseHabilite.TEM_VALIDE_KEY, "N"));

		EODirecteurTheseHabilite verifDTH = EODirecteurTheseHabilite.fetchDirecteurTheseHabilite(edcDirTheseHab(), qual);
		// Si cette personne est deja dth
		// alors si elle a tem_valide = N on la repasse a tem_valide = O
		// (suppression de celui crée lors de l'action adddirtheseshab et modif
		// de celui recuperer)
		// sinon message erreur
		// sinon creation d'un nouveau DTH
		if (verifDTH != null) {
			edcDirTheseHab().deleteObject(editingDirTheseHab());
			setEditingDirTheseHab(verifDTH);
			editingDirTheseHab.setToStructureEtabFwkPersRelationship(verifDTH.toStructureEtabFwkPers());
			editingDirTheseHab.setToRneFwkPersRelationship(verifDTH.toRneFwkPers());
			editingDirTheseHab.setToStructureLaboFwkpersRelationship(verifDTH.toStructureLaboFwkpers());
			editingDirTheseHab.setToStructureEdFwkpersRelationship(verifDTH.toStructureEdFwkpers());
			editingDirTheseHab.setLaboExterneDth(verifDTH.laboExterneDth());
			editingDirTheseHab.setTemValide("O");
			setIsLaboratoireLocal(verifDTH.isLaboratoireLocal());
			setIsEtablissementFrancais(verifDTH.isEtablissementFrancais());
		} else {
			editingDirTheseHab.setToIndividuFwkpersRelationship(editingIndividuDirecteur);
			editingDirTheseHab.setTemValide("O");
			
			//recherche infos disponibles
			DirecteurTheseService dts = DirecteurTheseService.creerNouvelleInstance(edc());
			editingDirTheseHab.setToRneFwkPersRelationship(dts.getEtablissementIndividu(editingIndividuDirecteur));
			editingDirTheseHab.setToStructureEdFwkpersRelationship(dts.getEcoleDoctoraleIndividu(editingIndividuDirecteur, factoryAssociation().membreAssociation(edc())));
			editingDirTheseHab.setToStructureLaboFwkpersRelationship(dts.getLaboratoireIndividu(editingIndividuDirecteur));
			editingDirTheseHab.setDateHdr(dts.getDateHDRIndividu(editingIndividuDirecteur));
		}
	}

	/**
	 * Retourne les laboratoires d'une ecole doctorale passee en parametre
	 * @param ecoleDoctorale
	 * @return
	 */
	private NSArray<EOStructure> getLaboratoiresEcoleDoctorale(EOStructure ecoleDoctorale) {
	    EOQualifier qualLabos = ERXQ.and(ERXQ.equals(EORepartAssociation.C_STRUCTURE_KEY, ecoleDoctorale.cStructure()), ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY,
	    		factoryAssociation().membreAssociation(edc())), ERXQ.equals(
	    		EORepartAssociation.TO_STRUCTURES_ASSOCIEES_KEY + '.' + EOStructure.TO_REPART_TYPE_GROUPES_KEY + '.' + EORepartTypeGroupe.TGRP_CODE_KEY, 
	    				EOTypeGroupe.TGRP_CODE_LA));
	    NSArray<EORepartAssociation> lesRepartAssoLabo = ERXArrayUtilities.arrayWithoutDuplicates(EORepartAssociation.fetchAll(edc(), qualLabos));
	    NSArray<EOStructure> labos = (NSArray<EOStructure>) lesRepartAssoLabo.valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
	    return labos;
    }

	/**
	 * Retourne toutes les ecoles doctorales
	 * @return 
	 */
	private NSArray<EOStructure> listeEcolesDoctorales() {
		EOQualifier qualDoct = ERXQ.and(ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, "ED"),
				ERXQ.equals(EOStructure.TEM_VALIDE_KEY, EOStructure.TEM_VALIDE_OUI));
		NSArray<EOStructure> lesEds = EOStructure.fetchAll(edc(), qualDoct, null);
		if (lesEds.count() == 0) {
			return null;
		} else {
			return lesEds;
		}
	}

	public EODirecteurTheseHabilite getSelectedDirTheseHab() {
		return selectedDirTheseHab;
	}

	public void setSelectedDirTheseHab(EODirecteurTheseHabilite selectedDirTheseHab) {
		this.selectedDirTheseHab = selectedDirTheseHab;
	}

	public boolean isDirTheseHabTVSelectable() {
		return !isEditionDirecteur;
	}

	public boolean isEditionDirecteur() {
		return isEditionDirecteur;
	}

	public void setIsEditionDirecteur(boolean isEditionDirecteur) {
		this.isEditionDirecteur = isEditionDirecteur;
	}

	public boolean isAjout() {
		return isAjout;
	}

	public void setIsAjout(boolean isAjout) {
		this.isAjout = isAjout;
	}

	public void setIsLaboratoireLocal(Boolean isLaboratoireLocal) {
		this.isLaboratoireLocal = isLaboratoireLocal;
	}

	public Boolean isLaboratoireLocal() {
		return isLaboratoireLocal;
	}

	public void setIsEtablissementFrancais(Boolean isEtablissementFrancais) {
		this.isEtablissementFrancais = isEtablissementFrancais;
	}

	public Boolean isEtablissementFrancais() {
		return isEtablissementFrancais;
	}

	public EODirecteurTheseHabilite editingDirTheseHab() {
		return editingDirTheseHab;
	}

	public void setEditingDirTheseHab(EODirecteurTheseHabilite object) {

		this.editingDirTheseHab = object;
		AjaxUpdateContainer.updateContainerWithID(aucInfoDTHId(), context());
	}

	public void setEdcDirTheseHab(ERXEC edcDirTheseHab) {
		this.edcDirTheseHab = edcDirTheseHab;
	}

	public EOEditingContext edcDirTheseHab() {
		if (edcDirTheseHab == null) {
			edcDirTheseHab = edc();
		}
		return edcDirTheseHab;
	}
	
	public String aucDirTheseGlobalId() {
		return getComponentId() + "_aucDirTheseGlobal";
	}

	public String aucDirTheseHabId() {
		return getComponentId() + "_aucDirTheseHab";
	}

	/**
	 * @return the aucInfoDTHId
	 */
	public String aucInfoDTHId() {
		return getComponentId() + "_aucInfoDTH";
	}

	public void setIsListeDirecteursChargee(Boolean isListeDirecteursChargee) {
		this.isListeDirecteursChargee = isListeDirecteursChargee;
	}

	public Boolean isListeDirecteursChargee() {
		return isListeDirecteursChargee;
	}

	public EOIndividu editingIndividuDirecteur() {
		return editingIndividuDirecteur;
	}

	public void setIsValiderDirecteurDisable(Boolean isValiderDirecteurDisable) {
		this.isValiderDirecteurDisable = isValiderDirecteurDisable;
	}

	public Boolean isValiderDirecteurDisable() {
		return isValiderDirecteurDisable;
	}

	public void setEditingIndividuDirecteur(EOIndividu editingIndividuDirecteur) {
		this.editingIndividuDirecteur = editingIndividuDirecteur;
	}

	public WOActionResults majDirecteurs() {
		MettreAJourDirecteursTheseHabilitesTask task = new MettreAJourDirecteursTheseHabilitesTask();
		task.run();
		return doNothing();
	}
	
}