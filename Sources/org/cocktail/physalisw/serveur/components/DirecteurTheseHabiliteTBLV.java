package org.cocktail.physalisw.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODirecteurTheseHabilite;
import org.cocktail.physalisw.serveur.components.common.PhysalisTableView;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class DirecteurTheseHabiliteTBLV extends PhysalisTableView {
	private static final long serialVersionUID = 1L;

	private static final String NOM_COMPLET = EODirecteurTheseHabilite.TO_INDIVIDU_FWKPERS_KEY + ".getNomAndPrenom";
	private static final String LABO = "libelleLabo";
	private static final String LABOEXT = EODirecteurTheseHabilite.LABO_EXTERNE_DTH_KEY;
	private static final String DATE_HDR = EODirecteurTheseHabilite.DATE_HDR_KEY;
	// private static final String ECOLE_DOCTORALE =
	// EODirecteurTheseHabilite.TO_STRUCTURE_ED_FWKPERS_KEY +
	// ".nomCompletAffichage";
	private static final String ECOLE_DOCTORALE = EODirecteurTheseHabilite.TO_STRUCTURE_ED_FWKPERS_KEY + "." + EOStructure.LC_STRUCTURE_KEY;

	public DirecteurTheseHabiliteTBLV(WOContext context) {
		super(context);
	}

	static {

		CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
		col0.setLibelle("Directeur");
		col0.setOrderKeyPath(NOM_COMPLET);
		CktlAjaxTableViewColumnAssociation ass0 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + NOM_COMPLET, " ");
		col0.setAssociations(ass0);
		_colonnesMap.takeValueForKey(col0, NOM_COMPLET);

		CktlAjaxTableViewColumn col1 = new CktlAjaxTableViewColumn();
		col1.setLibelle("Labo");
		col1.setOrderKeyPath(LABO);
		CktlAjaxTableViewColumnAssociation ass1 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + LABO, " ");
		col1.setAssociations(ass1);
		_colonnesMap.takeValueForKey(col1, LABO);

		CktlAjaxTableViewColumn col3 = new CktlAjaxTableViewColumn();
		col3.setLibelle("Ecole Doctorale");
		col3.setOrderKeyPath(ECOLE_DOCTORALE);
		col3.setRowCssClass("alignToLeft useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass3 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + ECOLE_DOCTORALE, " ");
		col3.setAssociations(ass3);
		_colonnesMap.takeValueForKey(col3, ECOLE_DOCTORALE);

		CktlAjaxTableViewColumn col4 = new CktlAjaxTableViewColumn();
		col4.setLibelle("Date HDR");
		col4.setOrderKeyPath(DATE_HDR);
		col4.setRowCssClass("alignToCenter useMinWidth nowrap");
		CktlAjaxTableViewColumnAssociation ass4 = new CktlAjaxTableViewColumnAssociation(OBJ_KEY + "." + DATE_HDR, " ");
		ass4.setDateformat("%d/%m/%Y");
		col4.setAssociations(ass4);
		_colonnesMap.takeValueForKey(col4, DATE_HDR);
	}

	public NSArray<String> DEFAULT_COLONNES_KEYS() {
		// return new NSArray<String>(new String[] { NOM_COMPLET, LABO, DATE_HDR
		// });
		return new NSArray<String>(new String[] { NOM_COMPLET, ECOLE_DOCTORALE, DATE_HDR });
	}

}