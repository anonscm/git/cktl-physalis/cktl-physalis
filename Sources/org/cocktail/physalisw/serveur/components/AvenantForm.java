package org.cocktail.physalisw.serveur.components;

import org.cocktail.physalisw.serveur.components.common.PhysalisWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class AvenantForm extends PhysalisWOComponent {
	
    private static final long serialVersionUID = -673338764636185383L;
	private static final String B_dateDebutAvenant = "dateDebutAvenant";
	private static final String B_dateFinAvenant = "dateFinAvenant";
	private static final String B_commentaireAvenant = "commentaireAvenant";
	private static final String B_titreAvenant = "titreAvenant";
	private static final String B_isTitreDisabled = "isTitreDisabled";

	public AvenantForm(WOContext context) {
		super(context);
	}

	public NSTimestamp dateDebutAvenant() {
		return (NSTimestamp) valueForBinding(B_dateDebutAvenant);
	}

	public void setDateDebutAvenant(NSTimestamp dateDebutAvenant) {
		setValueForBinding(dateDebutAvenant, B_dateDebutAvenant);
	}

	public NSTimestamp dateFinAvenant() {
		return (NSTimestamp) valueForBinding(B_dateFinAvenant);
	}

	public void setDateFinAvenant(NSTimestamp dateFinAvenant) {
		setValueForBinding(dateFinAvenant, B_dateFinAvenant);
	}

	public String titreAvenant() {
		return (String) valueForBinding(B_titreAvenant);
	}

	public void setTitreAvenant(String titreAvenant) {
		setValueForBinding(titreAvenant, B_titreAvenant);
	}

	public String commentaireAvenant() {
		return (String) valueForBinding(B_commentaireAvenant);
	}

	public void setCommentaireAvenant(String commentaireAvenant) {
		setValueForBinding(commentaireAvenant, B_commentaireAvenant);
	}
	
	public Boolean isTitreDisabled() {
	    return (Boolean) valueForBinding(B_isTitreDisabled);
	}

}