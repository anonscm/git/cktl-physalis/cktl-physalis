<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="convocation_jury" pageWidth="595" pageHeight="842" columnWidth="515" leftMargin="40" rightMargin="40" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="32"/>
	<subDataset name="membres_jury">
		<parameter name="ID_DOCTORANT" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT DISTINCT I.NOM_USUEL as NOM_MEMBRE_JURY, I.PRENOM as PRENOM_MEMBRE_JURY, CORPS.LL_CORPS, TITRE_SPECIAL, ASS_CODE, LL_RNE, LIBELLE_STRUCT_EXTERNE
FROM RECHERCHE.DOCTORANT D
LEFT OUTER JOIN RECHERCHE.DOCTORANT_THESE T ON D.ID_DOCTORANT = T.ID_DOCTORANT
INNER JOIN RECHERCHE.MEMBRE_JURY_THESE MJT ON MJT.ID_THESE = T.ID_THESE
LEFT OUTER JOIN CORPS ON CORPS.C_CORPS = MJT.C_CORPS
LEFT OUTER JOIN RNE ON RNE.C_RNE = MJT.C_RNE
INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CP_ORDRE = MJT.CP_ORDRE
INNER JOIN ACCORDS.CONTRAT C ON CP.CON_ORDRE = C.CON_ORDRE
INNER JOIN INDIVIDU_ULR I ON I.PERS_ID = CP.PERS_ID
INNER JOIN GRHUM.REPART_ASSOCIATION RA ON RA.PERS_ID = CP.PERS_ID AND RA.C_STRUCTURE = C.CON_GROUPE_PARTENAIRE
INNER JOIN GRHUM.ASSOCIATION A ON A.ASS_ID = RA.ASS_ID
INNER JOIN GRHUM.ASSOCIATION_RESEAU AR ON A.ASS_ID = AR.ASS_ID_FILS
WHERE D.ID_DOCTORANT = $P{ID_DOCTORANT}
-- membre du jury sauf invité
AND ASS_ID_PERE = (select ass_id from GRHUM.ASSOCIATION where ass_code = 'D_TYPE_JURY')
AND ASS_CODE != 'D_JR_INV'
ORDER BY DECODE(ASS_CODE,'D_JR_PRES', 1, 'D_JR_MEM', 2, 3)]]>
		</queryString>
		<field name="NOM_MEMBRE_JURY" class="java.lang.String"/>
		<field name="PRENOM_MEMBRE_JURY" class="java.lang.String"/>
		<field name="LL_CORPS" class="java.lang.String"/>
		<field name="TITRE_SPECIAL" class="java.lang.String"/>
		<field name="ASS_CODE" class="java.lang.String"/>
		<field name="LL_RNE" class="java.lang.String"/>
		<field name="LIBELLE_STRUCT_EXTERNE" class="java.lang.String"/>
	</subDataset>
	<parameter name="ID_DOCTORANT" class="java.lang.Integer"/>
	<queryString>
		<![CDATA[SELECT D.ID_DOCTORANT,
I.C_CIVILITE as CILITE_DOCTORANT,
I.NOM_USUEL as NOM_USUEL_DOCTORANT,
I.PRENOM as PRENOM_DOCTORANT,
T.DATE_SOUTENANCE,
T.LIEU_SOUTENANCE,
DOM.LIBELLE_DOMAINE,
C.CON_OBJET,
ED.LL_STRUCTURE as NOM_ECOLE_DOCTORALE,
STR_ETAB.LL_STRUCTURE as NOM_ETABLISSEMENT,
AD_PRO_ETAB.VILLE as VILLE_ETABLISSEMENT,
(select param_value from GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY = 'org.cocktail.physalis.SIGNATURE') as SIGNATURE,
(select param_value from GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY = 'URL_LOGO') as LOGO_ETABLISSEMENT

FROM RECHERCHE.DOCTORANT D
INNER JOIN GRHUM.INDIVIDU_ULR I ON I.NO_INDIVIDU = D.NO_INDIVIDU
LEFT OUTER JOIN GRHUM.ETUDIANT E ON E.ETUD_NUMERO = D.ETUD_NUMERO
LEFT OUTER JOIN RECHERCHE.DOCTORANT_THESE T ON D.ID_DOCTORANT = T.ID_DOCTORANT
LEFT OUTER JOIN RECHERCHE.THESE_DOMAINE DOM ON DOM.ID_THESE_DOMAINE = T.ID_THESE_DOMAINE
LEFT OUTER JOIN ACCORDS.CONTRAT C ON C.CON_ORDRE = T.CON_ORDRE

-- ecole doctorale
INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CON_ORDRE = C.CON_ORDRE
INNER JOIN GRHUM.STRUCTURE_ULR ED ON ED.PERS_ID = CP.PERS_ID
INNER JOIN GRHUM.REPART_TYPE_GROUPE RTG ON RTG.C_STRUCTURE = ED.C_STRUCTURE AND TGRP_CODE = 'ED'
,

-- infos sur l etablissement
STRUCTURE_ULR STR_ETAB
LEFT OUTER JOIN REPART_PERSONNE_ADRESSE RPA_PRO_ETAB ON STR_ETAB.PERS_ID = RPA_PRO_ETAB.PERS_ID AND RPA_PRO_ETAB.RPA_VALIDE = 'O'
AND RPA_PRO_ETAB.TADR_CODE = 'PRO' AND RPA_PRO_ETAB.RPA_VALIDE = 'O'
LEFT OUTER JOIN ADRESSE AD_PRO_ETAB ON RPA_PRO_ETAB.ADR_ORDRE = AD_PRO_ETAB.ADR_ORDRE

WHERE D.ID_DOCTORANT = $P{ID_DOCTORANT}
AND STR_ETAB.c_structure = (select param_value from grhum_parametres where param_key = 'org.cocktail.physalis.C_STRUCTURE_ETAB_GESTIONNAIRE_THESE')]]>
	</queryString>
	<field name="ID_DOCTORANT" class="java.math.BigDecimal"/>
	<field name="CILITE_DOCTORANT" class="java.lang.String"/>
	<field name="NOM_USUEL_DOCTORANT" class="java.lang.String"/>
	<field name="PRENOM_DOCTORANT" class="java.lang.String"/>
	<field name="DATE_SOUTENANCE" class="java.sql.Timestamp"/>
	<field name="LIEU_SOUTENANCE" class="java.lang.String"/>
	<field name="LIBELLE_DOMAINE" class="java.lang.String"/>
	<field name="CON_OBJET" class="java.lang.String"/>
	<field name="NOM_ECOLE_DOCTORALE" class="java.lang.String"/>
	<field name="NOM_ETABLISSEMENT" class="java.lang.String"/>
	<field name="VILLE_ETABLISSEMENT" class="java.lang.String"/>
	<field name="SIGNATURE" class="java.lang.String"/>
	<field name="LOGO_ETABLISSEMENT" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="650" splitType="Stretch">
			<frame>
				<reportElement positionType="Float" x="0" y="120" width="515" height="210"/>
				<textField isStretchWithOverflow="true">
					<reportElement x="0" y="0" width="515" height="20"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" size="12" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{NOM_ECOLE_DOCTORALE}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement positionType="Float" x="0" y="83" width="515" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<text><![CDATA[Titre de la thèse :]]></text>
				</staticText>
				<textField isStretchWithOverflow="true">
					<reportElement positionType="Float" x="0" y="98" width="515" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["« " + $F{CON_OBJET} + " »"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement positionType="Float" x="0" y="126" width="129" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Prénom et nom de l'auteur :]]></text>
				</staticText>
				<textField isStretchWithOverflow="true">
					<reportElement positionType="Float" x="129" y="126" width="386" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{PRENOM_DOCTORANT} + " " + $F{NOM_USUEL_DOCTORANT}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement positionType="Float" x="0" y="152" width="515" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="10"/>
					</textElement>
					<text><![CDATA[Membres du Jury :]]></text>
				</staticText>
				<componentElement>
					<reportElement positionType="Float" x="21" y="167" width="494" height="13"/>
					<jr:list xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" printOrder="Vertical">
						<datasetRun subDataset="membres_jury">
							<datasetParameter name="ID_DOCTORANT">
								<datasetParameterExpression><![CDATA[Integer.parseInt($F{ID_DOCTORANT}.toString())]]></datasetParameterExpression>
							</datasetParameter>
							<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
						</datasetRun>
						<jr:listContents height="13" width="494">
							<textField isStretchWithOverflow="true">
								<reportElement x="0" y="0" width="494" height="13"/>
								<textElement verticalAlignment="Middle">
									<font fontName="Arial" size="10" isBold="true"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{PRENOM_MEMBRE_JURY} + " " +
$F{NOM_MEMBRE_JURY} +
(($F{LL_CORPS} == null) ? "" : ", " + $F{LL_CORPS}) +
(($F{TITRE_SPECIAL} == null) ? "" : ", " + $F{TITRE_SPECIAL}) +
(($F{LL_RNE} == null) ? "" : ", " + $F{LL_RNE}) +
(($F{LIBELLE_STRUCT_EXTERNE} == null) ? "" : ", " + $F{LIBELLE_STRUCT_EXTERNE})]]></textFieldExpression>
							</textField>
						</jr:listContents>
					</jr:list>
				</componentElement>
				<staticText>
					<reportElement positionType="Float" x="0" y="195" width="112" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Date de la soutenance :]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement positionType="Float" x="112" y="195" width="403" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="10" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format($F{DATE_SOUTENANCE})]]></textFieldExpression>
				</textField>
			</frame>
			<staticText>
				<reportElement x="0" y="150" width="515" height="20"/>
				<box topPadding="0" leftPadding="0" bottomPadding="0" rightPadding="0">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[AVIS DU JURY SUR LA REPRODUCTION DE THESE SOUTENUE]]></text>
			</staticText>
			<image hAlign="Center" vAlign="Middle">
				<reportElement x="0" y="0" width="119" height="104"/>
				<imageExpression><![CDATA[$F{LOGO_ETABLISSEMENT}]]></imageExpression>
			</image>
			<frame>
				<reportElement positionType="Float" x="0" y="341" width="515" height="103"/>
				<staticText>
					<reportElement x="0" y="0" width="515" height="15"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[Reproduction de la thèse soutenue :]]></text>
				</staticText>
				<staticText>
					<reportElement x="100" y="25" width="415" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[thèse pouvant être reproduite en l'état]]></text>
				</staticText>
				<staticText>
					<reportElement x="100" y="47" width="415" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[thèse ne pouvant être reproduite]]></text>
				</staticText>
				<rectangle>
					<reportElement x="69" y="27" width="8" height="8"/>
				</rectangle>
				<rectangle>
					<reportElement x="69" y="49" width="8" height="8"/>
				</rectangle>
				<staticText>
					<reportElement x="100" y="69" width="415" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[thèse pouvant être reproduite après corrections suggerées au cours de la soutenance]]></text>
				</staticText>
				<rectangle>
					<reportElement x="69" y="71" width="8" height="8"/>
				</rectangle>
				<staticText>
					<reportElement x="100" y="91" width="415" height="12"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial"/>
					</textElement>
					<text><![CDATA[thèse pouvant être reproduite en l'état]]></text>
				</staticText>
				<rectangle>
					<reportElement x="69" y="93" width="8" height="8"/>
				</rectangle>
			</frame>
			<staticText>
				<reportElement positionType="Float" x="372" y="460" width="143" height="13"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<text><![CDATA[Le Président du jury,]]></text>
			</staticText>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
