<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Diplome" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="762" leftMargin="40" rightMargin="40" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<subDataset name="membres_jury_sans_president">
		<parameter name="ID_THESE" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT DISTINCT I.NOM_USUEL as NOM_MEMBRE_JURY, I.PRENOM as PRENOM_MEMBRE_JURY, CORPS.LL_CORPS, TITRE_SPECIAL, ASS_CODE, LL_RNE, LIBELLE_STRUCT_EXTERNE
FROM RECHERCHE.DOCTORANT_THESE T
INNER JOIN RECHERCHE.MEMBRE_JURY_THESE MJT ON MJT.ID_THESE = T.ID_THESE
LEFT OUTER JOIN CORPS ON CORPS.C_CORPS = MJT.C_CORPS
LEFT OUTER JOIN RNE ON RNE.C_RNE = MJT.C_RNE
INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CP_ORDRE = MJT.CP_ORDRE
INNER JOIN ACCORDS.CONTRAT C ON CP.CON_ORDRE = C.CON_ORDRE
INNER JOIN INDIVIDU_ULR I ON I.PERS_ID = CP.PERS_ID
INNER JOIN GRHUM.REPART_ASSOCIATION RA ON RA.PERS_ID = CP.PERS_ID AND RA.C_STRUCTURE = C.CON_GROUPE_PARTENAIRE
INNER JOIN GRHUM.ASSOCIATION A ON A.ASS_ID = RA.ASS_ID
INNER JOIN GRHUM.ASSOCIATION_RESEAU AR ON A.ASS_ID = AR.ASS_ID_FILS
WHERE T.ID_THESE = $P{ID_THESE}
-- membre du jury sauf invité
AND ASS_ID_PERE = (select ass_id from GRHUM.ASSOCIATION where ass_code = 'D_TYPE_JURY')
AND ASS_CODE != 'D_JR_INV'
AND ASS_CODE != 'D_JR_PRES'
ORDER BY DECODE(ASS_CODE, 'D_JR_MEM', 1, 2)]]>
		</queryString>
		<field name="NOM_MEMBRE_JURY" class="java.lang.String"/>
		<field name="PRENOM_MEMBRE_JURY" class="java.lang.String"/>
		<field name="LL_CORPS" class="java.lang.String"/>
		<field name="TITRE_SPECIAL" class="java.lang.String"/>
		<field name="ASS_CODE" class="java.lang.String"/>
		<field name="LL_RNE" class="java.lang.String"/>
		<field name="LIBELLE_STRUCT_EXTERNE" class="java.lang.String"/>
	</subDataset>
	<subDataset name="president_jury">
		<parameter name="ID_THESE" class="java.lang.Integer">
			<defaultValueExpression><![CDATA[]]></defaultValueExpression>
		</parameter>
		<queryString>
			<![CDATA[SELECT DISTINCT I.NOM_USUEL as NOM_PRESIDENT, I.PRENOM as PRENOM_PRESIDENT, CORPS.LL_CORPS, TITRE_SPECIAL, ASS_CODE, LL_RNE, LIBELLE_STRUCT_EXTERNE
FROM RECHERCHE.DOCTORANT_THESE T
INNER JOIN RECHERCHE.MEMBRE_JURY_THESE MJT ON MJT.ID_THESE = T.ID_THESE
LEFT OUTER JOIN CORPS ON CORPS.C_CORPS = MJT.C_CORPS
LEFT OUTER JOIN RNE ON RNE.C_RNE = MJT.C_RNE
INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CP_ORDRE = MJT.CP_ORDRE
INNER JOIN ACCORDS.CONTRAT C ON CP.CON_ORDRE = C.CON_ORDRE
INNER JOIN INDIVIDU_ULR I ON I.PERS_ID = CP.PERS_ID
INNER JOIN GRHUM.REPART_ASSOCIATION RA ON RA.PERS_ID = CP.PERS_ID AND RA.C_STRUCTURE = C.CON_GROUPE_PARTENAIRE
INNER JOIN GRHUM.ASSOCIATION A ON A.ASS_ID = RA.ASS_ID
INNER JOIN GRHUM.ASSOCIATION_RESEAU AR ON A.ASS_ID = AR.ASS_ID_FILS
WHERE T.ID_THESE = $P{ID_THESE}
AND ASS_ID_PERE = (select ass_id from GRHUM.ASSOCIATION where ass_code = 'D_TYPE_JURY')
AND ASS_CODE = 'D_JR_PRES']]>
		</queryString>
		<field name="NOM_PRESIDENT" class="java.lang.String"/>
		<field name="PRENOM_PRESIDENT" class="java.lang.String"/>
		<field name="LL_CORPS" class="java.lang.String"/>
		<field name="TITRE_SPECIAL" class="java.lang.String"/>
		<field name="ASS_CODE" class="java.lang.String"/>
		<field name="LL_RNE" class="java.lang.String"/>
		<field name="LIBELLE_STRUCT_EXTERNE" class="java.lang.String"/>
	</subDataset>
	<subDataset name="decrets_diplome">
		<parameter name="CODE_TYPE_DIPLOME_EDITION" class="java.lang.String"/>
		<queryString>
			<![CDATA[select LIB_DECRET
from SCO_SCOLARITE.DIPLOME_DECRETS
where ID_TYPE_DIPLOME_EDITION = (select ID_TYPE_DIPLOME_EDITION from SCO_SCOLARITE.TYPE_DIPLOME_EDITION where code = $P{CODE_TYPE_DIPLOME_EDITION})
and DATE_DEBUT <= sysdate
and (DATE_FIN is null
or DATE_FIN >= sysdate
)]]>
		</queryString>
		<field name="LIB_DECRET" class="java.lang.String"/>
	</subDataset>
	<parameter name="ID_THESE" class="java.lang.Integer"/>
	<parameter name="NOM_DIPLOME" class="java.lang.String"/>
	<parameter name="MEMBRES_JURY" class="java.lang.String"/>
	<parameter name="CODE_TYPE_DIPLOME_EDITION" class="java.lang.String"/>
	<parameter name="DECRETS" class="java.lang.String"/>
	<parameter name="PRESIDENT" class="java.lang.String"/>
	<parameter name="SOUS_TITRE_DIPLOME" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT DISTINCT D.ID_DOCTORANT,
I.NOM_USUEL as NOM_USUEL_DOCTORANT,
I.NOM_PATRONYMIQUE as NOM_PATRONYMIQUE_DOCTORANT,
initcap(I.C_CIVILITE) as C_CIVILITE_DOCTORANT,
initcap(I.PRENOM) as PRENOM_DOCTORANT,
I.D_NAISSANCE,
initcap(I.VILLE_DE_NAISSANCE) as VILLE_DE_NAISSANCE,
I.C_DEPT_NAISSANCE,
I.C_PAYS_NAISSANCE,
initcap(LL_PAYS) as LL_PAYS,
T.DATE_SOUTENANCE,
C.CON_OBJET,
STR_ETAB.ll_structure as ll_etablissement,
STR_ED.LL_STRUCTURE as LL_STRUCTURE_ED,
CODE_SISE_DIPLOME,
LIB_MENTION,
COTUTELLE,
COT.ll_structure as NOM_ETABLISSEMENT_COTUTELLE,
(select param_value from GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY = 'org.cocktail.physalis.INTITULE_MINISTERE') as INTITULE_MINISTERE,
(select param_value from GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY = 'org.cocktail.physalis.QUALITE_CHEF_ETABLISSEMENT') as QUALITE_CHEF_ETABLISSEMENT,
(select param_value from GRHUM.GRHUM_PARAMETRES WHERE PARAM_KEY = 'org.cocktail.physalis.VILLE_EDITION_DIPLOME') as VILLE_EDITION_DIPLOME

FROM STRUCTURE_ULR STR_ETAB,

RECHERCHE.DOCTORANT_THESE T
LEFT OUTER JOIN RECHERCHE.DOCTORANT D ON D.ID_DOCTORANT = T.ID_DOCTORANT
INNER JOIN GRHUM.INDIVIDU_ULR I ON I.NO_INDIVIDU = D.NO_INDIVIDU
LEFT OUTER JOIN GRHUM.PAYS P ON I.C_PAYS_NAISSANCE = P.C_PAYS
LEFT OUTER JOIN GRHUM.ETUDIANT E ON E.ETUD_NUMERO = D.ETUD_NUMERO
LEFT OUTER JOIN GRHUM.SITUATION_FAMILIALE S ON I.IND_C_SITUATION_FAMILLE = S.C_SITUATION_FAMILLE
LEFT OUTER JOIN RECHERCHE.THESE_DOMAINE TD ON T.ID_THESE_DOMAINE = TD.ID_THESE_DOMAINE
LEFT OUTER JOIN RECHERCHE.THESE_MENTION TM ON T.ID_MENTION = TM.ID_MENTION
INNER JOIN ACCORDS.CONTRAT C ON C.CON_ORDRE = T.CON_ORDRE

INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CON_ORDRE = C.CON_ORDRE
INNER JOIN GRHUM.STRUCTURE_ULR STR_ED ON STR_ED.PERS_ID = CP.PERS_ID
INNER JOIN GRHUM.REPART_TYPE_GROUPE RTG ON RTG.C_STRUCTURE = STR_ED.C_STRUCTURE AND TGRP_CODE = 'ED'

-- cotutelle
LEFT OUTER JOIN (

select etab_cot.ll_structure, c.con_ordre
  from GRHUM.REPART_ASSOCIATION RA
INNER JOIN GRHUM.ASSOCIATION A_COT ON A_COT.ASS_ID = RA.ASS_ID
INNER join grhum.structure_ulr etab_cot on etab_cot.pers_id = ra.pers_id
INNER join ACCORDS.contrat_partenaire cp on cp.pers_id = ra.pers_id
INNER join ACCORDS.contrat c on cp.con_ordre = c.con_ordre and C.CON_GROUPE_PARTENAIRE = RA.C_STRUCTURE
-- adresse de l etablissement de cotutelle
INNER JOIN GRHUM.REPART_PERSONNE_ADRESSE RPA_COT ON RPA_COT.PERS_ID = etab_cot.PERS_ID AND RPA_COT.RPA_PRINCIPAL = 'O'
INNER JOIN GRHUM.ADRESSE AD_cot ON RPA_COT.ADR_ORDRE = AD_cot.ADR_ORDRE
INNER JOIN GRHUM.PAYS P_cot ON P_cot.C_PAYS = AD_cot.C_PAYS
WHERE  A_COT.ASS_CODE = 'D_COT_ETAB'
) COT on COT.con_ordre = c.con_ordre

WHERE T.ID_THESE = $P{ID_THESE}
AND STR_ETAB.c_structure = (select param_value from grhum_parametres where param_key = 'org.cocktail.physalis.C_STRUCTURE_ETAB_GESTIONNAIRE_THESE')]]>
	</queryString>
	<field name="ID_DOCTORANT" class="java.math.BigDecimal"/>
	<field name="NOM_USUEL_DOCTORANT" class="java.lang.String"/>
	<field name="NOM_PATRONYMIQUE_DOCTORANT" class="java.lang.String"/>
	<field name="C_CIVILITE_DOCTORANT" class="java.lang.String"/>
	<field name="PRENOM_DOCTORANT" class="java.lang.String"/>
	<field name="D_NAISSANCE" class="java.sql.Timestamp"/>
	<field name="VILLE_DE_NAISSANCE" class="java.lang.String"/>
	<field name="C_DEPT_NAISSANCE" class="java.lang.String"/>
	<field name="C_PAYS_NAISSANCE" class="java.lang.String"/>
	<field name="LL_PAYS" class="java.lang.String"/>
	<field name="DATE_SOUTENANCE" class="java.sql.Timestamp"/>
	<field name="CON_OBJET" class="java.lang.String"/>
	<field name="LL_ETABLISSEMENT" class="java.lang.String"/>
	<field name="LL_STRUCTURE_ED" class="java.lang.String"/>
	<field name="CODE_SISE_DIPLOME" class="java.lang.String"/>
	<field name="LIB_MENTION" class="java.lang.String"/>
	<field name="COTUTELLE" class="java.lang.String"/>
	<field name="NOM_ETABLISSEMENT_COTUTELLE" class="java.lang.String"/>
	<field name="INTITULE_MINISTERE" class="java.lang.String"/>
	<field name="QUALITE_CHEF_ETABLISSEMENT" class="java.lang.String"/>
	<field name="VILLE_EDITION_DIPLOME" class="java.lang.String"/>
	<background>
		<band height="555" splitType="Stretch">
			<staticText>
				<reportElement mode="Transparent" x="0" y="0" width="762" height="555" forecolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font size="100" isBold="true"/>
				</textElement>
				<text><![CDATA[Proposition
de
trame]]></text>
			</staticText>
		</band>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="244" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement x="0" y="10" width="762" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="11" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{INTITULE_MINISTERE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="40" width="762" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LL_ETABLISSEMENT} + ($F{NOM_ETABLISSEMENT_COTUTELLE} == null ? "" : "\n"+$F{NOM_ETABLISSEMENT_COTUTELLE})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement positionType="Float" x="0" y="70" width="762" height="35"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="html">
					<font fontName="Arial" size="28" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{NOM_DIPLOME} + ($P{SOUS_TITRE_DIPLOME}.equals("")
    ? "" : "<span style='font-size:12px'><br/>" + $P{SOUS_TITRE_DIPLOME} + "</span>")]]></textFieldExpression>
			</textField>
			<frame>
				<reportElement positionType="Float" x="0" y="120" width="762" height="122" isPrintWhenDetailOverflows="true"/>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement x="0" y="0" width="762" height="15"/>
					<textElement verticalAlignment="Middle" markup="html"/>
					<textFieldExpression><![CDATA[$P{DECRETS}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement positionType="Float" x="0" y="50" width="762" height="72"/>
					<textElement verticalAlignment="Middle" markup="html">
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression><![CDATA["Préparée au sein de " +
($F{LL_STRUCTURE_ED}.toLowerCase().startsWith("ecole doctorale") ?
    "l'" + $F{LL_STRUCTURE_ED}.toLowerCase()
    : ($F{LL_STRUCTURE_ED}.toLowerCase().startsWith("école doctorale") ?
        "l'" + $F{LL_STRUCTURE_ED}.toLowerCase()
        : "l'école doctorale" + $F{LL_STRUCTURE_ED}.toLowerCase())) +
", devant un jury présidé par " + $P{PRESIDENT}
+ " et composé de " + $P{MEMBRES_JURY} + " ;<br/>" +

"Vu la délibération du jury ;<br/>" +

"Le diplôme national de DOCTEUR en " + $F{CODE_SISE_DIPLOME}
+ ", <i>mention " + $F{LIB_MENTION}.toLowerCase() + "</i><br/>" +

"Est délivré à <span style='font-size: 13px; font-weight: bold'>" + $F{C_CIVILITE_DOCTORANT}
+ " " + $F{PRENOM_DOCTORANT} + " " + $F{NOM_PATRONYMIQUE_DOCTORANT} + "</span><br/>" +

"Et confère le grade de docteur <br/>" +

"Pour en jouir avec les droits et prérogatives qui y sont attachés."]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement positionType="Float" x="0" y="15" width="762" height="20"/>
					<textElement verticalAlignment="Middle" markup="html">
						<font fontName="Arial"/>
					</textElement>
					<textFieldExpression><![CDATA["Vu les pièces justificatives produites par " + $F{C_CIVILITE_DOCTORANT} + " "
+ $F{PRENOM_DOCTORANT} + " " + $F{NOM_PATRONYMIQUE_DOCTORANT}.toUpperCase() + ", " +
($F{C_CIVILITE_DOCTORANT}.equals("M.") ? "né le " : "née le ") +
new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format($F{D_NAISSANCE}) +
" à " + $F{VILLE_DE_NAISSANCE} +
($F{C_PAYS_NAISSANCE}.equals("100") ? " (" + $F{C_DEPT_NAISSANCE} + ")"
    : "(" + $F{LL_PAYS} + ")") + ", en vue de son inscription au doctorat ;<br/>" +
"Vu le procès-verbal du jury attestant que " +
($F{C_CIVILITE_DOCTORANT}.equals("M.") ? "l'intéressé " : "l'intéressée ") +
"a soutenu, le " + (($F{DATE_SOUTENANCE} == null) ? "(à completer dans Physalis)" :
new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format($F{DATE_SOUTENANCE})) +
" une thèse portant sur le sujet suivant :"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true">
					<reportElement positionType="Float" x="0" y="35" width="762" height="15"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["« " + $F{CON_OBJET} + " »"]]></textFieldExpression>
				</textField>
			</frame>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="124" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="25" width="100" height="20"/>
				<textElement>
					<font isItalic="true"/>
				</textElement>
				<text><![CDATA[Le titulaire]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement x="290" y="25" width="140" height="20"/>
				<textElement>
					<font isItalic="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{QUALITE_CHEF_ETABLISSEMENT}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="615" y="25" width="147" height="28"/>
				<textElement>
					<font isItalic="true"/>
				</textElement>
				<text><![CDATA[Le Recteur d'Académie,
Chancelier des universités]]></text>
			</staticText>
			<textField isStretchWithOverflow="true">
				<reportElement x="0" y="5" width="290" height="20"/>
				<textFieldExpression><![CDATA["Fait à " + $F{VILLE_EDITION_DIPLOME} + ", le " + new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format(new Date())]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
